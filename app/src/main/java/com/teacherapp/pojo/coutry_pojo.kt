package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by hitesh.mathur on 1/24/2018.
 */
class coutry_pojo : Serializable{

    val id :String = ""
    val country :String = ""
    val code :String = ""
    val dial_code :String = ""
    val currency_code :String = ""
}