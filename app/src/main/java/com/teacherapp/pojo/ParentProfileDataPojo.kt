package com.teacherapp.pojo

/**
 * Created by upasna.mishra on 4/12/2018.
 */
class ParentProfileDataPojo{

    var id : String  = ""
    var school_id : String = ""
    var father_name : String = ""
    var father_email : String = ""
    var father_phone : String = ""
    var primary_email : String = ""
    var primary_phone : String = ""
    var username : String = ""
    var password : String = ""
    var mother_name : String = ""
    var mother_email : String = ""
    var mother_phone : String = ""
    var father_occupation : String = ""
    var father_place_of_work : String = ""
    var mother_occupation : String = ""
    var mother_place_of_work : String = ""
    var address : String = ""
    var country_id : String = ""
    var profile_pic : String = ""
    var passport_number : String = ""
    var status : String  = ""
    var reset_password_token : String = ""
    var force_change_status : String = ""
    var profile_update_status : String = ""
    var is_Expiry : String = ""
    var isConnected : String = ""
    var device_token : String = ""
    var device_type : String  = ""
    var created : String = ""
    var country_name : String = ""
    var students : StudentProfileDataPojo? = null
}