package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/24/2018.
 */
class EventCommentDataPojo : Serializable{

    var id : String = ""
    var event_id : String = ""
    var user_id : String = ""
    var description : String = ""
    var created : String = ""
    var father_name : String = ""
    var profile_pic : String? = ""
    var type : String = ""
    var school_id : String = ""
    var diary_id : String = ""
    var teacher_id : String = ""
    var parent_id : String = ""
    var comment : String = ""
    var created_at : String = ""
    var t_name : String = ""
    var t_profile_pic : String? = ""
    var p_name : String = ""
    var p_profile_pic : String? = ""

}