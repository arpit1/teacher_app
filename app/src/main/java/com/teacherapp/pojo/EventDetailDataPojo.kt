package com.schoolmanagement.parent.pojo

import com.teacherapp.pojo.EventCommentDataPojo

/**
 * Created by upasna.mishra on 2/24/2018.
 */
class EventDetailDataPojo {

    var id : String = ""
    var school_id : String = ""
    var event_name : String = ""
    var start_date : String = ""
    var end_date : String = ""
    var start_time : String = ""
    var end_time : String = ""
    var times : String = ""
    var audience : String = ""
    var category : String = ""
    var details : String = ""
    var registrationDetails : String = ""
    var image : String? = ""
    var created_at : String = ""
    var updated_at : String = ""
    var like : String = ""
    lateinit var comments : ArrayList<EventCommentDataPojo>
}