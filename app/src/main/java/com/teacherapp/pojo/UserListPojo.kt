package com.teacherapp.pojo

import org.json.JSONObject

/**
 * Created by hitesh.mathur on 3/26/2018.
 */
class UserListPojo {
    var id: String? = null
    var father_name: String? = null
    var student_name : String? = null
    var profile_pic: String? = null
    var login_id: String? = null
    var isConnected: String? = null
    var message_counter : Int = 0
    var type : String? = null
    var primary_email : String? =null
    var msg : String? =null
    var created_at : String? =null
    var last_msg: JSONObject? = null
    var chat_size: Int = 0
}