package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by hitesh.mathur on 2/5/2018.
 */
class ImagePojo : Serializable{
    var files: String = ""
    var thumbnail: String = ""
    var media_id: String = ""
    var id : String = ""
    var file : String = ""
}