package com.teacherapp.pojo

import org.json.JSONArray

/**
 * Created by hitesh.mathur on 3/28/2018.
 */
class ChatUserListPojo {
    private var id: String? = null
    internal var fname: String? = null
    private var mname: String? = null
    internal var lname: String? = null
    internal var profile_pic: String? = null
    internal var isConnected: String? = null
    private var login_id: String? = null
    private var chatArray :  JSONArray? = null

    fun getChat_array(): JSONArray? {
        return chat_array
    }

    fun setChat_array(chat_array: JSONArray) {
        this.chat_array = chat_array
    }

    private var chat_array: JSONArray? = null

    fun getLogin_id(): String? {
        return login_id
    }

    fun setLogin_id(login_id: String) {
        this.login_id = login_id
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getFname(): String? {
        return fname
    }

    fun setFname(fname: String) {
        this.fname = fname
    }

    fun getMname(): String? {
        return mname
    }

    fun setMname(mname: String) {
        this.mname = mname
    }

    fun getLname(): String? {
        return lname
    }

    fun setLname(lname: String) {
        this.lname = lname
    }

    fun getProfile_pic(): String? {
        return profile_pic
    }

    fun setProfile_pic(profile_pic: String) {
        this.profile_pic = profile_pic
    }

    fun getIsConnected(): String? {
        return isConnected
    }

    fun setisConnected(isConnected: String) {
        this.isConnected = isConnected
    }
}