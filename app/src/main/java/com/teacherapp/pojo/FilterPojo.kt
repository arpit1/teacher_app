package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by hitesh.mathur on 2/7/2018.
 */
class FilterPojo : Serializable{
    var selected_date : String? = ""
    var category : String = ""
    var album_name : String = ""
    var start_date : String = ""
    var end_date : String = ""

}