package com.teacherapp.pojo

/**
 * Created by hitesh.mathur on 12/22/2017.
 */
class StudentPojoData  {
    var id: String = ""
    var school_id : String = ""
    var parent_id : String = ""
    var fname : String = ""
    var name : String = ""
    var lname : String = ""
    var clas_studen : String = ""
    var section : String = ""
    var enroll : String = ""
    var profile_pic : String = ""
    var email : String = ""
    var mobile : String = ""
    var phone : String = ""
    var address : String = ""
    var presnt_school : String = ""
    var f_qualification : String = ""
    var f_occupation : String = ""
    var f_designation : String = ""
    var m_qualification : String = ""
    var m_occupation : String = ""
    var m_designation : String = ""
    var annual_fee_status : String = ""
    var first_Ins_status : String = ""
    var two_Ins_status : String = ""
    var three_Ins_status : String = ""
    var four_Ins_status : String = ""
    var created_at : String = ""
    var updated_at : String = ""
    var is_checked : Boolean = false
    var is_checked_done : Boolean = false

}

