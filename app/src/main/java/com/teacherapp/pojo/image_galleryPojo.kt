package com.teacherapp.pojo

import android.graphics.Bitmap

/**
 * Created by hitesh.mathur on 2/9/2018.
 */
class image_galleryPojo {
    var media_id : String = ""
    var file : String = ""
    var image_type : String = ""
    var media_type : String = ""
    lateinit var bitmap : Bitmap
}