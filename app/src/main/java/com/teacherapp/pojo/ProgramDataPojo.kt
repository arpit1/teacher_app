package com.teacherapp.pojo

/**
 * Created by arpit.jain on 2/27/2018.
 */
class ProgramDataPojo {
    var id : String = ""
    var school_id : String = ""
    var programs : String = ""
    var description : String = ""
    var created_at : String = ""
}