package com.teacherapp.pojo

import android.text.Editable

/**
 * Created by hitesh.mathur on 12/23/2017.
 */
class StudentDetailsPogo {

    var id: String = ""
    var school_id: String = ""
    var student_id: String = ""
    var physical_devel: String = ""
    var crayon: String = ""
    var tower: String = ""
    var jumps: String = ""
    var lang_devel: String = ""
    var repeat_word: String = ""
    var listens: String = ""
    var answers: String = ""
    var numeracy: String = ""
    var count_ten: String = ""
    var recognize: String = ""
    var distinguish: String = ""
    var emotional: String = ""
    var share: String = ""
    var cheerful: String = ""
    var creative_art: String = ""
    var color: String = ""
    var model: String = ""
    var team: String = ""
    var tear: String = ""
    var created_at: String = ""
    var updated_at: String = ""

}