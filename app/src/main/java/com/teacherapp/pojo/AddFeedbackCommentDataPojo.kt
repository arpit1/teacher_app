package com.schoolmanagement.parent.pojo

/**
 * Created by upasna.mishra on 2/19/2018.
 */
class AddFeedbackCommentDataPojo {

    var id : String = ""
    var feedback_id : String = ""
    var user_id : String = ""
    var comment : String = ""
    var created : String = ""

    var school_id : String = ""
    var diary_id : String = ""
    var teacher_id : String = ""
    var parent_id : String = ""
    var type : String = ""
    var created_at : String = ""
    var t_name : String = ""
    var t_profile_pic : String = ""
    var p_name : String = ""
    var p_profile_pic : String = ""
}