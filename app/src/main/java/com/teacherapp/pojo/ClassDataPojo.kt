package com.teacherapp.pojo

/**
 * Created by matangi.agarwal on 12/4/2017.
 */
class ClassDataPojo{
    var id : String = ""
    var section : ArrayList<String>? = null
    var classes : String = ""
    var class_capacity : String = ""
    var school_id : String = ""
}