package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 11/28/2017.
 */
class LoginDataPojo : Serializable{
    var id : String = ""
    var school_id : String = ""
    var fname : String = ""
    var mname : String = ""
    var lname : String = ""
    var email : String = ""
    var username : String = ""
    var mobile_number : String = ""
    var emeregency_contact_name : String = ""
    var emergency_contact_no : String = ""
    var profile_pic : String? = ""
    var address1 : String = ""
    var address2 : String = ""
    var address3 : String = ""
    var city : String = ""
    var state : String = ""
    var country : String = ""
    var country_name : String = ""
    var pincode : String = ""
    var qualification : String = ""
    var specialization : String = ""
    var passport_id : String = ""
    var program : String = ""
    var batch : String = ""
    var classes : String = ""
    var document : String = ""
    var status : String = ""
    var force_change_status : String = ""
    var profile_update_status : String = ""
}
