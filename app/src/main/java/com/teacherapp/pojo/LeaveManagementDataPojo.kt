package com.teacherapp.pojo

/**
 * Created by upasna.mishra on 12/4/2017.
 */
class LeaveManagementDataPojo {

    var id : String = ""
    var school_id : String =""
    var teacher_id : String = ""
    var from_date : String = ""
    var to_date : String = ""
    var reason : String = ""
    var status : String = ""
    var created_at : String = ""
    var profile_pic : String? = ""
    var ug_qualification : String = ""
    var qualification : String = ""
    var inter: String = ""
}