package com.teacherapp.pojo

/**
 * Created by hitesh.mathur on 12/26/2017.
 */
class EventPojoData {
    var school_id: String = ""
    var category: String = ""
    var event_name: String = ""
    var file: String = ""
    var dates: String = ""
    var id: String = ""
}