package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by hitesh.mathur on 1/3/2018.
 */
class TaskListDataPojo :Serializable{
    var id : String = ""
    var school_id : String = ""
    var teacher_id : String  = ""
    var student_id : String  = ""
    var title : String = ""
    var description : String = ""
    var start_date : String = ""
    var end_date : String = ""
    var reminder_time = ""
    var assign_name : String = ""
    var assign_email : String = ""
    var complete_status : String = ""
    var createt_at : String = ""
    var programs : String? = ""
    var classes : String =""
    var batch_name : String = ""
    var name : String? = ""
    var qualification : String = ""
    var type : String = ""
    var program_id : String = ""
    var batch_id : String = ""
    var classes_id : String = ""
}