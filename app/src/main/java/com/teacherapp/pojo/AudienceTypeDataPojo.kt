package com.teacherapp.pojo

/**
 * Created by upasna.mishra on 4/27/2018.
 */
class AudienceTypeDataPojo(){

    var programs : String = ""
    var id : String = ""
    var school_id : String = ""
    var program : String = ""
    var batch : String = ""
    var classes : String = ""
    var class_capacity : String = ""
    var start_time : String = ""
    var end_time : String = ""
    var status : String = ""
    var created : String = ""
    var description : String = ""
    var created_at : String = ""
}