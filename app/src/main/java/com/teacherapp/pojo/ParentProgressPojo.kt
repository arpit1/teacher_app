package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by hitesh.mathur on 1/18/2018.
 */
class ParentProgressPojo : Serializable{
    var beginning : String = ""
    var developing : String = ""
    var secure : String = ""
}