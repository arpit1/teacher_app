package com.teacherapp.pojo

/**
 * Created by hitesh.mathur on 1/25/2018.
 */
class UserDataPojo {
    var id: String = ""
    var post_time: String = ""
    var description: String = ""
    var postImage: String = ""
    var name: String = ""
    var profile_pic: String = ""
}