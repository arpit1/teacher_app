package com.teacherapp.pojo

/**
 * Created by matangi.agarwal on 12/6/2017.
 */
class GetDiaryDataPojo {

   var id : String = ""
   var school_id : String = ""
   var qualfction : String = ""
   var classes : String = ""
   var classs : String = ""
   var section : String = ""
   var time : String = ""
   var dates : String = ""
   var title : String = ""
   var description : String = ""
   var name : String = ""
   var profile_pic : String? = ""
   var programs : String = ""
   var batch : String = ""
   var status : String = ""
   var batch_name : String = ""
   var class_name : String = ""
   lateinit var files : ArrayList<DiaryFilePojo>
   var comments : ArrayList<ChatPojo>? = null

}

