package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by hitesh.mathur on 12/26/2017.
 */
class GalleryPojoData : Serializable{
    var id: String = ""
    var school_id: String = ""
    var category: String = ""
    var event_name: String = ""
    var album_name: String = ""
    var discussion: String = ""
    var file : ArrayList<ImagePojo>? = null
    var uploaded_by: String = ""
    var upload_type: String = ""
    var dates: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var uploaderName : String = ""
    var uploaderEmail : String = ""
    var profile_pic : String? = ""
    var time : String = ""
    var type : String = ""
    var end_date : String = ""
    var start_date : String = ""
    var start_time : String = ""
    var end_time : String  = ""
    var audience : String  = ""
    var details : String = ""
    var registrationDetails : String = ""
}