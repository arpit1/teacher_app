package com.teacherapp.pojo

/**
 * Created by upasna.mishra on 1/25/2018.
 */
class QuestionDataFieldPojo {

    var field : String = ""
    var section_id : String = ""
    var id : String = ""
    var section : String = ""
    var answer : String = ""
    var dates : String = ""
    var question : String = ""
    var created_at : String = ""
    var remarks : String = ""
}