package com.teacherapp.pojo

/**
 * Created by upasna.mishra on 12/6/2017.
 */
class StudentLeaveDataPojo {
    var id : String =""
    var school_id : String =""
    var parent_id : String =""
    var student_id : String =""
    var student_fname : String =""
    var student_mname: String = ""
    var student_lname: String = ""
    var profile_pic : String? = ""
    var classes: String = ""
    var created_at : String = ""
    var from_date: String = ""
    var to_date : String = ""
    var reason : String = ""
}