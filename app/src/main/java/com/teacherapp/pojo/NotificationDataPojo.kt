package com.teacherapp.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 3/29/2018.
 */
class NotificationDataPojo : Serializable{
    var id : String = ""
    var student_id : String = ""
    var teacher_id : String = ""
    var parent_id : String = ""
    var type : String? = ""
    var title : String = ""
    var created : String = ""
    var profile_pic : String? = ""
    var logo : String? = ""
    var school_id : String = ""
    var audience_type : String = ""
    var program : String = ""
    var description : String = ""
    var uploaded_by : String = ""
    var file : String? = ""
    var classes : String = ""
    var notification_id : String = ""
    var father_name : String = ""
}