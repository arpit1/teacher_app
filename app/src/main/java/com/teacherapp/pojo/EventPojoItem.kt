package com.teacherapp.pojo

/**
 * Created by hitesh.mathur on 12/26/2017.
 */
class EventPojoItem {
    var id: String = ""
    var school_id: String = ""
    var category: String = ""
    var event_name: String = ""
    var dates: String = ""
    var upload_type: String = ""
    var discussion: String = ""
    lateinit var file : ArrayList<ImagefilePojo>


}