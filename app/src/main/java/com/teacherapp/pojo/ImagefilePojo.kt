package com.teacherapp.pojo

/**
 * Created by hitesh.mathur on 2/5/2018.
 */
class ImagefilePojo {
    var media_id: String = ""
    var file: String = ""
    var thumbnail: String = ""
}
