package com.teacherapp.pojo

/**
 * Created by arpit.jain on 2/28/2018.
 */
class FilesDataPojo {
    var id : String = ""
    var diary_id : String = ""
    var file : String? = ""
    var created_at : String = ""
}