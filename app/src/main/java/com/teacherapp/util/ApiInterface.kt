package com.teacherapp.util

import com.schoolmanagement.parent.pojo.AddFeedbackCommentPojo
import com.schoolmanagement.parent.pojo.EventDetailPojo
import com.teacherapp.pojo.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by upasna.mishra on 11/28/2017.
 */
interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    fun login(@Field("username") email: String, @Field("device_token") device_token: String, @Field("password") password: String,
              @Field("device_type") device_type: String): Call<LoginPojo>

    @GET("getcountries")
    fun country_list(): Call<Country_Pojo>

    @FormUrlEncoded
    @POST("force_changePassword")
    fun force_change_pass(@Field("user_id") user_id: String, @Field("password") password: String): Call<LoginPojo>

    @FormUrlEncoded
    @POST("getquestionsanswer")
    fun getQuestionAnswer(@Field("student_id") student_id: String, @Field("template_id") template_id: String): Call<QuestionPojo>


    @FormUrlEncoded
    @POST("forgetPassword")
    fun forgotPassword(@Field("username") email: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("addDiaryComment")
    fun addComment(@Field("school_id") school_id: String, @Field("user_id") user_id: String,
                   @Field("diary_id") diary_id: String, @Field("comment") comment: String,
                   @Field("comment_id") comment_id: String): Call<AddCommentPojo>

    @FormUrlEncoded
    @POST("getPost")
    fun homePost(@Field("school_id") school_id: String, @Field("user_id") user_id: String): Call<HomePostPojo>

    @FormUrlEncoded
    @POST("studentLeave")
    fun getStudentLeave(@Field("user_id") user_id: String): Call<StudentLeavePojo>

    @FormUrlEncoded
    @POST("taskList")
    fun getTaskList(@Field("user_id") user_id: String): Call<TaskListPojo>

    @FormUrlEncoded
    @POST("teacherTasksList")
    fun getTeacherTaskList(@Field("user_id") user_id: String): Call<TaskListPojo>

    @FormUrlEncoded
    @POST("taskFilter")
    fun getTaskListFiltered(@Field("user_id") user_id: String, @Field("task_date") task_date: String): Call<TaskListPojo>


    @FormUrlEncoded
    @POST("get_teacher_leave")
    fun getTeacherLeave(@Field("user_id") user_id: String): Call<LeaveManagementPojo>

    @FormUrlEncoded
    @POST("gettotalLeave")
    fun getTotalLeave(@Field("user_id") user_id: String, @Field("status") status: String): Call<LeaveManagementPojo>

    @FormUrlEncoded
    @POST("add_teacher_leave")
    fun addTeacherLeave(@Field("school_id") school_id: String, @Field("user_id") user_id: String,
                        @Field("from_date") from_date: String,
                        @Field("to_date") to_date: String, @Field("reason") reason: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("add_teacher_task")
    fun addTask(@Field("user_id") user_id: String, @Field("student_id") student_id: String,
                @Field("title") title: String, @Field("start_date") start_date: String,
                @Field("end_date") end_date: String, @Field("assign_email") assign_email: String,
                @Field("assign_name") assign_name: String, @Field("description") description: String,
                @Field("reminder_time") reminder_time: String, @Field("school_id") school_id: String,
                @Field("program_id") program_id: String, @Field("batch_id") batch_id: String,
                @Field("classes_id") classes_id: String, @Field("teacher_name") teacher_name: String): Call<LoginPojo>

    @FormUrlEncoded
    @POST("updateteachertask")
    fun updateTask(@Field("id") task_id: String, @Field("teacher_id") teacher_id: String,
                   @Field("student_id") student_id: String, @Field("title") title: String,
                   @Field("start_date") start_date: String,
                   @Field("end_date") end_date: String, @Field("assign_email") assign_email: String,
                   @Field("assign_name") assign_name: String, @Field("description") description: String,
                   @Field("reminder_time") reminder_time: String, @Field("school_id") school_id: String,
                   @Field("complete_status") complete_status: String, @Field("program_id") program_id: String,
                   @Field("batch_id") batch_id: String, @Field("classes_id") classes_id: String,
                   @Field("teacher_name") teacher_name: String): Call<LoginPojo>

    @Multipart
    @POST("editProfile")
    fun editProfile(@Part("id") id: RequestBody, @Part("fname") fname: RequestBody,
                    @Part("mname") mname: RequestBody, @Part("lname") lname: RequestBody,
                    @Part("qualification") qualification: RequestBody, @Part("passport_id") passport_id: RequestBody,
                    @Part("mobile_number") mobile_number: RequestBody,
                    @Part("address1") address1: RequestBody, @Part("address2") address2: RequestBody,
                    @Part("address3") address3: RequestBody, @Part profile_pic: MultipartBody.Part,
                    @Part("city") city: RequestBody, @Part("state") state: RequestBody,
                    @Part("pincode") pincode: RequestBody, @Part("country") country: RequestBody,
                    @Part("emeregency_contact_name") emeregency_contact_name: RequestBody,
                    @Part("emergency_contact_no") emergency_contact_no: RequestBody,
                    @Part("specialization") specialization: RequestBody): Call<LoginPojo>

    @Multipart
    @POST("editProfile")
    fun editProfileWithoutImage(@Part("id") id: RequestBody, @Part("fname") fname: RequestBody,
                                @Part("mname") mname: RequestBody, @Part("lname") lname: RequestBody,
                                @Part("qualification") qualification: RequestBody,
                                @Part("passport_id") passport_id: RequestBody,
                                @Part("mobile_number") mobile_number: RequestBody,
                                @Part("address1") address1: RequestBody, @Part("address2") address2: RequestBody,
                                @Part("address3") address3: RequestBody, @Part("city") city: RequestBody,
                                @Part("state") state: RequestBody, @Part("pincode") pincode: RequestBody,
                                @Part("country") country: RequestBody,
                                @Part("emeregency_contact_name") emeregency_contact_name: RequestBody,
                                @Part("emergency_contact_no") emergency_contact_no: RequestBody,
                                @Part("specialization") specialization: RequestBody): Call<LoginPojo>


    @FormUrlEncoded
    @POST("getTeacherProfile")
    fun getProfile(@Field("user_id") user_id: String): Call<LoginPojo>

    @FormUrlEncoded
    @POST("getEventDetails")
    fun getEventDetail(@Field("id") id: String, @Field("user_id") user_id: String): Call<EventDetailPojo>

    @FormUrlEncoded
    @POST("addLike")
    fun addLike(@Field("event_id") event_id: String, @Field("user_id") user_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("addComment")
    fun addEventComment(@Field("event_id") event_id: String, @Field("user_id") user_id: String,
                        @Field("description") description: String): Call<AddEventCommentPojo>

    @FormUrlEncoded
    @POST("programs")
    fun getProgram(@Field("user_id") user_id: String): Call<ProgramPojo>

    @FormUrlEncoded
    @POST("getBatch")
    fun getBatch(@Field("school_id") school_id: String, @Field("program_id") program_id: String): Call<BatchPojo>

    @FormUrlEncoded
    @POST("getClass")
    fun getClass(@Field("school_id") school_id: String, @Field("program_id") program_id: String, @Field("batch") batch: String, @Field("teacher_id") teacher_id: String): Call<ClassPojo>

    @FormUrlEncoded
    @POST("addSchoolDiary")
    fun addSchoolDiaryWithoutFiles(@Field("user_id") user_id: String, @Field("school_id") school_id: String,
                                   @Field("batch") batch: String, @Field("classs") classes: String,
                                   @Field("description") description: String, @Field("dates") dates: String,
                                   @Field("title") title: String, @Field("program_id") program_id: String,
                                   @Field("teacher_name") teacher_name: String,
                                   @Field("student_id") student_id: String): Call<section_pojo>

    @FormUrlEncoded
    @POST("addGallery")
    fun addgallery(@Field("user_id") user_id: String, @Field("school_id") school_id: String, @Field("category") category: String
                   , @Field("album_name") album_name: String, @Field("upload_type") upload_type: String, @Field("dates") dates: String, @Field("discussion") discussion: String, @Field("album_id") album_id: String): Call<section_pojo>


    @FormUrlEncoded
    @POST("getSchoolDiary")
    fun getSchoolDiary(@Field("user_id") user_id: String, @Field("dates") dates: String,
                       @Field("school_id") school_id: String,
                       @Field("month") month: String, @Field("year") year: String): Call<GetDiaryPojo>

    @FormUrlEncoded
    @POST("editDiary")
    fun getSchoolDiaryDetail(@Field("diary_id") diary_id: String): Call<FilesPojo>

    @FormUrlEncoded
    @POST("getAllComment")
    fun getAllComment(@Field("diary_id") diary_id: String,
                      @Field("teacher_id") teacher_id: String,
                      @Field("parent_id") parent_id: String): Call<AllCommentPojo>

    @Streaming
    @GET
    fun downloadFileWithDynamicUrlSync(@Url fileUrl: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("student")
    fun studentList(@Field("school_id") school_id: String, @Field("program_id") program_id: String,
                    @Field("batch") batch: String, @Field("classs") classs: String): Call<StudentPojo>

    @FormUrlEncoded
    @POST("event_gallery")
    fun student_event_list(@Field("teacher_id") student_id: String,
                           @Field("album_name") album_name: String, @Field("category") category: String,
                           @Field("school_id") school_id: String, @Field("start_date") start_date: String,
                           @Field("end_date") end_date: String): Call<EventPojo>

    @FormUrlEncoded
    @POST("galleryfilter")
    fun filter_gallery(@Field("teacher_id") teacher_id: String, @Field("category") category: String, @Field("dates") dates: String, @Field("album_name") album_name: String): Call<EventPojo>

    @FormUrlEncoded
    @POST("getactivities")
    fun student_activity_list(@Field("teacher_id") teacher_id: String,
                              @Field("category") category: String,
                              @Field("album_name") album_name: String, @Field("start_date") start_date: String,
                              @Field("end_date") end_date: String): Call<EventPojo>

    @FormUrlEncoded
    @POST("savequestionsanswer")
    fun addAnswerList(@Field("student_id") student_id: String, @Field("template_id") template_id: String,
                      @Field("remarks") remarks: String, @Field("questions") questions: JSONArray,
                      @Field("device_type") device_type: String,
                      @Field("teacher_name") teacher_name: String, @Field("user_id") user_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getalbum")
    fun gallery_event_list(@Field("user_id") user_id: String, @Field("album_id") album_id: String): Call<GalleryPojo>

    @FormUrlEncoded
    @POST("getTemplates")
    fun getTemplate(@Field("school_id") school_id: String, @Field("program") program: String): Call<TemplatePojo>

    @FormUrlEncoded
    @POST("delete_album")
    fun deletePost(@Field("album_id") album_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("delete_attachment")
    fun deleteAttachment(@Field("attachment_id") attachment_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getAllNotifications")
    fun getNotificationList(@Field("user_id") user_id: String): Call<NotificationPojo>

    @FormUrlEncoded
    @POST("listNotice")
    fun listNotice(@Field("teacher_id") teacher_id: String): Call<NotificationPojo>

    @FormUrlEncoded
    @POST("getChatContactsList")
    fun getUserList(@Field("user_id") user_id: String, @Field("type") type: String, @Field("school_id") school_id: String): Call<UserData>

    @FormUrlEncoded
    @POST("logout")
    fun logout(@Field("user_id") user_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getParentProfile")
    fun getParentProfile(@Field("user_id") user_id: String): Call<ParentProfilePojo>

    @FormUrlEncoded
    @POST("updateTeacherTaskssss")
    fun updateTeacherTask(@Field("status") status: String, @Field("type") type: String,
                          @Field("id") id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("changePassword")
    fun changePassword(@Field("user_id") user_id: String,
                       @Field("new_password") new_password: String,
                       @Field("old_password") old_password: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getAudienceType")
    fun getAudienceType(@Field("type") type: String,
                        @Field("teacher_id") teacher_id: String): Call<AudienceTypePojo>

    @FormUrlEncoded
    @POST("addDiaryComment")
    fun addDairyComment(@Field("diary_id") diary_id: String,
                        @Field("school_id") school_id: String,
                        @Field("user_id") user_id: String,
                        @Field("comment") comment: String,
                        @Field("comment_id") comment_id: String,
                        @Field("parent_id") teacher_id: String,
                        @Field("t_name") t_name: String): Call<AddFeedbackCommentPojo>

    @FormUrlEncoded
    @POST("deleteComment")
    fun deleteDiaryComment(@Field("id") comment_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("addNotice")
    fun addNotification(@Field("school_id") school_id: String,
                        @Field("type") type: String, @Field("audience_type") audience_type: String,
                        @Field("title") title: String, @Field("description") description: String,
                        @Field("teacher_id") teacher_id: String, @Field("program") program: String, @Field("name") name: String): Call<BasePojo>

    @Multipart
    @POST("addNoticewithAttachement")
    fun addNoticewithAttachement(@Part("school_id") school_id: RequestBody,
                                 @Part("type") type: RequestBody, @Part("audience_type") audience_type: RequestBody,
                                 @Part("title") title: RequestBody, @Part("description") description: RequestBody,
                                 @Part("teacher_id") teacher_id: RequestBody, @Part file: MultipartBody.Part,
                                 @Part("program") program: RequestBody, @Part("name") name: RequestBody): Call<BasePojo>

}