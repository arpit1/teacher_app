package com.teacherapp.util

import android.app.Activity

object TeacherConstant{

  // const val SERVER_URL : String = "http://192.168.1.234:3007/"
  // const val SERVER_URL : String = "http://in2.endivesoftware.com:3008/"
   const val SERVER_URL : String = "http://63.33.29.220:3000/"
    @JvmStatic val BASE_URL : String = SERVER_URL+"api/mobile/"
    @JvmStatic val IMAGE_URl : String = SERVER_URL+"portal/uploads/media/"
    @JvmStatic val THUMB_IMAGE_URl : String = SERVER_URL+"portal/uploads/media/thumbnail/"
    @JvmStatic val IMAGE_BASE_URL : String = SERVER_URL+"portal/uploads/teacherProfile/"
    @JvmStatic val POSTIMAGE_BASE_URL : String = SERVER_URL+"portal/uploads/teacherpost/"
    @JvmStatic val STUDENT_PROFILE_BASE_URL : String = SERVER_URL+"portal/uploads/studentProfile/"
    @JvmStatic val NOTICE_FILE_BASE_URL : String = SERVER_URL+"portal/uploads/notices/"
    @JvmStatic val ATTACH_DIARY_IMAGE_URL : String = SERVER_URL+"portal/uploads/diary_files/"
    @JvmStatic val SCHOOL_PROFILE_PIC: String = SERVER_URL + "portal/uploads/logo/"
    @JvmStatic val EVENT_IMAGE_URL: String = SERVER_URL + "portal/uploads/event/"
    @JvmStatic val DEVICE_ID = "device_id"
    @JvmStatic val PREF_NAME = "com.teacher.prefs"
    @JvmStatic val IMAGE = "image"
    @JvmStatic val FIRST_NAME = "firstName"
    @JvmStatic val LAST_NAME = "lastName"
    @JvmStatic val MIDDLE_NAME = "middleName"
    @JvmStatic val ID = "id"
    @JvmStatic val SCHOOL_ID = "schoolId"
    @JvmStatic val EMAIL = "email"
    @JvmStatic val QUALIFICATION = "qualification"
    @JvmStatic val FORCE_CHANGE_PASSWORD = "forceChangePassword"
    @JvmStatic val FORCE_UPDATE_PROFILE = "forceUpdateProfile"
    @JvmStatic val DEFAULT_VALUE = ""
    @JvmStatic var DIARY_REFRESH_REQUIRED = false
    var GALLERY_REFRESH : Boolean = false
    var TASK_REFRESH : Boolean = false

    @JvmStatic var ACTIVITIES = java.util.ArrayList<Activity?>()
}