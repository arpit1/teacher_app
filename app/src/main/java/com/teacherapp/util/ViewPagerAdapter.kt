package com.teacherapp.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.teacherapp.fragment.ActivitiesFragment

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        var fragment: Fragment? = null
        if (position == 0) {
           // fragment = EventFragment()
            fragment = ActivitiesFragment()
        } else if (position == 1) {
            fragment = ActivitiesFragment()
        }
        return fragment
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = "EVENT"
        } else if (position == 1) {
            title = "ACTIVITIES"
        }
        return title
    }
}

