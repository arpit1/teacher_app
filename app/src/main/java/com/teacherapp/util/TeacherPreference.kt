package com.teacherapp.util

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.teacherapp.pojo.FilterPojo

/**
 * Created by upasna.mishra on 11/30/2017.
 */
class TeacherPreference(context: Context) {

    private var sharedPreferences: SharedPreferences = context.getSharedPreferences("Teacher", Context.MODE_PRIVATE)
    private lateinit var editor: SharedPreferences.Editor
    private lateinit var context: Context

    fun setSelectOption(selectOption: String) {
        editor = sharedPreferences.edit()
        editor.putString("selectOption", selectOption).apply()
        editor.commit()
    }

    fun getSelectOption(): String {
        return sharedPreferences.getString("selectOption", "null")
    }

    fun setSchoolGallery(album_id: String, event_name: String, upload_type: String, category: String) {
        editor = sharedPreferences.edit()
        editor.putString("album_id", album_id)
        editor.putString("event_name", event_name)
        editor.putString("upload_type", upload_type)
        editor.putString("category", category)
        editor.commit()
    }

    fun setFilterData(selected_filter: FilterPojo?) {
        editor = sharedPreferences.edit()
        val gson = Gson()
        val json = gson.toJson(selected_filter)
        editor.putString("selected_filter", json)
        editor.commit()
    }

    /*    fun setEditedValue(edit_gallery: GalleryPojoData?) {
            editor = sharedPreferences.edit()
            val gson = Gson()
            val json = gson.toJson(edit_gallery)
            editor.putString("edit_gallery", json)
            editor.commit()
        }
        fun getEditedValue(): GalleryPojoData? {
            val gson = Gson()
            val json = sharedPreferences.getString("edit_gallery", "")
            return gson.fromJson<GalleryPojoData>(json, GalleryPojoData::class.java!!)
        }*/
    fun getFilterData(): FilterPojo? {
        val gson = Gson()
        val json = sharedPreferences.getString("selected_filter", "")
        return gson.fromJson<FilterPojo>(json, FilterPojo::class.java)
    }

    fun getEventName(): String {
        return sharedPreferences.getString("event_name", "null")
    }

    fun getCategory(): String {
        return sharedPreferences.getString("category", "null")
    }

    fun getUploaded_type(): String {
        return sharedPreferences.getString("upload_type", "null")
    }

    fun getAlbumId(): String {
        return sharedPreferences.getString("album_id", "null")
    }

    fun setProfilePic(profilePic: String) {
        editor = sharedPreferences.edit()
        editor.putString("profilePic", profilePic).apply()
        editor.commit()
    }

    fun setUserName(userName: String) {
        editor = sharedPreferences.edit()
        editor.putString("userName", userName).apply()
        editor.commit()
    }

    fun getUserName(): String {
        return sharedPreferences.getString("userName", "null")
    }

    fun setQualification(qualification: String) {
        editor = sharedPreferences.edit()
        editor.putString("qualification", qualification).apply()
        editor.commit()
    }

    fun getQualfction(): String {
        return sharedPreferences.getString("qualfction", "null")
    }

    fun getQualification(): String {
        return sharedPreferences.getString("qualification", "null")
    }

    fun setForcePasswordChanged(qualification: String) {
        editor = sharedPreferences.edit()
        editor.putString("changePassword", qualification).apply()
        editor.commit()
    }

    fun getForcePasswordChanged(): String {
        return sharedPreferences.getString("changePassword", "null")
    }

    fun setEnhancedProfileUpdated(qualification: String) {
        editor = sharedPreferences.edit()
        editor.putString("enhancedProfile", qualification).apply()
        editor.commit()
    }

    fun getEnhancedProfileUpdated(): String {
        return sharedPreferences.getString("enhancedProfile", "null")
    }

}