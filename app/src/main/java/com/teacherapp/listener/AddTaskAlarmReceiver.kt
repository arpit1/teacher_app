package com.teacherapp.listener

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import com.teacherapp.R
import com.teacherapp.R.string.app_name
import com.teacherapp.activity.HomeActivity
import com.teacherapp.activity.SplashActivity


/**
 * Created by matangi.agarwal on 5/2/2018.
 */

class AddTaskAlarmReceiver :BroadcastReceiver() {
    override fun onReceive(context:Context, intent:Intent) {
        val notificationIntent = Intent(context, SplashActivity::class.java)
        val title = intent.getStringExtra("notification_title")
        notificationIntent.putExtra("content_type", "localNotification")
        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(HomeActivity::class.java)
        stackBuilder.addNextIntent(notificationIntent)
        val pendingIntent_teacher = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = NotificationCompat.Builder(context)

        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        builder.setSound(uri)

        val notification = builder.setContentTitle(context.getString(app_name))
                .setContentText(title)
                .setTicker(context.resources.getString(R.string.remindar_ticker))
                .setSmallIcon(R.mipmap.notificationicon_tray)
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setContentIntent(pendingIntent_teacher).build()
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(1, notification)
    }
}