package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.*
import com.teacherapp.R
import com.teacherapp.adapter.ImagesAdapter
import com.teacherapp.pojo.GalleryPojo
import com.teacherapp.pojo.GalleryPojoData
import com.teacherapp.pojo.ImagePojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 4/9/2018.
 */
class HomeDetailActivity : BaseActivity() {

    private lateinit var iv_edit_profile: ImageView
    private lateinit var activity_list: GridView
    private lateinit var activityPostList: ArrayList<GalleryPojoData>
    private lateinit var galleryDataPojo: GalleryPojoData
    private lateinit var imageUrl: ArrayList<ImagePojo>
    private lateinit var adapter_add: ImagesAdapter
    private var home_detail_ctx: HomeDetailActivity = this
    private lateinit var tv_no_data: TextView
    private lateinit var event_name: String
    private lateinit var dates: String
    private lateinit var discussion: String
    private lateinit var datetime: TextView
    private lateinit var show_title: TextView
    private lateinit var description: TextView
    private lateinit var getUploaded_type: String
    private lateinit var descGallery: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity_detail_screen)
        initialize()
        setListener()
    }

    private fun initialize() {
        galleryDataPojo = intent.getSerializableExtra("home_post") as GalleryPojoData
        setHeaderTitle(galleryDataPojo.event_name)

        val headerGallery: LinearLayout = findViewById(R.id.header_gallery)
        headerGallery.visibility = View.GONE

        iv_edit_profile = findViewById(R.id.iv_edit_profile)
        iv_edit_profile.visibility = View.GONE
        imageUrl = ArrayList()
        activityPostList = ArrayList()
        activity_list = findViewById(R.id.activity_list)
        tv_no_data = findViewById(R.id.tv_no_data)
        datetime = findViewById(R.id.datetim)
        show_title = findViewById(R.id.stdetail)
        description = findViewById(R.id.description)
        descGallery = findViewById(R.id.descGallery)
        descGallery.movementMethod = ScrollingMovementMethod()

        event_name = galleryDataPojo.event_name

        getUploaded_type = galleryDataPojo.upload_type
        show_title.text = getUploaded_type
    }

    override fun onResume() {
        super.onResume()
        getEventGalleryList(galleryDataPojo.id)
    }

    private fun setListener() {
        activity_list.onItemClickListener = AdapterView.OnItemClickListener { _, _, pos, _ ->
            startActivity(Intent(home_detail_ctx, FullScreenImage::class.java).putExtra("Image", imageUrl)
                    .putExtra("discussion", discussion)
                    .putExtra("mediatype", getUploaded_type)
                    .putExtra("from", "home")
                    .putExtra("header", galleryDataPojo.event_name)
                    .putExtra("dates", dates)
                    .putExtra("pos", pos))
        }
    }

    private fun getEventGalleryList(albumId: String) {
        if (ConnectionDetector.isConnected(home_detail_ctx)) {
            val d = TeacherDialog.showLoading(home_detail_ctx)
            d.setCanceledOnTouchOutside(false)

            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<GalleryPojo> = apiService.gallery_event_list(getFromPrefs(TeacherConstant.ID).toString(), albumId)
            call.enqueue(object : Callback<GalleryPojo> {
                override fun onResponse(call: Call<GalleryPojo>, response: Response<GalleryPojo>?) {
                    imageUrl = ArrayList()
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            galleryDataPojo = response.body()!!.data
                            description.text = galleryDataPojo.event_name
                            discussion = galleryDataPojo.event_name
                            descGallery.text = galleryDataPojo.discussion
                            dates = galleryDataPojo.dates
                            datetime.text = getChangedDate(dates)
                            tv_no_data.visibility = View.GONE
                            getUploaded_type = galleryDataPojo.upload_type
                            for (i in 0 until galleryDataPojo.file!!.size) {
                                imageUrl.add(galleryDataPojo.file!![i])
                            }

                            adapter_add = ImagesAdapter(home_detail_ctx, imageUrl, getUploaded_type)
                            activity_list.adapter = adapter_add
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<GalleryPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}