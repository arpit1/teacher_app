package com.teacherapp.activity

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.splunk.mint.Mint
import com.teacherapp.R
import com.teacherapp.pojo.FilterPojo
import com.teacherapp.service.ExitService
import com.teacherapp.util.TeacherConstant
import com.teacherapp.util.TeacherPreference


class SplashActivity : BaseActivity() {

    private lateinit var prefs: TeacherPreference
    private var splashTimeHandler: Handler? = null
    private var finalizer: Runnable? = null
    private lateinit var filterPojo: FilterPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initialize()

        val intent = Intent(this, ExitService::class.java)
        startService(intent)

        Mint.initAndStartSession(this.application, "77cc26b6")

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    private fun initialize() {
        prefs = TeacherPreference(this)
        filterPojo = FilterPojo()
        prefs.setFilterData(filterPojo)
        splashTimeHandler = Handler()
        println("xhxhxhxhxhxh "+getFromPrefs(TeacherConstant.ID))
        finalizer = Runnable {
            when {
                getFromPrefs(TeacherConstant.ID) == "" -> {
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
                getFromPrefs(TeacherConstant.FORCE_CHANGE_PASSWORD) != "1" -> {
                    startActivity(Intent(this, ForceChangePasswordActivity::class.java))
                    finish()
                }
                getFromPrefs(TeacherConstant.FORCE_UPDATE_PROFILE) != "1" -> {
                    startActivity(Intent(this, EditProfileActivity::class.java)
                            .putExtra("from", "first_login"))
                    finish()
                }
                else -> {
                    subscribeSocket()
                    val intent = Intent(this, HomeActivity::class.java)
//                    intent.putExtra("content_id", getIntent().getStringExtra("content_id"))
                    intent.putExtra("content_type", getIntent().getStringExtra("content_type"))
                    intent.putExtra("sender_id", getIntent().getStringExtra("sender_id"))
                    intent.putExtra("diary_id", getIntent().getStringExtra("diary_id"))
                    intent.putExtra("parent_id", getIntent().getStringExtra("parent_id"))
                    intent.putExtra("name", getIntent().getStringExtra("name"))
                    intent.putExtra("id", getIntent().getStringExtra("id"))
//                    intent.putExtra("description", getIntent().getStringExtra("description"))
//                    intent.putExtra("title", getIntent().getStringExtra("title"))
                    startActivity(intent)
                    finish()
                }
            }
        }
        splashTimeHandler!!.postDelayed(finalizer, 2000)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (splashTimeHandler != null && finalizer != null)
            splashTimeHandler!!.removeCallbacks(finalizer)
    }
}