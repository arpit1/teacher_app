package com.teacherapp.activity

import android.Manifest.permission.CAMERA
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.media.ExifInterface
import android.support.v4.content.FileProvider
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.Country_Pojo
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.pojo.coutry_pojo
import com.teacherapp.util.*
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by upasna.mishra on 3/22/2018.
 */
class EditProfileActivity : BaseActivity(){

    private lateinit var iv_navi : ImageView
    private lateinit var iv_back : ImageView
    private lateinit var iv_notification : ImageView

    private lateinit var iv_profile_pic: CircleImageView
    private lateinit var iv_change_pic : ImageView
    private lateinit var et_name_edit : EditText
    private lateinit var et_last_name : EditText
    private lateinit var et_middle_name : EditText
    private lateinit var et_qualification_edit : EditText
    private lateinit var et_id : TextView
    private lateinit var et_passport : EditText
    private lateinit var phone_no : EditText
    private lateinit var email : TextView
    private lateinit var et_address : EditText
    private lateinit var address2 : EditText
    private lateinit var address3 : EditText
    private lateinit var city : EditText
    private lateinit var state : EditText
    private lateinit var zipcode : EditText
    private lateinit var et_country : EditText
    private lateinit var tv_emergency_name : EditText
    private lateinit var tv_emergency_no : EditText
    private lateinit var et_specializaton : EditText
    private lateinit var tv_save : TextView

    private var permissionsToRequest: ArrayList<String>? = null
    private var permissionsRejected: ArrayList<String>? = null
    private val READ_EXTERNAL_STORAGE = 101
    private var sharedPreferences: SharedPreferences? = null
    private var image_picker_dialog: Dialog? = null
    private lateinit var cross_dialog: ImageView
    private val SELECT_PHOTO = 457
    private val CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456
    private var mCurrentPhotoPath: String? = null
    private var photoPath: String? = ""
    var country_name: ArrayList<String>? = null
    private lateinit var country_list_: ArrayList<coutry_pojo>
    var stringsOrNulls = arrayOfNulls<String>(10)
    private var country_id: String = ""
    private var profileFrom: String = "dashboard"

    private lateinit var login_pojo: LoginPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_profile_activity)
        initialize()
        setListener()
        getDataFromIntent()
        countryList()
    }

    private fun getDataFromIntent() {
        profileFrom = intent.getStringExtra("from")
        if(profileFrom == "first_login") {
            iv_navi.visibility = View.GONE
            iv_back.visibility = View.GONE
            iv_notification.visibility = View.GONE
            getProfile()
        }else {
            iv_navi.visibility = View.GONE
            iv_back.visibility = View.VISIBLE
            iv_notification.visibility = View.VISIBLE
            login_pojo = intent.getSerializableExtra("user_details") as LoginPojo
            setDataOnFields(login_pojo)
        }
    }

    private fun initialize() {
        setHeaderTitle(resources.getString(R.string.edit_profile))
        country_list_ = ArrayList()
        iv_navi = findViewById(R.id.iv_navi)
        iv_back = findViewById(R.id.iv_back)
        iv_notification = findViewById(R.id.iv_notification)
        iv_profile_pic = findViewById(R.id.iv_profile_pic)
        iv_change_pic = findViewById(R.id.iv_change_pic)
        et_name_edit = findViewById(R.id.et_name_edit)
        et_last_name = findViewById(R.id.et_last_name)
        et_middle_name = findViewById(R.id.et_middle_name)
        et_qualification_edit = findViewById(R.id.et_qualification_edit)
        et_id = findViewById(R.id.et_id)
        et_passport = findViewById(R.id.et_passport)
        phone_no = findViewById(R.id.phone_no)
        email = findViewById(R.id.email)
        et_address = findViewById(R.id.et_address)
        address2 = findViewById(R.id.address2)
        address3 = findViewById(R.id.address3)
        city = findViewById(R.id.city)
        state = findViewById(R.id.state)
        zipcode = findViewById(R.id.zipcode)
        et_country = findViewById(R.id.et_country)
        tv_emergency_name = findViewById(R.id.tv_emergency_name)
        tv_emergency_no = findViewById(R.id.tv_emergency_no)
        et_specializaton = findViewById(R.id.et_specializaton)
        tv_save = findViewById(R.id.tv_save)
        sharedPreferences = getSharedPreferences(TeacherConstant.PREF_NAME, Activity.MODE_PRIVATE)
    }

    private fun getProfile() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.getProfile(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1" && response.body()?.data != null) {
                            login_pojo = response.body()!!
                            setDataOnFields(login_pojo)
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setDataOnFields(login_pojo: LoginPojo) {
        val loginDataPojo = login_pojo.data!!
        country_id = loginDataPojo.country
        if (loginDataPojo.profile_pic != null && loginDataPojo.profile_pic != "") {
            setProfileImageInLayout(this, TeacherConstant.IMAGE_BASE_URL + loginDataPojo.profile_pic, iv_profile_pic)
        } else
            iv_profile_pic.setImageResource(R.mipmap.user_icon_default)
        et_name_edit.setText(loginDataPojo.fname)
        et_last_name.setText(loginDataPojo.lname)
        et_middle_name.setText(loginDataPojo.mname)
        et_qualification_edit.setText(loginDataPojo.qualification)
        et_id.text = loginDataPojo.id
        et_passport.setText(loginDataPojo.passport_id)
        phone_no.setText(loginDataPojo.mobile_number)
        email.text = loginDataPojo.email
        et_address.setText(loginDataPojo.address1)
        address2.setText(loginDataPojo.address2)
        address3.setText(loginDataPojo.address3)
        city.setText(loginDataPojo.city)
        state.setText(loginDataPojo.state)
        zipcode.setText(loginDataPojo.pincode)
        et_country.setText(loginDataPojo.country_name)
        tv_emergency_name.setText(loginDataPojo.emeregency_contact_name)
        tv_emergency_no.setText(loginDataPojo.emergency_contact_no)
        et_specializaton.setText(loginDataPojo.specialization)
    }

    private fun setListener() {
        iv_change_pic.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                addPermissionDialogMarshMallow()
            } else {
                showChooserDialog()
            }
        }

        tv_save.setOnClickListener {
            validate()
        }

        et_country.setOnClickListener {
            selectProgress()
        }
    }

    private fun selectProgress() {
        var select: String
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.select_country_popup))
        builder.setItems(stringsOrNulls,
                { _, position ->
                    select = stringsOrNulls[position].toString()
                    et_country.setText(select)
                    country_id = country_list_[position].id
                })
        val alert = builder.create()
        alert.show()
        return
    }

    private fun addPermissionDialogMarshMallow() {
        val permissions = ArrayList<String>()
        val resultCode: Int = READ_EXTERNAL_STORAGE

        permissions.add(WRITE_EXTERNAL_STORAGE)
        permissions.add(CAMERA)

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions)
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest!!.size > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                //mark all these as asked..
                for (perm in permissionsToRequest!!) {
                    sharedPreferences?.let { markAsAsked(perm, it) }
                }
            } else {
                //show the success banner
                if (permissionsRejected!!.size < permissions.size) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog()
                }

                if (permissionsRejected!!.size > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application
                    requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                    //mark all these as asked..
                    for (perm in permissionsToRequest!!) {
                        sharedPreferences?.let { markAsAsked(perm, it) }
                    }
                }
            }
        }
    }

    private fun showChooserDialog() {
        image_picker_dialog = Dialog(this, android.R.style.Theme_Translucent_NoTitleBar)
        image_picker_dialog!!.setContentView(R.layout.custom_imageupload_dialog)
        image_picker_dialog!!.setCancelable(true)

        val gallery: LinearLayout = image_picker_dialog!!.findViewById(R.id.galleryLayout)
        val camera: LinearLayout = image_picker_dialog!!.findViewById(R.id.cameraLayout)

        cross_dialog = image_picker_dialog!!.findViewById(R.id.cross_dialog)
        cross_dialog.setOnClickListener({ image_picker_dialog!!.dismiss() })

        gallery.setOnClickListener {
            val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

            startActivityForResult(i, SELECT_PHOTO)
            image_picker_dialog!!.dismiss()
            image_picker_dialog!!.hide()
        }

        camera.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            //				File f = null;
            try {
                //					f = setUpPhotoFile();

                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
                val imageFileName = "bmh" + timeStamp + "_"
                val albumF = getAlbumDir()
                val imageF = File.createTempFile(imageFileName, "bmh", albumF)

                mCurrentPhotoPath = imageF.absolutePath
                //takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                val photoURI: Uri = FileProvider.getUriForFile(this, applicationContext.packageName + ".my.package.name.provider", imageF)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
            image_picker_dialog!!.dismiss()
            image_picker_dialog!!.hide()
        }
        image_picker_dialog!!.show()
    }

    private fun getAlbumDir(): File? {
        var storageDir: File? = null

        if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraPicture")

            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    //		Log.d("CameraSample", "failed to create directory");
                    return null
                }
            }
        } else {
            //		Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }
        return storageDir
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data == null) {
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                val selectedImage = data!!.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                val cursor = this.contentResolver.query(selectedImage, filePathColumn, null, null, null)
                cursor.moveToFirst()

                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                val imagePath1 = cursor.getString(columnIndex)

                //                File f =new File(compressImage(imagePath1, 1));
                val f = File(compressImage(imagePath1))
                val myBitmap: Bitmap
                myBitmap = BitmapFactory.decodeFile(f.absolutePath)

                iv_profile_pic.setImageBitmap(myBitmap)
                photoPath = f.absolutePath
                cursor.close()
            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                val f = File(mCurrentPhotoPath?.let { compressImage(it) })
                val myBitmap = BitmapFactory.decodeFile(f.absolutePath)

                iv_profile_pic.setImageBitmap(myBitmap)
                photoPath = f.absolutePath
            }
        }
    }

    private fun compressImage(imageUri: String): String {
        val filePath: String = imageUri

        var scaledBitmap: Bitmap? = null

        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var bmp: Bitmap?
        bmp = BitmapFactory.decodeFile(filePath, options)

        var actualHeight = options.outHeight
        var actualWidth = options.outWidth
        val maxHeight = 816.0f
        val maxWidth = 612.0f

        var imgRatio = (actualWidth / actualHeight).toFloat()
        val maxRatio = maxWidth / maxHeight

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            when {
                imgRatio < maxRatio -> {
                    imgRatio = maxHeight / actualHeight
                    actualWidth = (imgRatio * actualWidth).toInt()
                    actualHeight = maxHeight.toInt()
                }
                imgRatio > maxRatio -> {
                    imgRatio = maxWidth / actualWidth
                    actualHeight = (imgRatio * actualHeight).toInt()
                    actualWidth = maxWidth.toInt()
                }
                else -> {
                    actualHeight = maxHeight.toInt()
                    actualWidth = maxWidth.toInt()

                }
            }
        }

        val utils = ImageLoadingUtlis(this)
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight)
        options.inJustDecodeBounds = false
        options.inDither = false
        //        options.inPurgeable = true;
        //        options.inInputShareable = true;
        options.inTempStorage = ByteArray(16 * 1024)

        try {
            bmp = BitmapFactory.decodeFile(filePath, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }

        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)

        val canvas = Canvas(scaledBitmap!!)
        canvas.matrix = scaleMatrix
        canvas.drawBitmap(bmp, middleX - bmp!!.width / 2, middleY - bmp.height / 2, Paint(Paint.FILTER_BITMAP_FLAG))


        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath)

            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            when (orientation) {
                6 -> {
                    matrix.postRotate(90f)
                    Log.d("EXIF", "Exif: $orientation")
                }
                3 -> {
                    matrix.postRotate(180f)
                    Log.d("EXIF", "Exif: $orientation")
                }
                8 -> {
                    matrix.postRotate(270f)
                    Log.d("EXIF", "Exif: $orientation")
                }
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val out: FileOutputStream?
        val filename = getFilename()
        try {
            out = FileOutputStream(filename)
            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return filename
    }

    private fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().path, "MyFolder/Images")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"
    }

    private fun validate() {
        val first_name = et_name_edit.text.toString().trim()
        val middle_name = et_middle_name.text.toString().trim()
        val last_name = et_last_name.text.toString().trim()
        val qualification_edit = et_qualification_edit.text.toString().trim()
        val passport = et_passport.text.toString().trim()
        val mobile_no_edit = phone_no.text.toString().trim()
        val address = et_address.text.toString().trim()
        val city = city.text.toString().trim()
        val state_name = state.text.toString().trim()
        val zip_code = zipcode.text.toString().trim()
        val emergency_name = tv_emergency_name.text.toString().trim()
        val emergency_no = tv_emergency_no.text.toString().trim()
        val selected_country = et_country.text.toString().trim()
        val specialization = et_specializaton.text.toString().trim()
        if (first_name.isEmpty()) {
            displayToast(resources.getString(R.string.first_name))
        }else if(last_name.isEmpty()){
            displayToast(resources.getString(R.string.last_name))
        }  else if (qualification_edit.isEmpty()) {
            displayToast(resources.getString(R.string.qualification_msg))
        } else if (passport.isEmpty()) {
            displayToast(resources.getString(R.string.passport_id_msg))
        } else if (mobile_no_edit.isEmpty()) {
            displayToast(resources.getString(R.string.mobile_no_msg))
        } else if (mobile_no_edit != "" && !mobile_no_edit.matches("[0-9]+".toRegex())) run {
            displayToast(resources.getString(R.string.valid_mobile_no_msg))
        } else if (mobile_no_edit.length < 8) {
            displayToast(resources.getString(R.string.no_between_msg))
        } else if (address.isEmpty()) {
            displayToast(resources.getString(R.string.address))
        } else if(city.isEmpty()){
            displayToast(resources.getString(R.string.city_msg))
        }else if(state_name.isEmpty()){
            displayToast(resources.getString(R.string.state_msg))
        }else if(zip_code.isEmpty()){
            displayToast(resources.getString(R.string.zip_code_msg))
        } else if (zip_code.length < 3) {
            displayToast(resources.getString(R.string.zipcode_btwn_msg))
        }else if (selected_country.isEmpty()) {
            displayToast(resources.getString(R.string.select_country))
        } else if (emergency_name.isEmpty()) {
            displayToast(resources.getString(R.string.emergency_name_msg))
        } else if (emergency_no.isEmpty()) {
            displayToast(resources.getString(R.string.emergency_mobile_no_msg))
        } else if (emergency_no.length < 8) {
            displayToast(resources.getString(R.string.no_between_msg))
        } else if (specialization.isEmpty()) {
            displayToast(resources.getString(R.string.specialization_msg))
        } else {
            save(first_name, middle_name, last_name, qualification_edit, passport, mobile_no_edit, address, city, state_name,
                    zip_code, emergency_name, emergency_no, country_id, specialization)
            hideSoftKeyboard()
        }
    }

    private fun save(first_name: String, middle_name: String, last_name: String, qualification_edit: String,
                     passport: String, mobile_no_edit: String, address: String,
                     city: String, state_name: String, zip_code: String, emergency_name: String,
                     emergency_no: String, country_id: String, specialization: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val file = File(photoPath)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body: MultipartBody.Part?
            val call: Call<LoginPojo>?
            if (photoPath != null && photoPath != "") {
                body = MultipartBody.Part.createFormData("profile_pic", file.name, requestFile)
                call = apiService.editProfile(getMultiPartData(getFromPrefs(TeacherConstant.ID).toString()),
                        getMultiPartData(first_name), getMultiPartData(middle_name), getMultiPartData(last_name),
                        getMultiPartData(qualification_edit), getMultiPartData(passport),
                        getMultiPartData(mobile_no_edit), getMultiPartData(address),
                        getMultiPartData(address2.text.toString()), getMultiPartData(address3.text.toString()),
                        body, getMultiPartData(city), getMultiPartData(state_name),
                        getMultiPartData(zip_code), getMultiPartData(country_id),
                        getMultiPartData(emergency_name), getMultiPartData(emergency_no), getMultiPartData(specialization))
            } else {
                call = apiService.editProfileWithoutImage(getMultiPartData(getFromPrefs(TeacherConstant.ID).toString()),
                        getMultiPartData(first_name), getMultiPartData(middle_name), getMultiPartData(last_name),
                        getMultiPartData(qualification_edit), getMultiPartData(passport),
                        getMultiPartData(mobile_no_edit), getMultiPartData(address),
                        getMultiPartData(address2.text.toString()), getMultiPartData(address3.text.toString()),
                        getMultiPartData(city), getMultiPartData(state_name),
                        getMultiPartData(zip_code), getMultiPartData(country_id),
                        getMultiPartData(emergency_name), getMultiPartData(emergency_no), getMultiPartData(specialization))
            }

            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            removeActivity(resources.getString(R.string.package_name) + ".ProfileActivity")
                            saveIntoPrefs(TeacherConstant.IMAGE, response.body()!!.data!!.profile_pic!!)
                            saveIntoPrefs(TeacherConstant.FIRST_NAME, response.body()!!.data!!.fname)
                            saveIntoPrefs(TeacherConstant.MIDDLE_NAME, response.body()!!.data!!.mname)
                            saveIntoPrefs(TeacherConstant.LAST_NAME, response.body()!!.data!!.lname)
                            saveIntoPrefs(TeacherConstant.EMAIL, response.body()!!.data!!.email)
                            saveUserData(response.body()!!)
                            if (profileFrom == "first_login") {
                                startActivity(Intent(applicationContext, HomeActivity::class.java))
                                saveIntoPrefs(TeacherConstant.FORCE_UPDATE_PROFILE, "1")
                            } else {
                                finish()
                            }
                            finish()
                        }
                        displayToast(response.body()!!.message)

                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun getMultiPartData(value: String): RequestBody {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), value)
    }

    private fun countryList() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            country_name = ArrayList()
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<Country_Pojo> = apiService.country_list()
            call.enqueue(object : Callback<Country_Pojo> {
                override fun onResponse(call: Call<Country_Pojo>, response: Response<Country_Pojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            country_list_.addAll(response.body()!!.data)
                            stringsOrNulls = arrayOfNulls(country_list_.size)
                            for (i in 0 until country_list_.size) {
                                country_name!!.add(country_list_[i].country)
                                stringsOrNulls[i] = country_list_[i].country
                            }
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<Country_Pojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}