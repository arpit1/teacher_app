package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 11/25/2017.
 */
class ForceChangePasswordActivity : BaseActivity() {

    private lateinit var et_password: EditText
    private lateinit var et_confirm_password: EditText
    private lateinit var tv_submit: TextView
    private lateinit var iv_navi: ImageView
    private lateinit var iv_back: ImageView
    private lateinit var tv_header: TextView
    private lateinit var prefs: TeacherPreference
    private lateinit var iv_notification: ImageView
    private lateinit var iv_logout: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_force_change_password)
        setHeaderTitle(resources.getString(R.string.force_change_password))
        initialize()
        setListener()
    }

    private fun initialize() {
        et_password = findViewById(R.id.et_password)
        et_confirm_password = findViewById(R.id.et_confirm_password)
        tv_submit = findViewById(R.id.tv_submit)
        iv_navi = findViewById(R.id.iv_navi)
        iv_back = findViewById(R.id.iv_back)
        iv_notification = findViewById(R.id.iv_notification)
        iv_logout = findViewById(R.id.iv_logout)
        tv_header = findViewById(R.id.tv_header)
        iv_notification.visibility = View.GONE
        iv_logout.visibility = View.GONE
        iv_navi.visibility = View.GONE
        iv_back.visibility = View.GONE
    }

    private fun setListener() {
        tv_submit.setOnClickListener {
            hideSoftKeyboard()
            if (validate()) {
                forceChangePass(et_password.text.toString())
            }
        }
    }

    private fun validate(): Boolean {
        val pass = et_password.text.toString();
        val confirmPass = et_confirm_password.text.toString();
        prefs = TeacherPreference(this)
        if (pass.isEmpty()) {
            displayToast(resources.getString(R.string.password_msg))
            return false
        } else if (confirmPass.isEmpty()) {
            displayToast(resources.getString(R.string.confirm_password_msg))
            return false
        } else if (pass.length < 6) {
            displayToast(resources.getString(R.string.min_password_msg))
            return false
        } /*else if (confirmPass.length < 6) {
            displayToast(resources.getString(R.string.min_confirm_password_msg))
            return false
        }*/ else if (confirmPass != pass) {
            displayToast(resources.getString(R.string.password_nomatch_msg))
            return false
        } else if (!passCombination(pass)) {
            displayToast(resources.getString(R.string.combination_msg))
            return false
        }
        return true
    }

    private fun forceChangePass(pass: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.force_change_pass(getFromPrefs(TeacherConstant.ID).toString(), pass)
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response!!.body()!!.status == "1") {
                        saveIntoPrefs(TeacherConstant.FORCE_CHANGE_PASSWORD, "1")
                        displayToast(response.body()!!.message)
                        startActivity(Intent(applicationContext, EditProfileActivity::class.java)
                                .putExtra("from", "first_login"))
                        finish()

                    } else {
                        displayToast(response.body()!!.message)
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}