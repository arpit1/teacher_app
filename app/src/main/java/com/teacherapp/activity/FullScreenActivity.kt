package com.teacherapp.activity

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.github.rtoshiro.view.video.FullscreenVideoLayout
import com.teacherapp.R
import java.io.IOException

/**
 * Created by hitesh.mathur on 3/26/2018.
 */
class FullScreenActivity : BaseActivity() {

    private var videoview: FullscreenVideoLayout? = null
    private var view_image: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_acrivity)

        videoview = findViewById(R.id.videoview_fullscreen)
        view_image = findViewById(R.id.view_image)
        val intent = intent
        val media_url = intent.getStringExtra("mediaUrl")
        val media_type = intent.getStringExtra("media_type")
        videoview!!.setActivity(this)
        videoview!!.isShouldAutoplay = true
        if (media_type == "image") {
            videoview!!.visibility = View.GONE
            view_image!!.visibility = View.VISIBLE
            setHomePostImageInLayout(this, media_url, view_image!!)
        } else {
            videoview!!.visibility = View.VISIBLE
            view_image!!.visibility = View.GONE
            loadVideo(media_url)
        }
    }

    private fun loadVideo(video: String) {
        val videoUri = Uri.parse(video)
        try {
            videoview!!.setVideoURI(videoUri)
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
}