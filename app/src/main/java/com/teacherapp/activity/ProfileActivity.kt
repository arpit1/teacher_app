package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.util.*
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable

class ProfileActivity : BaseActivity() {

    private lateinit var iv_notification: ImageView
    private lateinit var iv_edit_profile: ImageView

    private lateinit var iv_profile_pic: CircleImageView
    private lateinit var tv_name: TextView
    private lateinit var tv_email: TextView
    private lateinit var tv_qualification: TextView
    private lateinit var tv_mobile_no: TextView
    private lateinit var et_id: TextView
    private lateinit var et_passport: TextView
    private lateinit var et_address: TextView
    private lateinit var tv_emergency_name: TextView
    private lateinit var tv_emergency_no: TextView
    private lateinit var et_country: TextView
    private lateinit var et_specializaton: TextView
    private lateinit var changePassword: TextView
    private val ctx: ProfileActivity = this
    private lateinit var login_pojo: LoginPojo
    private lateinit var prefs: TeacherPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        setContentView(R.layout.activity_profile)
//        TeacherConstant.ACTIVITIES.add(this)
        initialize()
        setListener()
    }

    private fun initialize() {
        prefs = TeacherPreference(this)
        login_pojo = LoginPojo()
        iv_profile_pic = findViewById(R.id.iv_profile_pic)
        tv_name = findViewById(R.id.tv_name)
        tv_qualification = findViewById(R.id.tv_qualification)
        tv_email = findViewById(R.id.tv_email)
        tv_mobile_no = findViewById(R.id.tv_mobile_no)
        et_id = findViewById(R.id.et_id)
        et_passport = findViewById(R.id.et_passport)
        et_address = findViewById(R.id.et_address)
        et_country = findViewById(R.id.et_country)
        tv_emergency_name = findViewById(R.id.tv_emergency_name)
        tv_emergency_no = findViewById(R.id.tv_emergency_no)
        et_specializaton = findViewById(R.id.et_specializaton)
        changePassword = findViewById(R.id.changePassword)
        //getProfile()
        iv_notification = findViewById(R.id.iv_notification)
        iv_edit_profile = findViewById(R.id.iv_edit_profile)
        iv_notification.visibility = View.GONE
        iv_edit_profile.visibility = View.VISIBLE

        setHeaderTitle(resources.getString(R.string.profile))
    }

    override fun onResume() {
        super.onResume()
        getProfile()
    }

    private fun setDataOnFields(login_pojo: LoginPojo) {
        val loginDataPojo = login_pojo.data!!
        if (loginDataPojo.profile_pic != null && loginDataPojo.profile_pic != "") {
            setProfileImageInLayout(this, TeacherConstant.IMAGE_BASE_URL + loginDataPojo.profile_pic, iv_profile_pic)
        } else
            iv_profile_pic.setImageResource(R.mipmap.user_icon_default)
        tv_name.text = getTeacherName()
        tv_qualification.text = loginDataPojo.qualification
        tv_email.text = loginDataPojo.email
        tv_mobile_no.text = loginDataPojo.mobile_number
        et_id.text = loginDataPojo.id
        et_passport.text = loginDataPojo.passport_id
        if (loginDataPojo.address2 == "" && loginDataPojo.address3 == "") {
            et_address.text = loginDataPojo.address1 + ", " + loginDataPojo.city + ", " + loginDataPojo.state + ", " + loginDataPojo.pincode
        } else if (loginDataPojo.address2 != "" && loginDataPojo.address3 == "") {
            et_address.text = loginDataPojo.address1 + ", " + loginDataPojo.address2 + ", " +
                    loginDataPojo.city + ", " + loginDataPojo.state + ", " + loginDataPojo.pincode
        } else if (loginDataPojo.address3 != "" && loginDataPojo.address2 == "") {
            et_address.text = loginDataPojo.address1 + ", " + loginDataPojo.address3 +
                    ", " + loginDataPojo.city + ", " + loginDataPojo.state + ", " + loginDataPojo.pincode
        } else if (loginDataPojo.address3 != "" && loginDataPojo.address2 != "") {
            et_address.text = loginDataPojo.address1 + ", " + loginDataPojo.address2 + ", " + loginDataPojo.address3 +
                    ", " + loginDataPojo.city + ", " + loginDataPojo.state + ", " + loginDataPojo.pincode
        }
        et_country.text = loginDataPojo.country_name
        tv_emergency_name.text = loginDataPojo.emeregency_contact_name
        tv_emergency_no.text = loginDataPojo.emergency_contact_no
        et_specializaton.text = loginDataPojo.specialization
    }

    private fun setListener() {
        iv_edit_profile.setOnClickListener {
            startActivity(Intent(applicationContext, EditProfileActivity::class.java)
                    .putExtra("user_details", login_pojo as Serializable)
                    .putExtra("from", "edit"))
        }

        changePassword.setOnClickListener {
            startActivity(Intent(ctx, ChangePasswordActivity::class.java))
        }
    }

    private fun getProfile() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.getProfile(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1" && response.body()?.data != null) {
                            login_pojo = response.body()!!
                            setDataOnFields(login_pojo)
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}