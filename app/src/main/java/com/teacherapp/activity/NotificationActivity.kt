package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.adapter.NotificationAdapter
import com.teacherapp.pojo.NotificationDataPojo
import com.teacherapp.pojo.NotificationPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by arpit.jain on 1/30/2018.
 */
class NotificationActivity : BaseActivity(), View.OnClickListener {


    private lateinit var rvNotifictionList: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var notificationList: ArrayList<NotificationDataPojo>
    private lateinit var adapter: NotificationAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private var ctx: NotificationActivity = this
    private var searchView: android.support.v7.widget.SearchView? = null
    private var notificationDataFilter: ArrayList<NotificationDataPojo>? = null
    var fragmentFrom: String = "diary"
    lateinit var home_ctx: HomeActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        setHeaderTitle(resources.getString(R.string.notifications))
        TeacherConstant.ACTIVITIES.add(this)
        initialize()
        setListener()
    }

    private fun initialize() {
        home_ctx = HomeActivity()

        notificationDataFilter = ArrayList()
        mLayoutManager = LinearLayoutManager(this)
        val ivNotification = findViewById<ImageView>(R.id.iv_notification)
        rvNotifictionList = findViewById(R.id.rvNotifictionList)
        searchView = findViewById(R.id.search_box)
        tv_no_data = findViewById(R.id.tv_no_data)
        ivNotification.visibility = View.GONE
        val ivNavigation: ImageView = findViewById(R.id.iv_navi)
        val ivBack: ImageView = findViewById(R.id.iv_back)
        ivNavigation.visibility = View.GONE
        ivBack.visibility = View.VISIBLE
        rvNotifictionList.layoutManager = mLayoutManager
        getNotification()
    }

    override fun onResume() {
        super.onResume()
        notificationList = ArrayList()
        if (TeacherConstant.TASK_REFRESH) {
            getNotification()
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val task: ArrayList<NotificationDataPojo> = notificationList
                notificationDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until task.size)
                            .filter {
                                task[it].title.toLowerCase().contains(newText.toLowerCase()) ||
                                        task[it].created.toLowerCase().contains(newText.toLowerCase())
                            }
                            .forEach { notificationDataFilter!!.add(task[it]) }
                } else {
                    notificationDataFilter!!.addAll(task)
                }
                adapter = NotificationAdapter(ctx, notificationDataFilter!!)
                rvNotifictionList.adapter = adapter
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }
        })
    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.row_click -> {
                    val notificationData = view.getTag(R.string.data) as NotificationDataPojo
                    redirectActivity(notificationData)
                }
            }
        }
    }

    private fun redirectActivity(notificationData: NotificationDataPojo) {
        val type = notificationData.type
        val id = notificationData.notification_id
        val name = notificationData.father_name
        val parentId = notificationData.parent_id
        if (type != null) {
            when (type) {
                "Task" -> {
                    removeAllActivities()
                    val intent = Intent(this, HomeActivity::class.java)
                    intent.putExtra("content_type", type)
                    startActivity(intent)
                    finish()
                }
                "Leave" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        removeActivity(resources.getString(R.string.package_name) + ".StudentLeaveActivity")
                        startActivity(Intent(ctx, StudentLeaveActivity::class.java).putExtra("from", "Leave Management"))
                    }, 150)
                }
                "diaryComment" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, SelectDiaryCommentActivity::class.java)
                                .putExtra("diary_id", id)
                                .putExtra("parent_id", parentId)
                                .putExtra("name", name)
                                .putExtra("from", "notification"))
                    }, 150)
                }
                "leaveApproved" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, TeacherShowLeaveListActivity::class.java)
                                .putExtra("header", getString(R.string.total_approv))
                                .putExtra("type", "1")
                                .putExtra("year", Calendar.getInstance().get(Calendar.YEAR)))
                    }, 150)
                }
                "leaveRejected" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, TeacherShowLeaveListActivity::class.java)
                                .putExtra("header", getString(R.string.total_reject))
                                .putExtra("type", "2")
                                .putExtra("year", Calendar.getInstance().get(Calendar.YEAR)))
                    }, 150)
                }
            }
        }
    }

    private fun getNotification() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<NotificationPojo> = apiService.getNotificationList(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<NotificationPojo> {
                override fun onResponse(call: Call<NotificationPojo>, response: Response<NotificationPojo>?) {
                    if (response != null) {
                        if (response.body()!!.data.size > 0) {
                            TeacherConstant.TASK_REFRESH = false
                            rvNotifictionList.visibility = View.VISIBLE
                            tv_no_data.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                notificationList.addAll(response.body()!!.data)
                                adapter = NotificationAdapter(ctx, notificationList)
                                rvNotifictionList.adapter = adapter
                            } else {
                                displayToast(response.body()!!.message)
                            }
                        } else {
                            rvNotifictionList.visibility = View.GONE
                            tv_no_data.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<NotificationPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}