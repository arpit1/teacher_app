package com.teacherapp.activity

import android.Manifest
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.teacherapp.R
import com.teacherapp.adapter.FilesListAdapter
import com.teacherapp.adapter.SchoolDiaryCommentAdapter
import com.teacherapp.pojo.EventCommentDataPojo
import com.teacherapp.pojo.FilesDataPojo
import com.teacherapp.pojo.FilesPojo
import com.teacherapp.util.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

/**
 * Created by arpit.jain on 2/28/2018.
 */
class SchoolDiaryDetailActivity : BaseActivity(), View.OnClickListener {

    private lateinit var descSchoolDiary: TextView
    private lateinit var noDataAvailable: TextView
    private lateinit var rvAttachFile: RecyclerView
    private var ctx: SchoolDiaryDetailActivity = this

    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var fileList: ArrayList<FilesDataPojo>
    private lateinit var adapter: FilesListAdapter

    private var folder: File? = null
    private var file_name: String? = null
    private lateinit var data: FilesDataPojo
    private var d: ProgressDialog? = null
    private lateinit var ll_attach_section : LinearLayout
    private lateinit var ll_comment_section : LinearLayout
    private var commentAdapter: SchoolDiaryCommentAdapter? = null
    private lateinit var rvCommentList : RecyclerView
    private lateinit var commentLayoutManager: LinearLayoutManager
    private lateinit var commentList: ArrayList<EventCommentDataPojo>
    private lateinit var feedback_comment: EventCommentDataPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_diary_detail)
        setHeaderTitle(intent.getStringExtra("title"))
        initialize()
    }

    private fun initialize() {
        commentList = ArrayList()
        mLayoutManager = LinearLayoutManager(ctx)
        commentLayoutManager = LinearLayoutManager(ctx)
        rvAttachFile = findViewById(R.id.rvAttachFile)
        noDataAvailable = findViewById(R.id.noDataAvailable)
        descSchoolDiary = findViewById(R.id.descSchoolDiary)
        ll_attach_section = findViewById(R.id.ll_attach_section)
        ll_comment_section = findViewById(R.id.ll_comment_section)
        rvCommentList = findViewById(R.id.rvCommentList)
        descSchoolDiary.text = intent.getStringExtra("desc")
        descSchoolDiary.movementMethod = ScrollingMovementMethod()
        rvAttachFile.layoutManager = mLayoutManager
        rvCommentList.layoutManager = commentLayoutManager
        getDiaryDetail()
    }

    private fun getDiaryDetail() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<FilesPojo> = apiService.getSchoolDiaryDetail(intent.getStringExtra("diary_id"))
            call.enqueue(object : Callback<FilesPojo> {
                override fun onResponse(call: Call<FilesPojo>, response: Response<FilesPojo>?) {

                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            fileList = response.body()!!.data
                            if(response.body()!!.data.size>0) {
                                ll_attach_section.visibility = View.VISIBLE
                                adapter = FilesListAdapter(ctx, fileList)
                                rvAttachFile.adapter = adapter

                            } else {
                                ll_attach_section.visibility = View.GONE
                            }
                            if(response.body()!!.comment.size>0){
                                ll_comment_section.visibility = View.VISIBLE
                                commentList = response.body()!!.comment
                                if (commentAdapter == null) {
                                    commentAdapter = SchoolDiaryCommentAdapter(ctx, commentList)
                                    rvCommentList.adapter = commentAdapter
                                }
                            }else{
                                ll_comment_section.visibility = View.GONE
                            }
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<FilesPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    override fun onClick(v: View?) {
        if(v != null){
            when (v.id){
                R.id.row_layout -> {
                    data = v.getTag(R.string.data) as FilesDataPojo
                    viewAndDownload()
                }

                R.id.ll_parent_commnet -> {
                    feedback_comment = v.getTag(R.string.send_comment_list) as EventCommentDataPojo
                    startActivity(Intent(ctx, SelectDiaryCommentActivity::class.java)
                            .putExtra("comment_list", feedback_comment)
                            .putExtra("from", "diary"))
                }
            }
        }

    }

    private fun viewAndDownload() {
        folder = File(Environment.getExternalStorageDirectory().toString() + "/schoolManagement")
        if (folder != null && !folder!!.exists())
            folder!!.mkdir()
        file_name = data.file
        val file = File(folder.toString() + "/" + file_name)
        if (!file.exists()) {
            if (isStoragePermissionGranted())
                download()
        } else
            if (isStoragePermissionGranted())
                view()
    }

    private fun isStoragePermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("PER", "Permission is granted")
                true
            } else {
                Log.v("PER", "Permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("PER", "Permission is granted")
            true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("per", "Permission: " + permissions[0] + "was " + grantResults[0])
            download()
            //resume tasks needing this permission
        }
    }

    private fun download() {
        if (ConnectionDetector.isConnected(this)) {
            val downloadService = ApiClient.getClient()!!.create(ApiInterface::class.java)

            val call = downloadService.downloadFileWithDynamicUrlSync(TeacherConstant.ATTACH_DIARY_IMAGE_URL + file_name)
            d = TeacherDialog.showLoading(this)
            d?.setCanceledOnTouchOutside(false)
            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.d("Hello", "server contacted and has file")
                    val writtenToDisk = writeResponseBodyToDisk(response.body()!!)
                    println("xhxhxhhxx file download was a success? $writtenToDisk")
                    if (!writtenToDisk && d!!.isShowing) {
                        d?.dismiss()
                        displayToast(resources.getString(R.string.file_download))
                    }
                }

                override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun writeResponseBodyToDisk(body: ResponseBody): Boolean {
        try {
            val futureStudioIconFile = File(Environment.getExternalStorageDirectory().toString() + "/schoolManagement/" + file_name)

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                var fileSizeDownloaded: Long = 0

                inputStream = body.byteStream()
                outputStream = FileOutputStream(futureStudioIconFile)

                while (true) {
                    val read = inputStream!!.read(fileReader)

                    if (read == -1) {
                        break
                    }

                    outputStream.write(fileReader, 0, read)

                    fileSizeDownloaded += read.toLong()

//                    Log.d("Hello", "file download: $fileSizeDownloaded of $fileSize")
                    if (d != null && d!!.isShowing)
                        d?.dismiss()
                }
                view()
                outputStream.flush()

                return true
            } catch (e: IOException) {
                return false
            } finally {
                if (inputStream != null) {
                    inputStream.close()
                }

                if (outputStream != null) {
                    outputStream.close()
                }
            }
        } catch (e: IOException) {
            return false
        }

    }

    private fun view() {
        val pdfFile = File(Environment.getExternalStorageDirectory().toString() + "/schoolManagement/" + file_name)
        //        Uri path = Uri.fromFile(pdfFile);
        val photoURI = FileProvider.getUriForFile(ctx, applicationContext.packageName + ".my.package.name.provider", pdfFile)
        val pdfIntent = Intent(Intent.ACTION_VIEW)
        if (file_name.toString().toLowerCase().contains(".pdf"))
            pdfIntent.setDataAndType(photoURI, "application/pdf")
        else if (file_name.toString().toLowerCase().contains(".doc") || file_name.toString().toLowerCase().contains(".docx"))
            pdfIntent.setDataAndType(photoURI, "application/msword")
        else if (file_name.toString().toLowerCase().contains(".jpg") || file_name.toString().toLowerCase().contains(".jpeg") || file_name.toString().toLowerCase().contains(".png"))
            pdfIntent.setDataAndType(photoURI, "image/jpeg")
        pdfIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        try {
            startActivity(pdfIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(ctx, resources.getString(R.string.no_app), Toast.LENGTH_SHORT).show()
        }
    }
}