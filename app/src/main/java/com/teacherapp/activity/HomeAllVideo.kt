package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.GridView
import com.teacherapp.R
import com.teacherapp.adapter.HomeVideoAdapter
import com.teacherapp.pojo.ImagePojo

class HomeAllVideo : BaseActivity(){

    private lateinit var videoList : GridView
    private lateinit var videosList : ArrayList<ImagePojo>
    private lateinit var homeVideoAdapter : HomeVideoAdapter
    private var ctx : HomeAllVideo = this
    private lateinit var eventName : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_allvideo)
        initialize()
        setListener()
    }

    private fun initialize() {
        eventName = intent.getStringExtra("event_name")
        setHeaderTitle(eventName)
        videosList = intent.getSerializableExtra("video") as ArrayList<ImagePojo>
        videoList = findViewById(R.id.videoList)
        homeVideoAdapter = HomeVideoAdapter(ctx, videosList)
        videoList.adapter = homeVideoAdapter
    }

    private fun setListener() {
        videoList.onItemClickListener = AdapterView.OnItemClickListener { _, _, pos, _ ->
            startActivity(Intent(ctx, FullVideoScreenActivity::class.java).putExtra("Image", videosList[pos].file)
                    .putExtra("event_name",eventName))
        }
    }
}