package com.teacherapp.activity

import android.app.Activity
import android.app.ActivityManager
import android.content.*
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Base64
import android.view.GestureDetector
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.teacherapp.R
import com.teacherapp.fragment.NavDrawerFragment
import com.teacherapp.listener.NetworkStateReceiver
import com.teacherapp.pojo.BasePojo
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.util.*
import io.socket.client.Socket
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.UnsupportedEncodingException
import java.text.DateFormatSymbols
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


/**
 * Created by upasna.mishra on 11/24/2017.
 */
open class BaseActivity : AppCompatActivity(), NetworkStateReceiver.NetworkStateReceiverListener {

    private var sharedPreferences: SharedPreferences? = null
    private lateinit var prefs: TeacherPreference
    private var mDrawerLayout: DrawerLayout? = null
    private lateinit var appbar: Toolbar
    private lateinit var drawerButton: ImageView
    private lateinit var header: TextView
    private lateinit var fragment: NavDrawerFragment
    lateinit var iv_add_icon: ImageView
    lateinit var ivNotification: ImageView
    lateinit var iv_filter: ImageView
    lateinit var mSocket: Socket
    private var ctx: BaseActivity = this
    private var network_available = true
    private var networkStateReceiver: NetworkStateReceiver? = NetworkStateReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        prefs = TeacherPreference(this)
        val app = application as ChatApplication
        mSocket = app.socket!!
    }

    fun displayToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun saveIntoPrefs(key: String, value: String) {
        val prefs = getSharedPreferences(TeacherConstant.PREF_NAME, Context.MODE_PRIVATE)
        val edit = prefs.edit()
        edit.putString(key, value)
        edit.apply()
    }

    interface ClickListener {
        fun onClick(view: View, position: Int)

        fun onLongClick(view: View?, position: Int)
    }

    class RecyclerTouchListener(context: Context, recyclerView: RecyclerView, private val clickListener: ClickListener?) : RecyclerView.OnItemTouchListener {

        private val gestureDetector: GestureDetector

        init {
            gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    return true
                }

                override fun onLongPress(e: MotionEvent) {
                    val child = recyclerView.findChildViewUnder(e.x, e.y)
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child))
                    }
                }
            })
        }

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {

            val child = rv.findChildViewUnder(e.x, e.y)
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child))
            }
            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

        }
    }

    fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground: Boolean = true
        val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses: List<ActivityManager.RunningAppProcessInfo> = am.runningAppProcesses
            for (processInfo: ActivityManager.RunningAppProcessInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess: String in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo: List<ActivityManager.RunningTaskInfo> = am.getRunningTasks(1)
            val componentInfo: ComponentName = taskInfo[0].topActivity
            if (componentInfo.packageName == context.packageName) {
                isInBackground = false
            }
        }
        return isInBackground
    }

    fun encode(message: String): String? {
        var base64: String? = null
        try {
            val data = message.toByteArray(charset("UTF-8"))
            base64 = Base64.encodeToString(data, Base64.DEFAULT)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return base64
    }

    fun decode(message: String): String? {
        var text: String? = null
        val data = Base64.decode(message, Base64.DEFAULT)
        try {
            text = String(data)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return text
    }

    fun getFromPrefs(key: String): String? {
        val prefs = getSharedPreferences(TeacherConstant.PREF_NAME, Context.MODE_PRIVATE)
        return prefs.getString(key, TeacherConstant.DEFAULT_VALUE)
    }

    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun passCombination(pass: String): Boolean {
        val expression = "(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@\$%^&*-]).{6,}\$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(pass)
        return matcher.matches()
    }

    fun hideSoftKeyboard() {
        val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (currentFocus != null)
            inputManager.hideSoftInputFromWindow(currentFocus.windowToken, InputMethodManager.SHOW_FORCED)
    }

    override fun onResume() {
        super.onResume()
        networkStateReceiver?.addListener(this)
        this.registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        if (!getFromPrefs(TeacherConstant.ID).equals("")) {
            if (!mSocket.connected()) {
                mSocket.connect()
                subscribeSocket()
            }
        }
    }

    override fun onPause() {
        try {
            this.unregisterReceiver(networkStateReceiver)
        } catch (e: IllegalArgumentException) {
        }

        super.onPause()
    }

    override fun networkAvailable() {
        if (!network_available) {
            network_available = true
            displayToast("Network available")
            if (!mSocket.connected()) {
                mSocket.connect()
                subscribeSocket()
            }
        }
    }

    override fun networkUnavailable() {
        network_available = false
        displayToast("Network not available")
    }

    fun subscribeSocket() {
        if (!getFromPrefs(TeacherConstant.ID).equals("")) {
            mSocket.emit("subscribe", "T_"+getFromPrefs(TeacherConstant.ID).toString())
        }
    }

    fun addClick(type: Int) {
        when (type) {
            0 -> iv_add_icon.setOnClickListener {
                val intent = Intent(this, AddDiaryUpdateActivity::class.java)
                startActivity(intent)
            }
            1 -> iv_add_icon.setOnClickListener {
                val intent = Intent(this, AddTaskManagementActivity::class.java)
                intent.putExtra("from", "add")
                startActivity(intent)
            }
            2 -> iv_add_icon.setOnClickListener {
                val intent = Intent(this, TeacherLeaveApplyActivity::class.java)
                startActivity(intent)
            }

            6 -> iv_add_icon.setOnClickListener {
                val intent = Intent(this, AddGalleryActivity::class.java)
                intent.putExtra("from", "new")
                startActivity(intent)
            }

        }
    }

    fun addFilter(type: Int) {
        when (type) {
            7 -> iv_filter.setOnClickListener {
                val intent = Intent(this, FilterGalleryActivity::class.java)
                startActivity(intent)
            }
        }
    }

    fun appLogout() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setMessage(resources.getString(R.string.applogout))
        builder.setPositiveButton("Yes", { _, _ ->
            logout()
        })
        builder.setNegativeButton("No", { dialog, _ ->
            dialog.cancel()
        })
        val alert = builder.create()
        alert.show()
    }

    private fun logout() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.logout(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            saveIntoPrefs(TeacherConstant.ID, "")
                            val obj = LoginPojo()
                            saveUserData(obj)
                            val intent = Intent(ctx, LoginActivity::class.java)
                            startActivity(intent)
                            mSocket.disconnect()
                            (0 until TeacherConstant.ACTIVITIES.size)
                                    .filter { TeacherConstant.ACTIVITIES[it] != null }
                                    .forEach { TeacherConstant.ACTIVITIES[it]!!.finish() }
                            finish()
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    fun setDrawerAndToolbar(name: String): Toolbar {
        appbar = findViewById(R.id.toolbar)
        setSupportActionBar(appbar)
        supportActionBar!!.title = ""
        drawerButton = findViewById(R.id.iv_navi)

        header = findViewById(R.id.tv_header)
        iv_filter = findViewById(R.id.iv_filter)

        iv_add_icon = findViewById(R.id.iv_add_icon)
        val ivLogout: ImageView = findViewById(R.id.iv_logout)

        ivNotification = findViewById<ImageView>(R.id.iv_notification)
        ivNotification.setOnClickListener {
            removeActivity(resources.getString(R.string.package_name) + ".NotificationActivity")
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        }

        header.text = name
        mDrawerLayout = findViewById(R.id.drawer_layout)
        drawerButton.setOnClickListener {
            hideSoftKeyboard()
            if (mDrawerLayout != null && !mDrawerLayout!!.isDrawerOpen(Gravity.START))
                mDrawerLayout?.openDrawer(Gravity.START)
            else if (mDrawerLayout != null)
                mDrawerLayout?.closeDrawers()
        }
        if (mDrawerLayout != null) {
            fragment = supportFragmentManager.findFragmentById(R.id.fragment_drawer) as NavDrawerFragment
            fragment.setUp(mDrawerLayout!!)
        }
        ivLogout.setOnClickListener {
            appLogout()
        }
        return appbar
    }

    fun setHeaderTitle(header: String) {
        val tvHeader: TextView = findViewById(R.id.tv_header)
        tvHeader.text = header
        val ivBack: ImageView = findViewById(R.id.iv_back)
        val ivNav: ImageView = findViewById(R.id.iv_navi)
        iv_add_icon = findViewById(R.id.iv_add_icon)
        ivNav.visibility = View.GONE
        ivBack.visibility = View.VISIBLE
        ivNotification = findViewById<ImageView>(R.id.iv_notification)
        val ivLogout = findViewById<ImageView>(R.id.iv_logout)
        ivBack.setOnClickListener {
            finish()
            hideSoftKeyboard()
        }
        ivNotification.setOnClickListener {
            removeActivity(resources.getString(R.string.package_name) + ".NotificationActivity")
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        }

        ivLogout.setOnClickListener {
            appLogout()
        }
    }

    fun saveUserData(filterData: LoginPojo) {
        val sharedPreferences = getSharedPreferences("Teacher", Activity.MODE_PRIVATE)
        val prefsEditor = sharedPreferences.edit()
        val gson = Gson()
        val json = gson.toJson(filterData)
        prefsEditor.putString("user_details", json)
        prefsEditor.apply()
    }

    fun getUserData(): LoginPojo? {
        val sharedPreferences = getSharedPreferences("Teacher", Activity.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString("user_details", "")
        return gson.fromJson<LoginPojo>(json, LoginPojo::class.java)
    }

    fun getTeacherName(): String {
        val loginPojo: LoginPojo = getUserData()!!
        return if (loginPojo.data!!.mname != "")
            loginPojo.data!!.fname + " " + loginPojo.data!!.mname + " " + loginPojo.data!!.lname
        else
            loginPojo.data!!.fname + " " + loginPojo.data!!.lname
    }

    fun getHomeActivity(): HomeActivity? {
        return (TeacherConstant.ACTIVITIES.size - 1 downTo 0)
                .firstOrNull { TeacherConstant.ACTIVITIES[it] != null && TeacherConstant.ACTIVITIES[it].toString().contains(resources.getString(R.string.package_name) + "HomeActivity") }
                ?.let { TeacherConstant.ACTIVITIES[it] as HomeActivity }
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     *
     * @return
     */
    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    /**
     * a method that will centralize the showing of a snackbar
     */
    fun makePostRequestSnack(message: String, size: Int) {
        Toast.makeText(applicationContext, size.toString() + " " + message, Toast.LENGTH_SHORT).show()
        finish()
    }

    fun hasPermission(permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission
     * @return
     */
    private fun shouldWeAsk(permission: String): Boolean {
        return sharedPreferences!!.getBoolean(permission, true)
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission
     */
    fun markAsAsked(permission: String, sharedPreferences: SharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply()
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     *
     * @param permission
     */
    fun clearMarkAsAsked(permission: String) {
        sharedPreferences?.edit()?.putBoolean(permission, true)?.apply()
    }

    fun findUnAskedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()

        for (perm in wanted) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }

        return result
    }

    fun findRejectedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()

        for (perm in wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm)) {
                result.add(perm)
            }
        }

        return result
    }

    fun setProfileImageInLayout(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Picasso.with(ctx).load(url).placeholder(R.mipmap.image_bg).error(R.mipmap.image_bg).into(image)
    }

    fun setHomePostImageInLayout(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Picasso.with(ctx).load(url).placeholder(R.mipmap.image_bg).error(R.mipmap.image_bg).into(image)
    }

    fun setGifImage(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Glide.with(ctx).load(url).asGif().placeholder(R.mipmap.image_bg).into(image)
    }

    fun setHomePostImage(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Picasso.with(ctx).load(url).placeholder(R.mipmap.image_bg).error(R.mipmap.image_bg).into(image)
    }

    fun getDateFormat(): SimpleDateFormat {
        val myFormat = "yyy-MM-dd" //In which you need put here
        return SimpleDateFormat(myFormat, Locale.US)
    }

    fun changeDateFormat(_date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val outputFormat = SimpleDateFormat("dd MMM, yyyy", Locale.US)
        val date = inputFormat.parse(_date)
        return outputFormat.format(date)
    }

    fun changeDateTimeFormat(_date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
        val outputFormat = SimpleDateFormat("dd MMMM, hh:mm a", Locale.US)
        val date = inputFormat.parse(_date)
        return outputFormat.format(date)
    }

    fun changeDateTimeFormatYear(date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
        val outputFormat = SimpleDateFormat("dd MMMM yyyy hh:mm a", Locale.US)
        val dateVal = inputFormat.parse(date)
        return outputFormat.format(dateVal)
    }

    fun changeDateTimeFormatDiary(_date: String): String {
        val inputFormat = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val outputFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.US)
        val date = inputFormat.parse(_date)
        return outputFormat.format(date)
    }

    fun getDate(date: String): Date {
        val dateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.US)
        var convertedDate = Date()
        try {
            convertedDate = dateFormat.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return convertedDate
    }

    fun getCurrentDate(): String {
        val now = Calendar.getInstance()
        val day = now.get(Calendar.MONTH) + 1
        return day.toString()
    }

    fun getChangedDate(date: String): String {
        val result: String
        val sds = date.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val day = sds[0]
        val month = getMonth(sds[1].toInt())

        val year = sds[2]
        result = "$day $month $year"

        return result
    }

    private fun getMonth(month: Int): String {
        return DateFormatSymbols().shortMonths[month - 1]
    }

    fun getClassShortName(): String {
        val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val cn = am.getRunningTasks(1)[0].topActivity
        return cn.shortClassName
    }

    fun removeAllActivities() {
        (0 until TeacherConstant.ACTIVITIES.size)
                .filter { TeacherConstant.ACTIVITIES[it] != null && !TeacherConstant.ACTIVITIES[it].toString().contains(resources.getString(R.string.package_name) + "HomeActivity") }
                .forEach { TeacherConstant.ACTIVITIES[it]?.finish() }
    }

    fun removeActivity(activity: String) {
        for (i in TeacherConstant.ACTIVITIES.size - 1 downTo 0) {
            if (TeacherConstant.ACTIVITIES[i] != null && TeacherConstant.ACTIVITIES[i].toString().contains(activity)) {
                TeacherConstant.ACTIVITIES[i]!!.finish()
                TeacherConstant.ACTIVITIES.removeAt(i)
//                break
            }
        }
    }
}