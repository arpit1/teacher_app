package com.teacherapp.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.DisplayMetrics
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.github.rtoshiro.view.video.FullscreenVideoView
import com.teacherapp.R
import com.teacherapp.adapter.HomeImageAdapter
import com.teacherapp.pojo.ImagePojo
import com.teacherapp.util.TeacherConstant
import com.teacherapp.util.TeacherPreference
import java.io.IOException


class FullScreenImage : BaseActivity() {

    private lateinit var fullimage: RelativeLayout
    private lateinit var datetim: TextView
    private lateinit var stdetail: TextView
    private lateinit var title: TextView
    private lateinit var prefs: TeacherPreference
    private lateinit var media_type: String
    private lateinit var img_share: ImageView
    private lateinit var image_path: String
    private lateinit var discussion: String
    private lateinit var pager: ViewPager
    private lateinit var tvShowImageCount: TextView
    private var ctx: FullScreenImage = this
    private lateinit var imageUrl: ArrayList<ImagePojo>
    private var pos: Int = 0
    private lateinit var videoview: FullscreenVideoView
    private lateinit var header: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_full_screen_image)

        init()
        setListener()
    }

    private fun init() {
        imageUrl = intent.getSerializableExtra("Image") as ArrayList<ImagePojo>
        pos = intent.getIntExtra("pos", 0)
        discussion = intent.getStringExtra("discussion")
        val dates = intent.getStringExtra("dates")
        media_type = intent.getStringExtra("mediatype")
        val from = intent.getStringExtra("from")
        header = intent.getStringExtra("header")
        stdetail = findViewById(R.id.stdetail)
        if (from == "home") {
            setHeaderTitle(header)
            stdetail.visibility = View.GONE
        } else {
            stdetail.visibility = View.VISIBLE
            setHeaderTitle(resources.getString(R.string.galley_detail))
        }
        img_share = findViewById(R.id.img_share)
        pager = findViewById(R.id.pager)
        tvShowImageCount = findViewById(R.id.tvShowImageCount)
        img_share.visibility = View.VISIBLE
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        datetim = findViewById(R.id.datetim)

        prefs = TeacherPreference(this)
        fullimage = findViewById(R.id.fullimage)
        title = findViewById(R.id.description)
        videoview = findViewById(R.id.videoview)

        datetim.text = getChangedDate(dates)
        title.text = discussion
        stdetail.text = prefs.getCategory()
        //title.text = discussion



        TeacherConstant.ACTIVITIES.add(this)
        if (media_type == "Video") {
            videoview.visibility = View.VISIBLE
            fullimage.visibility = View.GONE
            videoview.setActivity(this)
            videoview.isShouldAutoplay = true
            image_path = imageUrl[pos].files
            loadVideo(image_path)
        } else {
            videoview.visibility = View.GONE
            fullimage.visibility = View.VISIBLE
            pager.adapter = HomeImageAdapter(ctx, imageUrl, "full_screen")
            pager.currentItem = pos
            image_path = imageUrl[pos].files
            tvShowImageCount.text = (pos + 1).toString() + " " + ctx.resources.getString(R.string.of) + " " + imageUrl.size.toString()
            pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageSelected(position: Int) {
                    image_path = imageUrl[position].files
                    tvShowImageCount.text = (position + 1).toString() + " " + ctx.resources.getString(R.string.of) + " " + imageUrl.size.toString()
                }

                override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

                override fun onPageScrollStateChanged(arg0: Int) {}
            })
        }
    }

    private fun loadVideo(videoPath: String) {
        val videoUri = Uri.parse(TeacherConstant.IMAGE_URl + "" + videoPath)
        try {
            videoview.setVideoURI(videoUri)

        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private fun setListener() {
        img_share.setOnClickListener {
            shareContent()
        }
    }

    private fun shareContent() {
        val path = TeacherConstant.IMAGE_URl + "" + image_path
        val share = Intent(android.content.Intent.ACTION_SEND)
        share.type = "text/plain"
        share.putExtra(Intent.EXTRA_SUBJECT, header)
        share.putExtra(Intent.EXTRA_TEXT, path)
        startActivity(Intent.createChooser(share, "Share link!"))
    }
}