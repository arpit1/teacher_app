package com.teacherapp.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.WindowManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity() {

    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var tvLogin: TextView
    private lateinit var tvForgotPassword: TextView
    private lateinit var linForgot: LinearLayout
    private lateinit var prefs: TeacherPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(applicationContext, R.color.black_color_transparent)
        }
//        window.setBackgroundDrawable(ContextCompat.getDrawable(this, R.mipmap.splash_bg))
        initialize()
        setListener()
    }

    private fun initialize() {
        prefs = TeacherPreference(this)
        etEmail = findViewById(R.id.et_email)
        etPassword = findViewById(R.id.et_password)
        tvLogin = findViewById(R.id.tv_login)
        tvForgotPassword = findViewById(R.id.tv_forgot_password)
        linForgot = findViewById(R.id.lin_forgot)
        val content = SpannableString(tvForgotPassword.text.toString())
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        tvForgotPassword.text = content
    }

    private fun setListener() {
        tvLogin.setOnClickListener {
            hideSoftKeyboard()
            if (validate()) {
                login(etEmail.text.toString(), etPassword.text.toString())
            }
        }
        linForgot.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }
    }

    private fun validate(): Boolean {
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()
        return when {
            email.isEmpty() -> {
                displayToast(resources.getString(R.string.username_msg))
                false
            }
            password.isEmpty() -> {
                displayToast(resources.getString(R.string.password_msg))
                false
            }
            else -> true
        }
    }

    private fun login(email: String, pass: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.login(email, getFromPrefs(TeacherConstant.DEVICE_ID).toString(), pass, "android")
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1" && response.body()!!.data != null) {
                            val loginData = response.body()!!.data
                            saveIntoPrefs(TeacherConstant.ID, loginData!!.id)
                            saveIntoPrefs(TeacherConstant.SCHOOL_ID, loginData.school_id)
                            saveIntoPrefs(TeacherConstant.IMAGE, loginData.profile_pic!!)
                            saveIntoPrefs(TeacherConstant.FIRST_NAME, loginData.fname)
                            saveIntoPrefs(TeacherConstant.MIDDLE_NAME, loginData.mname)
                            saveIntoPrefs(TeacherConstant.LAST_NAME, loginData.lname)
                            saveIntoPrefs(TeacherConstant.EMAIL, loginData.email)
                            saveIntoPrefs(TeacherConstant.QUALIFICATION, loginData.qualification)
                            saveIntoPrefs(TeacherConstant.FORCE_CHANGE_PASSWORD, loginData.force_change_status)
                            saveIntoPrefs(TeacherConstant.FORCE_UPDATE_PROFILE, loginData.profile_update_status)
                            subscribeSocket()
                            saveUserData(response.body()!!)
                            when {
                                getFromPrefs(TeacherConstant.FORCE_CHANGE_PASSWORD) == "0" -> {
                                    startActivity(Intent(applicationContext, ForceChangePasswordActivity::class.java)
                                            .putExtra("user_id", loginData.id))
                                    finish()
                                }
                                getFromPrefs(TeacherConstant.FORCE_UPDATE_PROFILE) == "0" -> {
                                    startActivity(Intent(applicationContext, EditProfileActivity::class.java)
                                            .putExtra("from", "first_login"))
                                    finish()

                                }
                                else -> {
                                    startActivity(Intent(applicationContext, HomeActivity::class.java))
                                    finish()
                                }
                            }
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}