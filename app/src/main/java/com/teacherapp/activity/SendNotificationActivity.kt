package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.adapter.SendNotificationAdapter
import com.teacherapp.pojo.NotificationDataPojo
import com.teacherapp.pojo.NotificationPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 5/4/2018.
 */

class SendNotificationActivity : BaseActivity(), View.OnClickListener {



    private lateinit var rvNotifictionList: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var notificationList: ArrayList<NotificationDataPojo>
    private lateinit var adapter: SendNotificationAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private var ctx: SendNotificationActivity = this
    lateinit var home_ctx: HomeActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        setHeaderTitle(resources.getString(R.string.send_notifications))
        TeacherConstant.ACTIVITIES.add(this)
        initialize()
        setListener()
    }

    private fun initialize() {
        home_ctx = HomeActivity()

        mLayoutManager = LinearLayoutManager(this)
        val ivNotification = findViewById<ImageView>(R.id.iv_notification)
        rvNotifictionList = findViewById(R.id.rvNotifictionList)
        tv_no_data = findViewById(R.id.tv_no_data)
        ivNotification.visibility = View.GONE
        val ivNavigation: ImageView = findViewById(R.id.iv_navi)
        val ivBack: ImageView = findViewById(R.id.iv_back)
        ivNavigation.visibility = View.GONE
        ivBack.visibility = View.VISIBLE
        iv_add_icon.visibility = View.VISIBLE
        rvNotifictionList.layoutManager = mLayoutManager
        getNotification()
    }

    override fun onResume() {
        super.onResume()
        notificationList = ArrayList()
        if(TeacherConstant.TASK_REFRESH){
            getNotification()
        }
    }

    private fun setListener() {
        iv_add_icon.setOnClickListener {
            startActivity(Intent(ctx, AddNotificationActivity::class.java))
        }
    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.row_click -> redirectDetails(view)
            }
        }
    }

    private fun redirectDetails(view: View) {
        val notification_data : NotificationDataPojo = view.getTag(R.string.send_notification_data) as NotificationDataPojo
        if(notification_data.type == "0"){
            startActivity(Intent(ctx, SendNotificationDetailActivity::class.java).putExtra("notification_data", notification_data))
        }
    }


    private fun getNotification() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<NotificationPojo> = apiService.listNotice(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<NotificationPojo> {
                override fun onResponse(call: Call<NotificationPojo>, response: Response<NotificationPojo>?) {
                    if (response != null) {
                        if (response.body()!!.data.size > 0) {
                            TeacherConstant.TASK_REFRESH = false
                            rvNotifictionList.visibility = View.VISIBLE
                            tv_no_data.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                notificationList.addAll(response.body()!!.data)
                                adapter = SendNotificationAdapter(ctx, notificationList)
                                rvNotifictionList.adapter = adapter
                            } else {
                                displayToast(response.body()!!.message)
                            }
                        } else {
                            rvNotifictionList.visibility = View.GONE
                            tv_no_data.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<NotificationPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}