package com.teacherapp.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.teacherapp.R
import com.teacherapp.pojo.*
import com.teacherapp.util.*
import com.teacherapp.util.TeacherConstant.ACTIVITIES
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StudentProgressActivity : BaseActivity() {

    private var ctx: StudentProgressActivity = this
    private lateinit var row_student_progress: LinearLayout
    private lateinit var options: ArrayList<String>
    private var sp_selectStudent: Spinner? = null
    private var sp_selectTemplate: Spinner? = null
    private lateinit var tv_submit: TextView
    private lateinit var prefs: TeacherPreference
    private lateinit var selectedAanswerArray: JSONArray
    private lateinit var selectAanswerObject: JSONObject
    private lateinit var student_list: ArrayList<StudentPojoData>
    private lateinit var templat_list: ArrayList<TemplateDataPojo>
    private lateinit var mInflator: LayoutInflater
    private var answer_valid: Boolean = false
    private var select_student_id: String = ""
    private var select_template_id: String = ""
    private lateinit var arrQuestionAnswer: ArrayList<QuestionFieldPojo>
    private var program: String = ""
    private var classes: String = ""
    private var batch: String = ""
    private lateinit var remark: EditText
    private lateinit var ll_rmark: LinearLayout
    private lateinit var loginPojo: LoginPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_student_progress)
        initialize()
        setListener()

        ACTIVITIES.add(ctx)
        setHeaderTitle(resources.getString(R.string.student_progress))
    }

    private fun initialize() {
        student_list = ArrayList()
        templat_list = ArrayList()
        loginPojo = getUserData()!!
        options = ArrayList()
        selectedAanswerArray = JSONArray()
        arrQuestionAnswer = ArrayList()
        prefs = TeacherPreference(ctx)
        sp_selectStudent = findViewById(R.id.sp_selectStudent)
        sp_selectTemplate = findViewById(R.id.sp_selectTemplate)
        remark = findViewById(R.id.remark)
        remark.setOnTouchListener { v, motionEvent ->
            if (v.id == R.id.remark) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        }
        tv_submit = findViewById(R.id.tv_submit)
        ll_rmark = findViewById(R.id.ll_rmark)

        mInflator = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

        row_student_progress = findViewById(R.id.row_student_progress)

        classes = intent.getStringExtra("classes")
        batch = intent.getStringExtra("batch")
        program = intent.getStringExtra("program")
        studentList()
        getTemplate()
    }

    private fun studentList() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<StudentPojo> = apiService.studentList(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), program, batch, classes)
            call.enqueue(object : Callback<StudentPojo> {
                override fun onResponse(call: Call<StudentPojo>, response: Response<StudentPojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            student_list.clear()
                            val studentDataPojo = StudentPojoData()
                            studentDataPojo.id = ("-1")
                            studentDataPojo.name = resources.getString(R.string.select_student)
                            student_list = response.body()!!.data
                            student_list.add(0, studentDataPojo)
                            sp_selectStudent?.adapter = typeSpinnerAdapter
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<StudentPojo>, t: Throwable) {
                    // Log error here since request failed
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun getTemplate() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TemplatePojo> = apiService.getTemplate(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), program)
            call.enqueue(object : Callback<TemplatePojo> {
                override fun onResponse(call: Call<TemplatePojo>, response: Response<TemplatePojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            templat_list.clear()
                            val templateDataPojo = TemplateDataPojo()
                            templateDataPojo.id = ("-1")
                            templateDataPojo.name = resources.getString(R.string.select_template)
                            templat_list = response.body()!!.data
                            templat_list.add(0, templateDataPojo)
                            sp_selectTemplate?.adapter = templateSpinnerAdapter
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<TemplatePojo>, t: Throwable) {
                    // Log error here since request failed
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setListener() {
        tv_submit.setOnClickListener {
            clickSubmit()
        }
        sp_selectStudent?.onItemSelectedListener = typeSelectedListener
        sp_selectTemplate?.onItemSelectedListener = templateSelectedListener
    }

    private val typeSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            var selectedName = resources.getString(R.string.select_student)
            var selectedTemplate = resources.getString(R.string.select_template)
            if(sp_selectTemplate != null && sp_selectStudent != null) {
                selectedTemplate = sp_selectTemplate?.selectedItem.toString()
                selectedName = sp_selectStudent?.selectedItem.toString()
            }
//            for (i in 0 until student_list.size) {
//                if (selectName == student_list[i].name) {
            select_student_id = student_list[position].id
//                    break
//                }
//            }
            if (selectedName != resources.getString(R.string.select_student) && selectedTemplate != resources.getString(R.string.select_template)) {
                ll_rmark.visibility = View.VISIBLE
                row_student_progress.visibility = View.VISIBLE
                studentProgress(select_student_id, select_template_id)
            } else {
                ll_rmark.visibility = View.GONE
                row_student_progress.visibility = View.GONE
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val templateSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            var selectedName = resources.getString(R.string.select_student)
            var selectedTemplate = resources.getString(R.string.select_template)
            if(sp_selectTemplate != null && sp_selectStudent != null) {
                selectedTemplate = sp_selectTemplate?.selectedItem.toString()
                selectedName = sp_selectStudent?.selectedItem.toString()
            }
//            for (i in 0 until templat_list.size) {
//                if (selectDate == templat_list[i].name) {
            select_template_id = templat_list[position].id
//                    break
//                }
//            }
            if (selectedName != resources.getString(R.string.select_student) && selectedTemplate != resources.getString(R.string.select_template)) {
                ll_rmark.visibility = View.VISIBLE
                row_student_progress.visibility = View.VISIBLE
                studentProgress(select_student_id, select_template_id)
            } else {
                ll_rmark.visibility = View.GONE
                row_student_progress.visibility = View.GONE
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val typeSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = student_list[position].name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return student_list[position].name
        }

        override fun getCount(): Int {
            return student_list.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = student_list[position].name
            return convertView as View
        }
    }

    private val templateSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = templat_list[position].name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return templat_list[position].name
        }

        override fun getCount(): Int {
            return templat_list.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = templat_list[position].name
            return convertView as View
        }
    }

    private fun clickSubmit() {
        if (validate()) {
            for (i in 0 until arrQuestionAnswer.size) {
                val arr: ArrayList<QuestionDataFieldPojo> = arrQuestionAnswer[i].questions
                for (j in 0 until arr.size) {
                    selectAanswerObject = JSONObject()
                    selectAanswerObject.put("answer", arr[j].answer)
                    selectAanswerObject.put("question_id", arr[j].id)
                    selectedAanswerArray.put(selectAanswerObject)
                }
            }
            saveAnswerList(select_student_id, select_template_id, selectedAanswerArray)
            val printObject = JSONObject()
            printObject.put("arr", selectedAanswerArray)
            println("Json Array " + printObject.toString())
        }
    }

    private fun validate(): Boolean {
        val selectDate = sp_selectTemplate?.selectedItem.toString()
        val selectStudent = sp_selectStudent?.selectedItem.toString()
        return when {
            selectStudent == ctx.resources.getString(R.string.select_student) -> {
                ctx.displayToast(ctx.resources.getString(R.string.select_student_name))
                false
            }
            selectDate == ctx.resources.getString(R.string.select_template) -> {
                ctx.displayToast(ctx.resources.getString(R.string.select_period))
                false
            }
            else -> {
                answer_valid = validateAnswers()
                if (!answer_valid)
                    ctx.displayToast(ctx.resources.getString(R.string.answer_validation_msg))
                answer_valid
            }
        }
    }

    private fun validateAnswers(): Boolean {
        var checkAnswer = true
        for (i in 0 until arrQuestionAnswer.size) {
            val arr: ArrayList<QuestionDataFieldPojo> = arrQuestionAnswer[i].questions
            for (j in 0 until arr.size) {
                if (arr[j].answer == "") {
                    checkAnswer = false
                    break
                }
            }
            if (!checkAnswer)
                break
        }
        return checkAnswer
    }

    private fun saveAnswerList(student_id: String, template_id: String, answerList: JSONArray) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.addAnswerList(student_id, template_id,
                    remark.text.toString().trim(), answerList, "0",
                    loginPojo.data!!.fname + " " + loginPojo.data!!.lname, getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            finish()
                        }
                        ctx.displayToast(response.body()!!.message)
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun studentProgress(student_id: String, tmplate_id: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<QuestionPojo> = apiService.getQuestionAnswer(student_id, tmplate_id)
            call.enqueue(object : Callback<QuestionPojo> {
                override fun onResponse(call: Call<QuestionPojo>, response: Response<QuestionPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            arrQuestionAnswer.clear()
                            arrQuestionAnswer = response.body()!!.data
                            remark.setText(response.body()!!.data[0].questions[0].remarks)
                            options.clear()
                            options = response.body()!!.criteria
                            options.add(0, resources.getString(R.string.select_answer))
                            setQuestionData(response)
                        } else {
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<QuestionPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setQuestionData(response: Response<QuestionPojo>) {
        row_student_progress.removeAllViews()
        for (i in 0 until response.body()!!.data.size) {
            val itemView = LayoutInflater.from(ctx).inflate(R.layout.row_layout_student_progress, null, false)
            val lin_child_title = itemView.findViewById(R.id.lin_child_title) as LinearLayout
            val tv_header_name = itemView.findViewById(R.id.tv_header_name) as TextView
            val lin_header_title = itemView.findViewById(R.id.lin_header_title) as LinearLayout
            val header_plus_icon = itemView.findViewById(R.id.header_plus_icon) as ImageView
            tv_header_name.text = response.body()!!.data[i].subject
            lin_header_title.setOnClickListener {
                clickPlusIcon(lin_child_title, header_plus_icon)
            }
            val arr: ArrayList<QuestionDataFieldPojo> = response.body()!!.data[i].questions
            for (j in 0 until arr.size) {
                val childView = LayoutInflater.from(ctx).inflate(R.layout.activity_child_student_progress, null, false)
                val tv_child_name = childView.findViewById(R.id.tv_child_name) as TextView
                val spinnerOptions = childView.findViewById(R.id.spinner_options) as Spinner
                tv_child_name.text = arr[j].question
                val selectOptions = ArrayAdapter(ctx, R.layout.row_child_question_spinner, options)
                selectOptions.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerOptions.adapter = selectOptions
                if (arr[j].answer == "") {
                    spinnerOptions.setSelection(0)
                } else {
                    for (m in 0 until options.size) {
                        if (options[m] == arr[j].answer) {
                            spinnerOptions.setSelection(m)
                            break
                        }
                    }
                }
                spinnerOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                        if (pos == 0)
                            arr[j].answer = ""
                        else
                            arr[j].answer = spinnerOptions.selectedItem.toString()
                    }

                    override fun onNothingSelected(parent: AdapterView<out Adapter>?) {
                    }

                }
                lin_child_title.addView(childView)
            }
            row_student_progress.addView(itemView)
        }
    }

    private fun clickPlusIcon(lin_child_title: LinearLayout, header_plus_icon: ImageView) {
        if (lin_child_title.visibility == View.GONE) {
            ViewAnimationUtils.expand(lin_child_title)
            header_plus_icon.setImageResource(R.mipmap.minus_icon_s)
        } else {
            ViewAnimationUtils.collapse(lin_child_title)
            header_plus_icon.setImageResource(R.mipmap.plus_icon_s)
        }
    }
}