package com.teacherapp.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.*
import com.teacherapp.R
import com.teacherapp.pojo.*
import com.teacherapp.util.*
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.mime.MultipartEntity
import org.apache.http.entity.mime.content.ByteArrayBody
import org.apache.http.entity.mime.content.StringBody
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.protocol.BasicHttpContext
import org.apache.http.util.EntityUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AddDiaryUpdateActivity : BaseActivity(), View.OnClickListener {
    private lateinit var programSpinner: Spinner
    private lateinit var batchSpinner: Spinner
    private lateinit var classSpinner: Spinner
    private lateinit var studentSpinner: EditText
    private lateinit var rv_date: RelativeLayout
    private lateinit var tv_date: TextView
    private lateinit var tv_upload_file: TextView
    private lateinit var tv_submit: TextView
    private lateinit var et_description: EditText
    private lateinit var ed_title: EditText
    private lateinit var sdsd: ImageView
    private var ctx: AddDiaryUpdateActivity = this
    private lateinit var iv_notification: ImageView
    private lateinit var iv_edit_profile: ImageView
    private lateinit var adapter: ImageAdapter
    private lateinit var mInflator: LayoutInflater
    lateinit var selected_image: RecyclerView
    private lateinit var imageURI: String
    private lateinit var imagesUriList: ArrayList<Uri>
    private lateinit var encodedImageList: ArrayList<String>
    private var calendar: Calendar? = null
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private lateinit var prefs: TeacherPreference
    internal lateinit var programList: ArrayList<ProgramDataPojo>
    internal lateinit var classList: ArrayList<ClassDataPojo>
    internal lateinit var batchList: ArrayList<BatchDataPojo>
    internal lateinit var studentList: ArrayList<StudentPojoData>
    lateinit var innerstudentList: ArrayList<StudentPojoData>
    private lateinit var files: ArrayList<String?>
    private lateinit var map: ArrayList<Bitmap>
    private lateinit var user_id: String
    private val REQCODE = 100
    private lateinit var added_id: String
    private lateinit var title: String
    private var count: Int = 0
    private var selectedStudentcount: Int = 0
    internal var entity: MultipartEntity? = null
    private lateinit var d: ProgressDialog

    private var programId: String = ""
    private var batchId: String = ""
    private var classId: String = ""
    private var selectedStudentId: StringBuilder = StringBuilder()

    private var loginData: LoginPojo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_diary_update)

        setHeaderTitle(resources.getString(R.string.add_diary))
        TeacherConstant.ACTIVITIES.add(this)
        initialize()
        setListener()
    }

    private fun initialize() {
        loginData = getUserData()
        mInflator = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        programSpinner = findViewById(R.id.program_spinner)
        batchSpinner = findViewById(R.id.batch_spinner)
        classSpinner = findViewById(R.id.class_spinner)
        studentSpinner = findViewById(R.id.student_spinner)
        rv_date = findViewById(R.id.rv_date)
        tv_date = findViewById(R.id.tv_date)
        tv_upload_file = findViewById(R.id.tv_upload_file)
        tv_submit = findViewById(R.id.tv_submit)
        et_description = findViewById(R.id.et_description)
        et_description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.et_description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        ed_title = findViewById(R.id.et_title)
        iv_add_icon = findViewById(R.id.iv_add_icon)
        sdsd = findViewById(R.id.sdsd)
        iv_notification = findViewById(R.id.iv_notification)
        iv_edit_profile = findViewById(R.id.iv_edit_profile)
        selected_image = findViewById(R.id.diary_images)

        map = ArrayList()
        encodedImageList = ArrayList()
        programList = ArrayList()
        classList = ArrayList()
        batchList = ArrayList()
        studentList = ArrayList()
        innerstudentList = ArrayList()
        files = ArrayList()
        prefs = TeacherPreference(this)
        user_id = getFromPrefs(TeacherConstant.ID).toString()
        iv_add_icon.visibility = View.GONE
        iv_edit_profile.visibility = View.GONE

        calendar = Calendar.getInstance()
        val c = Calendar.getInstance()
        year = c.get(Calendar.YEAR)
        month = c.get(Calendar.MONTH)
        day = c.get(Calendar.DAY_OF_MONTH)
        getProgram()
    }

    private fun setListener() {
        rv_date.setOnClickListener {
            hideSoftKeyboard()
            showDatePicker()
        }
        programSpinner.onItemSelectedListener = programSelectedListener
        batchSpinner.onItemSelectedListener = batchSelectedListener
        classSpinner.onItemSelectedListener = classSelectedListener

        tv_upload_file.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Choose application"), REQCODE)
        }

        tv_submit.setOnClickListener {
            hideSoftKeyboard()
            validate()
        }

        studentSpinner.setOnClickListener {
            if (studentList.size > 0) {
                showStudentNameDialog()
            } else {
                displayToast(resources.getString(R.string.studentnot_found))
            }
        }

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ic_delete -> {
                val pos = v.getTag(R.string.pos) as Int
                map.removeAt(pos)
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun getProgram() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ProgramPojo> = apiService.getProgram(user_id)
            call.enqueue(object : Callback<ProgramPojo> {
                override fun onResponse(call: Call<ProgramPojo>, response: Response<ProgramPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                programList.addAll(response.body()!!.data)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ProgramPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val programSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setBatchHintSpinner()
            if (position != 0) {
                programId = programList[position].id
                getBatch()
            } else {
                programId = ""
                selectedStudentcount = 0
                studentSpinner.setText("")
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val programSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget)
            ftext!!.text = programList[position].programs
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return programList[position]
        }

        override fun getCount(): Int {
            return programList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget)
            ftext!!.text = programList[position].programs
            return convertView as View
        }
    }

    private fun setHintSpinner() {
        programList.clear()
        val programDataPojo = ProgramDataPojo()
        programDataPojo.id = "-1"
        programDataPojo.programs = resources.getString(R.string.select_program)
        programList.add(0, programDataPojo)
        programSpinner.adapter = programSpinnerAdapter
        programSpinnerAdapter.notifyDataSetChanged()
        setBatchHintSpinner()
    }

    private fun setBatchHintSpinner() {
        batchList.clear()
        val batchDataPojo = BatchDataPojo()
        batchDataPojo.batch_name = resources.getString(R.string.select_section)
        batchList.add(0, batchDataPojo)
        batchSpinner.adapter = batchSpinnerAdapter
        batchSpinnerAdapter.notifyDataSetChanged()
        setClassHintSpinner()
    }

    private fun setClassHintSpinner() {
        classList.clear()
        val classData = ClassDataPojo()
        classData.classes = resources.getString(R.string.select_class)
        classList.add(0, classData)
        classSpinner.adapter = classSpinnerAdapter
        classSpinnerAdapter.notifyDataSetChanged()
    }

    private fun showStudentNameDialog() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.studentlist_dialog)
        dialog.setTitle(resources.getString(R.string.select_student))
        val window = dialog.window
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)

        val rvstudent_list = dialog.findViewById<RecyclerView>(R.id.rvstudent_list)
        val tv_done = dialog.findViewById<TextView>(R.id.tv_done)

        val studentAdapter = StudentAdapter(studentList, ctx)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        rvstudent_list.layoutManager = mLayoutManager
        rvstudent_list.itemAnimator = DefaultItemAnimator()
        rvstudent_list.adapter = studentAdapter

        tv_done.setOnClickListener {
            val selectedStudentName = StringBuilder()
            selectedStudentcount = 0
            for (i in 0 until studentList.size) {
                if (studentList[i].is_checked) {
                    studentList[i].is_checked_done = true
                    if (selectedStudentcount == 0) {
                        selectedStudentName.append(studentList[i].name)
                    }
                    selectedStudentcount++
                } else {
                    studentList[i].is_checked_done = false
                }
            }

            if (selectedStudentcount > 0) {
                if (selectedStudentcount > 1) {
                    selectedStudentName.append(" and " + (selectedStudentcount - 1) + " other")
                }

                studentSpinner.setText(selectedStudentName)
            } else {
                studentSpinner.setText("")
//                studentSpinner.hint = resources.getString(R.string.select_student)
            }

            dialog.dismiss()
        }
        dialog.show()
    }

    private fun getBatch() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BatchPojo> = apiService.getBatch(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId)
            call.enqueue(object : Callback<BatchPojo> {
                override fun onResponse(call: Call<BatchPojo>, response: Response<BatchPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
//                            setBatchHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                batchList.addAll(response.body()!!.data)
                                batchSpinnerAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<BatchPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val batchSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById<TextView>(R.id.spinnerTarget)
            ftext!!.text = batchList[position].batch_name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return batchList[position].batch_name
        }

        override fun getCount(): Int {
            return batchList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById<TextView>(R.id.spinnerTarget)
            ftext!!.text = batchList[position].batch_name
            return convertView as View
        }
    }

    private val batchSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setClassHintSpinner()
            if (position != 0) {
                batchId = batchList[position].id
                getClass()
            } else {
                batchId = ""
                selectedStudentcount = 0
                studentSpinner.setText("")
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun getClass() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ClassPojo> = apiService.getClass(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<ClassPojo> {
                override fun onResponse(call: Call<ClassPojo>, response: Response<ClassPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setClassHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                classList.addAll(response.body()!!.data)
                                classSpinnerAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ClassPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val classSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            if (position != 0) {
                classId = classList[position].id
                getStudent()
            } else {
                classId = ""
                selectedStudentcount = 0
                studentSpinner.setText("")
                studentList.clear()
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val classSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById<TextView>(R.id.spinnerTarget)
            ftext!!.text = classList[position].classes
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return classList[position].classes
        }

        override fun getCount(): Int {
            return classList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget)
            ftext!!.text = classList[position].classes
            return convertView as View
        }
    }

    //student spinner functionality...
    private fun getStudent() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<StudentPojo> = apiService.studentList(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, classId)
            call.enqueue(object : Callback<StudentPojo> {
                override fun onFailure(call: Call<StudentPojo>?, t: Throwable?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onResponse(call: Call<StudentPojo>, response: Response<StudentPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
//                            setBatchHintSpinner()
                            studentList.clear()
                            if (response.body()!!.data.size > 0) {
                                studentList.addAll(response.body()!!.data)
                            }
                        }
                    }
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private class StudentAdapter(val istudentList: ArrayList<StudentPojoData>, context: AddDiaryUpdateActivity) : RecyclerView.Adapter<StudentAdapter.MyViewHolder>() {

        override fun getItemCount(): Int {
            return istudentList.size
        }

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var checkspinnerTarget: TextView = view.findViewById(R.id.checkspinnerTarget)
            var rv_mainlay: LinearLayout = view.findViewById(R.id.rv_mainlay)
            var unchecked_img: ImageView = view.findViewById(R.id.unchecked_img)
            var checked_img: ImageView = view.findViewById(R.id.checked_img)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.checkbox_row_spinner, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val studentPojo = istudentList[position]
            holder.checkspinnerTarget.text = studentPojo.name

            if (studentPojo.is_checked_done) {
                holder.unchecked_img.visibility = View.GONE
                holder.checked_img.visibility = View.VISIBLE
            } else {
                holder.unchecked_img.visibility = View.VISIBLE
                holder.checked_img.visibility = View.GONE
            }

            holder.rv_mainlay.setOnClickListener {
                if (holder.checked_img.visibility == View.VISIBLE) {
                    istudentList[position].is_checked = false
                    holder.unchecked_img.visibility = View.VISIBLE
                    holder.checked_img.visibility = View.GONE
                } else {
                    istudentList[position].is_checked = true
                    holder.unchecked_img.visibility = View.GONE
                    holder.checked_img.visibility = View.VISIBLE
                }
            }
        }
    }

    private inner class ImageAdapter(private val mContext: Context) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.imagelistrow, parent, false)
            return ViewHolder(v)
        }

        override fun getItemCount(): Int {
            return map.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindItems(map, ctx, position)
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(leave_list: ArrayList<Bitmap>, ctx: AddDiaryUpdateActivity, position: Int) {
            val iv_profile_pic: ImageView = itemView.findViewById(R.id.images)
            val delete_image: ImageView = itemView.findViewById(R.id.ic_delete)

            iv_profile_pic.setImageBitmap(leave_list[position])
            delete_image.setTag(R.string.pos, position)
            delete_image.setOnClickListener(ctx)
        }
    }

    private fun showDatePicker() {
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar?.set(year, month, day)
            this.year = year
            this.month = month
            this.day = day
            updateLabel()
        }, year, month, day)
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000
        dialog.show()
    }

    private fun updateLabel() {
        val myFormat = getString(R.string.dateformat)//In which you need put here

        val sdf = SimpleDateFormat(myFormat, Locale.US)
        //        visitDate = sdf.format(calendar.getTime());
        tv_date.text = sdf.format(calendar!!.time)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == REQCODE && resultCode == Activity.RESULT_OK && null != data) {
                // Get the Image from data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                imagesUriList = ArrayList()
                encodedImageList.clear()
                if (data.data != null) {
                    val mImageUri = data.data
                    // Get the cursor
                    val cursor = contentResolver.query(mImageUri!!, filePathColumn, null, null, null)
                    // Move to first row
                    cursor!!.moveToFirst()

                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    if (columnIndex != 0)
                        imageURI = cursor.getString(columnIndex)

                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, mImageUri)
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
                    val encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
                    encodedImageList.add(encodedImage)
                    map.add(bitmap)

                    cursor.close()

                    selected_image.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
                    adapter = ImageAdapter(this)
                    selected_image.adapter = adapter

                } else {
                    if (data.clipData != null) {
                        val mClipData = data.clipData
                        val mArrayUri = ArrayList<Uri>()
                        for (i in 0 until mClipData!!.itemCount) {

                            val item = mClipData.getItemAt(i)
                            val uri = item.uri
                            mArrayUri.add(uri)
                            // Get the cursor
                            val cursor = contentResolver.query(uri, filePathColumn, null, null, null)
                            // Move to first row
                            cursor!!.moveToFirst()

                            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                            imageURI = cursor.getString(columnIndex)
                            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                            val byteArrayOutputStream = ByteArrayOutputStream()
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
                            val encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
                            encodedImageList.add(encodedImage)
                            map.add(bitmap)
                            cursor.close()
                        }
                        selected_image.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
                        adapter = ImageAdapter(this)
                        selected_image.adapter = adapter
                    }
                }
            } else {
                Toast.makeText(this, getString(R.string.image_not_available), Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
        }
    }

    private fun validate() {
        val date = tv_date.text.toString()
        title = ed_title.text.toString().trim()
        val description = et_description.text.toString()
        selectedStudentId.setLength(0)
        for (i in 0 until studentList.size) {
            if (studentList[i].is_checked) {
                selectedStudentId.append(studentList[i].id)
                selectedStudentId.append(", ")
            }
        }
        if (selectedStudentId.isNotEmpty())
            selectedStudentId.delete(selectedStudentId.length - 2, selectedStudentId.length)
        when {
            title.isEmpty() -> displayToast(getString(R.string.enter_title))
            title.startsWith(" ") -> displayToast(getString(R.string.white_space_not_allowed_msg))
            date.isEmpty() -> displayToast(resources.getString(R.string.select_date))
            programId.isEmpty() -> displayToast(getString(R.string.select_program_msg))
            batchId.isEmpty() -> displayToast(getString(R.string.select_section_msg))
            classId.isEmpty() -> displayToast(getString(R.string.select_class_msg))
            description.isEmpty() -> displayToast(resources.getString(R.string.description_msg))
            description.startsWith(" ") -> displayToast(resources.getString(R.string.white_space_not_allowed_msg))
            else -> submit()
        }
    }

    private fun submit() {
        if (ConnectionDetector.isConnected(this)) {
            d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val description = et_description.text.trim().toString().trim()
            val date = tv_date.text.toString()
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<section_pojo> = apiService.addSchoolDiaryWithoutFiles(user_id, getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), batchId,
                    classId, description, date, title, programId, loginData!!.data!!.fname + " " + loginData!!.data!!.lname, selectedStudentId.toString())
            call.enqueue(object : Callback<section_pojo> {
                override fun onResponse(call: Call<section_pojo>, response: Response<section_pojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            added_id = response.body()!!.data
                            if (map.size > 0) {
                                ImageUploadTask().execute(count.toString() + "", "pk$count.jpg")
                            } else {
                                d.dismiss()
                                Toast.makeText(applicationContext, getString(R.string.school_diary_updated), Toast.LENGTH_SHORT).show()
                                finish()
                            }
                            TeacherConstant.DIARY_REFRESH_REQUIRED = true
                        } else {
                        }
                    }
                }

                override fun onFailure(call: Call<section_pojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    internal inner class ImageUploadTask : AsyncTask<String, Void, String>() {

        private var sResponse: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            d.setCanceledOnTouchOutside(false)
        }

        override fun doInBackground(vararg params: String): String? {
            try {
                val url = TeacherConstant.BASE_URL + "diaryFiles"
                val i = Integer.parseInt(params[0])
                val bitmap = map[i]
                val httpClient = HttpClientBuilder.create().build()
                val localContext = BasicHttpContext()
                val httpPost = HttpPost(url)
                entity = MultipartEntity()

                val bos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
                val data = bos.toByteArray()
                entity!!.addPart("id", StringBody(added_id))
                entity!!.addPart("image", ByteArrayBody(data, "image/jpeg", params[1]))

                httpPost.entity = entity
                val response = httpClient.execute(httpPost, localContext)
                sResponse = EntityUtils.getContentCharSet(response.entity)
            } catch (e: Exception) {

            }
            return sResponse
        }

        override fun onPostExecute(sResponse: String?) {
            try {
                if (sResponse != null) {
                    count++
                    if (count < map.size) {
                        ImageUploadTask().execute(count.toString() + "", "hm$count.jpg")
                    } else {
                        d.dismiss()
                        Toast.makeText(applicationContext, getString(R.string.school_diary_updated), Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            } catch (e: Exception) {
                Toast.makeText(applicationContext, e.message,
                        Toast.LENGTH_LONG).show()
                Log.e(e.javaClass.name, e.message, e)
            }
        }
    }
}