package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.*
import com.schoolmanagement.parent.pojo.EventDetailPojo
import com.teacherapp.R
import com.teacherapp.pojo.AddEventCommentPojo
import com.teacherapp.pojo.BasePojo
import com.teacherapp.pojo.EventCommentDataPojo

import com.teacherapp.util.*
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EventDetailActivity : BaseActivity() {

    private lateinit var postImage: ImageView
    private lateinit var shareIcon: ImageView
    private lateinit var imageLayout: RelativeLayout
    private lateinit var showComment: LinearLayout
    private lateinit var etComment: EditText
    private lateinit var eventDateTime: TextView
    private lateinit var eventEndDateTime: TextView
    private lateinit var eventName: TextView
    private lateinit var eventLocation: TextView
    private lateinit var eventDescription: TextView
    private var ctx: EventDetailActivity = this
    private var dialog: TeacherDialog? = null
    private lateinit var sendComment: ImageView
    private lateinit var seeAllComment: TextView
    private lateinit var childScroll: ScrollView
    private lateinit var parentScroll: ScrollView
    private lateinit var commentList: ArrayList<EventCommentDataPojo>
    private lateinit var addLike: ImageView
    private var shareImageLink: String = ""
    private var likeStatus: Boolean = false
    private var sendData : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)

        TeacherConstant.ACTIVITIES.add(ctx)
        initialize()
        setListener()
    }

    private fun initialize() {
        setHeaderTitle(resources.getString(R.string.event_detail))
        commentList = ArrayList()
        dialog = TeacherDialog(ctx)
        postImage = findViewById(R.id.postImage)
        shareIcon = findViewById(R.id.shareIcon)
        showComment = findViewById(R.id.showComment)
        imageLayout = findViewById(R.id.imageLayout)
        etComment = findViewById(R.id.etComment)
        eventDateTime = findViewById(R.id.eventDateTime)
        eventEndDateTime = findViewById(R.id.eventEndDateTime)
        eventName = findViewById(R.id.eventName)
        eventLocation = findViewById(R.id.eventLocation)
        eventDescription = findViewById(R.id.eventDescription)
        etComment.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.etComment) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        sendComment = findViewById(R.id.sendComment)
        seeAllComment = findViewById(R.id.seeAllComment)
        parentScroll = findViewById(R.id.parentScroll)
        childScroll = findViewById(R.id.childScroll)
        addLike = findViewById(R.id.addLike)
        eventDetail(intent.getStringExtra("event_id"))
    }

    private fun setListener() {
        sendComment.setOnClickListener {
            val comment = etComment.text.toString().trim()
            if (comment.isEmpty()) {
                displayToast(resources.getString(R.string.comment_msg))
            } else {
                addComment(intent.getStringExtra("event_id"))
            }
        }

        seeAllComment.setOnClickListener {
            if (commentList.size > 0) {
                if (seeAllComment.text.toString() == resources.getString(R.string.see_all_comment)) {
                    seeAllComment.text = resources.getString(R.string.see_less_comment)
                    setAllComment(commentList, commentList.size)
                } else {
                    seeAllComment.text = resources.getString(R.string.see_all_comment)
                    setAllComment(commentList, 2)
                }
            }
        }

        parentScroll.setOnTouchListener { _, _ ->
            // Perform tasks here
            findViewById<ScrollView>(R.id.childScroll).parent.requestDisallowInterceptTouchEvent(false)
            false
        }

        childScroll.setOnTouchListener { v, _ ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            false
        }

        addLike.setOnClickListener {
            addLike(intent.getStringExtra("event_id"))
        }

        shareIcon.setOnClickListener {
            if (shareImageLink != "")
                sendData = eventDescription.text.toString()+"\n \n"+ shareImageLink
            else
                sendData = eventDescription.text.toString()
            val share = Intent(android.content.Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_SUBJECT, eventName.text.toString())
            share.putExtra(Intent.EXTRA_TEXT, sendData)
            startActivity(Intent.createChooser(share, "Share link!"))
        }
    }

    private fun eventDetail(id: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<EventDetailPojo> = apiService.getEventDetail(id, getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<EventDetailPojo> {
                override fun onResponse(call: Call<EventDetailPojo>?, response: Response<EventDetailPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setEventData(response)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<EventDetailPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun addLike(id: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.addLike(id, getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>?, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            if (likeStatus) {
                                likeStatus = false
                                addLike.setImageResource(R.mipmap.likesunelect)
                            } else {
                                likeStatus = true
                                addLike.setImageResource(R.mipmap.likeselect)
                            }

                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun addComment(id: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AddEventCommentPojo> = apiService.addEventComment(id, getFromPrefs(TeacherConstant.ID).toString(),
                    etComment.text.toString().trim())
            call.enqueue(object : Callback<AddEventCommentPojo> {
                override fun onResponse(call: Call<AddEventCommentPojo>?, response: Response<AddEventCommentPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            hideSoftKeyboard()
                            seeAllComment.text = resources.getString(R.string.see_all_comment)
                            etComment.setText("")
                            val eventCommentDataPojo = EventCommentDataPojo()
                            eventCommentDataPojo.id = response.body()!!.data.id
                            eventCommentDataPojo.event_id = response.body()!!.data.event_id
                            eventCommentDataPojo.user_id = getFromPrefs(TeacherConstant.ID).toString()
                            eventCommentDataPojo.description = response.body()!!.data.description
                            eventCommentDataPojo.created = response.body()!!.data.created
                            eventCommentDataPojo.father_name = getTeacherName()
                            eventCommentDataPojo.profile_pic = getFromPrefs(TeacherConstant.IMAGE)
                            commentList.add(0, eventCommentDataPojo)

                            if (commentList.size > 2) {
                                seeAllComment.visibility = View.VISIBLE
                                setAllComment(commentList, 2)
                            } else {
                                seeAllComment.visibility = View.GONE
                                setAllComment(commentList, commentList.size)
                            }
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AddEventCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setEventData(response: Response<EventDetailPojo>?) {
        val eventData = response!!.body()!!.data
        var startTime = eventData.start_time
        if(startTime == "")
            startTime = "00:00"
        var endTime = eventData.end_time
        if(endTime == "")
            endTime = "00:00"
        eventDateTime.text = resources.getString(R.string.from_date, changeDateTimeFormat(eventData.start_date + " " + startTime+":00"))
        eventEndDateTime.text = resources.getString(R.string.to_date, changeDateTimeFormat(eventData.end_date + " " + endTime+":00"))
        eventName.text = eventData.event_name
        eventLocation.text = eventData.details
        eventDescription.text = eventData.registrationDetails
        if (eventData.image != null && eventData.image != "") {
            shareImageLink = TeacherConstant.EVENT_IMAGE_URL + eventData.image
            if (eventData.image!!.contains(".gif")) {
                ctx.setGifImage(ctx, TeacherConstant.EVENT_IMAGE_URL + eventData.image, postImage)
            } else {
                ctx.setProfileImageInLayout(ctx, TeacherConstant.EVENT_IMAGE_URL + eventData.image, postImage)
            }
        } else {
            imageLayout.visibility = View.GONE
            childScroll.layoutParams.height = 1000
            childScroll.requestLayout()
            postImage.setImageResource(R.mipmap.ic_launcher)
        }
        commentList = eventData.comments
        if (eventData.comments.size > 2) {
            seeAllComment.visibility = View.VISIBLE
            setAllComment(eventData.comments, 2)
        } else {
            seeAllComment.visibility = View.GONE
            setAllComment(eventData.comments, commentList.size)
        }

        if (eventData.like == "0") {
            likeStatus = false
            addLike.setImageResource(R.mipmap.likesunelect)
        } else {
            likeStatus = true
            addLike.setImageResource(R.mipmap.likeselect)
        }
    }

    private fun setAllComment(commentList: ArrayList<EventCommentDataPojo>, size: Int) {
        showComment.removeAllViews()
        for (i in 0 until size) {
            val itemView = LayoutInflater.from(ctx).inflate(R.layout.row_layout_event_comment, null, false)
            val profilePic = itemView.findViewById<ImageView>(R.id.profilePic) as CircleImageView
            val userName = itemView.findViewById(R.id.userName) as TextView
            val commentTime = itemView.findViewById(R.id.commentTime) as TextView
            val commentDescription = itemView.findViewById(R.id.commentDescription) as TextView

            if (commentList[i].profile_pic != null && commentList[i].profile_pic != "") {
                if(commentList[i].type == "1")
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.IMAGE_BASE_URL + commentList[i].profile_pic, profilePic)
                else
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.STUDENT_PROFILE_BASE_URL + commentList[i].profile_pic, profilePic)
            } else {
                profilePic.setImageResource(R.mipmap.usericon)
            }

            userName.text = commentList[i].father_name
            if (commentList[i].created != "") {
                val newDate = commentList[i].created.replace(".000Z", "").split("T")
                commentTime.text = ctx.changeDateTimeFormat(newDate[0] + " " + newDate[1])
            }
            commentDescription.text = commentList[i].description

            showComment.addView(itemView)
        }
    }
}