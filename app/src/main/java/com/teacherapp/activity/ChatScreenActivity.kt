package com.teacherapp.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ThumbnailUtils
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.teacherapp.R
import com.teacherapp.adapter.ChatAdapter
import com.teacherapp.interfaces.FileUploadSer
import com.teacherapp.interfaces.GlobalDataAccess
import com.teacherapp.interfaces.ServiceGenerator
import com.teacherapp.listener.NetworkStateReceiver
import com.teacherapp.pojo.ChatPojo
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.TeacherConstant
import io.socket.emitter.Emitter
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

class ChatScreenActivity : BaseActivity(), View.OnClickListener, NetworkStateReceiver.NetworkStateReceiverListener {

    private val ctx: ChatScreenActivity = this
    private var chatmessagelist: RecyclerView? = null
    private var message_input: EditText? = null
    private var send_button: ImageView? = null
    private var chat_message: ArrayList<ChatPojo>? = null
    private var adapter: ChatAdapter? = null
    internal var count = 0
    internal var callisTyping = 0
    private val mTypingHandler = Handler()
    private var txt_typing: TextView? = null
    private var tv_username: TextView? = null
    private var btn_sendImage: ImageView? = null
    private val SELECT_PHOTO = 457
    private val VIDEO_CAPTURE = 101
    private var cross_dialog: ImageView? = null
    private var permissionsToRequest: ArrayList<String>? = null
    private var permissionsRejected: ArrayList<String>? = null
    private var sharedPreferences: SharedPreferences? = null
    private var image_picker_dialog: Dialog? = null
    private var LOGIN_ID: String? = null
    private var RECEIVER_ID: String? = null
    private var cd: ConnectionDetector? = null
    private var filepath: String? = null
    internal lateinit var selectedIds: ArrayList<ChatPojo>
    private var ivBack: ImageView? = null
    private var network_available = true
    private var networkStateReceiver: NetworkStateReceiver? = NetworkStateReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_screen)

        init()
        setListener()
    }

    private fun init() {

        chat_message = ArrayList()
        selectedIds = ArrayList()
        sharedPreferences = getSharedPreferences(TeacherConstant.PREF_NAME, Context.MODE_PRIVATE)
        RECEIVER_ID = intent.getStringExtra("receiver_id")
        LOGIN_ID = getFromPrefs(TeacherConstant.ID).toString()
        chatmessagelist = findViewById(R.id.chatmessagelist)
        message_input = findViewById(R.id.message_input)
        message_input!!.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.message_input) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        send_button = findViewById(R.id.send_button)
        txt_typing = findViewById(R.id.tv_typing)
        tv_username = findViewById(R.id.tv_username)
        tv_username!!.text = intent.getStringExtra("username")
        txt_typing!!.text = ""
        btn_sendImage = findViewById(R.id.btn_sendImage)
        ivBack = findViewById(R.id.iv_back)
        cd = ConnectionDetector

        //get new message
        mSocket.on("get_private_chat", onNewMessage)
        //get typing status
        mSocket.on("get_typing_message", onTextTyping)
        //get my send message
        mSocket.on("get_send_message", mySentMessage)
        //check delivered status
        mSocket.on("check_status", isDelivered)
        //check read status
        mSocket.on("get_isRead", getReadStatus)
        //Receive Chat
        mSocket.on("receiveChat", getChatList)
        //Receive Is Read Status for Multiple Messages
        mSocket.on("receiveIsReadStatus", receiveIsReadStatus)

        val jsonObject = JSONObject()
        jsonObject.put("receiver_id", RECEIVER_ID)
        jsonObject.put("sender_id", "T_$LOGIN_ID")
        mSocket.emit("getChat", jsonObject)
    }

    private fun setListener() {

        send_button!!.setOnClickListener {
            val message = message_input!!.text.toString().trim { it <= ' ' }
            if (cd!!.isConnected(this)) {
                if (mSocket.connected()) {
                    if (!message.isEmpty()) {
                        message_input!!.setText("")
                        mTypingHandler.removeCallbacks(onTypingTimeout)
                        onTypingTimeout.run()
                        val jsonObject = JSONObject()
                        try {
                            jsonObject.put("sender_id", "T_$LOGIN_ID")
                            jsonObject.put("receiver_id", RECEIVER_ID)
                            jsonObject.put("message", encode(message))
                            jsonObject.put("sender_name", getTeacherName())
                            jsonObject.put("type", "text")
                            mSocket.emit("send_private_chat", jsonObject)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                } else {
                    mSocket.connect()
                }
            } else {
                Toast.makeText(applicationContext, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
            }
            count = 0
        }
        message_input!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                if (callisTyping == 0) {
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("sender_id", "T_$LOGIN_ID")
                        jsonObject.put("receiver_id", RECEIVER_ID)
                        jsonObject.put("type", "Typing....")
                        jsonObject.put("sender_name", getTeacherName())
                        mSocket.emit("send_typing_message", jsonObject)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    callisTyping++
                }
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mTypingHandler.removeCallbacks(onTypingTimeout)
            }

            override fun afterTextChanged(s: Editable) {
                mTypingHandler.postDelayed(onTypingTimeout, 1500)
            }
        })
        btn_sendImage!!.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                addPermissionDialogMarshMallow()
            } else {
                showChooserDialog()
            }
        }
        ivBack!!.setOnClickListener {
            finish()
        }
    }

    override fun onClick(v: View?) {
        if (v != null) {
            val chatItem: ChatPojo = v.getTag(R.string.data) as ChatPojo
            val mediaUrl: String?
            val mediaType: String? = chatItem.type
            val intent = Intent(applicationContext, FullScreenActivity::class.java)

            if (mediaType == "image") {
                mediaUrl = chatItem.image_url
                intent.putExtra("mediaUrl", mediaUrl)
                intent.putExtra("media_type", mediaType)
                startActivity(intent)
            } else if (mediaType == "video") {
                mediaUrl = chatItem.video_url
                intent.putExtra("mediaUrl", mediaUrl)
                intent.putExtra("media_type", mediaType)
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        networkStateReceiver?.addListener(this)
        this.registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onPause() {
        try {
            this.unregisterReceiver(networkStateReceiver)
        } catch (e: IllegalArgumentException) {
        }
        super.onPause()
    }

    override fun networkAvailable() {
        if (!network_available) {
            network_available = true
            if (!mSocket.connected()) {
                mSocket.connect()
                subscribeSocket()
            }
            val handler = Handler()
            handler.postDelayed({
                val jsonObject = JSONObject()
                jsonObject.put("receiver_id", RECEIVER_ID)
                jsonObject.put("sender_id", "T_$LOGIN_ID")
                mSocket.emit("getChat", jsonObject)
            }, 250)

            displayToast("Network available")
        }
    }

    override fun networkUnavailable() {
        network_available = false
        displayToast("Network not available")
    }

    private fun setListAdapter() {
        adapter = ChatAdapter(ctx, chat_message!!, selectedIds)
        val layoutManager = LinearLayoutManager(this)
        chatmessagelist!!.layoutManager = layoutManager
        chatmessagelist!!.itemAnimator = DefaultItemAnimator()
        chatmessagelist!!.adapter = adapter
        layoutManager.stackFromEnd = true
        adapter!!.notifyItemInserted(chat_message!!.size - 1)
    }

    //base 64
    companion object {
        private const val READ_EXTERNAL_STORAGE = 101
        fun encodeToBase64(image: Bitmap?, compressFormat: Bitmap.CompressFormat, quality: Int): String {
            val byteArrayOS = ByteArrayOutputStream()
            image!!.compress(compressFormat, quality, byteArrayOS)
            return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT)
        }
    }

    //onTyping method
    private val onTypingTimeout = Runnable {
        callisTyping = 0
        val jsonObject = JSONObject()
        try {
            jsonObject.put("sender_id", "T_$LOGIN_ID")
            jsonObject.put("receiver_id", RECEIVER_ID)
            jsonObject.put("type", "")
            mSocket.emit("send_typing_message", jsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //show permission dialog
    private fun addPermissionDialogMarshMallow() {
        val permissions = ArrayList<String>()
        val resultCode: Int = READ_EXTERNAL_STORAGE

        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.CAMERA)
        permissions.add(Manifest.permission.RECORD_AUDIO)

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions)
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest!!.size > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                //mark all these as asked..
                for (perm in permissionsToRequest!!) {
                    markAsAsked(perm, sharedPreferences!!)
                }
            } else {
                //show the success banner
                if (permissionsRejected!!.size < permissions.size) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog()
                }

                if (permissionsRejected!!.size > 0) {

                    requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                    //mark all these as asked..
                    for (perm in permissionsToRequest!!) {
                        markAsAsked(perm, sharedPreferences!!)
                    }
                }
            }
        }
    }

    //call when image pic from gallery and capture from camera
    private fun showChooserDialog() {
        if (cd!!.isConnected(this)) {
            image_picker_dialog = Dialog(this, android.R.style.Theme_Translucent_NoTitleBar)
            image_picker_dialog!!.setContentView(R.layout.custom_imageupload_dialog)
            image_picker_dialog!!.setCancelable(true)
            // set the custom dialog components - text, image and button
            val gallery: ImageView = image_picker_dialog!!.findViewById(R.id.gallery_image)
            gallery.setImageResource(R.drawable.gallery)
            val camera: ImageView = image_picker_dialog!!.findViewById(R.id.camera_image)
            camera.setImageResource(R.drawable.camera)
            cross_dialog = image_picker_dialog!!.findViewById(R.id.cross_dialog)
            cross_dialog!!.setOnClickListener { image_picker_dialog!!.dismiss() }

            gallery.setOnClickListener {
                val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                pickIntent.type = "image/* video/*"
                startActivityForResult(pickIntent, SELECT_PHOTO)
                image_picker_dialog!!.dismiss()
                image_picker_dialog!!.hide()
            }

            camera.setOnClickListener {
                val intent = Intent(applicationContext, CustomCameraActivity::class.java)
                startActivityForResult(intent, VIDEO_CAPTURE)
                image_picker_dialog!!.dismiss()
                image_picker_dialog!!.hide()
            }
            image_picker_dialog!!.show()
        } else {
            Toast.makeText(applicationContext, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (resultCode == VIDEO_CAPTURE) {

                filepath = data.getStringExtra("code")
                val type = data.getStringExtra("type")

                if (type.equals("image", ignoreCase = true)) {
                    //  filepath = getImagePath();
                    val orientation = 1
                    var valueOrientation = 0
                    valueOrientation = when (orientation) {
                        3 -> 180
                        6 -> 90
                        8 -> 270
                        else -> 0
                    }
                    val matrix = Matrix()
                    matrix.postRotate(valueOrientation.toFloat())
                    val bitmap = getThumbnailBitmap(filepath, 200)
                    val ss = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 70)
                    sendImage(filepath, ss)
                } else {
                    val bitmap = ThumbnailUtils.createVideoThumbnail(filepath, MediaStore.Video.Thumbnails.MINI_KIND)
                    val tempUri = getImageUri(applicationContext, bitmap)
                    val ss = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 70)
                    sendVideo(filepath, tempUri, ss)
                }
            } else if (requestCode == SELECT_PHOTO) {
                if (resultCode == Activity.RESULT_OK) {
                    val selectedMediaUri = data.data
                    filepath = GlobalDataAccess.getPath(selectedMediaUri, this)

                    if (selectedMediaUri!!.toString().toLowerCase().contains("image")) {
                        val bitmap = getThumbnailBitmap(filepath, 200)
                        val ss = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 70)
                        sendImage(filepath, ss)
                    } else {
                        val bitmap = ThumbnailUtils.createVideoThumbnail(filepath, MediaStore.Video.Thumbnails.MINI_KIND)
                        val ss = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 70)
                        sendVideo(filepath, selectedMediaUri, ss)
                    }
                }
            }
        }
    }

    private fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 70, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    // send image on local server and get chat id and image path
    private fun sendImage(photoPaths: String?, imagethumbnail: String) {
        if (ConnectionDetector.isConnected(this)) {
            var call: Call<ChatPojo>? = null
            val service = ServiceGenerator.createService(FileUploadSer.FileUploadService::class.java)
            val file = File(photoPaths!!)

            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body: MultipartBody.Part?
            if (photoPaths != "") {
                body = MultipartBody.Part.createFormData("compress_image", file.name, requestFile)
                call = service.sendImage(getMultiPartData("T_$LOGIN_ID"), getMultiPartData(RECEIVER_ID), getMultiPartData("image"), getMultiPartData("200"), getMultiPartData("200"), getMultiPartData(imagethumbnail), body!!)
            }
            call!!.enqueue(object : Callback<ChatPojo> {
                override fun onResponse(call: Call<ChatPojo>, response: Response<ChatPojo>) {
                    val imageUrl = response.body()!!.image_url
                    val chat_id = response.body()!!.chat_id
                    sendImageMessage(imageUrl, chat_id, imagethumbnail)
                }

                override fun onFailure(call: Call<ChatPojo>, t: Throwable) {
                    // Log error here since request failed
                    println("retrofit hh failure " + t.message)
                }
            })
        } else {
            Toast.makeText(applicationContext, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
        }
    }

    // send image on local server and get chat id and image path
    private fun sendVideo(photoPaths: String?, uri: Uri, thumbimage: String) {
        if (ConnectionDetector.isConnected(this)) {
            var call: Call<ChatPojo>? = null
            val service = ServiceGenerator.createService(FileUploadSer.FileUploadService::class.java)
            val file = File(photoPaths!!)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body: MultipartBody.Part?
            if (photoPaths != "") {
                body = MultipartBody.Part.createFormData("media", file.name, requestFile)
                call = service.sendVideo(getMultiPartData("T_$LOGIN_ID"), getMultiPartData(RECEIVER_ID), getMultiPartData("video"), getMultiPartData("200"), getMultiPartData("200"), getMultiPartData(thumbimage), body!!)
            }
            call!!.enqueue(object : Callback<ChatPojo> {
                override fun onResponse(call: Call<ChatPojo>, response: Response<ChatPojo>) {
                    val videoUrl = response.body()!!.video_url
                    val chat_id = response.body()!!.chat_id
                    sendVideoMessage(videoUrl, chat_id, thumbimage)
                }

                override fun onFailure(call: Call<ChatPojo>, t: Throwable) {
                    // Log error here since request failed
                    println("retrofit hh failure " + t.message)
                }
            })
        } else {
            Toast.makeText(applicationContext, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
        }
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri): String {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column_index)
        } finally {
            if (cursor != null) {
                cursor.close()
            }
        }
    }

    // Send video message
    private fun sendVideoMessage(videoUrl: String?, chat_id: String?, thumbnail: String) {
        if (mSocket.connected()) {
            val jsonObject = JSONObject()
            try {
                jsonObject.put("sender_id", "T_$LOGIN_ID")
                jsonObject.put("receiver_id", RECEIVER_ID)
                jsonObject.put("image_thumb", thumbnail)
                jsonObject.put("type", "video")
                jsonObject.put("image_size_height", "200")
                jsonObject.put("image_size_width", "200")
                jsonObject.put("video_url", videoUrl)
                jsonObject.put("name", getTeacherName())
                jsonObject.put("chat_id", chat_id)
                mSocket.emit("post_video", jsonObject)
                count = 0
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else {
            mSocket.connect()
        }
    }

    //call when image message send
    private fun sendImageMessage(imageUrl: String?, chat_id: String?, imagethumb: String) {
        if (mSocket.connected()) {
            val jsonObject = JSONObject()
            try {
                jsonObject.put("sender_id", "T_$LOGIN_ID")
                jsonObject.put("receiver_id", RECEIVER_ID)
                jsonObject.put("image_thumb", imagethumb)
                jsonObject.put("type", "image")
                jsonObject.put("image_size_height", "200")
                jsonObject.put("image_size_width", "200")
                jsonObject.put("image_url", imageUrl)
                jsonObject.put("chat_id", chat_id)
                jsonObject.put("sender_name", getTeacherName())
                mSocket.emit("post_image", jsonObject)
                count = 0
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else {
            mSocket.connect()
        }
    }

    private fun getThumbnailBitmap(path: String?, thumbnailSize: Int): Bitmap? {
        val bounds = BitmapFactory.Options()
        bounds.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, bounds)
        if (bounds.outWidth == -1 || bounds.outHeight == -1) {
            return null
        }
        val originalSize = if (bounds.outHeight > bounds.outWidth)
            bounds.outHeight
        else
            bounds.outWidth
        val opts = BitmapFactory.Options()
        opts.inSampleSize = originalSize / thumbnailSize
        return BitmapFactory.decodeFile(path, opts)
    }

    private fun getMultiPartData(value: String?): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value!!)
    }

    //check user is typing or not
    private val onTextTyping = Emitter.Listener { args ->
        runOnUiThread {
            val data = args[0] as JSONObject
            val json = data.getJSONObject("data")
            if (json.optString("sender_id") == RECEIVER_ID) {
                try {
                    txt_typing!!.text = json.optString("type")
                    if (json.optString("type") == "")
                        txt_typing!!.visibility = View.GONE
                    else
                        txt_typing!!.visibility = View.VISIBLE
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    //check message is read or not
    private val getReadStatus = Emitter.Listener { args ->
        runOnUiThread {
            val chatData = args[0] as JSONObject
            println("xhxhxhxhxxhxh getRead Status $chatData")
            try {
                val chatObj = chatData.optJSONObject("data")
                if (chatObj != null) {
                    val chatId = chatObj.optString("chat_id")
                    for (i in chat_message!!.size - 1 downTo 0) {
                        if (chatId == chat_message!![i].chat_id) {
                            chat_message!![i].isRead = "1"
                            break
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            notifyAdapter()
        }
    }

    //check message is read or not
    private val receiveIsReadStatus = Emitter.Listener { args ->
        runOnUiThread {
            val chatData = args[0] as JSONObject
            println("xhxhxhxhxxhxh receiveIsRead Status $chatData")
            try {
                val chatObj = chatData.optJSONObject("data")
                val userId = chatObj.optString("user_id")
                if (userId == RECEIVER_ID) {
                    for (i in chat_message!!.size - 1 downTo 0) {
                        val msg = chat_message!![i].isRead
                        chat_message!![i].isRead = "1"
                        if (msg == "1")
                            break
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            notifyAdapter()
        }
    }

    //check message is delivered or not
    private val isDelivered = Emitter.Listener { args ->
        runOnUiThread {
            val chatData = args[0] as JSONObject
            println("xhxhxhxhxxhxh get is Delivered $chatData")
            try {
                val chatObj = chatData.optJSONObject("data")
                val chatId = chatObj.optString("chat_id")
                for (i in chat_message!!.size - 1 downTo 0) {
                    if (chatId == chat_message!![i].chat_id) {
                        chat_message!![i].isRead = "0"
                        break
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            notifyAdapter()
        }
    }

    //call when my message is send to server
    private val mySentMessage = Emitter.Listener { args ->
        runOnUiThread {
            val chatData = args[0] as JSONObject
            try {
                val chatObj = chatData.getJSONObject("data")
                setDataInList(chatObj, "0")
                setListAdapter()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    // call when new message received
    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread {
            val data = args[0] as JSONObject
            println("MY MESSAGE onNewMessage $data")
            try {
                val chatObj = data.optJSONObject("data")
                if (chatObj.optString("sender_id") == RECEIVER_ID) {
                    setDataInList(chatObj, "1")
                    if ((getClassShortName() == ".activity.ChatScreenActivity")) {
                        val jsonObject = JSONObject()
                        try {
                            jsonObject.put("chat_id", chatObj.optString("chat_id"))
                            jsonObject.put("is_read", true)
                            jsonObject.put("sender_id", chatObj.optString("sender_id"))
                            mSocket.emit("set_isRead", jsonObject)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            notifyAdapter()
        }
    }

    private fun isLastVisible(): Boolean {
        val layoutManager: LinearLayoutManager = chatmessagelist!!.layoutManager as LinearLayoutManager
        val pos = layoutManager.findLastCompletelyVisibleItemPosition()
        val numItems = chatmessagelist?.adapter?.itemCount as Int

        return numItems - pos <= 2
    }

    private fun notifyAdapter() {
        if (!isLastVisible()) {
            if (adapter != null)
                adapter?.notifyDataSetChanged()
            else
                setListAdapter()
        } else {
            setListAdapter()
        }
    }

    private fun setDataInList(chatObj: JSONObject, isSend: String) {
        val obj = ChatPojo()
        obj.sender_id = chatObj.optString("sender_id")
        obj.receiver_id = chatObj.optString("receiver_id")
        obj.message = decode(chatObj.optString("message"))
        obj.type = chatObj.optString("type")
        if (obj.sender_id == RECEIVER_ID) {
            obj.message_side = "left"
        } else {
            obj.message_side = "right"
        }
        obj.image_thumb = chatObj.optString("image_thumb")
        obj.image_size_height = chatObj.optString("image_size_height")
        obj.image_size_width = chatObj.optString("image_size_width")
        obj.isSend = isSend
        obj.isRead = chatObj.optString("isRead")
        obj.time = chatObj.optString("time")
        obj.chat_id = chatObj.optString("chat_id")
        obj.image_url = chatObj.optString("image_url")
        obj.video_url = chatObj.optString("video_url")
        obj.video = chatObj.optString("video")
        obj.audio = chatObj.optString("audio")
        obj.file = chatObj.optString("file")
        obj.isPass = chatObj.optString("isPass")
        if (chatObj.optString("dates") == "")
            obj.dates = chatObj.optString("date")
        else
            obj.dates = chatObj.optString("dates")
        obj.isPending = chatObj.optString("isPending")
        obj.created_at = chatObj.optString("created_at")
        chat_message!!.add(obj)
    }

    private val getChatList = Emitter.Listener { args ->
        runOnUiThread {
            val chatData = args[0] as JSONObject
            println("xhxhxhxhxxhxh getChatList $chatData")
            try {
                val chatArray = chatData.optJSONArray("data")
                chat_message = ArrayList()
                if (chatArray.length() > 0) {
                    for (i in 0 until chatArray.length()) {
                        val chatObj = chatArray.optJSONObject(i)
                        setDataInList(chatObj, chatObj.optString("isSend"))
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            setListAdapter()
        }
    }

    override fun onBackPressed() {
        mSocket.off("get_private_chat", onNewMessage)
        mSocket.off("get_typing_message", onTextTyping)
        mSocket.off("get_send_message", mySentMessage)
        mSocket.off("check_status", isDelivered)
        mSocket.off("get_isRead", getReadStatus)
        mSocket.off("receiveChat", getChatList)
        mSocket.off("receiveIsReadStatus", receiveIsReadStatus)
        super.onBackPressed()
    }
}