package com.teacherapp.activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.NotificationDataPojo
import com.teacherapp.util.TeacherConstant

/**
 * Created by upasna.mishra on 5/5/2018.
 */
class SendNotificationDetailActivity : BaseActivity(){

    private lateinit var descNotification : TextView
    private lateinit var notification_image : ImageView
    private lateinit var notification_data : NotificationDataPojo
    private var ctx : SendNotificationDetailActivity = this
    private lateinit var audience_type : TextView
    private lateinit var time : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notification_detail_page)
        notification_data = intent.getSerializableExtra("notification_data") as NotificationDataPojo
        initialize()
        setListener()
    }

    private fun initialize() {
        setHeaderTitle(notification_data.title)
        descNotification = findViewById(R.id.descNotification)
        notification_image = findViewById(R.id.notification_image)
        audience_type = findViewById(R.id.audience_type)
        time = findViewById(R.id.time)
        if(notification_data.audience_type == "1")
            audience_type.text = ctx.resources.getString(R.string.program)+ ": "+notification_data.program
        else if(notification_data.audience_type == "2")
            audience_type.text = ctx.resources.getString(R.string.classes)+ ": "+notification_data.classes
        else
            audience_type.text = ctx.resources.getString(R.string.all_school)
        descNotification.text = notification_data.description
        val create_date = notification_data.created.split("T").toMutableList()
        val created_time = create_date[1].split(".").toMutableList()
        time.text = ctx.changeDateTimeFormat(create_date[0] + " " + created_time[0])
        if(notification_data.file != null || notification_data.file != ""){
            setProfileImageInLayout(ctx, TeacherConstant.NOTICE_FILE_BASE_URL + notification_data.file, notification_image)
        }
    }

    private fun setListener() {

    }
}