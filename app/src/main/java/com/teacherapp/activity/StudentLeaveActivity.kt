package com.teacherapp.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.adapter.StudentLeaveAdapter
import com.teacherapp.pojo.StudentLeaveDataPojo
import com.teacherapp.pojo.StudentLeavePojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class StudentLeaveActivity : BaseActivity() {

    private lateinit var rv_student_leave_list: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var studentLeaveAdapter: StudentLeaveAdapter
    private var ctx: StudentLeaveActivity = this
    private lateinit var leave_list: ArrayList<StudentLeaveDataPojo>
    private lateinit var prefs: TeacherPreference
    private lateinit var student_search: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.student_leave_activity)
        setHeaderTitle(intent.getStringExtra("from"))
        initialize()
        setListener()
        setHeaderTitle(resources.getString(R.string.student_leave))
        TeacherConstant.ACTIVITIES.add(this)
    }

    private fun initialize() {
        prefs = TeacherPreference(this)
        mLayoutManager = LinearLayoutManager(this)
        leave_list = ArrayList()
        rv_student_leave_list = findViewById(R.id.rv_student_leave_list)
        tv_no_data = findViewById(R.id.tv_no_data)
        iv_add_icon = findViewById(R.id.iv_add_icon)
        student_search = findViewById(R.id.student_search)
        iv_add_icon.visibility = View.GONE
        rv_student_leave_list.layoutManager = mLayoutManager
        if (ConnectionDetector.isConnected(this)) {
            //studentLeaveList(prefs.getUserId())
            studentLeaveList()
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setListener() {
        student_search.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (leave_list.size > 0) {
                    filter(s.toString())
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    private fun filter(text: String) {
        //new array list that will hold the filtered data
        val filterdNames = ArrayList<StudentLeaveDataPojo>()

        for (sty in leave_list) {
            //if the existing elements contains the search input
            if (sty.student_fname.toLowerCase().contains(text.toLowerCase()) || sty.reason.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(sty)
            }
        }
        //calling a method of the adapter class and passing the filtered list
        studentLeaveAdapter.filterList(filterdNames)
    }

    private fun studentLeaveList() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<StudentLeavePojo> = apiService.getStudentLeave(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<StudentLeavePojo> {
                override fun onResponse(call: Call<StudentLeavePojo>, response: Response<StudentLeavePojo>?) {
                    if (response != null) {
                        if (response.body()!!.data.size > 0) {
                            rv_student_leave_list.visibility = View.VISIBLE
                            tv_no_data.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                leave_list.addAll(response.body()!!.data)
                                studentLeaveAdapter = StudentLeaveAdapter(ctx, leave_list)
                                rv_student_leave_list.adapter = studentLeaveAdapter
                            } else {
                                displayToast(response.body()!!.message)
                            }
                        } else {
                            rv_student_leave_list.visibility = View.GONE
                            tv_no_data.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<StudentLeavePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}