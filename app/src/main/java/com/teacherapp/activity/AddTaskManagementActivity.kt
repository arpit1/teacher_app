package com.teacherapp.activity

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.*
import com.teacherapp.R
import com.teacherapp.pojo.*
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class AddTaskManagementActivity : BaseActivity() {

    private lateinit var et_task_name: EditText
    private lateinit var rl_starting_date: RelativeLayout
    private lateinit var tv_starting_date: TextView
    private lateinit var rl_due_date: RelativeLayout
    private lateinit var tv_due_date: TextView
    private lateinit var rl_time: RelativeLayout
    private lateinit var tv_reminder_time: TextView
    private lateinit var et_assign_name: EditText
    private lateinit var et_assign_email: EditText
    private lateinit var ed_description: EditText
    private lateinit var tv_submit: TextView
    private lateinit var prefs: TeacherPreference
    private lateinit var mInflator: LayoutInflater
    private lateinit var programSpinner: Spinner
    private lateinit var batchSpinner: Spinner
    private lateinit var classSpinner: Spinner
    private lateinit var studentSpinner: EditText
    private lateinit var programList: ArrayList<ProgramDataPojo>
    private lateinit var classList: ArrayList<ClassDataPojo>
    private lateinit var batchList: ArrayList<BatchDataPojo>
    private lateinit var student_list: ArrayList<StudentPojoData>
    private var calendar: Calendar? = null
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private var endYear: Int = 0
    private var endMonth: Int = 0
    private var endDay: Int = 0
    private var hour: Int = 0
    private var minute: Int = 0
    private var second: Int = 0
    private var student_id: String = "0"
    private var programId: String = ""
    private var batchId: String = ""
    private var classId: String = ""
    private var loginData: LoginPojo? = null
    private var selectedStudentcount: Int = 0
    private lateinit var selectedStudentId: StringBuilder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_task_management_activity)
        initialize()
        setListener()

        setHeaderTitle(getString(R.string.addTaskmanagement))
        TeacherConstant.ACTIVITIES.add(this)
    }

    private fun initialize() {
        programList = ArrayList()
        classList = ArrayList()
        batchList = ArrayList()
        student_list = ArrayList()
        loginData = getUserData()
        programSpinner = findViewById(R.id.program_spinner)
        batchSpinner = findViewById(R.id.batch_spinner)
        classSpinner = findViewById(R.id.class_spinner)
        studentSpinner = findViewById(R.id.student_spinner)
        mInflator = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        prefs = TeacherPreference(this)
        et_task_name = findViewById(R.id.et_task_name)
        rl_starting_date = findViewById(R.id.rl_starting_date)
        tv_starting_date = findViewById(R.id.tv_starting_date)
        rl_due_date = findViewById(R.id.rl_due_date)
        tv_due_date = findViewById(R.id.tv_due_date)

        rl_time = findViewById(R.id.rl_time)
        tv_reminder_time = findViewById(R.id.tv_reminder_time)
        et_assign_name = findViewById(R.id.et_assign_name)
        et_assign_email = findViewById(R.id.et_assign_email)
        ed_description = findViewById(R.id.et_description)
        ed_description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.et_description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        tv_submit = findViewById(R.id.tv_submit)

        calendar = Calendar.getInstance()
        val c = Calendar.getInstance()
        month = c.get(Calendar.MONTH)
        day = c.get(Calendar.DAY_OF_MONTH)

        hour = c.get(Calendar.HOUR_OF_DAY)
        minute = c.get(Calendar.MINUTE)
        second = c.get(Calendar.SECOND)
        getProgram()
    }

    private fun setListener() {
        tv_submit.setOnClickListener {
            val task_name = et_task_name.text.toString().trim()
            val starting_date = tv_starting_date.text.toString()
            val due_date = tv_due_date.text.toString()
            val desc = ed_description.text.toString().trim()
            val assign_mail = et_assign_email.text.toString().trim()
            val assign_name = et_assign_name.text.toString().trim()
            val rem_data = tv_reminder_time.text.toString().split("+//s")[0]
            selectedStudentId = StringBuilder()
            selectedStudentId.setLength(0)
            for (i in 0 until student_list.size) {
                if (student_list[i].is_checked) {
                    selectedStudentId.append(student_list[i].id)
                    selectedStudentId.append(",")
                }
            }
            if (selectedStudentId.isNotEmpty())
                selectedStudentId.delete(selectedStudentId.length - 1, selectedStudentId.length)
            if (validate(programId, batchId, classId, student_id, task_name, starting_date, due_date/*, assign_mail*/)) {
                val email = assign_mail.split("@")

                if (!assign_mail.isEmpty()) {
                    when {
                        email[0].matches("[0-9]+".toRegex()) -> displayToast(resources.getString(R.string.email_vaild_id_msg))
                        email[1].split(".")[0].matches("[0-9]+".toRegex()) -> displayToast(resources.getString(R.string.email_vaild_id_msg))
                        else -> addTask(selectedStudentId.toString(), task_name, starting_date, due_date, assign_mail, assign_name, desc, rem_data)
                    }
                } else
                    addTask(selectedStudentId.toString(), task_name, starting_date, due_date, assign_mail, assign_name, desc, rem_data)
            }
        }

        rl_starting_date.setOnClickListener {
            hideSoftKeyboard()
            showDatePicker(getString(R.string.start_date), tv_starting_date.text.toString())
        }
        rl_due_date.setOnClickListener {
            hideSoftKeyboard()
            if (!tv_starting_date.text.isEmpty())
                showEndDatePicker(getString(R.string.Due_date))
            else
                displayToast(resources.getString(R.string.start_date_msg))
        }
        rl_time.setOnClickListener {
            hideSoftKeyboard()
            showTimePicker()
        }
        programSpinner.onItemSelectedListener = programSelectedListener
        batchSpinner.onItemSelectedListener = batchSelectedListener
        classSpinner.onItemSelectedListener = classSelectedListener
//        studentSpinner.onItemSelectedListener = studentSelectedListener

        studentSpinner.setOnClickListener {
            if (student_list.size > 0) {
                showStudentNameDialog()
            } else {
                displayToast(resources.getString(R.string.studentnot_found))
            }
        }
    }

    private fun showStudentNameDialog() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.studentlist_dialog)
        dialog.setTitle(resources.getString(R.string.select_student))
        val window = dialog.window
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)

        val rvstudent_list = dialog.findViewById<RecyclerView>(R.id.rvstudent_list)
        val tv_done = dialog.findViewById<TextView>(R.id.tv_done)

        val studentAdapter = StudentAdapterTask(student_list, this)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        rvstudent_list.layoutManager = mLayoutManager
        rvstudent_list.itemAnimator = DefaultItemAnimator()
        rvstudent_list.adapter = studentAdapter

        tv_done.setOnClickListener {
            val selectedStudentName = StringBuilder()
            selectedStudentcount = 0
            for (i in 0 until student_list.size) {
                if (student_list[i].is_checked) {
                    student_list[i].is_checked_done = true
                    if (selectedStudentcount == 0) {
                        selectedStudentName.append(student_list[i].name)
                    }
                    selectedStudentcount++
                } else {
                    student_list[i].is_checked_done = false
                }
            }

            if (selectedStudentcount > 0) {
                if (selectedStudentcount > 1) {
                    selectedStudentName.append(" and " + (selectedStudentcount - 1) + " other")
                }

                studentSpinner.setText(selectedStudentName)
            } else {
                studentSpinner.setText("")
//                studentSpinner.hint = resources.getString(R.string.select_student)
            }

            dialog.dismiss()
        }
        dialog.show()
    }

    private class StudentAdapterTask(val istudentList: ArrayList<StudentPojoData>, context: AddTaskManagementActivity) : RecyclerView.Adapter<StudentAdapterTask.MyViewHolder>() {
        private var checkCounter: Int = 0
        var ctx = context
        override fun getItemCount(): Int {
            return istudentList.size
        }

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var checkspinnerTarget: TextView = view.findViewById(R.id.checkspinnerTarget)
            var rv_mainlay: LinearLayout = view.findViewById(R.id.rv_mainlay)
            var unchecked_img: ImageView = view.findViewById(R.id.unchecked_img)
            var checked_img: ImageView = view.findViewById(R.id.checked_img)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.checkbox_row_spinner, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val studentPojo = istudentList[position]
            holder.checkspinnerTarget.text = studentPojo.name

            if (studentPojo.is_checked_done) {
                holder.unchecked_img.visibility = View.GONE
                holder.checked_img.visibility = View.VISIBLE
            } else {
                holder.unchecked_img.visibility = View.VISIBLE
                holder.checked_img.visibility = View.GONE
            }

            holder.rv_mainlay.setOnClickListener {
                checkCounter = 0
                for (i in 0 until istudentList.size) {
                    if (istudentList[i].is_checked)
                        checkCounter++
                }

                if (holder.checked_img.visibility == View.VISIBLE) {
                    istudentList[position].is_checked = false
                    holder.unchecked_img.visibility = View.VISIBLE
                    holder.checked_img.visibility = View.GONE
                } else {
                    if (checkCounter < 5) {
                        istudentList[position].is_checked = true
                        holder.unchecked_img.visibility = View.GONE
                        holder.checked_img.visibility = View.VISIBLE
                    } else {
                        ctx.displayToast(ctx.resources.getString(R.string.student_selection_limit))
                    }
                }
            }
        }
    }

    private fun getProgram() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ProgramPojo> = apiService.getProgram(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<ProgramPojo> {
                override fun onResponse(call: Call<ProgramPojo>, response: Response<ProgramPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                programList.addAll(response.body()!!.data)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ProgramPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val programSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setBatchHintSpinner()
            if (position != 0) {
                programId = programList[position].id
                getBatch()
            } else {
                programId = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val programSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = programList[position].programs
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return programList[position]
        }

        override fun getCount(): Int {
            return programList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = programList[position].programs
            return convertView as View
        }
    }

    private fun setHintSpinner() {
        programList.clear()
        val programDataPojo = ProgramDataPojo()
        programDataPojo.id = "-1"
        programDataPojo.programs = resources.getString(R.string.select_program)
        programList.add(0, programDataPojo)
        programSpinner.adapter = programSpinnerAdapter
        programSpinnerAdapter.notifyDataSetChanged()
        setBatchHintSpinner()
    }

    private fun setBatchHintSpinner() {
        batchList.clear()
        val batchDataPojo = BatchDataPojo()
        batchDataPojo.batch_name = resources.getString(R.string.select_section)
        batchList.add(0, batchDataPojo)
        batchSpinner.adapter = batchSpinnerAdapter
        batchSpinnerAdapter.notifyDataSetChanged()
        setClassHintSpinner()
    }

    private fun setClassHintSpinner() {
        classList.clear()
        val classData = ClassDataPojo()
        classData.classes = resources.getString(R.string.select_class)
        classList.add(0, classData)
        classSpinner.adapter = classSpinnerAdapter
        classSpinnerAdapter.notifyDataSetChanged()
        setStudentHintSpinner()
    }

    private fun getBatch() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BatchPojo> = apiService.getBatch(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId)
            call.enqueue(object : Callback<BatchPojo> {
                override fun onResponse(call: Call<BatchPojo>, response: Response<BatchPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                batchList.addAll(response.body()!!.data)
                                batchSpinnerAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<BatchPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val batchSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = batchList[position].batch_name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return batchList[position].batch_name
        }

        override fun getCount(): Int {
            return batchList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = batchList[position].batch_name
            return convertView as View
        }
    }

    private val batchSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setClassHintSpinner()
            if (position != 0) {
                batchId = batchList[position].id
                getClass()
            } else {
                batchId = ""
            }


        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun getClass() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ClassPojo> = apiService.getClass(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<ClassPojo> {
                override fun onResponse(call: Call<ClassPojo>, response: Response<ClassPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setClassHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                classList.addAll(response.body()!!.data)
                                classSpinnerAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ClassPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val classSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setStudentHintSpinner()
            if (position != 0) {
                classId = classList[position].id
                studentList()
            } else {
                classId = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val classSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = classList[position].classes
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return classList[position].classes
        }

        override fun getCount(): Int {
            return classList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = classList[position].classes
            return convertView as View
        }
    }

    private fun setStudentHintSpinner() {
        student_list.clear()
        val studentData = StudentPojoData()
        studentData.name = resources.getString(R.string.select_student)
        studentSpinner.setText("")
        selectedStudentcount = 0
//        student_list.add(0, studentData)
//        studentSpinner.adapter = studentSpinnerAdapter
        studentSpinnerAdapter.notifyDataSetChanged()
    }

    private fun studentList() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<StudentPojo> = apiService.studentList(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, classId)
            call.enqueue(object : Callback<StudentPojo> {
                override fun onResponse(call: Call<StudentPojo>, response: Response<StudentPojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            student_list.addAll(response.body()!!.data)
                            studentSpinnerAdapter.notifyDataSetChanged()
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<StudentPojo>, t: Throwable) {
                    // Log error here since request failed
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val studentSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = student_list[position].name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return student_list[position].name
        }

        override fun getCount(): Int {
            return student_list.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = student_list[position].name
            return convertView as View
        }
    }

    /*private val studentSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            if (position != 0) {
                val selectName = studentSpinner.selectedItem.toString()
                (0 until student_list.size)
                        .filter { selectName == student_list[it].name }
                        .forEach { student_id = student_list[it].id }
            } else {
                student_id = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }*/

    private fun showDatePicker(str: String, start_date: String) {
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar?.set(year, month, day)

            this.year = year
            this.month = month
            this.day = day


            endYear = year
            endMonth = month
            endDay = day
            updateLabel(str)
        }, year, month, day)
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000
        dialog.show()
    }

    private fun showEndDatePicker(str: String) {
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar?.set(year, month, day)

            this.endYear = year
            this.endMonth = month
            this.endDay = day
            updateLabel(str)
        }, endYear, endMonth, endDay)
        val cal: Calendar = Calendar.getInstance()

        cal.set(year, month, day)
        dialog.datePicker.minDate = cal.timeInMillis - 1000
        dialog.show()
    }

    private fun showTimePicker() {
        val mTimePicker: TimePickerDialog

        mTimePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
            var status = getString(R.string.time_am)
            var hour_of_12_hour_format: Int

            if (selectedHour > 11) {
                status = getString(R.string.time_pm)
                hour_of_12_hour_format = selectedHour
                if (selectedHour != 12)
                    hour_of_12_hour_format = selectedHour - 12
            } else {
                hour_of_12_hour_format = selectedHour
            }
            hour = selectedHour
            minute = selectedMinute

            tv_reminder_time.text = String.format("%02d:%02d", hour_of_12_hour_format, selectedMinute) + " " + status
        }, hour, minute, true)
        mTimePicker.setTitle(getString(R.string.select_time))
        mTimePicker.show()
    }

    private fun updateLabel(strtype: String) {
        val myFormat = getString(R.string.dateformat) //In which you need put here

        val sdf = SimpleDateFormat(myFormat, Locale.US)
        if (strtype == "startdate") {
            tv_starting_date.text = sdf.format(calendar!!.time)
            tv_due_date.text = ""
        } else {
            tv_due_date.text = sdf.format(calendar!!.time)
        }
    }

    private fun addTask(student_id: String, task_name: String, starting_date: String, due_date: String, assign_email: String, assign_name: String, desc: String, reminder_time: String) {

        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.addTask(getFromPrefs(TeacherConstant.ID).toString(),
                    student_id, task_name, starting_date, due_date, assign_email,
                    assign_name, desc, reminder_time, getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, classId,
                    loginData!!.data!!.fname + " " + loginData!!.data!!.lname)
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            println("Student list id    "+selectedStudentId.length)
                            if(selectedStudentId.length == 0){
                                setAddTaskReminder(starting_date, task_name, reminder_time)
                            }
                            finish()
                        }
                        displayToast(response.body()!!.message)
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    //function for set remindar on teacher task...
    private fun setAddTaskReminder(reminderDate: String, reminderTitle: String, reminderTime: String) {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val notificationIntent = Intent("android.media.action.TEACHER_NOTIFICATION")
        notificationIntent.addCategory("android.intent.category.DEFAULT")
        notificationIntent.putExtra("notification_title", reminderTitle)
        val broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val Datesdf: SimpleDateFormat
        val remindarDate_: Date
        if (reminderTime == "") {
            Datesdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            remindarDate_ = Datesdf.parse(reminderDate)
        } else {
            Datesdf = SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US)
            remindarDate_ = Datesdf.parse("$reminderDate $reminderTime")
        }

        val cal = Calendar.getInstance()
        cal.timeInMillis = remindarDate_.time

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.timeInMillis, broadcast)
        }
    }

    private fun validate(program: String, batch: String, classes: String, student_id: String, task_name: String, starting_date: String, due_date: String/*, assign_mail: String*/): Boolean {
        return when {
            task_name.isEmpty() -> {
                displayToast(resources.getString(R.string.task_name_err))
                false
            }
            task_name.startsWith(" ") -> {
                displayToast(resources.getString(R.string.white_space_not_allowed_msg))
                false
            }
        /*program.isEmpty() -> {
            displayToast(resources.getString(R.string.select_program_msg))
            false
        }*/
            program.isNotEmpty() && batch.isEmpty() -> {
                displayToast(resources.getString(R.string.select_section_msg))
                false
            }
            program.isNotEmpty() && classes.isEmpty() -> {
                displayToast(resources.getString(R.string.select_class_msg))
                false
            }
            program.isNotEmpty() && selectedStudentId.isEmpty() -> {
                displayToast(resources.getString(R.string.selectstudent))
                false
            }
            starting_date.isEmpty() -> {
                displayToast(resources.getString(R.string.starting_date_err))
                false
            }
            due_date.isEmpty() -> {
                displayToast(resources.getString(R.string.due_date_error))
                false
            }
        /*!assign_mail.isEmpty() && !isEmailValid(assign_mail) -> {
            displayToast(resources.getString(R.string.email_vaild_id_msg))
            false
        }*/

            else -> true
        }
    }
}