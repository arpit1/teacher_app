package com.teacherapp.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.adapter.FilterListWithDateAdapter
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.pojo.TaskListDataPojo
import com.teacherapp.pojo.TaskListPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class FilterListWithDate : BaseActivity() {

    private lateinit var tast_adapter: FilterListWithDateAdapter
    private lateinit var prefs: TeacherPreference
    private lateinit var rv_task_list: RecyclerView
    private lateinit var tv_no_data: TextView
    private var ctx: FilterListWithDate = this
    private lateinit var task_list: ArrayList<TaskListDataPojo>
    private lateinit var loginPojo : LoginPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_listwith_date)
        init()
        setHeaderTitle(getString(R.string.filter_list))
        TeacherConstant.ACTIVITIES.add(this)
    }

    private fun init() {
        prefs = TeacherPreference(applicationContext)
        task_list = ArrayList()
        loginPojo = getUserData()!!
        rv_task_list = findViewById(R.id.rv_task_lists)
        tv_no_data = findViewById(R.id.tv_no_data)
    }

    override fun onResume() {
        super.onResume()
        val intent = intent
        val getFrom = intent.getStringExtra("datess")
        taskList(getFrom)
    }

    private fun taskList(strdate: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)

            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TaskListPojo> = apiService.getTaskListFiltered(getFromPrefs(TeacherConstant.ID).toString(), strdate)
            call.enqueue(object : Callback<TaskListPojo> {
                override fun onResponse(call: Call<TaskListPojo>, response: Response<TaskListPojo>?) {
                    if (response != null) {
                        if (response.body()!!.data.size > 0) {
                            task_list = ArrayList()
                            rv_task_list.visibility = View.VISIBLE
                            tv_no_data.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                task_list.addAll(response.body()!!.data)
                                tast_adapter = FilterListWithDateAdapter(ctx, task_list, loginPojo)
                                val llm = LinearLayoutManager(applicationContext)
                                llm.orientation = LinearLayoutManager.VERTICAL
                                rv_task_list.layoutManager = llm
                                rv_task_list.adapter = tast_adapter

                            } else {
                                ctx.displayToast(response.body()!!.message)
                            }
                        } else {
                            rv_task_list.visibility = View.GONE
                            tv_no_data.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<TaskListPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}