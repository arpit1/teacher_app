package com.teacherapp.activity

import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import com.github.rtoshiro.view.video.FullscreenVideoView
import com.teacherapp.R
import com.teacherapp.util.TeacherConstant
import java.io.IOException

/**
 * Created by upasna.mishra on 2/13/2018.
 */
class FullVideoScreenActivity : BaseActivity() {

    private lateinit var image_path: String
    private lateinit var videoview: FullscreenVideoView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.full_video_activity)
        initialize()
    }

    private fun initialize() {
        setHeaderTitle(intent.getStringExtra("event_name"))
        image_path = intent.getStringExtra("Image")
        image_path = intent.getStringExtra("Image")
        videoview = findViewById(R.id.videoview)

        videoview.setActivity(this)
        videoview.isShouldAutoplay = true

        loadVideo(image_path)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        videoview.resize()
    }

    private fun loadVideo(videoPath: String) {
        val videoUri = Uri.parse(TeacherConstant.IMAGE_URl + "" + videoPath)
        try {
            videoview.setVideoURI(videoUri)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}