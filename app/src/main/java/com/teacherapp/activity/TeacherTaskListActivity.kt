package com.teacherapp.activity

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.teacherapp.R
import com.teacherapp.adapter.TeacherTaskAdapter
import com.teacherapp.pojo.BasePojo
import com.teacherapp.pojo.TaskListDataPojo
import com.teacherapp.pojo.TaskListPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by upasna.mishra on 4/13/2018.
 */
class TeacherTaskListActivity : BaseActivity() {

    private lateinit var rv_task_list: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var tast_adapter: TeacherTaskAdapter
    private var ctx: TeacherTaskListActivity = this
    private lateinit var task_list: ArrayList<TaskListDataPojo>
    private lateinit var calendarView: MaterialCalendarView
    private lateinit var datesLow: HashSet<CalendarDay>
    private lateinit var datesMedium: HashSet<CalendarDay>
    private lateinit var datesHigh: HashSet<CalendarDay>
    private val colors = intArrayOf(
            R.drawable.circle,
            R.drawable.circle_task_low,
            R.drawable.circle_medium)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher_task)
        initialize()
    }

    private fun initialize() {
        setHeaderTitle(resources.getString(R.string.teacher_task))
        datesLow = HashSet()
        datesMedium = HashSet()
        datesHigh = HashSet()
        task_list = ArrayList()
        calendarView = findViewById(R.id.calendar_view)
        rv_task_list = findViewById(R.id.rv_task_list)
        tv_no_data = findViewById(R.id.tv_no_data)
        taskList()
    }

    private fun taskList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TaskListPojo> = apiService.getTeacherTaskList(ctx.getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<TaskListPojo> {
                override fun onResponse(call: Call<TaskListPojo>, response: Response<TaskListPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1" && response.body()!!.data.size > 0) {
                            task_list = ArrayList()
                            rv_task_list.visibility = View.VISIBLE
                            tv_no_data.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                task_list.addAll(response.body()!!.data)
                                selectDataOnCalendar(task_list)
                                tast_adapter = TeacherTaskAdapter(ctx, task_list)
                                val llm = LinearLayoutManager(ctx)
                                llm.orientation = LinearLayoutManager.VERTICAL
                                rv_task_list.layoutManager = llm
                                rv_task_list.adapter = tast_adapter
                            } else {
                                ctx.displayToast(response.body()!!.message)
                            }
                        } else {
                            rv_task_list.visibility = View.GONE
                            tv_no_data.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<TaskListPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    fun updateTaskList(id: String, progress: String, pos: Int, type: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.updateTeacherTask(progress, type, id)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        displayToast(response.body()!!.message)
                        task_list[pos].complete_status = progress
                        selectDataOnCalendar(task_list)
                        tast_adapter.notifyDataSetChanged()
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    fun selectDataOnCalendar(task_list: ArrayList<TaskListDataPojo>) {
        datesLow.clear()
        datesMedium.clear()
        datesHigh.clear()
        for (i in 0 until task_list.size) {
            val date = task_list[i].start_date.split("-")
            val day = date[0].toInt()
            val month = date[1].toInt() - 1
            val year = date[2].toInt()
            val statusComplete = task_list[i].complete_status.toInt()
            val dateToHighLight = CalendarDay.from(year, month, day)
            when {
                statusComplete <= 30 -> datesLow.add(dateToHighLight)
                statusComplete in 31..60 -> datesMedium.add(dateToHighLight)
                statusComplete > 60 -> datesHigh.add(dateToHighLight)
            }
        }
        calendarView.addDecorators(CircleDecoratorLow(ctx, datesLow), CircleDecoratorMedium(ctx, datesMedium), CircleDecoratorHigh(ctx, datesHigh))
    }

    inner class CircleDecoratorLow(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[1])!!
        private var datesLow: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesLow.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))
            view.setSelectionDrawable(drawable)
        }
    }

    inner class CircleDecoratorMedium(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[2])!!
        private var datesMedium: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesMedium.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))

            view.setSelectionDrawable(drawable)
        }
    }

    inner class CircleDecoratorHigh(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[0])!!
        private var datesHigh: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesHigh.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))

            view.setSelectionDrawable(drawable)
        }
    }
}