package com.teacherapp.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.BasePojo
import com.teacherapp.util.*
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class TeacherLeaveApplyActivity : BaseActivity() {

    private lateinit var iv_profile_pic: CircleImageView
    private lateinit var tv_user_name: TextView
    private lateinit var tv_qualification: TextView
    private lateinit var tv_from_date: TextView
    private lateinit var tv_to_date: TextView
    private lateinit var et_description: EditText
    private lateinit var tv_submit: TextView
    private lateinit var ll_start_date: LinearLayout
    private lateinit var ll_end_date: LinearLayout
    private var ctx: TeacherLeaveApplyActivity = this
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private lateinit var calendar: Calendar
    private lateinit var calendarEnd: Calendar
    private var yearEnd: Int = 0
    private var monthEnd: Int = 0
    private var dayEnd: Int = 0
    private var startDate: String = null.toString()
    private var endDate: String = null.toString()
    //private lateinit var iv_add_icon : ImageView
    private lateinit var iv_edit_profile: ImageView
    private lateinit var prefs: TeacherPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.apply_new_list_activity)
        initialize()
        setListener()
        TeacherConstant.ACTIVITIES.add(this)
        setHeaderTitle(resources.getString(R.string.request_leave))
    }

    private fun initialize() {
        prefs = TeacherPreference(this)
        calendar = Calendar.getInstance()
        calendarEnd = Calendar.getInstance()
        year = calendar.get(Calendar.YEAR)
        month = calendar.get(Calendar.MONTH)
        day = calendar.get(Calendar.DAY_OF_MONTH)
        iv_profile_pic = findViewById(R.id.iv_profile_pic)
        tv_user_name = findViewById(R.id.tv_user_name)
        tv_qualification = findViewById(R.id.tv_qualification)
        tv_from_date = findViewById(R.id.tv_from_date)
        tv_to_date = findViewById(R.id.tv_to_date)
        et_description = findViewById(R.id.et_description)
        et_description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.et_description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        tv_submit = findViewById(R.id.tv_submit)
        ll_start_date = findViewById(R.id.ll_start_date)
        ll_end_date = findViewById(R.id.ll_end_date)
        iv_add_icon = findViewById(R.id.iv_add_icon)
        iv_edit_profile = findViewById(R.id.iv_edit_profile)

        iv_add_icon.visibility = View.GONE
        iv_edit_profile.visibility = View.GONE

        if (getFromPrefs(TeacherConstant.IMAGE) != null && !getFromPrefs(TeacherConstant.IMAGE).equals("")) {
            ctx.setProfileImageInLayout(ctx, TeacherConstant.IMAGE_BASE_URL + getFromPrefs(TeacherConstant.IMAGE), iv_profile_pic)
        } else {
            iv_profile_pic.setImageResource(R.mipmap.user_icon_default)
        }
        tv_user_name.text = getTeacherName()
        tv_qualification.text = getUserData()!!.data!!.qualification
    }

    private fun setListener() {
        tv_submit.setOnClickListener {
            if (validate()) {
                applyTeacherLeave(tv_from_date.text.toString(),
                        tv_to_date.text.toString(), et_description.text.toString().trim())
            }
        }

        ll_start_date.setOnClickListener {
            showDatePicker()
        }

        ll_end_date.setOnClickListener {
            showDatePickerEndDate()
        }
    }

    private fun applyTeacherLeave(from_date: String, to_date: String, reason: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)

            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.addTeacherLeave(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), getFromPrefs(TeacherConstant.ID).toString(), from_date, to_date, reason)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            onBackPressed()
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun validate(): Boolean {
        val startDate = tv_from_date.text.toString()
        val endDate = tv_to_date.text.toString()
        val description = et_description.text.toString().trim()
        return when {
            startDate.isEmpty() -> {
                displayToast(resources.getString(R.string.select_start_date))
                false
            }
            endDate.isEmpty() -> {
                displayToast(resources.getString(R.string.select_end_date))
                false
            }
            description.isEmpty() -> {
                displayToast(resources.getString(R.string.enter_description))
                false
            }
            twoDateDifference(startDate, endDate) > 30 -> {
                displayToast(resources.getString(R.string.select_from_to_date))
                false
            }
            else -> true
        }
    }

    private fun showDatePicker() {
        val dialog = DatePickerDialog(ctx, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar.set(year, month, day)
            ctx.year = year
            ctx.month = month
            ctx.day = day

            ctx.yearEnd = year
            ctx.monthEnd = month
            ctx.dayEnd = day
            updateLabel(tv_from_date, calendar)
            tv_to_date.text = ""
            startDate = getDateFormat().format(calendar.time)
            endDate = ""
        }, year, month, day)
        val cal = Calendar.getInstance()
        cal.add(Calendar.MONTH, 2)
        dialog.datePicker.minDate = System.currentTimeMillis()
        dialog.datePicker.maxDate = cal.timeInMillis
        dialog.setTitle(resources.getString(R.string.leave_from))
        dialog.show()
    }

    private fun showDatePickerEndDate() {
        val dialog = DatePickerDialog(ctx, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendarEnd.set(year, month, day)
            ctx.yearEnd = year
            ctx.monthEnd = month
            ctx.dayEnd = day
            updateLabel(tv_to_date, calendarEnd)
            endDate = getDateFormat().format(calendarEnd.time)
        }, yearEnd, monthEnd, dayEnd)
        val cal = Calendar.getInstance()
        cal.add(Calendar.MONTH, 2)
        dialog.datePicker.minDate = calendar.timeInMillis
        dialog.datePicker.maxDate = cal.timeInMillis
        dialog.setTitle(resources.getString(R.string.leave_to))
        dialog.show()
    }

    private fun updateLabel(textView: TextView, cal: Calendar) {
        textView.text = getDateFormat().format(cal.time)
    }

    private fun twoDateDifference(from_date: String, to_date: String): Int {
        val sampleFormat = SimpleDateFormat(getString(R.string.date_formet_year), Locale.US)
        var daysBetween = 0
        try {
            val dateBefore = sampleFormat.parse(from_date)
            val dateAfter = sampleFormat.parse(to_date)
            val difference = dateAfter.time - dateBefore.time
            daysBetween = (difference / (1000 * 60 * 60 * 24)).toInt()

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return daysBetween
    }
}