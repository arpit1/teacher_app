package com.teacherapp.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.LinearLayout
import com.teacherapp.R
import com.teacherapp.adapter.HomeTabLayoutAdapter
import com.teacherapp.fragment.CalendarFrament
import com.teacherapp.pojo.GetDiaryDataPojo
import com.teacherapp.util.TeacherConstant
import com.teacherapp.util.TeacherPreference
import java.util.*

class HomeActivity : BaseActivity(), View.OnClickListener {
    var pager: ViewPager? = null
    private lateinit var tab_adapter: HomeTabLayoutAdapter
    lateinit var tabHome: LinearLayout
    private lateinit var tab_chat: LinearLayout
    lateinit var tab_calendar: LinearLayout
    private lateinit var tab_gallery: LinearLayout
    private lateinit var tab_student_progress: LinearLayout
    lateinit var prefs: TeacherPreference
    var type: Int = 0
    var fragmentFrom: String = "diary"
    private var doubleBackToExitPressedOnce = false
    private var ctx: HomeActivity = this
    var calendarSelect: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setDrawerAndToolbar(resources.getString(R.string.feed))
        initialize()
        setListener()
        TeacherConstant.ACTIVITIES.add(this)
    }

    private fun initialize() {
        tab_adapter = HomeTabLayoutAdapter(supportFragmentManager)
        pager = findViewById(R.id.pager)
        pager!!.adapter = tab_adapter
        pager!!.offscreenPageLimit = 4
        mSocket.connect()
        tabHome = findViewById(R.id.tab_home)
        tab_chat = findViewById(R.id.tab_chat)
        tab_calendar = findViewById(R.id.tab_calendar)
        tab_gallery = findViewById(R.id.tab_gallery)
        tab_student_progress = findViewById(R.id.tab_student_progress)
        iv_add_icon = findViewById(R.id.iv_add_icon)
        iv_filter = findViewById(R.id.iv_filter)
        iv_add_icon.visibility = View.GONE

        prefs = TeacherPreference(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        /* val jsonObject = JSONObject()
         try {
             val userID = "T_"+getFromPrefs(TeacherConstant.ID).toString()
             jsonObject.put("user_id",userID)
             mSocket.emit("disconnect", jsonObject)
         } catch (e: JSONException) {
             e.printStackTrace()
         }*/
        mSocket.disconnect()
    }


    private fun setListener() {
        pager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                tabStyle(position)
            }
        })

        tabHome.setOnClickListener {
            if (pager!!.currentItem != 0) {
                pager!!.currentItem = 0
                tabStyle(0)
            }
        }

        tab_chat.setOnClickListener {
            if (pager!!.currentItem != 1) {
                pager!!.currentItem = 1
                tabStyle(1)
            }
        }

        tab_calendar.setOnClickListener {
            if (pager!!.currentItem != 2) {
                pager!!.currentItem = 2
                tabStyle(2)
            }
        }

        tab_gallery.setOnClickListener {
            if (pager!!.currentItem != 3) {
                addClick(6)
                addFilter(7)
                pager!!.currentItem = 3
                tabStyle(3)
            }
        }

        tab_student_progress.setOnClickListener {
            if (pager!!.currentItem != 4) {
                pager!!.currentItem = 4
                tabStyle(4)
            }
        }

        val type = intent.getStringExtra("content_type")
        if (type != null) {
            when (type) {
                "Task" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        tab_calendar.performClick()
                        fragmentFrom = "tasks"
                        (supportFragmentManager.fragments[3] as CalendarFrament).switchFragment("HomeActivity")
                    }, 150)
                }
                "Leave" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, StudentLeaveActivity::class.java).putExtra("from", "Leave Management"))
                    }, 150)
                }
                "diaryComment" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, SelectDiaryCommentActivity::class.java)
                                .putExtra("diary_id", intent.getStringExtra("diary_id"))
                                .putExtra("parent_id", intent.getStringExtra("parent_id"))
                                .putExtra("name", intent.getStringExtra("name"))
                                .putExtra("from", "notification"))
                    }, 150)
                }
                "AddEvent" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        removeActivity("EventDetailActivity")
                        startActivity(Intent(ctx, EventDetailActivity::class.java).putExtra("event_id", intent.getStringExtra("id")))
                    }, 150)
                }
                "leaveApproved" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, TeacherShowLeaveListActivity::class.java)
                                .putExtra("header", getString(R.string.total_approv))
                                .putExtra("type", "1")
                                .putExtra("year", Calendar.getInstance().get(Calendar.YEAR)))
                    }, 150)
                }
                "leaveRejected" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, TeacherShowLeaveListActivity::class.java)
                                .putExtra("header", getString(R.string.total_reject))
                                .putExtra("type", "2")
                                .putExtra("year", Calendar.getInstance().get(Calendar.YEAR)))
                    }, 150)
                }
                "galleryUpload" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        if (pager!!.currentItem != 3) {
                            pager!!.currentItem = 3
                            tabStyle(3)
                        }
                    }, 150)
                }
                "chat" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        /*if (pager!!.currentItem != 1) {
                            pager!!.currentItem = 1
                            tabStyle(1)
                        }*/
                        startActivity(Intent(ctx, ChatScreenActivity::class.java)
                                .putExtra("receiver_id", intent.getStringExtra("sender_id"))
                                .putExtra("username", intent.getStringExtra("name")))
                    }, 150)
                }
                "localNotification" -> {
                    val handler = Handler()
                    handler.postDelayed({
                        startActivity(Intent(ctx, TeacherTaskListActivity::class.java))
                    }, 150)
                }
            }
        }
    }

    private fun tabStyle(position: Int) {
        hideSoftKeyboard()
        when (position) {
            0 -> {
                setDrawerAndToolbar(resources.getString(R.string.feed))
                tabHome.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_select_color))
                tab_chat.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_calendar.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_gallery.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_student_progress.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                iv_add_icon.visibility = View.GONE
                iv_filter.visibility = View.GONE
            }
            1 -> {
                setDrawerAndToolbar(resources.getString(R.string.message))
                tabHome.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_chat.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_select_color))
                tab_calendar.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_gallery.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_student_progress.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                iv_add_icon.visibility = View.GONE
                iv_filter.visibility = View.GONE
            }
            2 -> {
                ctx.addClick(calendarSelect)
                setDrawerAndToolbar(resources.getString(R.string.calendar))
                tabHome.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_chat.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_calendar.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_select_color))
                tab_gallery.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_student_progress.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                iv_add_icon.visibility = View.VISIBLE
                iv_filter.visibility = View.GONE
            }
            3 -> {
                ctx.addClick(6)
                setDrawerAndToolbar(resources.getString(R.string.gallery))
                tabHome.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_chat.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_calendar.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_gallery.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_select_color))
                tab_student_progress.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                iv_add_icon.visibility = View.VISIBLE
                iv_filter.visibility = View.VISIBLE
            }
            4 -> {
                setDrawerAndToolbar(resources.getString(R.string.student_progress))
                tabHome.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_chat.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_calendar.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_gallery.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_unselect_color))
                tab_student_progress.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_select_color))
                iv_add_icon.visibility = View.INVISIBLE
                iv_filter.visibility = View.GONE
            }
        }
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.rl_diary) {
            val obj = v.getTag(R.string.data) as GetDiaryDataPojo
            val intent = Intent(this, SchoolDiaryDetailActivity::class.java)
            intent.putExtra("diary_id", obj.id)
            intent.putExtra("title", obj.title)
            intent.putExtra("desc", obj.description)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        displayToast(resources.getString(R.string.back_exit))
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}