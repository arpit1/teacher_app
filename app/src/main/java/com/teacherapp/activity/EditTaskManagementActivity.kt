package com.teacherapp.activity

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.teacherapp.R
import com.teacherapp.pojo.*
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*



/**
 * Created by upasna.mishra on 2/6/2018.
 */
class EditTaskManagementActivity : BaseActivity() {

    private lateinit var et_task_name: EditText
    private lateinit var rl_starting_date: RelativeLayout
    private lateinit var tv_starting_date: TextView
    private lateinit var rl_due_date: RelativeLayout
    private lateinit var tv_due_date: TextView
    private lateinit var rl_time: RelativeLayout
    private lateinit var tv_reminder_time: TextView
    private lateinit var et_assign_name: EditText
    private lateinit var et_assign_email: EditText
    private lateinit var ed_description: EditText
    private lateinit var student_spinner: EditText
    private lateinit var tv_submit: TextView
    private lateinit var prefs: TeacherPreference
    private lateinit var mInflator: LayoutInflater
    private lateinit var programSpinner: Spinner
    private lateinit var batchSpinner: Spinner
    private lateinit var classSpinner: Spinner
    private lateinit var studentSpinner: Spinner
    private lateinit var programList: ArrayList<ProgramDataPojo>
    private lateinit var classList: ArrayList<ClassDataPojo>
    private lateinit var batchList: ArrayList<BatchDataPojo>
    private lateinit var student_list: ArrayList<StudentPojoData>

    private var calendar: Calendar? = null
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0

    private var endYear: Int = 0
    private var endMonth: Int = 0
    private var endDay: Int = 0

    private var hour: Int = 0
    private var minute: Int = 0
    private var second: Int = 0
    private var completestatus: String = ""
    private var id: String = ""
    private var student_id: String = "0"
    private var mainStudentId: String = "0"
    private var programId: String = ""
    private var batchId: String = ""
    private var mainBatchId: String = ""
    private var classId: String = ""
    private var mainClassId: String = ""
    private lateinit var taskDetail: TaskListDataPojo

    private lateinit var loginPojo : LoginPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_task_management_activity)
        initialize()
        setListener()

        setHeaderTitle(getString(R.string.editTaskmanagement))
        TeacherConstant.ACTIVITIES.add(this)
    }

    private fun initialize() {
        programList = ArrayList()
        classList = ArrayList()
        batchList = ArrayList()
        student_list = ArrayList()
        loginPojo = getUserData()!!
        programSpinner = findViewById(R.id.program_spinner)
        batchSpinner = findViewById(R.id.batch_spinner)
        classSpinner = findViewById(R.id.class_spinner)
        studentSpinner = findViewById(R.id.sp_selectStudent)
        student_spinner = findViewById(R.id.student_spinner)
        student_spinner.visibility = View.GONE
        studentSpinner.visibility = View.VISIBLE
        mInflator = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        prefs = TeacherPreference(this)
        et_task_name = findViewById(R.id.et_task_name)
        rl_starting_date = findViewById(R.id.rl_starting_date)
        tv_starting_date = findViewById(R.id.tv_starting_date)
        rl_due_date = findViewById(R.id.rl_due_date)
        tv_due_date = findViewById(R.id.tv_due_date)
        student_list = ArrayList()
        rl_time = findViewById(R.id.rl_time)
        tv_reminder_time = findViewById(R.id.tv_reminder_time)
        et_assign_name = findViewById(R.id.et_assign_name)
        et_assign_email = findViewById(R.id.et_assign_email)
        ed_description = findViewById(R.id.et_description)
        ed_description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.et_description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        tv_submit = findViewById(R.id.tv_submit)

        calendar = Calendar.getInstance()
        val c = Calendar.getInstance()

        hour = c.get(Calendar.HOUR_OF_DAY)
        minute = c.get(Calendar.MINUTE)
        second = c.get(Calendar.SECOND)

        setData()
    }

    private fun setData() {
        val intent = intent
        taskDetail = intent.getSerializableExtra("value") as TaskListDataPojo
        et_task_name.setText(taskDetail.title)

        tv_starting_date.text = taskDetail.start_date
        val arr = taskDetail.start_date.split("-")
        year = Integer.parseInt(arr[2])
        month = Integer.parseInt(arr[1]) - 1
        day = Integer.parseInt(arr[0])

        tv_due_date.text = taskDetail.end_date
        val arrEnd = taskDetail.end_date.split("-")
        endYear = Integer.parseInt(arrEnd[2])
        endMonth = Integer.parseInt(arrEnd[1]) - 1
        endDay = Integer.parseInt(arrEnd[0])

        if (taskDetail.reminder_time != "") {
            tv_reminder_time.text = taskDetail.reminder_time
            val now = taskDetail.reminder_time
            val inFormat = SimpleDateFormat("hh:mm aa", Locale.US)
            val outFormat = SimpleDateFormat("HH:mm", Locale.US)
            val time24 = outFormat.format(inFormat.parse(now))
            val time = time24.split(":")
            hour = Integer.parseInt(time[0])
            minute = Integer.parseInt(time[1])
        }

        et_assign_name.setText(taskDetail.assign_name)
        et_assign_email.setText(taskDetail.assign_email)
        ed_description.setText(taskDetail.description)
        completestatus = taskDetail.complete_status
        id = taskDetail.id

        classId = taskDetail.classes
        mainClassId = taskDetail.classes
        batchId = taskDetail.batch_name
        mainBatchId = taskDetail.batch_name
        programId = taskDetail.programs!!
        student_id = taskDetail.name!!
        mainStudentId = taskDetail.student_id
        getProgram()
    }

    private fun setListener() {

        tv_submit.setOnClickListener {
            val task_name = et_task_name.text.toString()
            val starting_date = tv_starting_date.text.toString()
            val due_date = tv_due_date.text.toString()
            val desc = ed_description.text.toString()
            val assign_mail = et_assign_email.text.toString()
            val assign_name = et_assign_name.text.toString()
            val rem_data = tv_reminder_time.text.toString().split("+//s")[0]

            if (validate(programId, batchId, classId, student_id, task_name.trim(), starting_date, due_date, assign_mail)) {
                val email = assign_mail.split("@")
                if (!assign_mail.isEmpty()) {
                    when {
                        email[0].matches("[0-9]+".toRegex()) -> displayToast(resources.getString(R.string.email_vaild_id_msg))
                        email[1].split(".")[0].matches("[0-9]+".toRegex()) -> displayToast(resources.getString(R.string.email_vaild_id_msg))
                        else -> addTask(student_id, task_name, starting_date, due_date, assign_mail, assign_name, desc, rem_data)
                    }
                } else
                    addTask(student_id, task_name, starting_date, due_date, assign_mail, assign_name, desc, rem_data)
            }
        }

        rl_starting_date.setOnClickListener {
            hideSoftKeyboard()
            showDatePicker(getString(R.string.start_date))
        }
        rl_due_date.setOnClickListener {
            hideSoftKeyboard()
            if (!tv_starting_date.text.isEmpty())
                showEndDatePicker(getString(R.string.Due_date))
            else
                displayToast(resources.getString(R.string.start_date_msg))
        }
        rl_time.setOnClickListener {
            hideSoftKeyboard()
            showTimePicker()
        }
        programSpinner.onItemSelectedListener = programSelectedListener
        batchSpinner.onItemSelectedListener = batchSelectedListener
        classSpinner.onItemSelectedListener = classSelectedListener
        studentSpinner.onItemSelectedListener = studentSelectedListener

        programSpinner.isEnabled = false
        programSpinner.isClickable = false
        batchSpinner.isEnabled = false
        batchSpinner.isClickable = false
        classSpinner.isEnabled = false
        classSpinner.isClickable = false
        studentSpinner.isEnabled = false
        studentSpinner.isClickable = false

        programSpinner.adapter = programSpinnerAdapter
        batchSpinner.adapter = batchSpinnerAdapter
        classSpinner.adapter = classSpinnerAdapter
        studentSpinner.adapter = studentSpinnerAdapter
    }

    private fun getProgram() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ProgramPojo> = apiService.getProgram(getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<ProgramPojo> {
                override fun onResponse(call: Call<ProgramPojo>, response: Response<ProgramPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                programList.addAll(response.body()!!.data)
                            }
                            for (i in 0 until programList.size) {
                                if (programId == programList[i].programs) {
                                    programSpinner.setSelection(i)
                                    programSpinnerAdapter.notifyDataSetChanged()
                                    break
                                }
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ProgramPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val programSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setBatchHintSpinner()
            if (position != 0) {
                programId = programList[position].id
                getBatch()
            } else {
                programId = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val programSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = programList[position].programs
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return programList[position]
        }

        override fun getCount(): Int {
            return programList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = programList[position].programs
            return convertView as View
        }
    }

    private fun setHintSpinner() {
        programList.clear()
        val programDataPojo = ProgramDataPojo()
        programDataPojo.id = "-1"
        programDataPojo.programs = resources.getString(R.string.select_program)
        programList.add(0, programDataPojo)
        programSpinnerAdapter.notifyDataSetChanged()
        setBatchHintSpinner()
    }

    private fun setBatchHintSpinner() {
        batchList.clear()
        val batchDataPojo = BatchDataPojo()
        batchDataPojo.batch_name = resources.getString(R.string.select_section)
        batchList.add(0, batchDataPojo)
        batchSpinnerAdapter.notifyDataSetChanged()
        setClassHintSpinner()
    }

    private fun setClassHintSpinner() {
        classList.clear()
        val classData = ClassDataPojo()
        classData.classes = resources.getString(R.string.select_class)
        classList.add(0, classData)
        classSpinnerAdapter.notifyDataSetChanged()
        setStudentHintSpinner()
    }

    private fun getBatch() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BatchPojo> = apiService.getBatch(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId)
            call.enqueue(object : Callback<BatchPojo> {
                override fun onResponse(call: Call<BatchPojo>, response: Response<BatchPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                batchList.addAll(response.body()!!.data)
                                batchSpinnerAdapter.notifyDataSetChanged()
                            }

                            if(mainBatchId != "") {
                                for (i in 0 until batchList.size) {
                                    if (mainBatchId == batchList[i].batch_name) {
                                        batchSpinner.setSelection(i)
                                        batchSpinnerAdapter.notifyDataSetChanged()
                                        break
                                    }
                                }
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<BatchPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val batchSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = batchList[position].batch_name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return batchList[position].batch_name
        }

        override fun getCount(): Int {
            return batchList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = batchList[position].batch_name
            return convertView as View
        }
    }

    private val batchSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setClassHintSpinner()
            if (position != 0) {
                batchId = batchList[position].id
                mainBatchId = ""
                getClass()
            } else {
                batchId = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun getClass() {
        if (ConnectionDetector.isConnected(this)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ClassPojo> = apiService.getClass(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<ClassPojo> {
                override fun onResponse(call: Call<ClassPojo>, response: Response<ClassPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setClassHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                classList.addAll(response.body()!!.data)
                                classSpinnerAdapter.notifyDataSetChanged()
                            }
                            if(mainClassId != "") {
                                for (i in 0 until classList.size) {
                                    if (mainClassId == classList[i].classes) {
                                        classSpinner.setSelection(i)
                                        classSpinnerAdapter.notifyDataSetChanged()
                                        break
                                    }
                                }
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ClassPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val classSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            hideSoftKeyboard()
            setStudentHintSpinner()
            if (position != 0) {
                classId = classList[position].id
                mainClassId = ""
                studentList()
            } else {
                classId = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val classSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = classList[position].classes
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return classList[position].classes
        }

        override fun getCount(): Int {
            return classList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = classList[position].classes
            return convertView as View
        }
    }

    private fun setStudentHintSpinner() {
        student_list.clear()
        val studentData = StudentPojoData()
        studentData.name = resources.getString(R.string.select_student)
        student_list.add(0, studentData)
        studentSpinnerAdapter.notifyDataSetChanged()
    }

    private fun studentList() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<StudentPojo> = apiService.studentList(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, classId)
            call.enqueue(object : Callback<StudentPojo> {
                override fun onResponse(call: Call<StudentPojo>, response: Response<StudentPojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            student_list.addAll(response.body()!!.data)
                            studentSpinnerAdapter.notifyDataSetChanged()
                        }

                        if(mainStudentId != "") {
                            for (i in 0 until student_list.size) {
                                if (mainStudentId == student_list[i].id) {
                                    studentSpinner.setSelection(i)
                                    studentSpinnerAdapter.notifyDataSetChanged()
                                    break
                                }
                            }
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<StudentPojo>, t: Throwable) {
                    // Log error here since request failed
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val studentSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = student_list[position].name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return student_list[position].name
        }

        override fun getCount(): Int {
            return student_list.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = student_list[position].name
            return convertView as View
        }
    }

    private val studentSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            if (position != 0) {
                student_id = student_list[position].id
                mainStudentId = ""
            } else {
                student_id = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun showDatePicker(str: String) {
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar?.set(year, month, day)

            this.year = year
            this.month = month
            this.day = day

            endYear = year
            endMonth = month
            endDay = day
            updateLabel(str)
        }, year, month, day)
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000
        dialog.show()
    }

    private fun showEndDatePicker(str: String) {
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar?.set(year, month, day)

            this.endYear = year
            this.endMonth = month
            this.day = day
            updateLabel(str)
        }, endYear, endMonth, endDay)
        val cal: Calendar = Calendar.getInstance()

        cal.set(year, month, day)
        dialog.datePicker.minDate = cal.timeInMillis - 1000
        dialog.show()
    }

    private fun showTimePicker() {
        val mTimePicker: TimePickerDialog

        mTimePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
            var status = getString(R.string.time_am)
            var hour_of_12_hour_format: Int

            if (selectedHour > 11) {
                status = getString(R.string.time_pm)
                hour_of_12_hour_format = selectedHour
                if (selectedHour != 12)
                    hour_of_12_hour_format = selectedHour - 12
            } else {
                hour_of_12_hour_format = selectedHour
            }
            hour = selectedHour
            minute = selectedMinute

            tv_reminder_time.text = String.format("%02d:%02d", hour_of_12_hour_format, selectedMinute) + " " + status
        }, hour, minute, true)
        mTimePicker.setTitle(getString(R.string.select_time))
        mTimePicker.show()
    }

    private fun updateLabel(strtype: String) {
        val myFormat = getString(R.string.dateformat) //In which you need put here

        val sdf = SimpleDateFormat(myFormat, Locale.US)
        if (strtype == "startdate") {
            tv_starting_date.text = sdf.format(calendar!!.time)
            tv_due_date.text = ""
        } else {
            tv_due_date.text = sdf.format(calendar!!.time)
        }
    }

    private fun addTask(student_id: String, task_name: String, starting_date: String, due_date: String, assign_email: String, assign_name: String, desc: String, reminder_time: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)

            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo>
            call = apiService.updateTask(taskDetail.id, getFromPrefs(TeacherConstant.ID).toString(), student_id, task_name, starting_date, due_date, assign_email, assign_name,
                    desc, reminder_time, getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), completestatus, programId, batchId, classId,
                    loginPojo.data!!.fname+" "+loginPojo.data!!.lname)
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            finish()
                        }
                        displayToast(response.body()!!.message)
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun validate(program: String, batch: String, classes: String, student_id: String, task_name: String, starting_date: String, due_date: String, assign_mail: String): Boolean {
        return when {
            task_name.isEmpty() -> {
                displayToast(resources.getString(R.string.task_name_err))
                false
            }
            program.isEmpty() -> {
                displayToast(resources.getString(R.string.select_program_msg))
                false
            }
            batch.isEmpty() -> {
                displayToast(resources.getString(R.string.select_section_msg))
                false
            }
            classes.isEmpty() -> {
                displayToast(resources.getString(R.string.select_class_msg))
                false
            }
            student_id.isEmpty() -> {
                displayToast(resources.getString(R.string.selectstudent))
                false
            }
            starting_date.isEmpty() -> {
                displayToast(resources.getString(R.string.starting_date_err))
                false
            }
            due_date.isEmpty() -> {
                displayToast(resources.getString(R.string.due_date_error))
                false
            }
            !assign_mail.isEmpty() && !isEmailValid(assign_mail) -> {
                displayToast(resources.getString(R.string.email_vaild_id_msg))
                false
            }
            else -> true
        }
    }
}