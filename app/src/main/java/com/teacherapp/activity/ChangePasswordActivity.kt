package com.teacherapp.activity

import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.BasePojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 4/12/2018.
 */
class ChangePasswordActivity : BaseActivity() {


    private lateinit var old_password: EditText
    private lateinit var password: EditText
    private lateinit var confirm_password: EditText
    private lateinit var submit: TextView
    private val ctx = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password_activity)
        setHeaderTitle(resources.getString(R.string.change_password))
        initialize()
        setListener()
    }

    private fun initialize() {
        old_password = findViewById(R.id.old_password)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_password)
        submit = findViewById(R.id.tv_save)
    }

    private fun setListener() {
        submit.setOnClickListener {
            if(validate()){
                passwordChange()
            }
        }
    }

    private fun validate(): Boolean {
        val old_pass = old_password!!.text.toString()
        val pass = password!!.text.toString()
        val confirm_pass = confirm_password!!.text.toString()
        if (old_pass.length == 0) {
            displayToast(resources.getString(R.string.old_pass_msg))
            return false
        } else if (pass.length == 0) {
            displayToast(resources.getString(R.string.password_msg))
            return false
        } else if (confirm_pass.length == 0) {
            displayToast(resources.getString(R.string.confirm_pass_msg))
            return false
        } else if (pass.length < 6) {
            displayToast(resources.getString(R.string.min_password_msg))
            return false
        } else if (!passCombination(pass)) {
            displayToast(resources.getString(R.string.combination_msg))
            return false
        }else if (confirm_pass != pass) {
            displayToast(resources.getString(R.string.password_nomatch_msg))
            return false
        }else {
            return true
        }
    }

    private fun passwordChange() {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.changePassword(getFromPrefs(TeacherConstant.ID).toString(),
                    password.text.toString(), old_password.text.toString())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response!!.body()!!.status == "1") {
                        finish()
                    }
                    displayToast(response.body()!!.message)
                    d.dismiss()
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}