package com.teacherapp.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.media.ExifInterface
import android.support.v4.content.FileProvider
import android.support.v7.widget.RecyclerView
import android.text.InputFilter
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.teacherapp.R
import com.teacherapp.pojo.AudienceTypeDataPojo
import com.teacherapp.pojo.AudienceTypePojo
import com.teacherapp.pojo.BasePojo
import com.teacherapp.util.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by upasna.mishra on 4/27/2018.
 */
class AddNotificationActivity : BaseActivity(), View.OnClickListener {


    private lateinit var notice_type_spinner: Spinner
    private lateinit var et_title: EditText
    private lateinit var audience_type: Spinner
    private lateinit var program: Spinner
    private lateinit var et_description: EditText
    private lateinit var tv_upload_file: TextView
    private lateinit var tv_submit: TextView
    private lateinit var mInflator: LayoutInflater
    private lateinit var notice_type_list: ArrayList<String>
    private lateinit var audience_type_list: ArrayList<String>
    private lateinit var llProgram: LinearLayout
    private lateinit var program_list: ArrayList<AudienceTypeDataPojo>
    private var type: String = ""
    private var notice_type: String = ""
    private var audience_type_select: String = ""
    private var program_type_select: String = ""
    private lateinit var encodedImageList: ArrayList<String>
    private lateinit var map: ArrayList<Bitmap>
    private lateinit var adapter: ImageAdapter
    private var ctx: AddNotificationActivity = this
    private lateinit var notice_image: ImageView
    private lateinit var imageToUpload: ImageView
    private val READ_EXTERNAL_STORAGE = 101
    private var permissionsToRequest: java.util.ArrayList<String>? = null
    private var permissionsRejected: java.util.ArrayList<String>? = null
    private var sharedPreferences: SharedPreferences? = null
    private var image_picker_dialog: Dialog? = null
    private lateinit var cross_dialog: ImageView
    private val SELECT_PHOTO = 458
    private var mCurrentPhotoPath: String? = null
    private val CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 459
    private var photoPath: String? = ""
    private lateinit var uploadImageLayout : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_notification_activity)
        initialize()
        setListener()
    }

    private fun initialize() {
        setHeaderTitle(resources.getString(R.string.add_notification))
        ivNotification.visibility = View.GONE
        mInflator = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        notice_type_list = ArrayList()
        audience_type_list = ArrayList()
        program_list = ArrayList()
        encodedImageList = ArrayList()
        map = ArrayList()
        notice_type_spinner = findViewById(R.id.notice_type_spinner)
        et_title = findViewById(R.id.et_title)
        audience_type = findViewById(R.id.audience_type)
        notice_image = findViewById(R.id.notice_image)
        uploadImageLayout = findViewById(R.id.uploadImageLayout)
        imageToUpload = findViewById(R.id.imageToUpload)
        program = findViewById(R.id.program)
        et_description = findViewById(R.id.et_description)
        et_description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.et_description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        tv_upload_file = findViewById(R.id.tv_upload_file)
        tv_submit = findViewById(R.id.tv_submit)
        llProgram = findViewById(R.id.llProgram)
        notice_type_list.add(0, resources.getString(R.string.select_notice_type))
        notice_type_list.add(1, resources.getString(R.string.normal))
        notice_type_list.add(2, resources.getString(R.string.alert))
        audience_type_list.add(0, resources.getString(R.string.select_audience_type))
        audience_type_list.add(1, resources.getString(R.string.program))
        audience_type_list.add(2, resources.getString(R.string.classes))
        audience_type_list.add(3, resources.getString(R.string.all_school))
        notice_type_spinner.adapter = noticeTypeSpinnerAdapter
        audience_type.adapter = audienceTypeSpinnerAdapter
        sharedPreferences = getSharedPreferences(TeacherConstant.PREF_NAME, Activity.MODE_PRIVATE)
    }

    private fun setListener() {
        tv_upload_file.setOnClickListener(this)
        tv_submit.setOnClickListener(this)
        audience_type.onItemSelectedListener = audienceSelectedListener
        notice_type_spinner.onItemSelectedListener = noticeSelectedListener
        program.onItemSelectedListener = programSelectedListener
    }

    private val programSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            val selectProgram = program.selectedItem.toString()
            if (selectProgram != resources.getString(R.string.select_class_msg) ||
                    selectProgram != resources.getString(R.string.select_program_msg)) {
                for (i in 0 until program_list.size) {
                    if (selectProgram == program_list[i].classes + "-" + program_list[i].programs ||
                            selectProgram == program_list[i].programs) {
                        program_type_select = (program_list[i].id)
                    }
                }
            }

        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val noticeSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            val selectNotice = notice_type_spinner.selectedItem.toString()
            notice_type = when (selectNotice) {
                resources.getString(R.string.normal) -> "0"
                resources.getString(R.string.alert) -> "1"
                else -> ""
            }
            val fArray = arrayOfNulls<InputFilter>(1)
            val maxLength: Int
            if (notice_type == "1") {
                maxLength = 200
                uploadImageLayout.visibility = View.GONE
                et_description.hint = resources.getString(R.string.descriptions_150)
                if(!et_description.text.isEmpty() && et_description.text.length > 200){
                    et_description.setText(et_description.text.substring(0, 200))
                }
            } else {
                maxLength = 2500
                uploadImageLayout.visibility = View.VISIBLE
                et_description.hint = resources.getString(R.string.descriptions)
            }
            fArray[0] = InputFilter.LengthFilter(maxLength)
            et_description.filters = fArray
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.tv_upload_file -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogMarshMallow()
                } else {
                    showChooserDialog()
                }
            }

            R.id.tv_submit -> {
                validate()
            }

            R.id.ic_delete -> {
                val pos = view.getTag(R.string.pos) as Int
                map.removeAt(pos)
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun addPermissionDialogMarshMallow() {
        val permissions = java.util.ArrayList<String>()
        val resultCode: Int = READ_EXTERNAL_STORAGE

        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.CAMERA)

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions)
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest!!.size > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                //mark all these as asked..
                for (perm in permissionsToRequest!!) {
                    sharedPreferences?.let { markAsAsked(perm, it) }
                }
            } else {
                //show the success banner
                if (permissionsRejected!!.size < permissions.size) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog()
                }

                if (permissionsRejected!!.size > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application
                    requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                    //mark all these as asked..
                    for (perm in permissionsToRequest!!) {
                        sharedPreferences?.let { markAsAsked(perm, it) }
                    }
                }
            }
        }
    }

    private fun showChooserDialog() {
        image_picker_dialog = Dialog(this, android.R.style.Theme_Translucent_NoTitleBar)
        image_picker_dialog!!.setContentView(R.layout.custom_imageupload_dialog)
        image_picker_dialog!!.setCancelable(true)

        val gallery: LinearLayout = image_picker_dialog!!.findViewById(R.id.galleryLayout)
        val camera: LinearLayout = image_picker_dialog!!.findViewById(R.id.cameraLayout)

        cross_dialog = image_picker_dialog!!.findViewById(R.id.cross_dialog)
        cross_dialog.setOnClickListener({ image_picker_dialog!!.dismiss() })

        gallery.setOnClickListener {
            val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

            startActivityForResult(i, SELECT_PHOTO)
            image_picker_dialog!!.dismiss()
            image_picker_dialog!!.hide()
        }

        camera.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            //				File f = null;
            try {
                //					f = setUpPhotoFile();

                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
                val imageFileName = "bmh" + timeStamp + "_"
                val albumF = getAlbumDir()
                val imageF = File.createTempFile(imageFileName, "bmh", albumF)

                mCurrentPhotoPath = imageF.absolutePath
                //takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                val photoURI: Uri = FileProvider.getUriForFile(this, applicationContext.packageName + ".my.package.name.provider", imageF)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
            image_picker_dialog!!.dismiss()
            image_picker_dialog!!.hide()
        }
        image_picker_dialog!!.show()
    }

    private fun getAlbumDir(): File? {
        var storageDir: File? = null

        if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraPicture")

            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    //		Log.d("CameraSample", "failed to create directory");
                    return null
                }
            }
        } else {
            //		Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }
        return storageDir
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data == null) {
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                val selectedImage = data!!.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                val cursor = this.contentResolver.query(selectedImage, filePathColumn, null, null, null)
                cursor.moveToFirst()

                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                val imagePath1 = cursor.getString(columnIndex)

                //                File f =new File(compressImage(imagePath1, 1));
                val f = File(compressImage(imagePath1))
                val myBitmap: Bitmap
                myBitmap = BitmapFactory.decodeFile(f.absolutePath)
                // notice_image.visibility = View.VISIBLE
                imageToUpload.setImageBitmap(myBitmap)
                photoPath = f.absolutePath
                cursor.close()
            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                val f = File(mCurrentPhotoPath?.let { compressImage(it) })
                val myBitmap = BitmapFactory.decodeFile(f.absolutePath)
                //notice_image.visibility = View.VISIBLE
                imageToUpload.setImageBitmap(myBitmap)
                photoPath = f.absolutePath
            }
        }
    }

    private fun compressImage(imageUri: String): String {
        val filePath: String = imageUri

        var scaledBitmap: Bitmap? = null

        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var bmp: Bitmap?
        bmp = BitmapFactory.decodeFile(filePath, options)

        var actualHeight = options.outHeight
        var actualWidth = options.outWidth
        val maxHeight = 816.0f
        val maxWidth = 612.0f

        var imgRatio = (actualWidth / actualHeight).toFloat()
        val maxRatio = maxWidth / maxHeight

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            when {
                imgRatio < maxRatio -> {
                    imgRatio = maxHeight / actualHeight
                    actualWidth = (imgRatio * actualWidth).toInt()
                    actualHeight = maxHeight.toInt()
                }
                imgRatio > maxRatio -> {
                    imgRatio = maxWidth / actualWidth
                    actualHeight = (imgRatio * actualHeight).toInt()
                    actualWidth = maxWidth.toInt()
                }
                else -> {
                    actualHeight = maxHeight.toInt()
                    actualWidth = maxWidth.toInt()

                }
            }
        }

        val utils = ImageLoadingUtlis(this)
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight)
        options.inJustDecodeBounds = false
        options.inDither = false
        //        options.inPurgeable = true;
        //        options.inInputShareable = true;
        options.inTempStorage = ByteArray(16 * 1024)

        try {
            bmp = BitmapFactory.decodeFile(filePath, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }

        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)

        val canvas = Canvas(scaledBitmap!!)
        canvas.matrix = scaleMatrix
        canvas.drawBitmap(bmp, middleX - bmp!!.width / 2, middleY - bmp.height / 2, Paint(Paint.FILTER_BITMAP_FLAG))


        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath)

            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            when (orientation) {
                6 -> {
                    matrix.postRotate(90f)
                    Log.d("EXIF", "Exif: $orientation")
                }
                3 -> {
                    matrix.postRotate(180f)
                    Log.d("EXIF", "Exif: $orientation")
                }
                8 -> {
                    matrix.postRotate(270f)
                    Log.d("EXIF", "Exif: $orientation")
                }
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val out: FileOutputStream?
        val filename = getFilename()
        try {
            out = FileOutputStream(filename)
            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return filename
    }

    private fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().path, "MyFolder/Images")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"
    }

    private fun validate() {
        var programSelect = ""
        val noticeType: String = notice_type_spinner.selectedItem.toString()
        val title: String = et_title.text.toString().trim()
        val audienceType: String = audience_type.selectedItem.toString()
        if (type != "") {
            programSelect = program.selectedItem.toString()
        }
        val description: String = et_description.text.toString().trim()
        when {
            noticeType == resources.getString(R.string.select_notice_type) -> displayToast(resources.getString(R.string.please_select_notice_type))
            title.isEmpty() -> displayToast(getString(R.string.enter_title))
            title.startsWith(" ") -> displayToast(getString(R.string.white_space_not_allowed_msg))
            audienceType == resources.getString(R.string.select_audience_type) -> displayToast(resources.getString(R.string.please_select_audience_type))
            programSelect == resources.getString(R.string.select_program_msg) -> displayToast(resources.getString(R.string.select_program_msg))
            programSelect == resources.getString(R.string.select_class_msg) -> displayToast(resources.getString(R.string.select_class_msg))
            description.isEmpty() -> displayToast(resources.getString(R.string.description_msg))
            description.startsWith(" ") -> displayToast(resources.getString(R.string.white_space_not_allowed_msg))
            else -> submit()
        }
    }

    private fun submit() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val file = File(photoPath)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body: MultipartBody.Part?
            val call: Call<BasePojo>?
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            if (photoPath != null && photoPath != "") {
                body = MultipartBody.Part.createFormData("profile_pic", file.name, requestFile)
                call = apiService.addNoticewithAttachement(getMultiPartData(getFromPrefs(TeacherConstant.SCHOOL_ID).toString()),
                        getMultiPartData(notice_type), getMultiPartData(audience_type_select),
                        getMultiPartData(et_title.text.toString().trim()), getMultiPartData(et_description.text.toString().trim()),
                        getMultiPartData(getFromPrefs(TeacherConstant.ID).toString()), body, getMultiPartData(program_type_select), getMultiPartData(getTeacherName()))
            } else {
                call = apiService.addNotification(getFromPrefs(TeacherConstant.SCHOOL_ID).toString(),
                        notice_type, audience_type_select,
                        et_title.text.toString().trim(), et_description.text.toString().trim(),
                        getFromPrefs(TeacherConstant.ID).toString(), program_type_select, getTeacherName())
            }

            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            TeacherConstant.TASK_REFRESH = true
                            photoPath = ""
                            finish()
                        }
                        displayToast(response.body()!!.message)
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun getMultiPartData(value: String): RequestBody {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), value)
    }

    private val audienceSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            val selectAudienceType = audience_type.selectedItem.toString()
            when (selectAudienceType) {
                resources.getString(R.string.classes) -> {
                    audience_type_select = "2"
                    type = "1"
                    llProgram.visibility = View.VISIBLE
                    getAudienceType(type)
                }
                resources.getString(R.string.program) -> {
                    audience_type_select = "1"
                    type = "0"
                    llProgram.visibility = View.VISIBLE
                    getAudienceType(type)
                }
                else -> {
                    audience_type_select = "3"
                    type = ""
                    llProgram.visibility = View.GONE
                }
            }

        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val noticeTypeSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = notice_type_list[position]
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return notice_type_list[position]
        }

        override fun getCount(): Int {
            return notice_type_list.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = notice_type_list[position]
            return convertView as View
        }
    }

    private val audienceTypeSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = audience_type_list[position]
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return audience_type_list[position]
        }

        override fun getCount(): Int {
            return audience_type_list.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = audience_type_list[position]
            return convertView as View
        }
    }

    private fun getAudienceType(type: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AudienceTypePojo> = apiService.getAudienceType(type, getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<AudienceTypePojo> {
                override fun onResponse(call: Call<AudienceTypePojo>, response: Response<AudienceTypePojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            program_list.clear()
                            val audienceTypeDataPojo = AudienceTypeDataPojo()
                            if (type == "1") {
                                audienceTypeDataPojo.id = ("-1")
                                audienceTypeDataPojo.classes = resources.getString(R.string.select_class_msg)
                            } else {
                                audienceTypeDataPojo.id = ("-1")
                                audienceTypeDataPojo.programs = resources.getString(R.string.select_program_msg)
                            }
                            program_list = response.body()!!.data
                            program_list.add(0, audienceTypeDataPojo)
                            program.adapter = programSpinnerAdapter
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<AudienceTypePojo>, t: Throwable) {
                    // Log error here since request failed
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val programSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            if (type == "0")
                ftext!!.text = program_list[position].programs
            else {
                if (program_list[position].classes == resources.getString(R.string.select_class_msg)) {
                    ftext!!.text = program_list[position].classes
                } else {
                    ftext!!.text = program_list[position].classes + "-" + program_list[position].programs
                }
            }

            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return if (type == "0")
                program_list[position].programs
            else {
                if (program_list[position].classes == resources.getString(R.string.select_class_msg)) {
                    program_list[position].classes
                } else {
                    program_list[position].classes + "-" + program_list[position].programs
                }
            }
        }

        override fun getCount(): Int {
            return program_list.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            if (type == "0")
                ftext!!.text = program_list[position].programs
            else {
                if (program_list[position].classes == resources.getString(R.string.select_class_msg)) {
                    ftext!!.text = program_list[position].classes
                } else {
                    ftext!!.text = program_list[position].classes + "-" + program_list[position].programs
                }
            }


            return convertView as View
        }
    }

    private inner class ImageAdapter : RecyclerView.Adapter<AddNotificationActivity.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddNotificationActivity.ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.imagelistrow, parent, false)
            return AddNotificationActivity.ViewHolder(v)
        }

        override fun getItemCount(): Int {
            return map.size
        }

        override fun onBindViewHolder(holder: AddNotificationActivity.ViewHolder, position: Int) {
            holder.bindItems(map, ctx, position)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(leave_list: ArrayList<Bitmap>, ctx: AddNotificationActivity, position: Int) {
            val ivProfilePic: ImageView = itemView.findViewById(R.id.images)
            val deleteImage: ImageView = itemView.findViewById(R.id.ic_delete)

            ivProfilePic.setImageBitmap(leave_list[position])
            deleteImage.setTag(R.string.pos, position)
            deleteImage.setOnClickListener(ctx)
        }
    }
}