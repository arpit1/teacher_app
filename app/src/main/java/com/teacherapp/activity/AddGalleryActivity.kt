package com.teacherapp.activity

import android.Manifest
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.teacherapp.R
import com.teacherapp.pojo.*
import com.teacherapp.util.*
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.mime.MultipartEntity
import org.apache.http.entity.mime.content.ByteArrayBody
import org.apache.http.entity.mime.content.FileBody
import org.apache.http.entity.mime.content.StringBody
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.protocol.BasicHttpContext
import org.apache.http.util.EntityUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AddGalleryActivity : BaseActivity(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private lateinit var category_spinner: Spinner
    private lateinit var mediatype_spinner: Spinner
    private lateinit var upload_btn: TextView
    private lateinit var gallery_media: RecyclerView
    var list_category = arrayOf("Event", "Activities")
    var select_type = arrayOf("Image", "Video")
    val REQCODE = 100
    private var ctx: AddGalleryActivity = this
    private lateinit var selectedVideos: ArrayList<String>
    private lateinit var adapter: VideoAdapter
    private lateinit var adapter_img: ImageAdapter
    private var et_type = "Event"
    private var media_type = "Image"
    private lateinit var tv_submit: TextView
    private lateinit var imagesUriList: ArrayList<Uri>
    private lateinit var encodedImageList: ArrayList<String>
    private lateinit var image_gallery: ArrayList<image_galleryPojo>
    private lateinit var video_gallery: ArrayList<video_galleryPojo>
    private lateinit var imageURI: String
    private lateinit var map: ArrayList<Bitmap>
    private lateinit var newImages: ArrayList<Bitmap>
    private lateinit var newVideo: ArrayList<String>
    private lateinit var tv_date: TextView
    private lateinit var et_album_name: EditText
    private lateinit var et_descriptions: EditText
    private lateinit var prefs: TeacherPreference
    private lateinit var user_id: String
    private lateinit var media_id: String
    private var count: Int = 0
    private var calendar: Calendar? = null
    private lateinit var albumDataPojo: GalleryPojoData
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    internal var entity: MultipartEntity? = null
    private var getfrom: String = "new"
    private var album_id: String = "0"
    private lateinit var d: ProgressDialog
    private val sb = StringBuilder()
    private val READ_EXTERNAL_STORAGE = 101
    private var permissionsToRequest: ArrayList<String>? = null
    private var permissionsRejected: ArrayList<String>? = null
    private var sharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_gallery)
        TeacherConstant.ACTIVITIES.add(this)
        init()
        setListener()
        setHeaderTitle(resources.getString(R.string.add_gallery))
        TeacherConstant.ACTIVITIES.add(this)
    }

    private fun setListener() {
        category_spinner.onItemSelectedListener = this
        mediatype_spinner.onItemSelectedListener = this

        tv_date.setOnClickListener {
            showDatePicker()
        }
    }

    private fun showDatePicker() {
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar?.set(year, month, day)
            this.year = year
            this.month = month
            this.day = day
            updateLabel()
        }, year, month, day)
        dialog.datePicker.maxDate = System.currentTimeMillis()
        dialog.show()
    }

    private fun updateLabel() {
        val myFormat = getString(R.string.date_formet_year)//In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tv_date.text = sdf.format(calendar!!.time)
    }

    private fun init() {
        sharedPreferences = getSharedPreferences(TeacherConstant.PREF_NAME, Context.MODE_PRIVATE)
        category_spinner = findViewById(R.id.categorys_spinner)
        mediatype_spinner = findViewById(R.id.mediatype_spinner)
        gallery_media = findViewById(R.id.gallery_images)
        upload_btn = findViewById(R.id.tv_upload_file)
        tv_date = findViewById(R.id.tv_date)
        et_descriptions = findViewById(R.id.et_description)
        et_descriptions.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.et_description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        et_album_name = findViewById(R.id.txt_album_name)
        map = ArrayList()
        selectedVideos = ArrayList()
        encodedImageList = ArrayList()
        image_gallery = ArrayList()
        video_gallery = ArrayList()
        tv_submit = findViewById(R.id.tv_submit)
        newImages = ArrayList()
        newVideo = ArrayList()

        prefs = TeacherPreference(this)
        user_id = getFromPrefs(TeacherConstant.ID).toString()

        gallery_media.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        adapter_img = ImageAdapter()

        gallery_media.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        adapter = VideoAdapter()

        calendar = Calendar.getInstance()
        val c = Calendar.getInstance()
        year = c.get(Calendar.YEAR)
        month = c.get(Calendar.MONTH)
        day = c.get(Calendar.DAY_OF_MONTH)

        val categoryAdapter = ArrayAdapter(this, R.layout.media_type_row, list_category)
        category_spinner.adapter = categoryAdapter

        val selectType = ArrayAdapter(this, R.layout.media_type_row, select_type)
        mediatype_spinner.adapter = selectType

        category_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        mediatype_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
        if (prefs.getSelectOption() == "Event")
            category_spinner.setSelection(0)
        else
            category_spinner.setSelection(1)
        upload_btn.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                addPermissionDialogMarshMallow()
            } else {
                chooseOption()
            }

        }
        val intent = intent
        if (intent.getStringExtra("from") == "edit") {
            albumDataPojo = intent.getSerializableExtra("edit_gallery") as GalleryPojoData
            getfrom = "edit"
            album_id = albumDataPojo.id

            val date = albumDataPojo.dates.split("-")
            year = date[0].toInt()
            month = date[1].toInt() - 1
            day = date[2].toInt()
            tv_date.text = albumDataPojo.dates
            tv_date.isFocusableInTouchMode = false
            tv_date.isClickable = false
            category_spinner.isFocusableInTouchMode = false
            category_spinner.isClickable = false
            category_spinner.isEnabled = false
            mediatype_spinner.isFocusableInTouchMode = false
            mediatype_spinner.isClickable = false
            mediatype_spinner.isEnabled = false

            tv_submit.text = resources.getString(R.string.update_btn)

            et_descriptions.setText(albumDataPojo.discussion)
            et_album_name.setText(albumDataPojo.event_name)
            map = ArrayList()
            selectedVideos = ArrayList()

            for (i in 0 until albumDataPojo.file!!.size) {
                var imagepath: String?
                if (albumDataPojo.upload_type == "Image") {
                    imagepath = TeacherConstant.IMAGE_URl + albumDataPojo.file!![i].files
                    media_type = "Image"
                    mediatype_spinner.setSelection(0)

                    val imageGalleryPojo = image_galleryPojo()
                    imageGalleryPojo.media_id = albumDataPojo.file!![i].media_id
                    imageGalleryPojo.file = imagepath
                    imageGalleryPojo.image_type = "0"
                    image_gallery.add(imageGalleryPojo)

                } else {
                    media_type = "Video"
                    mediatype_spinner.setSelection(1)
                    imagepath = TeacherConstant.THUMB_IMAGE_URl + albumDataPojo.file!![i].thumbnail
                    val imageGalleryPojo = video_galleryPojo()
                    imageGalleryPojo.media_id = albumDataPojo.file!![i].media_id
                    imageGalleryPojo.file = albumDataPojo.file!![i].files
                    imageGalleryPojo.thumbnail = imagepath
                    imageGalleryPojo.image_type = "0"
                    video_gallery.add(imageGalleryPojo)
                }
            }
            if (albumDataPojo.upload_type == "Image") {
                gallery_media.adapter = adapter_img
            } else {
                gallery_media.adapter = adapter
            }
        }
        tv_submit.setOnClickListener {
            validate()
        }
    }

    private fun chooseOption() {
        media_type = mediatype_spinner.selectedItem.toString()
        if (media_type == "Video") {
            val intent = Intent()
            intent.type = "video/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Choose application"), REQCODE)
        } else {
            val intent = Intent()
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Choose application"), REQCODE)
        }
    }
//READ PERMISSION

    //show permission dialog
    private fun addPermissionDialogMarshMallow() {
        val permissions = ArrayList<String>()
        val resultCode: Int = READ_EXTERNAL_STORAGE

        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.CAMERA)
//        permissions.add(Manifest.permission.RECORD_AUDIO)

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions)
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest!!.size > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                //mark all these as asked..
                for (perm in permissionsToRequest!!) {
                    markAsAsked(perm, sharedPreferences!!)
                }
            } else {
                //show the success banner
                if (permissionsRejected!!.size < permissions.size) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    chooseOption()
                }

                if (permissionsRejected!!.size > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
                    //mark all these as asked..
                    for (perm in permissionsToRequest!!) {
                        markAsAsked(perm, sharedPreferences!!)
                    }
                }
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(args: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (args!!.id) {
            R.id.categorys_spinner -> {
                val selectType = args.getItemAtPosition(position).toString()
                et_type = selectType
            }
            R.id.mediatype_spinner -> {
                media_type = args.getItemAtPosition(position).toString()
                if (media_type == "Video") {
                    map.clear()
                    image_gallery.clear()
                    gallery_media.adapter = adapter
                    gallery_media.adapter.notifyDataSetChanged()

                } else {
                    selectedVideos.clear()
                    video_gallery.clear()
                    gallery_media.adapter = adapter_img
                }
                adapter_img.notifyDataSetChanged()
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun validate() {
        val edDate = tv_date.text.toString()
        val edAlbumName = et_album_name.text.toString().trim()
        val description = et_descriptions.text.toString().trim()
        if (edDate.isEmpty()) {
            displayToast(resources.getString(R.string.select_date))
        } else if (edAlbumName.isEmpty()) {
            displayToast(getString(R.string.AlbumName))
        } else if (edAlbumName.startsWith(" ")) {
            displayToast(getString(R.string.white_space_not_allowed_msg))
        } else if (description.isEmpty()) {
            displayToast(getString(R.string.enter_description))
        } else if (description.startsWith(" ")) {
            displayToast(getString(R.string.white_space_not_allowed_msg))
        } else if (video_gallery.size == 0 && media_type == "Video") {
            displayToast(getString(R.string.select_media))
        } else if (image_gallery.size == 0 && media_type == "Image") {
            displayToast(getString(R.string.select_media))
        } else {
            if (sb.isNotEmpty()) {
                val str = sb.deleteCharAt(sb.length - 1)
                deleteImage(str.toString())
            }
            submitMedia(edDate, edAlbumName, description, album_id)
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (resultCode == RESULT_OK) {
                if (media_type == "Video") {
                    map = ArrayList()
                    map.clear()
                    getSelectedVideos(/*requestCode, */data!!)
                } else {
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    imagesUriList = ArrayList()
                    video_gallery = ArrayList()
                    encodedImageList.clear()
                    if (data!!.data != null) {
                        val mImageUri = data.data
                        // Get the cursor
                        val cursor = contentResolver.query(mImageUri!!, filePathColumn, null, null, null)
                        // Move to first row
                        cursor!!.moveToFirst()
                        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                        if (columnIndex != 0)
                            imageURI = cursor.getString(columnIndex)

                        val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, mImageUri)
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
                        val encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
                        encodedImageList.add(encodedImage)
                        map.add(bitmap)
                        cursor.close()

                        val imageGalleryPojo = image_galleryPojo()
                        imageGalleryPojo.media_id = "0"
                        imageGalleryPojo.file = filePathColumn[0]
                        imageGalleryPojo.image_type = "1"
                        imageGalleryPojo.bitmap = bitmap
                        image_gallery.add(imageGalleryPojo)
                        gallery_media.adapter = adapter_img
                    } else {
                        if (data.clipData != null) {
                            val mClipData = data.clipData
                            val mArrayUri = ArrayList<Uri>()
                            for (i in 0 until mClipData!!.itemCount) {

                                val item = mClipData.getItemAt(i)
                                val uri = item.uri
                                mArrayUri.add(uri)
                                // Get the cursor
                                val cursor = contentResolver.query(uri, filePathColumn, null, null, null)
                                // Move to first row
                                cursor!!.moveToFirst()

                                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                                if (columnIndex != 0)
                                    imageURI = cursor.getString(columnIndex)
                                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                                val byteArrayOutputStream = ByteArrayOutputStream()
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
                                val encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
                                encodedImageList.add(encodedImage)
                                map.add(bitmap)

                                val imageGalleryPojo = image_galleryPojo()
                                imageGalleryPojo.media_id = "0"
                                imageGalleryPojo.file = filePathColumn[0]
                                imageGalleryPojo.image_type = "1"
                                imageGalleryPojo.bitmap = bitmap
                                image_gallery.add(imageGalleryPojo)

                                cursor.close()
                            }
                        }
                        adapter_img.notifyDataSetChanged()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    inner class ImageAdapter : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.imagelistrow, parent, false)
            return ViewHolder(v)
        }

        override fun getItemCount(): Int {
            return image_gallery.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindItemsImg(image_gallery, ctx, position)
        }
    }

    private fun submitMedia(ed_date: String, ed_albumName: String, description: String, album_id: String) {
        if (ConnectionDetector.isConnected(this)) {
            d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<section_pojo> = apiService.addgallery(user_id, getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), et_type, ed_albumName, media_type, ed_date, description, album_id)
            call.enqueue(object : Callback<section_pojo> {
                override fun onResponse(call: Call<section_pojo>, response: Response<section_pojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            TeacherConstant.GALLERY_REFRESH = true
                            media_id = response.body()!!.data
                            val filterPojo = FilterPojo()
                            prefs.setFilterData(filterPojo)
                            if (media_type == "Video") {
                                for (i in 0 until video_gallery.size) {
                                    if (video_gallery[i].image_type == "1") {
                                        newVideo.add(video_gallery[i].file)
                                    }
                                }

                                if (newVideo.size != 0) {
                                    VideoUploadTask().execute(count.toString() + "", "pk$count.mp4")
                                } else {
                                    d.dismiss()
                                    finish()
                                }
                            } else {
                                for (i in 0 until image_gallery.size) {
                                    if (image_gallery[i].image_type == "1") {
                                        newImages.add(image_gallery[i].bitmap)
                                    }
                                }
                                if (newImages.size != 0) {
                                    ImageUploadTask().execute(count.toString() + "", "pk$count.jpg")
                                } else {
                                    d.dismiss()
                                    finish()
                                }
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<section_pojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun deleteImage(ids: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.deleteAttachment(ids)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            d.dismiss()
                        }
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    inner class VideoAdapter : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.imagelistrow, parent, false)
            return ViewHolder(v)
        }

        override fun getItemCount(): Int {
            return video_gallery.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindItems(video_gallery, ctx, position)
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(video_list: ArrayList<video_galleryPojo>, ctx: AddGalleryActivity, position: Int) {
            val iv_profile_pic: ImageView = itemView.findViewById(R.id.images)
            val delete_image: ImageView = itemView.findViewById(R.id.ic_delete)

            if (video_list[position].image_type == "0") {
                ctx.setHomePostImageInLayout(ctx, video_list[position].thumbnail, iv_profile_pic)
            } else {
                val bmThumbnail: Bitmap = ThumbnailUtils.createVideoThumbnail(video_list[position].file, MediaStore.Video.Thumbnails.MICRO_KIND)
                iv_profile_pic.setImageBitmap(bmThumbnail)
            }
            delete_image.setTag(R.string.pos, position)
            delete_image.setOnClickListener(ctx)
        }

        fun bindItemsImg(imageList: ArrayList<image_galleryPojo>, ctx: AddGalleryActivity, position: Int) {
            val iv_profile_pic: ImageView = itemView.findViewById(R.id.images)
            val delete_image: ImageView = itemView.findViewById(R.id.ic_delete)

            if (imageList[position].image_type == "0") {
                ctx.setHomePostImageInLayout(ctx, imageList[position].file, iv_profile_pic)
            } else {
                iv_profile_pic.setImageBitmap(imageList[position].bitmap)
            }
            delete_image.setTag(R.string.pos, position)
            delete_image.setOnClickListener(ctx)
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ic_delete -> {
                val pos = v.getTag(R.string.pos) as Int
                if (media_type == "Image") {
                    if (image_gallery[pos].media_id != "0") {
                        sb.append(image_gallery[pos].media_id)
                        sb.append(",")
                    }
                    image_gallery.removeAt(pos)
                    adapter_img.notifyDataSetChanged()
                } else {
                    if (video_gallery[pos].media_id != "0") {
                        sb.append(video_gallery[pos].media_id)
                        sb.append(",")
                    }
                    video_gallery.removeAt(pos)
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun getSelectedVideos(/*requestCode: Int, */data: Intent) {

        selectedVideos = ArrayList()

        val clipData = data.clipData
        if (clipData != null) {
            for (i in 0 until clipData.itemCount) {
                val videoItem = clipData.getItemAt(i)
                val videoURI = videoItem.uri
                val filePath = getPath(this, videoURI)
                // selectedVideos.add(filePath!!)

                val imageGalleryPojo = video_galleryPojo()
                imageGalleryPojo.media_id = "0"
                imageGalleryPojo.file = filePath!!
                imageGalleryPojo.thumbnail = ""
                imageGalleryPojo.image_type = "1"
                video_gallery.add(imageGalleryPojo)
            }
        } else {
            val videoURI = data.data
            val filePath = getPath(this, videoURI!!)

            val imageGalleryPojo = video_galleryPojo()
            imageGalleryPojo.media_id = "0"
            imageGalleryPojo.file = filePath!!
            imageGalleryPojo.thumbnail = ""
            imageGalleryPojo.image_type = "1"
            video_gallery.add(imageGalleryPojo)

            // selectedVideos.add(filePath!!)
        }
        gallery_media.adapter = adapter
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun getPath(context: Context, uri: Uri): String? {

        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

            } else if (isDownloadsDocument(uri)) {

                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)!!)

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])
                return getDataColumn(context, contentUri!!, selection, selectionArgs)
            }
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)

        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun getDataColumn(context: Context, uri: Uri, selection: String?, selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)
        try {
            cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    internal inner class ImageUploadTask : AsyncTask<String, Void, String>() {

        private var sResponse: String? = null
        private var bitmap: Bitmap? = null

        override fun doInBackground(vararg params: String): String? {
            try {
                val url = TeacherConstant.BASE_URL + "addgalleryfiles"
                val i = Integer.parseInt(params[0])
                bitmap = newImages[i]
                val httpClient = HttpClientBuilder.create().build()
                val localContext = BasicHttpContext()
                val httpPost = HttpPost(url)
                entity = MultipartEntity()

                val bos = ByteArrayOutputStream()
                bitmap = Bitmap.createScaledBitmap(bitmap, bitmap!!.width / 2, bitmap!!.height / 2, true)
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 50, bos)

                val data = bos.toByteArray()
                entity!!.addPart("media_id", StringBody(media_id))
                entity!!.addPart("user_id", StringBody(user_id))
                entity!!.addPart("upload_type", StringBody("Image"))
                entity!!.addPart("file", ByteArrayBody(data, "image/jpeg", params[1]))

                httpPost.entity = entity
                val response = httpClient.execute(httpPost, localContext)
                sResponse = EntityUtils.getContentCharSet(response.entity)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return sResponse
        }

        override fun onPostExecute(sResponse: String?) {
            try {
                if (sResponse != null) {
                    count++
                    if (count < newImages.size) {
                        ImageUploadTask().execute(count.toString() + "", "hm$count.jpg")
                    } else {
                        d.dismiss()
                        Toast.makeText(applicationContext, resources.getString(R.string.upload_successfully), Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            } catch (e: Exception) {
                Toast.makeText(applicationContext, e.message,
                        Toast.LENGTH_LONG).show()
                Log.e(e.javaClass.name, e.message, e)
            }
        }
    }

    internal inner class VideoUploadTask : AsyncTask<String, Void, String>() {

        private var sResponse: String? = null
        private lateinit var path: String

        override fun doInBackground(vararg params: String): String? {
            try {
                val url = TeacherConstant.BASE_URL + "addgalleryfiles"
                val i = Integer.parseInt(params[0])
                /* val bitmap = map[i]*/
                path = newVideo[i]
                val httpClient = HttpClientBuilder.create().build()
                val localContext = BasicHttpContext()
                val httpPost = HttpPost(url)
                entity = MultipartEntity()

                val file2 = File(path)
                val bin2 = FileBody(file2)
                //bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
                entity!!.addPart("media_id", StringBody(media_id))
                entity!!.addPart("user_id", StringBody(user_id))
                entity!!.addPart("upload_type", StringBody("video"))
                entity!!.addPart("file", bin2)

                httpPost.entity = entity
                val response = httpClient.execute(httpPost, localContext)
                sResponse = EntityUtils.getContentCharSet(response.entity)
            } catch (e: Exception) {

            }
            return sResponse
        }

        override fun onPostExecute(sResponse: String?) {
            try {
                if (sResponse != null) {
                    count++
                    if (count < newVideo.size) {
                        VideoUploadTask().execute(count.toString() + "", "hm$count.mp4")
                    } else {
                        d.dismiss()
                        Toast.makeText(applicationContext, resources.getString(R.string.upload_successfully), Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            } catch (e: Exception) {
                Toast.makeText(applicationContext, e.message,
                        Toast.LENGTH_LONG).show()
                Log.e(e.javaClass.name, e.message, e)
            }
        }
    }
}