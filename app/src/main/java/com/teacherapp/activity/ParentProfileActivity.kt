package com.teacherapp.activity

import android.os.Bundle
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.ParentProfilePojo
import com.teacherapp.util.*
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 4/12/2018.
 */
class ParentProfileActivity : BaseActivity(){

    private lateinit var fatherName : TextView
    private lateinit var father_email : TextView
    private lateinit var father_phone : TextView
    private lateinit var father_occup : TextView
    private lateinit var father_work_place : TextView
    private lateinit var mother_name : TextView
    private lateinit var mother_email : TextView
    private lateinit var mother_phone : TextView
    private lateinit var mother_occup : TextView
    private lateinit var mother_work_place : TextView
    private lateinit var student_id : TextView
    private lateinit var student_passportno : TextView
    private lateinit var student_address : TextView
    private lateinit var student_country : TextView
    private lateinit var tv_emergency_name : TextView
    private lateinit var tv_emergency_no : TextView
    private lateinit var studentPic : CircleImageView
    private lateinit var studentName : TextView
    private lateinit var grade : TextView
    private lateinit var birth_date : TextView
    private lateinit var gender : TextView
    private lateinit var program : TextView
    private lateinit var phone : TextView
    private lateinit var batch : TextView
    private lateinit var email : TextView
    private var ctx : ParentProfileActivity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_profile)
        initialize()
    }

    private fun initialize() {
        setHeaderTitle(resources.getString(R.string.parent_profile))
        fatherName = findViewById(R.id.fatherName)
        father_email = findViewById(R.id.father_email)
        father_phone = findViewById(R.id.father_phone)
        father_occup = findViewById(R.id.father_occup)
        father_work_place = findViewById(R.id.father_work_place)
        mother_name = findViewById(R.id.mother_name)
        mother_email = findViewById(R.id.mother_email)
        mother_phone = findViewById(R.id.mother_phone)
        mother_occup = findViewById(R.id.mother_occup)
        mother_work_place = findViewById(R.id.mother_work_place)
        student_id = findViewById(R.id.student_id)
        student_passportno = findViewById(R.id.student_passportno)
        student_address = findViewById(R.id.student_address)
        student_country = findViewById(R.id.student_country)
        tv_emergency_name = findViewById(R.id.tv_emergency_name)
        tv_emergency_no = findViewById(R.id.tv_emergency_no)
        studentPic = findViewById(R.id.studentPic)
        studentName = findViewById(R.id.studentName)
        grade = findViewById(R.id.grade)
        birth_date = findViewById(R.id.birth_date)
        gender = findViewById(R.id.gender)
        program = findViewById(R.id.program)
        phone = findViewById(R.id.phone)
        batch = findViewById(R.id.batch)
        email = findViewById(R.id.email)
        if(intent.getStringExtra("user_id") != null)
            getParentProfile(intent.getStringExtra("user_id"))
    }

    private fun getParentProfile(user_id : String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ParentProfilePojo> = apiService.getParentProfile(user_id)
            call.enqueue(object : Callback<ParentProfilePojo> {
                override fun onResponse(call: Call<ParentProfilePojo>, response: Response<ParentProfilePojo>) {
                    if (response != null) {
                        if (response.body()!!.status == "1" && response.body()!!.data != null) {
                            val parentData = response.body()!!.data
                            fatherName.text = parentData!!.father_name
                            father_email.text = parentData.father_email
                            father_phone.text = parentData.father_phone
                            father_occup.text = parentData.father_occupation
                            father_work_place.text = parentData.father_place_of_work
                            mother_name.text = parentData.mother_name
                            mother_email.text = parentData.mother_email
                            mother_phone.text = parentData.mother_phone
                            mother_occup.text = parentData.mother_occupation
                            mother_work_place.text = parentData.mother_place_of_work
                            student_id.text = parentData.students!!.id
                            student_passportno.text = parentData.passport_number
                            if (parentData.students!!.address_line2 == "" && parentData.students!!.address_line3 == "") {
                                student_address.text = parentData.students!!.address_line1 + ", " + parentData.students!!.city + ", " + parentData.students!!.state + ", " + parentData.students!!.pincode
                            } else if (parentData.students!!.address_line2 != "") {
                                student_address.text = parentData.students!!.address_line1 + ", " + parentData.students!!.address_line2 + ", " +
                                        parentData.students!!.city + ", " + parentData.students!!.state + ", " + parentData.students!!.pincode
                            } else if (parentData.students!!.address_line3!= "") {
                                student_address.text = parentData.students!!.address_line1 + ", " + parentData.students!!.address_line3 +
                                        ", " + parentData.students!!.city + ", " + parentData.students!!.state + ", " + parentData.students!!.pincode
                            }
                            student_country.text = parentData.country_name
                            tv_emergency_name.text = parentData.students!!.emergency_contact_name
                            tv_emergency_no.text = parentData.students!!.emergency_contact_number
                            if(parentData.students!!.student_mname == ""){
                                studentName.text = parentData.students!!.student_fname+" "+parentData.students!!.student_lname
                            }else{
                                studentName.text = parentData.students!!.student_fname + " " +
                                        parentData.students!!.student_mname + " " + parentData.students!!.student_lname
                            }
                            grade.text = parentData.students!!.classes
                            birth_date.text = parentData.students!!.date_of_birth
                            gender.text = parentData.students!!.gender
                            program.text = parentData.students!!.programs
                            phone.text = parentData.students!!.student_phone
                            batch.text = parentData.students!!.batch_name
                            email.text = parentData.students!!.student_email
                            if (parentData.students!!.profile_pic != null && parentData.students!!.profile_pic != "") {
                                setProfileImageInLayout(ctx, TeacherConstant.STUDENT_PROFILE_BASE_URL + parentData.students!!.profile_pic,studentPic)
                            } else {
                                studentPic.setImageResource(R.mipmap.usericon)
                            }
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<ParentProfilePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}