package com.teacherapp.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.pojo.AddFeedbackCommentPojo
import com.teacherapp.R
import com.teacherapp.adapter.AllCommentDiaryAdapter
import com.teacherapp.pojo.AllCommentPojo
import com.teacherapp.pojo.BasePojo
import com.teacherapp.pojo.EventCommentDataPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SelectDiaryCommentActivity : BaseActivity(), View.OnClickListener {

    private var ctx: SelectDiaryCommentActivity = this
    private lateinit var rvCommentList: RecyclerView
    private lateinit var etComment: TextView
    private lateinit var sendComment: ImageView
    private lateinit var commentList: ArrayList<EventCommentDataPojo>
    private lateinit var commentLayoutManager: LinearLayoutManager
    private var commentData: EventCommentDataPojo? = null
    private var adapter: AllCommentDiaryAdapter? = null
    private lateinit var feedback_comment: EventCommentDataPojo
    private var pos: Int = 0
    private var from_comment: String = "Add"
    private lateinit var noDataAvailable: TextView
    private var parentId : String? = null
    private var diaryId : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_comment_diary)
        initialize()
        setListener()
    }

    private fun initialize() {
        commentData = intent.getSerializableExtra("comment_list") as EventCommentDataPojo?
        if(commentData != null) {
            setHeaderTitle(commentData!!.father_name)
            diaryId = commentData!!.diary_id
            parentId = commentData!!.parent_id
        } else {
            setHeaderTitle(intent.getStringExtra("name"))
            diaryId = intent.getStringExtra("diary_id")
            parentId = intent.getStringExtra("parent_id")
        }
        commentLayoutManager = LinearLayoutManager(ctx)
        rvCommentList = findViewById(R.id.rvCommentList)
        etComment = findViewById(R.id.etComment)
        sendComment = findViewById(R.id.sendComment)
        noDataAvailable = findViewById(R.id.noDataAvailable)
        rvCommentList.layoutManager = commentLayoutManager
        commentList = ArrayList()
        getAllComment()
    }

    private fun setListener() {
        sendComment.setOnClickListener {
            hideSoftKeyboard()
            val comment = etComment.text.toString().trim()
            if (comment.isEmpty()) {
                displayToast(resources.getString(R.string.comment_msg))
            } else {
                if (from_comment == "Edit")
                    editDiaryComment(feedback_comment.id, pos)
                else
                    addFeedbackComment()
            }
        }
    }

    private fun addFeedbackComment() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AddFeedbackCommentPojo> = apiService.addDairyComment(diaryId!!, ctx.getFromPrefs(TeacherConstant.SCHOOL_ID).toString(),
                    getFromPrefs(TeacherConstant.ID).toString(),
                    etComment.text.toString().trim(), "", parentId!!, getTeacherName())
            call.enqueue(object : Callback<AddFeedbackCommentPojo> {
                override fun onResponse(call: Call<AddFeedbackCommentPojo>, response: Response<AddFeedbackCommentPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1") {
                            displayToast(response.body()!!.message)
                            addDataInPojo(response.body()!!, commentList.size)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AddFeedbackCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun editDiaryComment(comment_id: String, pos: Int) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AddFeedbackCommentPojo> = apiService.addDairyComment(diaryId!!, ctx.getFromPrefs(TeacherConstant.SCHOOL_ID).toString(),
                    getFromPrefs(TeacherConstant.ID).toString(),
                    etComment.text.toString().trim(), comment_id, parentId!!, getTeacherName())
            call.enqueue(object : Callback<AddFeedbackCommentPojo> {
                override fun onResponse(call: Call<AddFeedbackCommentPojo>, response: Response<AddFeedbackCommentPojo>?) {
                    if (response != null) {

                        if (response.body()!!.status == "1") {
                            from_comment = "Add"
                            displayToast(response.body()!!.message)
                            addDataInPojo(response.body()!!, pos)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AddFeedbackCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun addDataInPojo(obj: AddFeedbackCommentPojo, position: Int) {
        val commentDataPojo = EventCommentDataPojo()
        commentDataPojo.id = obj.data.id
        commentDataPojo.school_id = obj.data.school_id
        commentDataPojo.diary_id = obj.data.diary_id
        commentDataPojo.teacher_id = obj.data.teacher_id
        commentDataPojo.parent_id = obj.data.parent_id
        commentDataPojo.comment = etComment.text.toString().trim()
        commentDataPojo.type = obj.data.type
        commentDataPojo.created_at = obj.data.created_at
        commentDataPojo.t_name = getTeacherName()
        commentDataPojo.t_profile_pic = getFromPrefs(TeacherConstant.IMAGE).toString()
        commentDataPojo.p_name = obj.data.p_name
        commentDataPojo.p_profile_pic = obj.data.p_profile_pic
        if (commentList.size == position + 1)
            commentList[position] = commentDataPojo
        else
            commentList.add(commentDataPojo)
        etComment.text = ""
        rvCommentList.visibility = View.VISIBLE
        if (adapter == null) {
            adapter = AllCommentDiaryAdapter(ctx, commentList)
            rvCommentList.adapter = adapter
        }
        adapter!!.notifyDataSetChanged()
        rvCommentList.scrollToPosition(commentList.size - 1)
    }

    private fun getAllComment() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AllCommentPojo> = apiService.getAllComment(diaryId!!, getFromPrefs(TeacherConstant.ID).toString(), parentId!!)
            call.enqueue(object : Callback<AllCommentPojo> {
                override fun onResponse(call: Call<AllCommentPojo>, response: Response<AllCommentPojo>?) {

                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            commentList = response.body()!!.comment
                            adapter = AllCommentDiaryAdapter(ctx, commentList)
                            rvCommentList.adapter = adapter
                            rvCommentList.scrollToPosition(commentList.size - 1)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AllCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.ivEdit -> {
                    editFunction(v)
                }
                R.id.ivDelete -> {
                    feedback_comment = v.getTag(R.string.edit_msg) as EventCommentDataPojo
                    pos = v.getTag(R.string.edit_pos) as Int
                    val builder = AlertDialog.Builder(ctx)
                    builder.setCancelable(false)
                    builder.setMessage(ctx.resources.getString(R.string.appdelete))
                    builder.setPositiveButton("Yes", { _, _ ->
                        feedbackDelete(ctx, feedback_comment.id, pos)
                    })
                    builder.setNegativeButton("No", { dialog, _ ->
                        dialog.cancel()
                    })
                    val alert = builder.create()
                    alert.show()
                }
            }
        }

    }

    private fun feedbackDelete(feedback_ctx: SelectDiaryCommentActivity, id: String, position: Int) {
        if (ConnectionDetector.isConnected(feedback_ctx)) {
            val d = TeacherDialog.showLoading(feedback_ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.deleteDiaryComment(id)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ctx.displayToast(response.body()!!.message)
                            commentList.removeAt(position)
                            adapter!!.notifyDataSetChanged()
                            if (commentList.size == 0) {
                                rvCommentList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(feedback_ctx.resources.getString(R.string.no_internet_connection))
        }
    }

    @SuppressLint("NewApi")
    private fun editFunction(view: View?) {
        feedback_comment = view!!.getTag(R.string.edit_msg) as EventCommentDataPojo
        pos = view.getTag(R.string.edit_pos) as Int
        from_comment = view.getTag(R.string.edit_from) as String
        etComment.text = feedback_comment.comment
        etComment.requestFocus()
        //etComment.setSelection(etComment.text.length)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etComment, InputMethodManager.SHOW_IMPLICIT)
    }
}