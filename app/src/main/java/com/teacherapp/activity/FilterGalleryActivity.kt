package com.teacherapp.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.*
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import com.teacherapp.R
import com.teacherapp.pojo.FilterPojo
import com.teacherapp.util.TeacherConstant
import com.teacherapp.util.TeacherPreference
import java.util.*

class FilterGalleryActivity : BaseActivity(), AdapterView.OnItemSelectedListener, OnDateSelectedListener, View.OnClickListener {
    private lateinit var category_spinner: Spinner
    private lateinit var ed_album_name: EditText
    var list_category = arrayOf("Category", "Event", "Activities")
    private lateinit var tv_submit: TextView
    private lateinit var tv_clear: TextView
    private var selected_date: String? = null
    private var category: String? = null
    private var album_name: String? = null
    private lateinit var prefs: TeacherPreference
    private lateinit var filterPojo: FilterPojo
    lateinit var selectDate: HashSet<CalendarDay>
    private lateinit var llStartDate: LinearLayout
    private lateinit var startDate: TextView
    private lateinit var llEndDate: LinearLayout
    private lateinit var endDate: TextView
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private lateinit var calendar: Calendar
    private lateinit var calendarEnd: Calendar
    private var yearEnd: Int = 0
    private var monthEnd: Int = 0
    private var dayEnd: Int = 0
    private var startDateSelect: String = null.toString()
    private var endDateSelect: String = null.toString()
    private var ctx : FilterGalleryActivity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_gallery)

        init()
        setListener()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(args: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (args!!.id) {
            R.id.category_spinner -> {
                category = list_category[position]
            }
        }
    }

    override fun onDateSelected(widget: MaterialCalendarView, date: CalendarDay, selected: Boolean) {
        /*val df = SimpleDateFormat(getString(R.string.date_formet_year), Locale.US)
        selected_date = df.format(date.date)
        calendarView.selectedDate = date
        calendarView.goToNext()
        val handler = Handler()
        handler.postDelayed({
            calendarView.goToPrevious()
        }, 0)*/
    }

    fun setListener() {
       // calendarView.setOnDateChangedListener(this)
        category_spinner.onItemSelectedListener = this
        tv_submit.setOnClickListener(this)
        tv_clear.setOnClickListener(this)
        llStartDate .setOnClickListener {
            showDatePicker()
        }

        llEndDate.setOnClickListener {
            showDatePickerEndDate()
        }
    }

    private fun showDatePicker() {
        val dialog = DatePickerDialog(ctx, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar.set(year, month, day)
            ctx.year = year
            ctx.month = month
            ctx.day = day
            updateLabel(startDate, calendar)
            endDate.text = resources.getString(R.string.end_date)
            startDateSelect = getDateFormat().format(calendar.time)
            endDateSelect = ""
        }, year, month, day)
        dialog.datePicker.maxDate = System.currentTimeMillis() + 1000
        dialog.setTitle(resources.getString(R.string.start_date))
        dialog.show()
    }

    private fun showDatePickerEndDate() {
        val dialog = DatePickerDialog(ctx, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendarEnd.set(year, month, day)
            ctx.yearEnd = year
            ctx.monthEnd = month
            ctx.dayEnd = day
            updateLabel(endDate, calendarEnd)
            endDateSelect = getDateFormat().format(calendarEnd.time)
        }, yearEnd, monthEnd, dayEnd)
        val cal: Calendar = Calendar.getInstance()
        cal.set(year, month, day)
        dialog.datePicker.maxDate = System.currentTimeMillis() - 1000
        dialog.datePicker.minDate = cal.timeInMillis
        dialog.setTitle(resources.getString(R.string.end_date))
        dialog.show()
    }

    private fun updateLabel(textView: TextView, cal: Calendar) {
        textView.text = getDateFormat().format(cal.time)
    }

    fun init() {
        setHeaderTitle(resources.getString(R.string.filter_gallery))
        calendar = Calendar.getInstance()
        calendarEnd = Calendar.getInstance()
        selectDate = HashSet()
        prefs = TeacherPreference(this)
        //calendarView = findViewById(R.id.calendar_view)
        category_spinner = findViewById(R.id.category_spinner)
        ed_album_name = findViewById(R.id.ed_album_name)
        tv_submit = findViewById(R.id.tv_submit)
        tv_clear = findViewById(R.id.tv_clear)
        llStartDate = findViewById(R.id.llStartDate)
        startDate = findViewById(R.id.startDate)
        llEndDate = findViewById(R.id.llEndDate)
        endDate = findViewById(R.id.endDate)
        filterPojo = FilterPojo()
        val categoryAdapter = ArrayAdapter(this, R.layout.media_type_row, list_category)
        category_spinner.adapter = categoryAdapter

        val filterDataPojo = prefs.getFilterData()
        if (filterDataPojo!!.album_name != "")
            ed_album_name.setText(filterDataPojo.album_name)
        if (filterDataPojo.category != "") {
            if (filterDataPojo.category == "Event")
                category_spinner.setSelection(1)
            else
                category_spinner.setSelection(2)
        }
        if(filterDataPojo.start_date != "" && filterDataPojo.start_date != null){
            startDate.text = filterDataPojo.start_date
            val date = filterDataPojo.start_date.split("-")
            day = date[2].toInt()
            month = date[1].toInt() - 1
            year = date[0].toInt()
        }else{
            year = calendar.get(Calendar.YEAR)
            month = calendar.get(Calendar.MONTH)
            day = calendar.get(Calendar.DAY_OF_MONTH)
        }
        if(filterDataPojo.end_date != "" && filterDataPojo.end_date != null){
            endDate.text = filterDataPojo.end_date
            val date = filterDataPojo.end_date.split("-")
            dayEnd = date[2].toInt()
            monthEnd = date[1].toInt() - 1
            yearEnd = date[0].toInt()
        }else{
            yearEnd = calendarEnd.get(Calendar.YEAR)
            monthEnd = calendarEnd.get(Calendar.MONTH)
            dayEnd = calendarEnd.get(Calendar.DAY_OF_MONTH)
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tv_submit -> {
                if (validate()) {
                    if (category.equals("Category")) {
                        category = ""
                    }

                    album_name = ed_album_name.text.toString()

                    filterPojo.selected_date = selected_date.toString()
                    filterPojo.category = category.toString()
                    filterPojo.album_name = album_name.toString()
                    filterPojo.start_date = startDate.text.toString()
                    filterPojo.end_date = endDate.text.toString()
                    TeacherConstant.GALLERY_REFRESH = true
                    prefs.setFilterData(filterPojo)
                    finish()
                }
            }
            R.id.tv_clear -> {
                filterPojo = FilterPojo()
                TeacherConstant.GALLERY_REFRESH = true
                prefs.setFilterData(filterPojo)
                finish()
            }
        }
    }

    private fun validate(): Boolean {
        return when {
            startDate.text.toString() == "" ->{
                displayToast(resources.getString(R.string.start_date_msg_filter))
                false
            }
            endDate.text.toString() == "" -> {
                displayToast(resources.getString(R.string.end_date_msg))
                false
            }
            category_spinner.selectedItem == "Category" -> {
                displayToast(resources.getString(R.string.select_category))
                false
            }
            else -> true
        }
    }
}
