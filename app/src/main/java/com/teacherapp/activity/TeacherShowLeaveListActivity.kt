package com.teacherapp.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.adapter.LeaveListAdapter
import com.teacherapp.pojo.LeaveManagementDataPojo
import com.teacherapp.pojo.LeaveManagementPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 12/4/2017.
 */
class TeacherShowLeaveListActivity : BaseActivity() {

    private lateinit var rv_leave_list: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var tv_header: TextView
    private lateinit var iv_edit_profile: ImageView
    private lateinit var type: String
    private lateinit var prefs: TeacherPreference
    private lateinit var leaveAdapter: LeaveListAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var leave_list: ArrayList<LeaveManagementDataPojo>
    private lateinit var filtered_leave_list: ArrayList<LeaveManagementDataPojo>
    private var ctx: TeacherShowLeaveListActivity = this
    private lateinit var qualfction: String
    private var searchView: android.support.v7.widget.SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activtiy_leave_list)
        initialize()
        setListener()
        TeacherConstant.ACTIVITIES.add(this)
        setHeaderTitle(intent.getStringExtra("header"))
    }

    private fun initialize() {
        mLayoutManager = LinearLayoutManager(this)
        leave_list = ArrayList()
        filtered_leave_list = ArrayList()
        prefs = TeacherPreference(this)
        rv_leave_list = findViewById(R.id.rv_leave_list)
        tv_no_data = findViewById(R.id.tv_no_data)
        tv_header = findViewById(R.id.tv_header)
        iv_add_icon = findViewById(R.id.iv_add_icon)
        iv_edit_profile = findViewById(R.id.iv_edit_profile)
        searchView = findViewById(R.id.search_box)
        iv_add_icon.visibility = View.GONE
        iv_edit_profile.visibility = View.GONE
        rv_leave_list.layoutManager = mLayoutManager
        setHeaderTitle(intent.getStringExtra("header"))
        type = intent.getStringExtra("type")
        qualfction = prefs.getQualfction()
        leaveList(type)
        searchView!!.clearFocus()
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                filtered_leave_list.clear()
                if (newText.isNotEmpty()) {
                    (0 until leave_list.size)
                            .filter { leave_list[it].reason.toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { filtered_leave_list.add(leave_list[it]) }
                } else {
                    filtered_leave_list.addAll(leave_list)
                }
                setAdapter(filtered_leave_list)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                hideSoftKeyboard()
                return false
            }

        })
    }

    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    private fun leaveList(status: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LeaveManagementPojo> = apiService.getTotalLeave(getFromPrefs(TeacherConstant.ID).toString(), status)
            call.enqueue(object : Callback<LeaveManagementPojo> {
                override fun onResponse(call: Call<LeaveManagementPojo>, response: Response<LeaveManagementPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                rv_leave_list.visibility = View.VISIBLE
                                tv_no_data.visibility = View.GONE
                                if (response.body()!!.status == "1") {
                                    leave_list.addAll(response.body()!!.data)
                                    filtered_leave_list.addAll(response.body()!!.data)
                                    setAdapter(filtered_leave_list)
                                } else {
                                    displayToast(response.body()!!.message)
                                }
                            } else {
                                rv_leave_list.visibility = View.GONE
                                tv_no_data.visibility = View.VISIBLE
                            }
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LeaveManagementPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setAdapter(leaveList: ArrayList<LeaveManagementDataPojo>) {
        leaveAdapter = LeaveListAdapter(ctx, leaveList, qualfction)
        rv_leave_list.adapter = leaveAdapter
        leaveAdapter.notifyDataSetChanged()
    }
}