package com.teacherapp.activity

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.pojo.BasePojo
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.TeacherDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 11/25/2017.
 */
class ForgotPasswordActivity : BaseActivity() {

    private lateinit var et_email: EditText
    private lateinit var tv_submit: TextView
    private lateinit var iv_notification: ImageView
    private lateinit var iv_logout: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        initialize()
        setListener()
    }

    private fun initialize() {
        setHeaderTitle(resources.getString(R.string.header_forgot))
        et_email = findViewById(R.id.et_email)
        tv_submit = findViewById(R.id.tv_submit)
        iv_notification = findViewById(R.id.iv_notification)
        iv_logout = findViewById(R.id.iv_logout)
        iv_notification.visibility = View.GONE
        iv_logout.visibility = View.GONE
    }

    private fun setListener() {
        tv_submit.setOnClickListener() {
            if (validate()) {
                forgotPassword(et_email.text.toString())
            }
        }
    }

    private fun validate(): Boolean {
        val email = et_email.text.toString()
        return if (email.isEmpty()) {
            displayToast(resources.getString(R.string.username_msg))
            false
        } else {
            true
        }
    }

    private fun forgotPassword(email: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = TeacherDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.forgotPassword(email)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            finish()
                        } else {
                            displayToast(response.body()!!.message)
                        }
                    } else {
                        displayToast(resources.getString(R.string.email_not_register))
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}