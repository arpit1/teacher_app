package com.teacherapp.activity

import android.os.Bundle
import com.teacherapp.R
import com.teacherapp.fragment.ImagesFragment
import com.teacherapp.util.TeacherConstant

class EventGalleryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_event_gallery)
        // setSupportActionBar(toolbar)
        //setHeaderTitle(resources.getString(R.string.event_gallery))
        TeacherConstant.ACTIVITIES.add(this)

        val fragment = ImagesFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.simpleFrameLayoutst, fragment)
        transaction.commit()
    }
}

