package com.teacherapp.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.AddGalleryActivity
import com.teacherapp.activity.EventGalleryActivity
import com.teacherapp.activity.FullScreenImage
import com.teacherapp.activity.NotificationActivity
import com.teacherapp.adapter.ImagesAdapter
import com.teacherapp.pojo.BasePojo
import com.teacherapp.pojo.GalleryPojo
import com.teacherapp.pojo.GalleryPojoData
import com.teacherapp.pojo.ImagePojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImagesFragment : Fragment() {

    private lateinit var view_fragment: View
    private lateinit var activity_list: GridView
    private lateinit var activityPostList: ArrayList<GalleryPojoData>
    private lateinit var galleryDataPojo: GalleryPojoData
    private lateinit var imageUrl: ArrayList<ImagePojo>
    private lateinit var adapter_add: ImagesAdapter
    private lateinit var ctxext: EventGalleryActivity
    private lateinit var tv_no_data: TextView
    private lateinit var event_name: String
    private lateinit var dates: String
    private lateinit var discussion: String
    private lateinit var prefs: TeacherPreference
    private lateinit var datetime: TextView
    private lateinit var show_title: TextView
    private lateinit var description: TextView
    private lateinit var getUploaded_type: String
    private lateinit var img_share: ImageView
    private lateinit var tv_delete: TextView
    private lateinit var descGallery: TextView
    private lateinit var ivBack: ImageView
    private lateinit var ivNavi: ImageView
    private lateinit var iv_add: ImageView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.fragment_gallery, container, false)
            init(view_fragment)
            setListener()
            setHeaderTitle(resources.getString(R.string.galley_detail))
        }
        return view_fragment
    }

    override fun onResume() {
        super.onResume()
        getEventList(prefs.getAlbumId())
    }

    private fun setListener() {
        activity_list.onItemClickListener = AdapterView.OnItemClickListener { _, _, pos, _ ->
            startActivity(Intent(activity, FullScreenImage::class.java).putExtra("Image", imageUrl)
                    .putExtra("discussion", discussion)
                    .putExtra("mediatype", getUploaded_type)
                    .putExtra("from", "gallery")
                    .putExtra("header", galleryDataPojo.event_name)
                    .putExtra("dates", dates)
                    .putExtra("pos", pos))
        }
        tv_delete.setOnClickListener {
            val builder = AlertDialog.Builder(ctxext)
            builder.setCancelable(false)
            builder.setMessage(ctxext.resources.getString(R.string.albumdelete))
            builder.setPositiveButton("Yes", { _, _ ->
                deletePost(prefs.getAlbumId())
            })
            builder.setNegativeButton("No", { dialog, _ ->
                dialog.cancel()
            })
            val alert = builder.create()
            alert.show()
        }
        iv_add.setOnClickListener {
            startActivity(Intent(ctxext, AddGalleryActivity::class.java)
                    .putExtra("from", "edit").putExtra("edit_gallery", galleryDataPojo))
        }
    }

    private fun deletePost(album_id: String) {
        if (ConnectionDetector.isConnected(ctxext)) {
            val d = TeacherDialog.showLoading(ctxext)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)

            val call: Call<BasePojo> = apiService.deletePost(album_id)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {

                    if (response != null) {
                        if (response.body()!!.status.equals("1")) {
                            TeacherConstant.GALLERY_REFRESH = true
                            ctxext.displayToast(getString(R.string.album_delete))
                            imageUrl.clear()
                            adapter_add.notifyDataSetChanged()
                            ctxext.finish()
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctxext.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun getEventList(albumId: String) {
        if (ConnectionDetector.isConnected(ctxext)) {
            val d = TeacherDialog.showLoading(ctxext)
            d.setCanceledOnTouchOutside(false)

            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<GalleryPojo> = apiService.gallery_event_list(ctxext.getFromPrefs(TeacherConstant.ID).toString(), albumId)
            call.enqueue(object : Callback<GalleryPojo> {
                override fun onResponse(call: Call<GalleryPojo>, response: Response<GalleryPojo>?) {
                    imageUrl = ArrayList()
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            galleryDataPojo = response.body()!!.data
                            description.text = galleryDataPojo.event_name
                            discussion = galleryDataPojo.event_name
                            descGallery.text = galleryDataPojo.discussion
                            dates = galleryDataPojo.dates
                            datetime.text = ctxext.getChangedDate(dates)
                            tv_no_data.visibility = View.GONE
                            getUploaded_type = galleryDataPojo.upload_type
                            for (i in 0 until galleryDataPojo.file!!.size) {
                                imageUrl.add(galleryDataPojo.file!![i])
                            }

                            adapter_add = ImagesAdapter(ctxext, imageUrl, getUploaded_type)
                            activity_list.adapter = adapter_add
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<GalleryPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctxext.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun init(view_fragment: View) {
        ctxext = activity as EventGalleryActivity
        prefs = TeacherPreference(ctxext)
        imageUrl = ArrayList()
        activityPostList = ArrayList()
        galleryDataPojo = GalleryPojoData()
        activity_list = view_fragment.findViewById(R.id.activity_list)
        tv_no_data = view_fragment.findViewById(R.id.tv_no_data)
        datetime = view_fragment.findViewById(R.id.datetim)
        show_title = view_fragment.findViewById(R.id.stdetail)
        description = view_fragment.findViewById(R.id.description)
        img_share = view_fragment.findViewById(R.id.img_share)
        tv_delete = view_fragment.findViewById(R.id.tv_delete)
        descGallery = view_fragment.findViewById(R.id.descGallery)
        descGallery.movementMethod = ScrollingMovementMethod()
        img_share.visibility = View.GONE

        ivBack = view_fragment.findViewById(R.id.iv_back)
        ivNavi = view_fragment.findViewById(R.id.iv_navi)
        iv_add = view_fragment.findViewById(R.id.iv_add_icon)
        iv_add.setImageResource(R.drawable.ic_edit_white_icon)
        ivBack.visibility = View.VISIBLE
        ivNavi.visibility = View.GONE
        iv_add.visibility = View.VISIBLE

        event_name = prefs.getEventName()

        getUploaded_type = prefs.getUploaded_type()
        show_title.text = getUploaded_type
    }

    fun setHeaderTitle(header: String) {
        val tvHeader: TextView = view_fragment.findViewById(R.id.tv_header)
        tvHeader.text = header
        val ivBack: ImageView = view_fragment.findViewById(R.id.iv_back)
        val ivNotification = view_fragment.findViewById<ImageView>(R.id.iv_notification)
        val ivLogout = view_fragment.findViewById<ImageView>(R.id.iv_logout)
        ivBack.setOnClickListener {
            ctxext.finish()
            ctxext.hideSoftKeyboard()
        }
        ivNotification.setOnClickListener {
            val intent = Intent(ctxext, NotificationActivity::class.java)
            startActivity(intent)
        }

        ivLogout.setOnClickListener {
            ctxext.appLogout()
        }
    }
}