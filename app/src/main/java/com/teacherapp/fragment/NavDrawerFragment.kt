package com.teacherapp.fragment

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.teacherapp.R
import com.teacherapp.activity.*
import com.teacherapp.util.TeacherConstant
import com.teacherapp.util.TeacherPreference
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 11/27/2017.
 */
class NavDrawerFragment : Fragment() {

    private lateinit var view_fragment: View
    private lateinit var iv_profile_pic: CircleImageView
    private lateinit var tv_user_name: TextView
    private lateinit var tv_email: TextView
    private lateinit var ll_feed: LinearLayout
    private lateinit var ll_logout: LinearLayout
    private lateinit var ll_school_diary: LinearLayout
    private lateinit var ll_request_leave: LinearLayout
    private lateinit var ll_student_leave: LinearLayout
    private lateinit var ll_task: LinearLayout
    private lateinit var ll_teacher_task: LinearLayout
    private lateinit var ll_send_notification: LinearLayout
    private lateinit var ll_profile_pic: LinearLayout
    private lateinit var drawerlayout: DrawerLayout
    private lateinit var mDrawerToggle: ActionBarDrawerToggle
    private var selectedPos: Int = -1
    private lateinit var ctx: HomeActivity
    private lateinit var prefs: TeacherPreference

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.nev_menu, container, false)
        }
        initialize()
        setListener()
        return view_fragment
    }

    private fun initialize() {
        iv_profile_pic = view_fragment.findViewById(R.id.iv_profile_pic)
        tv_user_name = view_fragment.findViewById(R.id.tv_user_name)
        tv_email = view_fragment.findViewById(R.id.tv_email)
        ll_feed = view_fragment.findViewById(R.id.ll_feed)
        ll_logout = view_fragment.findViewById(R.id.ll_logout)
        ll_school_diary = view_fragment.findViewById(R.id.ll_school_diary)
        ll_request_leave = view_fragment.findViewById(R.id.ll_request_leave)
        ll_student_leave = view_fragment.findViewById(R.id.ll_student_leave)
        ll_task = view_fragment.findViewById(R.id.ll_task)
        ll_teacher_task = view_fragment.findViewById(R.id.ll_teacher_task)
        ll_send_notification = view_fragment.findViewById(R.id.ll_send_notification)
        ll_profile_pic = view_fragment.findViewById(R.id.ll_profile_pic)
        ctx = activity as HomeActivity
        prefs = TeacherPreference(ctx)

        val profile_pic = ctx.getFromPrefs(TeacherConstant.IMAGE)
        val user_name = ctx.getTeacherName()
        val email = ctx.getFromPrefs(TeacherConstant.EMAIL)
        tv_user_name.text = user_name
        tv_email.text = email
        Picasso.with(ctx).load(TeacherConstant.IMAGE_BASE_URL + profile_pic).placeholder(R.mipmap.user_icon_default).error(R.mipmap.user_icon_default).into(iv_profile_pic)
    }

    private fun setListener() {
        ll_profile_pic.setOnClickListener {
            selectedPos = 1
            drawerlayout.closeDrawers()
        }

        ll_school_diary.setOnClickListener {
            selectedPos = 2
            drawerlayout.closeDrawers()
        }
        ll_request_leave.setOnClickListener {
            selectedPos = 3
            drawerlayout.closeDrawers()
        }
        ll_student_leave.setOnClickListener {
            selectedPos = 4
            drawerlayout.closeDrawers()
        }
        ll_task.setOnClickListener {
            selectedPos = 5
            drawerlayout.closeDrawers()

        }
        ll_teacher_task.setOnClickListener {
            selectedPos = 7
            drawerlayout.closeDrawers()
        }
        ll_send_notification.setOnClickListener {
            selectedPos = 6
            drawerlayout.closeDrawers()
        }
        ll_feed.setOnClickListener {
            selectedPos = 8
            drawerlayout.closeDrawers()
        }
        ll_logout.setOnClickListener {
            selectedPos = 9
            drawerlayout.closeDrawers()
        }
    }

    fun setUp(drawerlayout: DrawerLayout) {

        this.drawerlayout = drawerlayout
        mDrawerToggle = object : ActionBarDrawerToggle(activity, drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            override fun onDrawerOpened(drawerView: View?) {
                super.onDrawerOpened(drawerView)
                val profile_pic = ctx.getFromPrefs(TeacherConstant.IMAGE)
                val user_name = ctx.getTeacherName()
                val email = ctx.getFromPrefs(TeacherConstant.EMAIL)
                tv_user_name.text = user_name
                tv_email.text = email
                Picasso.with(ctx).load(TeacherConstant.IMAGE_BASE_URL + profile_pic).placeholder(R.mipmap.user_icon_default).error(R.mipmap.user_icon_default).into(iv_profile_pic)

            }

            @SuppressLint("NewApi")
            override fun onDrawerClosed(drawerView: View?) {
                val am = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
                val cn = am.getRunningTasks(1)[0].topActivity
                super.onDrawerClosed(drawerView)
                if (selectedPos != -1) {
                    when (selectedPos) {

                        1 -> if (cn.shortClassName != ".activity.ProfileActivity") {
                            navigateToProfile()
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        2 -> if (cn.shortClassName != ".activity.TaskListActivity") {
                            navigateToCaledar("diary")
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        3 -> if (cn.shortClassName != ".activity.TeacherLeaveApplyActivity") {
                            navigateToRequestLeave()
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        4 -> if (cn.shortClassName != ".activity.StudentLeaveActivity") {
                            navigateToStudenLeaveManagement()
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        5 -> if (cn.shortClassName != ".activity.TaskListActivity") {
                            navigateToCaledar("tasks")
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        7 -> if(cn.shortClassName != ".activity.TeacherTaskListActivity"){
                            navigateToTeacherTask()
                        }else {
                            drawerlayout.closeDrawers()
                        }

                        6 -> if (cn.shortClassName != ".activity.SendNotificationActivity") {
                            navigateToNotification()
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        8 -> if (cn.shortClassName != ".activity.HomeActivity") {
                            (activity as HomeActivity).removeAllActivities()
                            activity.finish()
                            (activity as HomeActivity).tabHome.performClick()
//                            startActivity(Intent(activity, HomeActivity::class.java))
                        } else {
                            (activity as HomeActivity).tabHome.performClick()
                            drawerlayout.closeDrawers()
                        }

                        9 -> (activity as BaseActivity).appLogout()
                        //6 -> (activity as BaseActivity)

                    }//navigateToContactUs();
                    selectedPos = -1
                }
            }
        }

        drawerlayout.setDrawerListener(mDrawerToggle)

    }



    private fun navigateToCaledar(from: String) {
        (activity as HomeActivity).tab_calendar.performClick()
        (activity as HomeActivity).fragmentFrom = from

        (activity.supportFragmentManager.fragments[3] as CalendarFrament).switchFragment("HomeActivity")
    }

    private fun navigateToProfile() {
        startActivity(Intent(context, ProfileActivity::class.java))
    }

    private fun navigateToRequestLeave() {
        startActivity(Intent(context, TeacherLeaveApplyActivity::class.java))
    }

    private fun navigateToStudenLeaveManagement() {
        startActivity(Intent(context, StudentLeaveActivity::class.java).putExtra("from", "Leave Management"))
    }
    private fun navigateToNotification() {
        startActivity(Intent(context, SendNotificationActivity::class.java).putExtra("from", "Send Notification"))
    }

    private fun navigateToTeacherTask() {
        startActivity(Intent(context, TeacherTaskListActivity::class.java))
    }

}