package com.teacherapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.HomePostAdapter
import com.teacherapp.pojo.GalleryPojoData
import com.teacherapp.pojo.HomePostPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 11/27/2017.
 */
class HomeFragment : Fragment() {

    private lateinit var view_fragment: View
    private lateinit var home_posts: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var adapter: HomePostAdapter
    private lateinit var ctx: HomeActivity
    private lateinit var home_post_list: ArrayList<GalleryPojoData>
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.fragment_home, container, false)
        }
        initialize()
        return view_fragment
    }

    private fun initialize() {
        ctx = activity as HomeActivity
        mLayoutManager = LinearLayoutManager(ctx)
        home_post_list = ArrayList()
        home_posts = view_fragment.findViewById(R.id.home_post)
        tv_no_data = view_fragment.findViewById(R.id.tv_no_data)
        home_posts.layoutManager = mLayoutManager
        homePost()
    }

    private fun homePost() {
        if (ConnectionDetector.isConnected(context)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<HomePostPojo> = apiService.homePost(ctx.getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), ctx.getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<HomePostPojo> {
                override fun onResponse(call: Call<HomePostPojo>, response: Response<HomePostPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                home_posts.visibility = View.VISIBLE
                                tv_no_data.visibility = View.GONE
                                home_post_list.addAll(response.body()!!.data)
                                adapter = HomePostAdapter(ctx, home_post_list)
                                home_posts.adapter = adapter
                            } else {
                                home_posts.visibility = View.GONE
                                tv_no_data.visibility = View.VISIBLE
                            }
                        } else {
                            ctx.displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<HomePostPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}