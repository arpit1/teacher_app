package com.teacherapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.activity.NotificationActivity

/**
 * Created by upasna.mishra on 11/27/2017.
 */
class CalendarFrament : Fragment() {
    private lateinit var view_fragment: View
    private lateinit var simpleFrameLayout: FrameLayout
    private lateinit var ctx: HomeActivity
    private lateinit var btn_school: TextView
    private lateinit var btn_task: TextView
    private lateinit var btn_leave: TextView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.calendar_fragment, container, false)
            initialize()
            setListener()
        }
        return view_fragment
    }

    private fun initialize() {
        ctx = activity as HomeActivity
        btn_school = view_fragment.findViewById(R.id.btn_school)
        btn_task = view_fragment.findViewById(R.id.btn_task)
        btn_leave = view_fragment.findViewById(R.id.btn_leave)
        simpleFrameLayout = view_fragment.findViewById(R.id.simpleFrameLayout)
        val fragment = SchoolDiaryFragment()
        fragmentManager.beginTransaction().apply {
            replace(R.id.simpleFrameLayout, fragment, getString(R.string.school_diary_calender))
        }.commit()
    }

    fun switchFragment(type: String) {
        if (type == "HomeActivity") {
            when {
                (activity as HomeActivity).fragmentFrom == getString(R.string.diary) -> setReplaceFragment(1)
                (activity as HomeActivity).fragmentFrom == getString(R.string.request_leave_small) -> setReplaceFragment(3)
                (activity as HomeActivity).fragmentFrom == getString(R.string.task_small) -> setReplaceFragment(2)
            }
        } else {
            when {
                (activity as NotificationActivity).fragmentFrom == getString(R.string.diary) -> setReplaceFragment(1)
                (activity as NotificationActivity).fragmentFrom == getString(R.string.request_leave_small) -> setReplaceFragment(3)
                (activity as NotificationActivity).fragmentFrom == getString(R.string.task_small) -> setReplaceFragment(2)
            }
        }
    }

    private fun setListener() {
        btn_school.setOnClickListener {
            setReplaceFragment(1)
        }
        btn_task.setOnClickListener {
            setReplaceFragment(2)
        }
        btn_leave.setOnClickListener {
            setReplaceFragment(3)
        }
    }

    private fun setFragment(fragment: Fragment?, tag: String?) {
        val fm = fragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.simpleFrameLayout, fragment, tag)
        ft.commit()
    }

    private fun setReplaceFragment(i: Int) {
        val fragment: Fragment?
        val tag: String?

        when (i) {
            1 -> {
                ctx.calendarSelect = 0
                ctx.addClick(0)
                tag = getString(R.string.school_diary_calender)
                fragment = SchoolDiaryFragment()

                btn_school.background = ContextCompat.getDrawable(ctx, R.drawable.selected_tab)
                btn_task.background = null
                btn_leave.background = null

                btn_school.setTextColor(ContextCompat.getColor(ctx, R.color.red_color))
                btn_task.setTextColor(ContextCompat.getColor(ctx, R.color.black))
                btn_leave.setTextColor(ContextCompat.getColor(ctx, R.color.black))
            }
            2 -> {
                ctx.calendarSelect = 1
                ctx.addClick(1)
                tag = getString(R.string.task_small)
                fragment = TaskListFragment()
                btn_task.background = ContextCompat.getDrawable(ctx, R.drawable.selected_tab)
                btn_school.background = null
                btn_leave.background = null

                btn_school.setTextColor(ContextCompat.getColor(ctx, R.color.black))
                btn_task.setTextColor(ContextCompat.getColor(ctx, R.color.red_color))
                btn_leave.setTextColor(ContextCompat.getColor(ctx, R.color.black))
            }
            else -> {
                ctx.calendarSelect = 2
                ctx.addClick(2)
                tag = getString(R.string.leave_small)
                fragment = LeaveManagementFragment()
                btn_leave.background = ContextCompat.getDrawable(ctx, R.drawable.selected_tab)
                btn_school.background = null
                btn_task.background = null

                btn_school.setTextColor(ContextCompat.getColor(ctx, R.color.black))
                btn_task.setTextColor(ContextCompat.getColor(ctx, R.color.black))
                btn_leave.setTextColor(ContextCompat.getColor(ctx, R.color.red_color))
            }
        }
        setFragment(fragment, tag)
    }
}