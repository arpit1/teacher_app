package com.teacherapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.pojo.FilterPojo
import com.teacherapp.util.TeacherConstant
import com.teacherapp.util.TeacherPreference

class GalleryFragment : Fragment() {
    private lateinit var view_fragment: View
    //   private lateinit var tabLayout: TabLayout
    private lateinit var ctx: HomeActivity
    //val st: OnHeadlineSelectedListener? = null
    private lateinit var btn_event: TextView
    private lateinit var btn_activity: TextView
    private lateinit var prefs: TeacherPreference


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.gallery_fragment, container, false)

            init()
            setListener()
        }
        return view_fragment
    }

    private fun init() {
        ctx = activity as HomeActivity
        prefs = TeacherPreference(ctx)
        btn_event = view_fragment.findViewById(R.id.btn_event)
        btn_activity = view_fragment.findViewById(R.id.btn_activity)

        // tabLayout = this.view_fragment.findViewById(R.id.tabviews)

        val fragment = EventFragment()
        val fm = fragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.simpleFrameLayouts, fragment, getString(R.string.event))
        ft.commit()
        prefs.setSelectOption("Event")
    }

    private fun setListener() {
        var fragment: Fragment?
        var tag: String?

        btn_event.setOnClickListener {
            ctx.addClick(6)
            tag = "event"
            prefs.setSelectOption("Event")
            fragment = EventFragment()
            btn_event.background = ContextCompat.getDrawable(ctx, R.drawable.selected_tab)
            btn_activity.background = null
            btn_event.setTextColor(ContextCompat.getColor(ctx, R.color.red_color))
            btn_activity.setTextColor(ContextCompat.getColor(ctx, R.color.black))
            setFragment(fragment, tag)
        }
        btn_activity.setOnClickListener {
            ctx.addClick(6)
            tag = "activity"
            prefs.setSelectOption("Activities")
            fragment = ActivitiesFragment()
            btn_activity.background = ContextCompat.getDrawable(ctx, R.drawable.selected_tab)
            btn_event.background = null
            btn_activity.setTextColor(ContextCompat.getColor(ctx, R.color.red_color))
            btn_event.setTextColor(ContextCompat.getColor(ctx, R.color.black))
            setFragment(fragment, tag)
        }
    }

    override fun onResume() {
        super.onResume()
        val filterpojo = prefs.getFilterData() as FilterPojo
        if (TeacherConstant.GALLERY_REFRESH) {
            if (filterpojo.category == "Activities") {
                btn_activity.performClick()
            } else {
                btn_event.performClick()
            }
        }
    }

    private fun setFragment(fragment: Fragment?, tag: String?) {
        val fm = fragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.simpleFrameLayouts, fragment, tag)
        ft.commit()
    }
}