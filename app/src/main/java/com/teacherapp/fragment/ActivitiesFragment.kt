package com.teacherapp.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.GridView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.EventGalleryActivity
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.ActivityAdapter
import com.teacherapp.pojo.EventPojo
import com.teacherapp.pojo.EventPojoItem
import com.teacherapp.pojo.FilterPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivitiesFragment : Fragment() {

    private lateinit var view_fragment: View
    private lateinit var activity_list: GridView
    private lateinit var activityPostList: ArrayList<EventPojoItem>
    private lateinit var adapter_add: ActivityAdapter
    private lateinit var ctxext: HomeActivity
    private lateinit var prefs: TeacherPreference
    private var searchView: android.support.v7.widget.SearchView? = null
    private lateinit var filterlist: ArrayList<EventPojoItem>
    private lateinit var noDataAvailable : TextView
    private lateinit var clear_filter : TextView
    private lateinit var filterPojo: FilterPojo

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.fragment_activities_fragement, container, false)

            init(view_fragment)
            setListener()
            ctxext.hideSoftKeyboard()
        }
        return view_fragment
    }

    override fun onResume() {
        super.onResume()
        val filterpojo = prefs.getFilterData() as FilterPojo
        if(TeacherConstant.GALLERY_REFRESH){
            searchView!!.setQuery("", false)
            if (filterpojo.category == "Activities") {
                clear_filter.visibility = View.VISIBLE
                getEventList(filterpojo.album_name, "", filterpojo.start_date, filterpojo.end_date)
            } else {
                clear_filter.visibility = View.GONE
                getEventList("", "", "", "")
            }
        }

        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    private fun getEventList(album_name: String, from : String, start_date : String, end_date : String) {
        if (ConnectionDetector.isConnected(ctxext)) {
            val d = TeacherDialog.showLoading(ctxext)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<EventPojo> = apiService.student_activity_list(ctxext.getFromPrefs(TeacherConstant.ID).toString(),
                    "Activities", album_name, start_date, end_date)
            call.enqueue(object : Callback<EventPojo> {
                override fun onResponse(call: Call<EventPojo>, response: Response<EventPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if(from == "clear")
                                clear_filter.visibility = View.GONE
                            val dat = response.body()!!.data
                            activityPostList = ArrayList()
                            filterlist = ArrayList()
                            if (dat.size > 0) {
                                noDataAvailable.visibility = View.GONE
                                activity_list.visibility = View.VISIBLE
                                for (i in 0 until dat.size) {
                                    if (dat[i].file.size > 0) {
                                        activityPostList.add(dat[i])
                                        filterlist.add(dat[i])
                                    }
                                }
                            } else {
                                activity_list.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                            ctxext.hideSoftKeyboard()
                            setAdapter(activityPostList)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<EventPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctxext.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setListener() {
        activity_list.onItemClickListener = AdapterView.OnItemClickListener { _, _, pos, _ ->
            prefs.setSchoolGallery(activityPostList[pos].id, activityPostList[pos].event_name, activityPostList[pos].upload_type, activityPostList[pos].category)
            startActivity(Intent(activity, EventGalleryActivity::class.java))
        }
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                activityPostList.clear()
                if (newText.isNotEmpty()) {
                    (0 until filterlist.size)
                            .filter { filterlist[it].event_name.toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { activityPostList.add(filterlist[it]) }
                } else {
                    activityPostList.addAll(filterlist)
                }
                setAdapter(activityPostList)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctxext.hideSoftKeyboard()
                return false
            }

        })

        clear_filter.setOnClickListener {
            filterPojo = FilterPojo()
            prefs.setFilterData(filterPojo)
            getEventList("", "clear", "", "")
        }
    }

    private fun setAdapter(activityPostList: ArrayList<EventPojoItem>) {
        adapter_add = ActivityAdapter(ctxext, activityPostList)
        activity_list.adapter = adapter_add
        adapter_add.notifyDataSetChanged()
    }

    private fun init(view_fragment: View) {
        ctxext = activity as HomeActivity
        activityPostList = ArrayList()
        activity_list = view_fragment.findViewById(R.id.activity_list)
        searchView = view_fragment.findViewById(R.id.search_box)
        noDataAvailable = view_fragment.findViewById(R.id.tv_no_data)
        clear_filter = view_fragment.findViewById(R.id.clear_filter)
        filterlist = ArrayList()
        prefs = TeacherPreference(ctxext)
        val filterpojo = prefs.getFilterData() as FilterPojo
        if (filterpojo.category == "Activities") {
            clear_filter.visibility = View.VISIBLE
            getEventList(filterpojo.album_name, "", filterpojo.start_date, filterpojo.end_date)
        } else {
            clear_filter.visibility = View.GONE
            getEventList("", "", "", "")
        }
    }
}
