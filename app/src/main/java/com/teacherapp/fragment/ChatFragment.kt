package com.teacherapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.ChatPagerAdapter
import com.teacherapp.util.TeacherDialog

/**
 * Created by upasna.mishra on 11/27/2017.
 */
class ChatFragment : Fragment(), View.OnClickListener {

    private lateinit var viewChatFragment: View
    private var dialog: TeacherDialog? = null
    private lateinit var  ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var chatViewPager : ViewPager
    private lateinit var chatLayout : LinearLayout
    private lateinit var chatTxt : TextView
    private lateinit var chatSelector : LinearLayout
    private lateinit var contactLayout : LinearLayout
    private lateinit var contactTxt : TextView
    private lateinit var contactSelector : LinearLayout
    private var mAdapter: ChatPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewChatFragment = inflater!!.inflate(R.layout.chat_fragment, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewChatFragment
    }

    private fun initialize() {
        dialog = TeacherDialog(ctx)
        mAdapter = ChatPagerAdapter(ctx.supportFragmentManager)
        chatViewPager = viewChatFragment.findViewById(R.id.chatViewPager)
        chatLayout = viewChatFragment.findViewById(R.id.chatLayout)
        chatTxt = viewChatFragment.findViewById(R.id.chatTxt)
        chatSelector = viewChatFragment.findViewById(R.id.chatSelector)
        contactLayout = viewChatFragment.findViewById(R.id.contactLayout)
        contactTxt = viewChatFragment.findViewById(R.id.contactTxt)
        contactSelector = viewChatFragment.findViewById(R.id.contactSelector)

        chatLayout.setOnClickListener(this)
        contactLayout.setOnClickListener(this)
    }

    private fun setListener() {
        chatViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                setStyle(position)
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(arg0: Int) {}
        })
    }

    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.chatLayout -> if (chatViewPager.currentItem != 0){
                chatViewPager.currentItem = 0
                setStyle(0)
            }

            R.id.contactLayout -> if (chatViewPager.currentItem != 1){
                chatViewPager.currentItem = 1
                setStyle(1)
            }
        }
    }


    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && mAdapter != null) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            if(chatViewPager.adapter == null) {
                chatViewPager.adapter = mAdapter
                chatViewPager.offscreenPageLimit = 1
            }
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
        }
    }

    private fun setStyle(position: Int) {
        when (position) {
            0 -> {
                chatTxt.setTextColor(ContextCompat.getColor(ctx, R.color.button_color))
                contactTxt.setTextColor(ContextCompat.getColor(ctx, R.color.grey_dark_color))

                chatSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.button_color))
                contactSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.grey))
            }
            1 -> {
                chatTxt.setTextColor(ContextCompat.getColor(ctx, R.color.grey_dark_color))
                contactTxt.setTextColor(ContextCompat.getColor(ctx, R.color.button_color))

                chatSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.grey))
                contactSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.button_color))
            }
        }
    }
}