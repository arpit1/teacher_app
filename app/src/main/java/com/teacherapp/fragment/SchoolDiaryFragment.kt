package com.teacherapp.fragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.*
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.DiaryListAdapter
import com.teacherapp.pojo.GetDiaryDataPojo
import com.teacherapp.pojo.GetDiaryPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class SchoolDiaryFragment : Fragment(), OnDateSelectedListener, OnMonthChangedListener {

    private lateinit var view_fragment: View
    private lateinit var calendar: MaterialCalendarView
    private lateinit var rv_diary_updates: RecyclerView
    private lateinit var prefs: TeacherPreference
    private lateinit var diary_list: ArrayList<GetDiaryDataPojo>
    private lateinit var adapter: DiaryListAdapter
    private lateinit var ctx: HomeActivity
    private lateinit var tv_no_data: TextView
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var datesAssigned: HashSet<CalendarDay>
    private var month: String = ""
    private var year: String = ""

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.fragment_school_diary, container, false)

            initialize()
            setListener()
        }
        return view_fragment
    }

    private fun initialize() {
        ctx = activity as HomeActivity

        prefs = TeacherPreference(activity)
        diary_list = ArrayList()
        calendar = view_fragment.findViewById(R.id.calendar_view)
        tv_no_data = view_fragment.findViewById(R.id.tv_no_data)
        calendar.selectedDate = CalendarDay.today()
        rv_diary_updates = view_fragment.findViewById(R.id.rv_diary_updates)
        mLayoutManager = LinearLayoutManager(ctx)
        rv_diary_updates.layoutManager = mLayoutManager
    }

    override fun onResume() {
        super.onResume()
        if (diary_list.size == 0) {
            calendar.selectedDate
            if (month == "") {
                month = (CalendarDay.today().month + 1).toString()
                year = CalendarDay.today().year.toString()
            }
            val handler = Handler()
            handler.postDelayed({
                getSchoolDiary("")
            }, 150)
        }
        if (TeacherConstant.DIARY_REFRESH_REQUIRED)
            getSchoolDiary("")
    }

    override fun onDateSelected(widget: MaterialCalendarView, date: CalendarDay, selected: Boolean) {
        calendar.selectedDate = date
        getSchoolDiaryData(date)
    }

    override fun onMonthChanged(widget: MaterialCalendarView?, date: CalendarDay?) {
        month = (date!!.month + 1).toString()
        year = date.year.toString()
        calendar.selectedDate = CalendarDay.today()
        val handler = Handler()
        handler.postDelayed({
            getSchoolDiary("")
        }, 150)
    }

    private fun getSchoolDiaryData(date: CalendarDay) {
        val df = SimpleDateFormat(ctx.getString(R.string.dateformat), Locale.UK)
        val dateVal = df.format(date.date)
        getSchoolDiary(dateVal)
    }

    private fun setListener() {
        calendar.setOnDateChangedListener(this)
        calendar.setOnMonthChangedListener(this)
    }

    private fun getSchoolDiary(dateVal: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<GetDiaryPojo> = apiService.getSchoolDiary(ctx.getFromPrefs(TeacherConstant.ID).toString(), dateVal,
                    ctx.getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), month, year)
            call.enqueue(object : Callback<GetDiaryPojo> {
                override fun onResponse(call: Call<GetDiaryPojo>, response: Response<GetDiaryPojo>?) {
                    if (response != null && response.body()?.status == "1") {
                        if (response.body()!!.data.size > 0) {
                            if (response.body()!!.status == "1") {
                                tv_no_data.visibility = View.GONE
                                diary_list.clear()
                                datesAssigned = HashSet()
                                datesAssigned.clear()
                                diary_list.addAll(response.body()!!.data)
                                TeacherConstant.DIARY_REFRESH_REQUIRED = false
                                val listDiary = diary_list
                                for (i in 0 until listDiary.size) {
                                    val date = listDiary[i].dates.split("-")
                                    val day = date[0].toInt()
                                    val month = date[1].toInt() - 1
                                    val year = date[2].toInt()
                                    val dateToHighLight = CalendarDay.from(year, month, day)
                                    datesAssigned.add(dateToHighLight)
                                }

                                calendar.addDecorators(CircleDecoratorLow(ctx, datesAssigned))
                                adapter = DiaryListAdapter(ctx, diary_list)
                                rv_diary_updates.adapter = adapter
                            }
                        } else {
                            tv_no_data.visibility = View.VISIBLE
                            diary_list = ArrayList()
                            adapter = DiaryListAdapter(ctx, diary_list)
                            rv_diary_updates.adapter = adapter
                            adapter.notifyDataSetChanged()
                        }

                    } else {
                        ctx.displayToast(response?.body()!!.message)
                    }

                    d.dismiss()
                }

                override fun onFailure(call: Call<GetDiaryPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    inner class CircleDecoratorLow(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {
        private val drawable: Drawable = ContextCompat.getDrawable(context, R.drawable.blue_circle_calendar)
        private var datesLow: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesLow.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.BLACK))
            view.setSelectionDrawable(drawable)
        }
    }
}