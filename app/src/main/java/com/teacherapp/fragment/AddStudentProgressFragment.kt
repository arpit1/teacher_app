package com.teacherapp.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.Spinner
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.activity.StudentProgressActivity
import com.teacherapp.pojo.*
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.TeacherConstant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class AddStudentProgressFragment : Fragment() {

    private lateinit var ctx: HomeActivity

    private lateinit var programSpinner: Spinner
    private lateinit var batchSpinner: Spinner
    private lateinit var classSpinner: Spinner

    private lateinit var tv_submit: TextView
    private lateinit var studentProgressView: View

    private lateinit var programList: ArrayList<ProgramDataPojo>
    private lateinit var classList: ArrayList<ClassDataPojo>
    private lateinit var batchList: ArrayList<BatchDataPojo>
    private lateinit var mInflator: LayoutInflater

    private var programId: String = ""
    private var batchId: String = ""
    private var classId: String = ""

    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            studentProgressView = inflater.inflate(R.layout.fragment_student_progress, container, false)
        }
        init()
        setListener()
        return studentProgressView
    }

    private fun init() {
        ctx = activity as HomeActivity
        mInflator = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        tv_submit = studentProgressView.findViewById(R.id.tv_submit)
        programList = ArrayList()
        classList = ArrayList()
        batchList = ArrayList()
        programSpinner = studentProgressView.findViewById(R.id.program_spinner)
        batchSpinner = studentProgressView.findViewById(R.id.batch_spinner)
        classSpinner = studentProgressView.findViewById(R.id.class_spinner)
    }

    private fun setListener() {
        tv_submit.setOnClickListener {
            clickSubmit()
        }
        programSpinner.onItemSelectedListener = programSelectedListener
        batchSpinner.onItemSelectedListener = batchSelectedListener
        classSpinner.onItemSelectedListener = classSelectedListener
    }

    private fun clickSubmit() {
        when {
            programId == "" -> ctx.displayToast(resources.getString(R.string.select_program_msg))
            batchId == "" -> ctx.displayToast(resources.getString(R.string.select_section_msg))
            classId == "" -> ctx.displayToast(resources.getString(R.string.select_class_msg))
            else -> startActivity(Intent(ctx, StudentProgressActivity::class.java).putExtra("classes", classId).putExtra("batch", batchId).putExtra("program", programId))
        }
    }

    private fun getProgram() {
        if (ConnectionDetector.isConnected(ctx)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ProgramPojo> = apiService.getProgram(ctx.getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<ProgramPojo> {
                override fun onResponse(call: Call<ProgramPojo>, response: Response<ProgramPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                programList.addAll(response.body()!!.data)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ProgramPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val programSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            ctx.hideSoftKeyboard()
            setBatchHintSpinner()
            if (position != 0) {
                programId = programList[position].id
                getBatch()
            } else {
                programId = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val programSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = programList[position].programs
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return programList[position]
        }

        override fun getCount(): Int {
            return programList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = programList[position].programs
            return convertView as View
        }
    }

    private fun setHintSpinner() {
        programList.clear()
        val programDataPojo = ProgramDataPojo()
        programDataPojo.id = "-1"
        programDataPojo.programs = resources.getString(R.string.select_program)
        programList.add(0, programDataPojo)
        programSpinner.adapter = programSpinnerAdapter
        programSpinnerAdapter.notifyDataSetChanged()
        setBatchHintSpinner()
    }

    private fun setBatchHintSpinner() {
        batchList.clear()
        val batchDataPojo = BatchDataPojo()
        batchDataPojo.batch_name = resources.getString(R.string.select_section)
        batchList.add(0, batchDataPojo)
        batchSpinner.adapter = batchSpinnerAdapter
        batchSpinnerAdapter.notifyDataSetChanged()
        setClassHintSpinner()
    }

    private fun setClassHintSpinner() {
        classList.clear()
        val classData = ClassDataPojo()
        classData.classes = resources.getString(R.string.select_class)
        classList.add(0, classData)
        classSpinner.adapter = classSpinnerAdapter
        classSpinnerAdapter.notifyDataSetChanged()
    }

    private fun getBatch() {
        if (ConnectionDetector.isConnected(ctx)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BatchPojo> = apiService.getBatch(ctx.getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId)
            call.enqueue(object : Callback<BatchPojo> {
                override fun onResponse(call: Call<BatchPojo>, response: Response<BatchPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                batchList.addAll(response.body()!!.data)
                                batchSpinnerAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<BatchPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val batchSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = batchList[position].batch_name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return batchList[position].batch_name
        }

        override fun getCount(): Int {
            return batchList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = batchList[position].batch_name
            return convertView as View
        }
    }

    private val batchSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            ctx.hideSoftKeyboard()
            setClassHintSpinner()
            if (position != 0) {
                batchId = batchList[position].id
                getClass()
            } else {
                batchId = ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun getClass() {
        if (ConnectionDetector.isConnected(ctx)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ClassPojo> = apiService.getClass(ctx.getFromPrefs(TeacherConstant.SCHOOL_ID).toString(), programId, batchId, ctx.getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<ClassPojo> {
                override fun onResponse(call: Call<ClassPojo>, response: Response<ClassPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            setClassHintSpinner()
                            if (response.body()!!.data.size > 0) {
                                classList.addAll(response.body()!!.data)
                                classSpinnerAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ClassPojo>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private val classSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            ctx.hideSoftKeyboard()
            classId = if (position != 0) {
                classList[position].id
            } else {
                ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val classSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = classList[position].classes
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return classList[position].classes
        }

        override fun getCount(): Int {
            return classList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = classList[position].classes
            return convertView as View
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            getProgram()
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
        }
    }
}