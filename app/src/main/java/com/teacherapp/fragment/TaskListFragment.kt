package com.teacherapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.TaskAdapter
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.pojo.TaskListDataPojo
import com.teacherapp.pojo.TaskListPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class TaskListFragment : Fragment() {

    private lateinit var view_fragment: View
    private lateinit var prefs: TeacherPreference
    private lateinit var rv_task_list: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var tast_adapter: TaskAdapter
    private lateinit var ctx: HomeActivity
    private lateinit var task_list: ArrayList<TaskListDataPojo>
    private lateinit var calendarView: MaterialCalendarView
    private lateinit var userData : LoginPojo

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.task_management_activity, container, false)
            initialize()
            setListener()
        }
        return view_fragment
    }
/*
    override fun calenderChangeMethod() {
        taskList(prefs.getUserId())
    }*/

    override fun onResume() {
        super.onResume()
        ctx.addClick(1)
        taskList()
    }

    private fun initialize() {
        ctx = activity as HomeActivity
        prefs = TeacherPreference(ctx)
        userData = ctx.getUserData()!!
        task_list = ArrayList()
        calendarView = view_fragment.findViewById(R.id.calendar_view)
        rv_task_list = view_fragment.findViewById(R.id.rv_task_list)
        tv_no_data = view_fragment.findViewById(R.id.tv_no_data)
    }

    private fun setListener() {
    }

    private fun taskList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TaskListPojo> = apiService.getTaskList(ctx.getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<TaskListPojo> {
                override fun onResponse(call: Call<TaskListPojo>, response: Response<TaskListPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1" && response.body()!!.data.size > 0) {
                            task_list = ArrayList()
                            rv_task_list.visibility = View.VISIBLE
                            tv_no_data.visibility = View.GONE
                            calendarView.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                task_list.addAll(response.body()!!.data)
                                tast_adapter = TaskAdapter(ctx, task_list, TaskListFragment(), ctx, userData)
                                val llm = LinearLayoutManager(ctx)
                                llm.orientation = LinearLayoutManager.VERTICAL
                                rv_task_list.layoutManager = llm
                                rv_task_list.adapter = tast_adapter
                            } else {
                                ctx.displayToast(response.body()!!.message)
                            }
                        } else {
                            rv_task_list.visibility = View.GONE
                            tv_no_data.visibility = View.VISIBLE
                            calendarView.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<TaskListPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }
}