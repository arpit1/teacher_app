package com.teacherapp.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.activity.TeacherShowLeaveListActivity
import com.teacherapp.pojo.LeaveManagementDataPojo
import com.teacherapp.pojo.LeaveManagementPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashSet

class LeaveManagementFragment : Fragment() {

    private lateinit var view_fragment: View
    private lateinit var rl_reject_leave: RelativeLayout
    private lateinit var rl_pending_leave: RelativeLayout
    private lateinit var rl_approved_leave: RelativeLayout
    private lateinit var rl_total_leave: RelativeLayout
    private lateinit var calendar: Calendar
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private lateinit var prefs: TeacherPreference
    private lateinit var tv_total_count: TextView
    private lateinit var tv_total_approved_count: TextView
    private lateinit var tv_total_pending_count: TextView
    private lateinit var tv_total_reject_count: TextView
    private lateinit var ctx: HomeActivity
    private var calendarView: MaterialCalendarView? = null
    private val colors = intArrayOf(
            R.drawable.circle1,
            R.drawable.circle3,
            R.drawable.circle
    )
    private lateinit var dates: ArrayList<LeaveManagementDataPojo>

    private lateinit var leavesPending: HashSet<CalendarDay>
    private lateinit var leavesRejected: HashSet<CalendarDay>
    private lateinit var leavesApproved: HashSet<CalendarDay>

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.leave_management_activity, container, false)
            dates = ArrayList()

            initialize()
            setListener()
        }
        return view_fragment
    }

    private fun initialize() {
        prefs = TeacherPreference(activity)
        ctx = activity as HomeActivity
        calendar = Calendar.getInstance()
        year = calendar.get(Calendar.YEAR)
        month = calendar.get(Calendar.MONTH)
        day = calendar.get(Calendar.DAY_OF_MONTH)

        rl_reject_leave = view_fragment.findViewById(R.id.rl_reject_leave)
        rl_pending_leave = view_fragment.findViewById(R.id.rl_pending_leave)
        rl_approved_leave = view_fragment.findViewById(R.id.rl_approved_leave)
        rl_total_leave = view_fragment.findViewById(R.id.rl_total_leave)
        tv_total_count = view_fragment.findViewById(R.id.tv_total_count)
        tv_total_approved_count = view_fragment.findViewById(R.id.tv_total_approved_count)
        tv_total_pending_count = view_fragment.findViewById(R.id.tv_total_pending_count)
        tv_total_reject_count = view_fragment.findViewById(R.id.tv_total_reject_count)

        calendarView = view_fragment.findViewById(R.id.calendar_view)// as CustomCalendarView

    }

    override fun onResume() {
        super.onResume()
        if (ConnectionDetector.isConnected(activity)) {
            leaveList()
        } else {
            ctx.displayToast(resources.getString(R.string.no_data_available))
        }
    }

    private fun setListener() {
        rl_reject_leave.setOnClickListener {
            startActivity(Intent(activity, TeacherShowLeaveListActivity::class.java)
                    .putExtra("header", getString(R.string.total_reject))
                    .putExtra("type", "2")
                    .putExtra("year", year))
        }
        rl_pending_leave.setOnClickListener {
            startActivity(Intent(activity, TeacherShowLeaveListActivity::class.java)
                    .putExtra("header", getString(R.string.total_pending))
                    .putExtra("type", "3")
                    .putExtra("year", year))
        }
        rl_approved_leave.setOnClickListener {
            startActivity(Intent(activity, TeacherShowLeaveListActivity::class.java)
                    .putExtra("header", getString(R.string.total_approv))
                    .putExtra("type", "1")
                    .putExtra("year", year))
        }
        rl_total_leave.setOnClickListener {
            startActivity(Intent(activity, TeacherShowLeaveListActivity::class.java)
                    .putExtra("header", getString(R.string.total_leave))
                    .putExtra("type", "0")
                    .putExtra("year", year))
        }
    }

    private fun leaveList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LeaveManagementPojo> = apiService.getTeacherLeave(ctx.getFromPrefs(TeacherConstant.ID).toString())
            call.enqueue(object : Callback<LeaveManagementPojo> {
                override fun onResponse(call: Call<LeaveManagementPojo>, response: Response<LeaveManagementPojo>) {
                    if (response.body()!!.status == "1") {
                        tv_total_count.text = "(" + response.body()!!.year_counts!!.total_leaves + ")"
                        tv_total_approved_count.text = "(" + response.body()!!.year_counts!!.approved_leave + ")"
                        tv_total_reject_count.text = "(" + response.body()!!.year_counts!!.rejected_leave + ")"
                        tv_total_pending_count.text = "(" + response.body()!!.year_counts!!.pending_leave + ")"

                        leavesPending = HashSet()
                        leavesRejected = HashSet()
                        leavesApproved = HashSet()

                        leavesPending.clear()
                        leavesRejected.clear()
                        leavesApproved.clear()

                        dates.addAll(response.body()!!.data)

                        for (i in 0 until dates.size) {
                            val startDate = dates[i].from_date.split("-")
                            val toDate = dates[i].to_date.split("-")
                            val datesList = getDates(startDate[2] + "-" + startDate[1] + "-" + startDate[0], toDate[2] + "-" + toDate[1] + "-" + toDate[0])

                            for (j in 0 until datesList.size) {
                                val date = datesList[j]
                                val calendar = Calendar.getInstance()
                                calendar.time = date

                                val dayToHighlight = CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                                when {
                                    dates[i].status == "1" -> {
                                        leavesApproved.add(dayToHighlight)
                                    }
                                    dates[i].status == "2" -> {
                                        leavesRejected.add(dayToHighlight)
                                    }
                                    dates[i].status == "3" -> {
                                        leavesPending.add(dayToHighlight)
                                    }
                                }
                            }
                        }
                        calendarView!!.setTileHeightDp(30)
                        calendarView!!.addDecorators(CircleDecoratorApproved(ctx, leavesApproved), CircleDecoratorRejected(ctx, leavesRejected), CircleDecoratorPending(ctx, leavesPending))
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<LeaveManagementPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun getDates(dateString1: String, dateString2: String): List<Date> {
        val dates = ArrayList<Date>()
        val df1 = SimpleDateFormat(ctx.getString(R.string.dateformat), Locale.US)

        var date1: Date? = null
        var date2: Date? = null

        try {
            date1 = df1.parse(dateString1)
            date2 = df1.parse(dateString2)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val cal1 = Calendar.getInstance()
        cal1.time = date1


        val cal2 = Calendar.getInstance()
        cal2.time = date2

        while (!cal1.after(cal2)) {
            dates.add(cal1.time)
            cal1.add(Calendar.DATE, 1)
        }
        return dates
    }

    inner class CircleDecoratorApproved(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[0])
        private var datesLow: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesLow.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))
            view.setSelectionDrawable(drawable)
        }
    }

    inner class CircleDecoratorRejected(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[2])
        private var datesMedium: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesMedium.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))
            view.setSelectionDrawable(drawable)
        }
    }

    inner class CircleDecoratorPending(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[1])
        private var datesHigh: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesHigh.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))
            view.setSelectionDrawable(drawable)
        }
    }
}