package com.teacherapp.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.BaseActivity
import com.teacherapp.activity.ChatScreenActivity
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.ChatContactAdapter
import com.teacherapp.pojo.UserListPojo
import com.teacherapp.util.TeacherConstant
import io.socket.emitter.Emitter
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class InnerChatFragment : Fragment() {

    private lateinit var viewChatfragment: View
    private lateinit var ctx: HomeActivity
    private lateinit var rvChatList: RecyclerView
    private lateinit var noDataAvailable: TextView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var LOGIN_ID: String
    private lateinit var SCHOOL_ID: String
    private var user_list_data: ArrayList<UserListPojo>? = null
    private lateinit var adapter: ChatContactAdapter
    private var searchView: android.support.v7.widget.SearchView? = null
    private var contactDataFilter: ArrayList<UserListPojo>? = null
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewChatfragment = inflater!!.inflate(R.layout.fragment_innerchat_layout, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewChatfragment
    }

    private fun initialize() {
        rvChatList = viewChatfragment.findViewById(R.id.rvChatList)
        searchView = viewChatfragment.findViewById(R.id.search_box)
        mLayoutManager = LinearLayoutManager(ctx)
        contactDataFilter = ArrayList()
        user_list_data = ArrayList()
        rvChatList.layoutManager = mLayoutManager
        noDataAvailable = viewChatfragment.findViewById(R.id.noDataAvailable)
        LOGIN_ID = ctx.getFromPrefs(TeacherConstant.ID).toString()
        SCHOOL_ID = ctx.getFromPrefs(TeacherConstant.SCHOOL_ID).toString()

        val jsonObject = JSONObject()
        try {
            jsonObject.put("user_id", "T_$LOGIN_ID")
            jsonObject.put("school_id", SCHOOL_ID)
            ctx.mSocket.emit("connectUser", jsonObject)
            ctx.mSocket.on("usersList", getUserList)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        ctx.mSocket.on("get_private_chat", onNewMessage)

    }

    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }

        val handler = Handler()
        handler.postDelayed({
            ctx.runOnUiThread {
                if ((ctx.getClassShortName() == ".activity.HomeActivity")) {
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("user_id", "T_$LOGIN_ID")
                        jsonObject.put("school_id", SCHOOL_ID)
                        ctx.mSocket.emit("connectUser", jsonObject)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }, 550)
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed) {
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            val jsonObject = JSONObject()
            try {
                jsonObject.put("user_id", "T_$LOGIN_ID")
                jsonObject.put("school_id", SCHOOL_ID)
                ctx.mSocket.emit("connectUser", jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()


        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            if (searchView != null)
                searchView!!.clearFocus()
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                val task: ArrayList<UserListPojo> = user_list_data!!
                contactDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until task.size)
                            .filter { getTeacherName(task[it]).toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { contactDataFilter!!.add(task[it]) }
                } else {
                    contactDataFilter!!.addAll(task)
                }
                setAdapter(contactDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }
        })

        rvChatList.addOnItemTouchListener(BaseActivity.RecyclerTouchListener(context, rvChatList, object : BaseActivity.ClickListener {
            override fun onClick(view: View, position: Int) {
                val task: ArrayList<UserListPojo> = contactDataFilter!!
                val username: String?
                val receiverId: String?
                if (!searchView!!.query.toString().isEmpty()) {
                    task[position].message_counter = 0
                    task[position].chat_size = 0
                    task[position].last_msg!!.put("isRead", "1")
                    username = task[position].father_name
                    receiverId = task[position].id
                } else {
                    user_list_data!![position].message_counter = 0
                    user_list_data!![position].chat_size = 0
                    user_list_data!![position].last_msg!!.put("isRead", "1")
                    username = user_list_data!![position].father_name
                    receiverId = user_list_data!![position].id
                }
//                adapter.notifyDataSetChanged()

                val intent = Intent(context, ChatScreenActivity::class.java)
                intent.putExtra("receiver_id", receiverId)
                intent.putExtra("login_id", LOGIN_ID)
                intent.putExtra("username", username)
                startActivity(intent)
            }

            override fun onLongClick(view: View?, position: Int) {
            }
        }))
    }

    private fun getTeacherName(teacherData: UserListPojo): String {
        return if (teacherData.father_name != "")
            teacherData.father_name!!
        else
            teacherData.father_name!!
    }

    private val getUserList = Emitter.Listener { args ->
        ctx.runOnUiThread {
            val data = args[0] as JSONObject
            try {
                val jarray = data.getJSONArray("data")
                println("USERLIST>>>>" + args[0])
                user_list_data = ArrayList()
                if (jarray.length() > 0) {
                    for (i in 0 until jarray.length()) {
                        val json = jarray.getJSONObject(i)
                        val user_data = UserListPojo()
                        user_data.id = json.getString("id")
                        user_data.login_id = "T_$LOGIN_ID"
                        user_data.isConnected = json.optString("isConnected")
                        user_data.profile_pic = json.optString("profile_pic")
                        user_data.chat_size = Integer.parseInt(json.optString("chat_size"))
                        user_data.last_msg = json.optJSONObject("last_msg")
                        user_data.type = json.optString("type")
                        if (json.optString("type") == "1") {
                            user_data.father_name = json.optString("fname") + " " + json.optString("lname")
                        } else {
                            user_data.father_name = json.optString("father_name")
                        }

                        user_data.message_counter = Integer.parseInt(json.optString("chat_size"))

                        user_list_data!!.add(user_data)
                    }
                    searchView?.setQuery("", false)
//                    if (searchView!!.query.toString().isNotEmpty()) {
//                        if (user_list_data!!.size > 0 && user_list_data != null) {
//                            val task: ArrayList<UserListPojo> = user_list_data!!
//                            contactDataFilter!!.clear()
//
//                            (0 until task.size)
//                                    .filter { getTeacherName(task[it]).toLowerCase().contains(searchView!!.query.toString().toLowerCase()) }
//                                    .forEach { contactDataFilter!!.add(task[it]) }
//                        }
//                        setAdapter(contactDataFilter!!)
//                    } else {
                    setAdapter(user_list_data)
//                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    // call when new message received
    private val onNewMessage = Emitter.Listener { args ->
        ctx.runOnUiThread {
            val data = args[0] as JSONObject
            if ((ctx.getClassShortName() == ".activity.HomeActivity")) {
                val jsonObject = JSONObject()
                try {
                    jsonObject.put("user_id", "T_$LOGIN_ID")
                    jsonObject.put("school_id", SCHOOL_ID)
                    ctx.mSocket.emit("connectUser", jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun setAdapter(user_list_data: ArrayList<UserListPojo>?) {
        adapter = ChatContactAdapter(ctx, user_list_data!!)
        rvChatList.adapter = adapter
    }
}