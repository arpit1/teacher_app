package com.teacherapp.fragment

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.Userlistadapter
import com.teacherapp.pojo.UserData
import com.teacherapp.pojo.UserListPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactFragment : Fragment() {

    private lateinit var viewContactFragment: View
    private lateinit var context: HomeActivity
    private lateinit var rvContactList: RecyclerView
    private lateinit var noDataAvailable: TextView
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var mLayoutManager: LinearLayoutManager
    private var dialog: TeacherDialog? = null
    private var searchView: android.support.v7.widget.SearchView? = null
    private var user_list_data: ArrayList<UserListPojo>? = null
    private var LOGIN_ID: String? = null
    private var SCHOOL_ID: String? = null
    private lateinit var adapter: Userlistadapter
    internal lateinit var multiselect_list: ArrayList<UserListPojo>
    private var contactDataFilter: ArrayList<UserListPojo>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewContactFragment = inflater!!.inflate(R.layout.contacts_fragements, container, false)
        context = activity as HomeActivity
        initialize()
        setListener()
        return viewContactFragment
    }

    private fun initialize() {
        dialog = TeacherDialog(context)
        mLayoutManager = LinearLayoutManager(context)
        searchView = viewContactFragment.findViewById(R.id.search_box)
        rvContactList = viewContactFragment.findViewById(R.id.rvContactList)
        noDataAvailable = viewContactFragment.findViewById(R.id.noDataAvailable)
        rvContactList.layoutManager = mLayoutManager
        contactDataFilter = ArrayList()
        multiselect_list = ArrayList()
        LOGIN_ID = context.getFromPrefs(TeacherConstant.ID).toString()
        SCHOOL_ID = context.getFromPrefs(TeacherConstant.SCHOOL_ID).toString()
        user_list_data = ArrayList()

        if (!fragmentResume && fragmentVisible && user_list_data != null && user_list_data?.size == 0) {   //only when first time fragment is created
            getUserList(LOGIN_ID!!, "1", context.getFromPrefs(TeacherConstant.SCHOOL_ID).toString())
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val task: ArrayList<UserListPojo> = user_list_data!!
                contactDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until task.size)
                            .filter { getParentName(task[it]).toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { contactDataFilter!!.add(task[it]) }
                } else {
                    contactDataFilter!!.addAll(task)
                }
                setAdapter(contactDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                context.hideSoftKeyboard()
                return false
            }
        })

       /* rvContactList.addOnItemTouchListener(BaseActivity.RecyclerTouchListener(context, rvContactList, object : BaseActivity.ClickListener {

            override fun onClick(view: View, position: Int) {
                val task: java.util.ArrayList<UserListPojo> = contactDataFilter!!
                val username: String?
                val receiverId: String?
                val counter: Int
                if (!searchView!!.query.toString().isEmpty()) {
                    counter = task[position].getMessageCounter()
                    task[position].setMessageCounter(0)
                    username = task[position].getFatherName()!!
                    receiverId = if (task[position].getUserType().equals("1"))
                        task[position].getId()!!
                    else
                        "P_" + task[position].getId()

                } else {
                    counter = user_list_data!![position].getMessageCounter()
                    user_list_data!![position].setMessageCounter(0)
                    username = user_list_data!![position].getFatherName()!!
                    receiverId = user_list_data!![position].getId()!!
                }

                val intent = Intent(context, ChatScreenActivity::class.java)
                intent.putExtra("receiver_id", receiverId)
                intent.putExtra("login_id", LOGIN_ID)
                intent.putExtra("username", username)
                intent.putExtra("counter", counter)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

            override fun onLongClick(view: View?, position: Int) {
            }
        }))*/
    }

    private fun getParentName(teacherData: UserListPojo): String {
        return if (teacherData.father_name != "")
            teacherData.father_name!!
        else
            teacherData.father_name!!
    }

    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            val handler = Handler()
            handler.postDelayed({
                getUserList(LOGIN_ID!!, "1", context.getFromPrefs(TeacherConstant.SCHOOL_ID).toString())
            }, 150)
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            if (searchView != null)
                searchView!!.clearFocus()
        }
    }

    private fun getUserList(user_id: String, type: String, school_id: String) {
        if (ConnectionDetector.isConnected(context)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<UserData> = apiService.getUserList(user_id, type, school_id)
            call.enqueue(object : Callback<UserData> {
                override fun onResponse(call: Call<UserData>, response: Response<UserData>?) {
                    if (response != null) {
                        user_list_data = ArrayList()
                        if (response.body()!!.status == "1") {
                            user_list_data!!.addAll(response.body()!!.data)
                            setAdapter(user_list_data)
                        } else {
                            context.displayToast(response.body()!!.message)
                        }
                    }
                }

                override fun onFailure(call: Call<UserData>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            context.displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    /*private val mActionModeCallback = object : ActionMode.Callback {

        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            // Inflate a menu resource providing context menu items
            val inflater = mode.menuInflater
            inflater.inflate(R.menu.menu_multi_select, menu)
            context_menu = menu
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            return false // Return false if nothing is done
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.action_delete -> {
                    val builder = AlertDialog.Builder(context)
                    builder.setMessage(R.string.delete_chatMessage).setPositiveButton(R.string.yes, dialogClickListener)
                            .setNegativeButton(R.string.no, dialogClickListener).show()
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            mActionMode = null
            isMultiSelect = false
            multiselect_list = ArrayList()
            refreshAdapter()
        }
    }
    //dialog for delete user chat
    internal var dialogClickListener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { _, which ->
        when (which) {
            DialogInterface.BUTTON_POSITIVE -> if (multiselect_list.size > 0) {
                for (i in multiselect_list.indices) {
                    val receiver_id = multiselect_list[i].getId()
                    context.db.deleteAllMessageId(LOGIN_ID!!, receiver_id!!)
                }
                if (mActionMode != null) {
                    mActionMode!!.finish()
                }
            }
            DialogInterface.BUTTON_NEGATIVE -> {
            }
        }
    }*/

    private fun setAdapter(user_list_data: ArrayList<UserListPojo>?) {
        adapter = Userlistadapter(context, user_list_data!!, multiselect_list)
        rvContactList.adapter = adapter
    }
}