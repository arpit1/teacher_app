package com.teacherapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.adapter.ChatListAdapter

class ChatsFragment : Fragment() {

    private lateinit var view_fragment : View
    private lateinit var chat_recyclerview: RecyclerView
    private lateinit var tv_chat_data: TextView
    private lateinit var chat_list : ArrayList<String>
    private lateinit var context : HomeActivity
    private lateinit var adapter: ChatListAdapter
    private lateinit var mLayoutManager : LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        if (inflater != null) {
            view_fragment = inflater.inflate(R.layout.fragement_chats_app, container, false)
            init()
        }
        return view_fragment
    }

    private fun init() {
        mLayoutManager = LinearLayoutManager(activity)
        chat_recyclerview = view_fragment.findViewById(R.id.chat_list)
        tv_chat_data = view_fragment.findViewById(R.id.tv_chat_data)
        context = activity as HomeActivity
        context.hideSoftKeyboard()
        chat_list = ArrayList()
        for (i in 0..19) {
            chat_list.add("Chat in progress")

        }
        adapter = ChatListAdapter(activity, chat_list)
        chat_recyclerview.adapter = adapter
        chat_recyclerview.layoutManager = mLayoutManager
    }
}