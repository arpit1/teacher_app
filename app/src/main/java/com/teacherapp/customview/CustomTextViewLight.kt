package com.teacherapp.customview

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView


/**
 * Created by upasna.mishra on 11/24/2017.
 */

class CustomTextViewLight(context: Context, attrs: AttributeSet) : TextView(context, attrs) {

    init {
        val face = Typeface.createFromAsset(context.assets, "font/ROBOTO-LIGHT.TTF")
        this.typeface = face
    }
    
}
