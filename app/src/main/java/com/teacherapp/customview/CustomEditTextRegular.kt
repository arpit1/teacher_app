package com.teacherapp.customview

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.EditText


/**
 * Created by upasna.mishra on 11/24/2017.
 */

class CustomEditTextRegular(context: Context, attrs: AttributeSet) : EditText(context, attrs) {

    init {
        val face = Typeface.createFromAsset(context.assets, "font/ROBOTO-REGULAR.TTF")
        this.typeface = face
    }

}
