package com.teacherapp.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.SchoolDiaryDetailActivity
import com.teacherapp.pojo.FilesDataPojo
import com.teacherapp.util.TeacherConstant

/**
 * Created by arpit.jain on 2/28/2018.
 */
class FilesListAdapter (val context: Activity, val fileList: ArrayList<FilesDataPojo>) : RecyclerView.Adapter<FilesListAdapter.ViewHolder>() {

    var ctx: SchoolDiaryDetailActivity = context as SchoolDiaryDetailActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilesListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_attach_file, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: FilesListAdapter.ViewHolder, position: Int) {
        holder.bindItems(fileList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return fileList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(fileList: FilesDataPojo, ctx: SchoolDiaryDetailActivity) {

            val filePic: ImageView = itemView.findViewById(R.id.filePic)
            val fileName: TextView = itemView.findViewById(R.id.fileName)
            val fileTime: TextView = itemView.findViewById(R.id.fileTime)
            val row_layout : LinearLayout = itemView.findViewById(R.id.row_layout)

            if(fileList.file != null) {
                val lastValue = after(fileList.file!!, ".").toLowerCase()
                if (lastValue == "pdf") {
                    filePic.setImageResource(R.mipmap.pdficon)
                } else if (lastValue == "docx" || lastValue == "doc") {
                    filePic.setImageResource(R.mipmap.texticon)
                } else if (lastValue == "png" || lastValue == "jpg" || lastValue == "jpeg") {
                    if (fileList.file != "") {
                        ctx.setProfileImageInLayout(ctx, TeacherConstant.ATTACH_DIARY_IMAGE_URL + fileList.file, filePic)
                    } else {
                        filePic.setImageResource(R.mipmap.ic_launcher)
                    }
                }

                fileName.text = fileList.file
                if (fileList.created_at != "") {
                    val new_date = fileList.created_at.replace(".000Z", "").split("T")
                    fileTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
                }
            }

            row_layout.setTag(R.string.data, fileList)
            row_layout.setOnClickListener(ctx)

        }

        private fun after(value: String, a: String): String {
            // Returns a substring containing all characters after a string.
            val posA = value.lastIndexOf(a)
            if (posA == -1) {
                return ""
            }
            val adjustedPosA = posA + a.length
            return if (adjustedPosA >= value.length) {
                ""
            } else value.substring(adjustedPosA)
        }
    }
}