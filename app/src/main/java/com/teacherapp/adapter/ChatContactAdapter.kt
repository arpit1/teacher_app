package com.teacherapp.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.pojo.UserListPojo
import com.teacherapp.util.TeacherConstant
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONObject
import java.util.*


class ChatContactAdapter(val context: Activity, private var teacherList: ArrayList<UserListPojo>) : RecyclerView.Adapter<ChatContactAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity
    private lateinit var user_name: TextView
    private lateinit var user_isconnected: TextView
    private lateinit var lastMsg: TextView
    private var layout: LinearLayout? = null
    private var image: ImageView? = null
    private lateinit var chat_count: TextView

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatContactAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.user_data_row, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ChatContactAdapter.ViewHolder, position: Int) {
        holder.bindItems(teacherList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return teacherList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("ResourceAsColor")
        fun bindItems(teacherList: UserListPojo, ctx: HomeActivity) {

            val profile_pic: CircleImageView = itemView.findViewById(com.teacherapp.R.id.user_img)
            user_name = itemView.findViewById(com.teacherapp.R.id.user_email)
            image = itemView.findViewById(com.teacherapp.R.id.image)
            user_isconnected = itemView.findViewById(com.teacherapp.R.id.is_connected)
            lastMsg = itemView.findViewById(com.teacherapp.R.id.lastMsg)
            val lastMsgTime: TextView = itemView.findViewById(R.id.lastMsgTime)
            chat_count = itemView.findViewById(com.teacherapp.R.id.chatcount)
            layout = itemView.findViewById(R.id.mainlayout)
            user_name.text = teacherList.father_name
            if (teacherList.type == "0") {
                user_name.setTextColor(ContextCompat.getColor(ctx, R.color.black))
            } else {
                user_name.setTextColor(ContextCompat.getColor(ctx, R.color.brown))
            }
            user_isconnected.visibility = View.VISIBLE
            if (teacherList.profile_pic != null && teacherList.profile_pic != "") {
                if (teacherList.type == "0")
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.STUDENT_PROFILE_BASE_URL + teacherList.profile_pic, profile_pic)
                else
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.SCHOOL_PROFILE_PIC + teacherList.profile_pic, profile_pic)
            } else {
                profile_pic.setImageResource(com.teacherapp.R.mipmap.usericon)
            }
            if (teacherList.message_counter > 0) {
                chat_count.visibility = View.VISIBLE
                chat_count.text = teacherList.message_counter.toString()
            } else {
                chat_count.visibility = View.GONE
            }
            if (teacherList.isConnected == "1") {
                user_isconnected.text = context.getString(R.string.online)
                user_isconnected.setTextColor(ContextCompat.getColor(ctx, R.color.selected_gray))
            } else {
                user_isconnected.text = context.getString(R.string.offline)
                user_isconnected.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
            }
            val arr: JSONObject = teacherList.last_msg!!

            lastMsgTime.visibility = View.VISIBLE
            if (arr.optString("isSend") == "1") {
                if (arr.optString("isRead") == "0")
                    lastMsgTime.setTextColor(ContextCompat.getColor(ctx, R.color.selected_gray))
                else
                    lastMsgTime.setTextColor(ContextCompat.getColor(ctx, R.color.hint_color))
            }

            when {
                DateUtils.isToday(ctx.getDate(arr.optString("dates")).time) -> lastMsgTime.text = arr.optString("time")
                DateUtils.isToday(ctx.getDate(arr.optString("dates")).time + DateUtils.DAY_IN_MILLIS) -> lastMsgTime.text = ctx.resources.getString(R.string.yesterday)
                else -> lastMsgTime.text = arr.optString("dates")
            }

            lastMsg.visibility = View.VISIBLE
            when {
                arr.optString("type") == "text" -> {
                    image!!.visibility = View.GONE
                    lastMsg.text = ctx.decode(arr.optString("message"))
                }
                arr.optString("type") == "video" -> {
                    image!!.visibility = View.VISIBLE
                    lastMsg.text = ctx.resources.getString(R.string.video)
                }
                arr.optString("type") == "image" -> {
                    image!!.visibility = View.VISIBLE
                    lastMsg.text = ctx.resources.getString(R.string.photo)
                }
            }
        }
    }
}