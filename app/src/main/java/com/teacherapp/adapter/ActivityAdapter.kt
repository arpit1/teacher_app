package com.teacherapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.BaseActivity
import com.teacherapp.activity.HomeActivity
import com.teacherapp.pojo.EventPojoItem
import com.teacherapp.util.TeacherConstant

class ActivityAdapter(ctxext: HomeActivity, activityPostList: ArrayList<EventPojoItem>) : BaseAdapter() {

    var ctx: HomeActivity = ctxext
    var activity_list: MutableList<EventPojoItem> = activityPostList
    //var arraylist: MutableList<EventPojoData> = activityPostList

    private lateinit var user_image: ImageView
    private lateinit var tv_name: TextView
    private lateinit var tv_date: TextView
    private lateinit var tv_des: TextView
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v: View? = null
        if (v == null)
            v = LayoutInflater.from(parent!!.context).inflate(R.layout.activity_grid_row, parent, false)

        user_image = v!!.findViewById(R.id.img_profile)
        tv_name = v.findViewById(R.id.tv_name)
        tv_date = v.findViewById(R.id.tv_date)
        tv_des = v.findViewById(R.id.tv_des)
        tv_name.visibility = View.VISIBLE
        tv_date.visibility = View.VISIBLE
        tv_des.visibility = View.VISIBLE

        tv_name.text = activity_list[position].event_name
        tv_date.text = ctx.getChangedDate(activity_list[position].dates)
        tv_des.text = activity_list[position].category

        if ((activity_list[position].upload_type == "Images" || activity_list[position].upload_type == "Image") && activity_list[position].file.size > 0) {
            (ctx as BaseActivity).setHomePostImageInLayout(ctx, TeacherConstant.IMAGE_URl + "" + activity_list[position].file[0].file, user_image)
        } else if (activity_list[position].upload_type == "Video" && activity_list[position].file.size > 0) {
            (ctx as BaseActivity).setHomePostImageInLayout(ctx, TeacherConstant.THUMB_IMAGE_URl + "" + activity_list[position].file[0].thumbnail, user_image)
        }
        return v
    }

    override fun getItem(position: Int): Any {
        return 0
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return activity_list.size
    }
    /*  // Filter Class
      fun filter(charText: String) {
          var charText = charText
          charText = charText.toLowerCase(Locale.getDefault())
          activity_list.clear()
          if (charText.isEmpty()) {
              activity_list.addAll(arraylist)
          } else {
              arraylist.indices
                      .map { arraylist[it] }
                      .filter { it.event_name.toLowerCase(Locale.getDefault()).contains(charText) }
                      .forEach { activity_list.add(it) }
          }
          notifyDataSetChanged()
      }*/
}