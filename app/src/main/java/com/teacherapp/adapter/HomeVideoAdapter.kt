package com.teacherapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.BaseActivity
import com.teacherapp.activity.HomeAllVideo
import com.teacherapp.pojo.ImagePojo
import com.teacherapp.util.TeacherConstant

/**
 * Created by upasna.mishra on 2/14/2018.
 */
class HomeVideoAdapter(ctx: HomeAllVideo, videoList: ArrayList<ImagePojo>) : BaseAdapter() {

    var ctx_home: HomeAllVideo = ctx
    var video_list = videoList
    private lateinit var postImage: ImageView
    private lateinit var playvideoicon: ImageView
    private lateinit var tv_name: TextView
    private lateinit var tv_date: TextView
    private lateinit var tv_des: TextView

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.activity_grid_row, parent, false)
        postImage = view.findViewById(R.id.img_profile)
        playvideoicon = view.findViewById(R.id.playvideoicon)
        tv_name = view.findViewById(R.id.tv_name)
        tv_date = view.findViewById(R.id.tv_date)
        tv_des = view.findViewById(R.id.tv_des)
        tv_name.visibility = View.GONE
        tv_date.visibility = View.GONE
        tv_des.visibility = View.GONE
        playvideoicon.visibility = View.VISIBLE
        (ctx_home as BaseActivity).setHomePostImageInLayout(ctx_home, TeacherConstant.THUMB_IMAGE_URl + "" + video_list[position].thumbnail, postImage)
        return view
    }

    override fun getItem(p0: Int): Any {
        return 0
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return video_list.size
    }

}