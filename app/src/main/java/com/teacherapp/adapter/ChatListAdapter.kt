package com.teacherapp.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity

class ChatListAdapter(val context: FragmentActivity, val chat_list: ArrayList<String>) : RecyclerView.Adapter<ChatListAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity
    //val chatlist2  = chat_list

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListAdapter.ViewHolder? {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.chat_list_row, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ChatListAdapter.ViewHolder, position: Int) {
        holder.bindItems(chat_list[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return chat_list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(home_post: String) {
            val textView: TextView = itemView.findViewById(R.id.textView)

            textView.text = home_post
        }
    }
}