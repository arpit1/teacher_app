package com.teacherapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.teacherapp.fragment.ContactFragment
import com.teacherapp.fragment.InnerChatFragment

/**
 * Created by upasna.mishra on 3/5/2018.
 */
class ChatPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return InnerChatFragment()
            1 -> return ContactFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 2
    }
}