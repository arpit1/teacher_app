package com.teacherapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.BaseActivity
import com.teacherapp.pojo.ImagePojo
import com.teacherapp.util.TeacherConstant

/**
 * Created by hitesh.mathur on 12/8/2017.
 */
class ImagesAdapter(val ctx: Activity, activityPostList: ArrayList<ImagePojo>, var uploaded_type: String) : BaseAdapter() {

    var activity_list = activityPostList
    private lateinit var user_image: ImageView
    private lateinit var tv_name: TextView
    private lateinit var tv_date: TextView
    private lateinit var tv_des: TextView
    private lateinit var playvideoicon: ImageView

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.activity_grid_row, parent, false)
        user_image = v.findViewById(R.id.img_profile)
        playvideoicon = v.findViewById(R.id.playvideoicon)
        tv_name = v.findViewById(R.id.tv_name)
        tv_date = v.findViewById(R.id.tv_date)
        tv_des = v.findViewById(R.id.tv_des)
        tv_name.visibility = View.GONE
        tv_date.visibility = View.GONE
        tv_des.visibility = View.GONE
        val lastValue = after(activity_list[position].files, ".").toLowerCase()
        if (lastValue == "png" || lastValue == "jpg" || lastValue == "jpeg" || lastValue == "gif") {
            playvideoicon.visibility = View.GONE
            (ctx as BaseActivity).setHomePostImageInLayout(ctx, TeacherConstant.IMAGE_URl + "" + activity_list[position].files, user_image)
        } else if (lastValue == "mp4" || lastValue == "mov" || lastValue == "3gp") {
            playvideoicon.visibility = View.VISIBLE
            (ctx as BaseActivity).setHomePostImageInLayout(ctx, TeacherConstant.THUMB_IMAGE_URl + "" + activity_list[position].thumbnail, user_image)
        }
        return v
    }

    private fun after(value: String, a: String): String {
        // Returns a substring containing all characters after a string.
        val posA = value.lastIndexOf(a)
        if (posA == -1) {
            return ""
        }
        val adjustedPosA = posA + a.length
        return if (adjustedPosA >= value.length) {
            ""
        } else value.substring(adjustedPosA)
    }

    override fun getItem(position: Int): Any {
        return 0
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return activity_list.size
    }
}