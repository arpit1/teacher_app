package com.teacherapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.teacherapp.fragment.AddStudentProgressFragment
import com.teacherapp.fragment.CalendarFrament
import com.teacherapp.fragment.ChatFragment
import com.teacherapp.fragment.GalleryFragment
import com.teacherapp.fragment.HomeFragment

/**
 * Created by upasna.mishra on 11/27/2017.
 */
class HomeTabLayoutAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm){

    override fun getItem(position: Int): Fragment? {
        when(position){
            0 -> return HomeFragment()
            1 -> return ChatFragment()
            2 -> return CalendarFrament()
            3 -> return GalleryFragment()
            4 -> return AddStudentProgressFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 5
    }

}