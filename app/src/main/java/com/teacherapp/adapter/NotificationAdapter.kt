package com.teacherapp.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.NotificationActivity
import com.teacherapp.pojo.NotificationDataPojo
import com.teacherapp.util.TeacherConstant


/**
 * Created by upasna.mishra on 3/29/2018.
 */
class NotificationAdapter(val context: Activity, var notification_list: ArrayList<NotificationDataPojo>) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    var ctx: NotificationActivity = context as NotificationActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_notification, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NotificationAdapter.ViewHolder, position: Int) {
        holder.bindItems(notification_list[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return notification_list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(notification_list: NotificationDataPojo, ctx: NotificationActivity) {
            val tv_title : TextView = itemView.findViewById(R.id.tv_title)
            val iv_profile_pic: ImageView = itemView.findViewById(R.id.iv_profile_pic)
            val tv_description: TextView = itemView.findViewById(R.id.tv_description)
            val createdAt: TextView = itemView.findViewById(R.id.createdAt)
            val row_click: LinearLayout = itemView.findViewById(R.id.row_click)

            when {
                notification_list.profile_pic != null -> ctx.setProfileImageInLayout(ctx, TeacherConstant.STUDENT_PROFILE_BASE_URL + notification_list.profile_pic, iv_profile_pic)
//                notification_list.logo != null -> ctx.setProfileImageInLayout(ctx, TeacherConstant.SCHOOL_PROFILE_PIC + notification_list.logo, iv_profile_pic)
                else -> iv_profile_pic.setImageResource(R.mipmap.usericon)
            }

            val create_date = notification_list.created.split("T").toMutableList()
            val created_time = create_date[1].split(".").toMutableList()
            createdAt.text = ctx.changeDateTimeFormat(create_date[0] + " " + created_time[0])

            tv_description.text = notification_list.type
            tv_title.text = notification_list.title

            row_click.setTag(R.string.data, notification_list)
            row_click.setOnClickListener(ctx)
        }
    }
}