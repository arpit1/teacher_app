package com.teacherapp.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.ChatScreenActivity
import com.teacherapp.activity.HomeActivity
import com.teacherapp.activity.ParentProfileActivity
import com.teacherapp.pojo.UserListPojo
import com.teacherapp.util.TeacherConstant
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by hitesh.mathur on 3/26/2018.
 */
class Userlistadapter(val context: Activity, var teacherList: ArrayList<UserListPojo>, var select_userlist: ArrayList<UserListPojo>) : RecyclerView.Adapter<Userlistadapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity
    private lateinit var user_name: TextView
    private lateinit var lastMsg: TextView
    private var image: ImageView? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Userlistadapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.user_data_row, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: Userlistadapter.ViewHolder, position: Int) {
        holder.bindItems(teacherList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return teacherList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("ResourceAsColor")
        fun bindItems(teacherList: UserListPojo, ctx: HomeActivity) {

            val profile_pic: CircleImageView = itemView.findViewById(R.id.user_img)
            user_name = itemView.findViewById(R.id.user_email)
            lastMsg = itemView.findViewById(R.id.lastMsg)
            image = itemView.findViewById(com.teacherapp.R.id.image)
            image!!.visibility = View.GONE
            user_name.text = teacherList.father_name
            lastMsg.text = teacherList.student_name
            lastMsg.visibility = View.VISIBLE

            if (teacherList.profile_pic != null && teacherList.profile_pic != "") {
                if (teacherList.type == "0")
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.STUDENT_PROFILE_BASE_URL + teacherList.profile_pic, profile_pic)
                else
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.SCHOOL_PROFILE_PIC + teacherList.profile_pic, profile_pic)
            } else {
                profile_pic.setImageResource(R.mipmap.usericon)
            }

            itemView.setOnClickListener {
                openTeacherDialog(ctx, teacherList)
            }

            if (teacherList.type == "0") {
                user_name.text = teacherList.father_name
                user_name.setTextColor(ContextCompat.getColor(ctx, R.color.black))

                itemView.setOnClickListener {
                    openTeacherDialog(ctx, teacherList)
                }
            } else {
                user_name.text = ctx.resources.getString(R.string.admin_name, teacherList.father_name)
                user_name.setTextColor(ContextCompat.getColor(ctx, R.color.brown))

                itemView.setOnClickListener {
                    val intent = Intent(context, ChatScreenActivity::class.java)
                    intent.putExtra("receiver_id", teacherList.id)
                    intent.putExtra("login_id", ctx.getFromPrefs(TeacherConstant.ID).toString())
                    intent.putExtra("username", teacherList.father_name)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    ctx.startActivity(intent)
                }
            }
        }
    }

    private fun openTeacherDialog(ctx: HomeActivity, teacherList: UserListPojo) {
        val crossImage: ImageView
        val profilePic: CircleImageView
        val parentName: TextView
        val parentEmail: TextView
        val llInfo: LinearLayout
        val ivInfo: ImageView
        val llChat: LinearLayout
        val ivChat: ImageView
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar)
        dialog.setContentView(R.layout.teacher_dialog_layout)
        crossImage = dialog.findViewById(R.id.crossImage)
        profilePic = dialog.findViewById(R.id.profilePic)
        parentName = dialog.findViewById(R.id.parentName)
        parentEmail = dialog.findViewById(R.id.parentEmail)
        llInfo = dialog.findViewById(R.id.llInfo)
        ivInfo = dialog.findViewById(R.id.ivInfo)
        llChat = dialog.findViewById(R.id.llChat)
        ivChat = dialog.findViewById(R.id.ivChat)
        crossImage.setOnClickListener {
            dialog.dismiss()
        }
        if (teacherList.profile_pic != null && teacherList.profile_pic != "") {
            ctx.setProfileImageInLayout(ctx, TeacherConstant.STUDENT_PROFILE_BASE_URL + teacherList.profile_pic, profilePic)
        } else {
            profilePic.setImageResource(R.mipmap.usericon)
        }
        parentName.text = teacherList.father_name
        parentEmail.text = teacherList.primary_email
        llInfo.setOnClickListener {
            dialog.dismiss()
            ctx.startActivity(Intent(ctx, ParentProfileActivity::class.java).putExtra("user_id", teacherList.id))
        }
        llChat.setOnClickListener {
            dialog.dismiss()
            val receiverId = "P_" + teacherList.id

            val intent = Intent(context, ChatScreenActivity::class.java)
            intent.putExtra("receiver_id", receiverId)
            intent.putExtra("login_id", ctx.getFromPrefs(TeacherConstant.ID).toString())
            intent.putExtra("username", teacherList.father_name)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            ctx.startActivity(intent)
        }
        if (!ctx.isFinishing)
            dialog.show()
    }
}