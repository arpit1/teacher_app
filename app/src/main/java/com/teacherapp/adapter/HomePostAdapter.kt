package com.teacherapp.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.EventDetailActivity
import com.teacherapp.activity.HomeActivity
import com.teacherapp.activity.HomeDetailActivity
import com.teacherapp.pojo.GalleryPojoData
import com.teacherapp.util.TeacherConstant
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 11/30/2017.
 */
class HomePostAdapter(val context: Activity, private val home_post_list: ArrayList<GalleryPojoData>) : RecyclerView.Adapter<HomePostAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomePostAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.raw_home_layout, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: HomePostAdapter.ViewHolder, position: Int) {
        holder.bindItems(home_post_list[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return home_post_list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(home_post: GalleryPojoData, ctx: HomeActivity) {
            val ivProfilePic: CircleImageView = itemView.findViewById(R.id.profilePic)
            val tvUserName: TextView = itemView.findViewById(R.id.userName)
            val tvTime: TextView = itemView.findViewById(R.id.dateTime)
            val tvDescription: TextView = itemView.findViewById(R.id.description)
            val ivPostImage: ImageView = itemView.findViewById(R.id.postImage)
            val rlVideo: LinearLayout = itemView.findViewById(R.id.llVideo)
            val videoIcon: ImageView = itemView.findViewById(R.id.videoIcon)
            val imageLayout: RelativeLayout = itemView.findViewById(R.id.imageLayout)
            tvUserName.text = home_post.event_name
            tvDescription.text = home_post.discussion

            if (home_post.created_at != "") {
                val newDate = home_post.created_at.replace(".000Z", "").split("T")
                tvTime.text = ctx.changeDateTimeFormatYear(newDate[0] + " " + newDate[1])
            }

            if (home_post.profile_pic != null && !home_post.profile_pic.equals("")) {
                if (home_post.uploaded_by == "0")
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.SCHOOL_PROFILE_PIC + home_post.profile_pic, ivProfilePic)
                else
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.IMAGE_BASE_URL + home_post.profile_pic, ivProfilePic)
            } else {
                ivProfilePic.setImageResource(R.mipmap.user_icon_default)
            }

            if (home_post.file != null && home_post.file!!.size > 0) {
                val lastValue = after(home_post.file!![0].file, ".").toLowerCase()
                if (lastValue == "gif") {
                    videoIcon.visibility = View.GONE
                    rlVideo.visibility = View.GONE
                    ivPostImage.visibility = View.VISIBLE
                    if (home_post.type == "Events")
                        ctx.setGifImage(ctx, TeacherConstant.EVENT_IMAGE_URL + home_post.file!![0].file, ivPostImage)
                    else
                        ctx.setGifImage(ctx, TeacherConstant.IMAGE_URl + home_post.file!![0].file, ivPostImage)
                } else if (lastValue == "png" || lastValue == "jpg" || lastValue == "jpeg") {
                    videoIcon.visibility = View.GONE
                    rlVideo.visibility = View.GONE
                    if (home_post.type == "Events")
                        ctx.setHomePostImageInLayout(ctx, TeacherConstant.EVENT_IMAGE_URL + home_post.file!![0].file, ivPostImage)
                    else
                        ctx.setHomePostImageInLayout(ctx, TeacherConstant.IMAGE_URl + home_post.file!![0].file, ivPostImage)
                } else if (lastValue == "mp4" || lastValue == "mov" || lastValue == "3gp") {
                    videoIcon.visibility = View.VISIBLE
                    rlVideo.visibility = View.VISIBLE
                    ctx.setHomePostImage(ctx, TeacherConstant.THUMB_IMAGE_URl + home_post.file!![0].thumbnail, ivPostImage)
//                    rlVideo.layoutParams.height = 675
//                    ivPostImage.layoutParams.height = 675
//                    val thread = Thread(Runnable {
//                        try {
//                            val url = URL(TeacherConstant.THUMB_IMAGE_URl + home_post.file!![0].thumbnail)
//                            val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())
//                            rlVideo.layoutParams.height = bmp.getScaledHeight(2600)
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                        }
//                    })
//
//                    thread.start()
                }
            }
            if (home_post.type == "Events") {
                if (home_post.file != null && home_post.file!!.size > 0 && home_post.file!![0].file == "") {
                    imageLayout.visibility = View.GONE
                    tvDescription.text = home_post.registrationDetails
                } else {
                    imageLayout.visibility = View.VISIBLE
                    tvDescription.text = home_post.details
                }
                itemView.setOnClickListener {
                    ctx.startActivity(Intent(ctx, EventDetailActivity::class.java).putExtra("event_id", home_post.id))
                }
            } else {
                imageLayout.visibility = View.VISIBLE
                itemView.setOnClickListener {
                    ctx.startActivity(Intent(ctx, HomeDetailActivity::class.java).putExtra("home_post", home_post))
                }
            }
        }

        private fun after(value: String, a: String): String {
            // Returns a substring containing all characters after a string.
            val posA = value.lastIndexOf(a)
            if (posA == -1) {
                return ""
            }
            val adjustedPosA = posA + a.length
            return if (adjustedPosA >= value.length) {
                ""
            } else value.substring(adjustedPosA)
        }
    }
}