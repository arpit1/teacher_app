package com.teacherapp.adapter

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.SendNotificationActivity
import com.teacherapp.pojo.NotificationDataPojo
import com.teacherapp.util.TeacherConstant

/**
 * Created by upasna.mishra on 5/4/2018.
 */
class SendNotificationAdapter(val context: Activity, var notification_list: ArrayList<NotificationDataPojo>) : RecyclerView.Adapter<SendNotificationAdapter.ViewHolder>() {

    var ctx: SendNotificationActivity = context as SendNotificationActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SendNotificationAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_send_notification, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: SendNotificationAdapter.ViewHolder, position: Int) {
        holder.bindItems(notification_list[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return notification_list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(notification_list: NotificationDataPojo, ctx: SendNotificationActivity) {
            val tv_title : TextView = itemView.findViewById(R.id.tv_title)
            val iv_profile_pic: ImageView = itemView.findViewById(R.id.iv_profile_pic)
            val tv_description: TextView = itemView.findViewById(R.id.tv_description)
            val createdAt: TextView = itemView.findViewById(R.id.createdAt)
            val row_click: LinearLayout = itemView.findViewById(R.id.row_click)
            val audience_type : TextView = itemView.findViewById(R.id.audience_type)
            val llBackground_color : LinearLayout = itemView.findViewById(R.id.llBackground_color)
            val imageLayout : LinearLayout = itemView.findViewById(R.id.imageLayout)
            println("xhxhxhxhxhx "+notification_list.file)
            when {
                notification_list.file != null && notification_list.file != "" -> {
                    imageLayout.visibility = View.VISIBLE
                    ctx.setProfileImageInLayout(ctx,TeacherConstant.NOTICE_FILE_BASE_URL + notification_list.file, iv_profile_pic)
                }
                else -> imageLayout.visibility = View.GONE
            }

            val create_date = notification_list.created.split("T").toMutableList()
            val created_time = create_date[1].split(".").toMutableList()
            createdAt.text = ctx.changeDateTimeFormat(create_date[0] + " " + created_time[0])

            tv_description.text = notification_list.description
            tv_title.text = notification_list.title
            if(notification_list.type == "0"){
                llBackground_color.setBackgroundColor(ContextCompat.getColor(ctx, R.color.white))
                tv_title.setTextColor(ContextCompat.getColor(ctx, R.color.button_color))
                tv_description.setTextColor(ContextCompat.getColor(ctx, R.color.grey_dark_color))
                createdAt.setTextColor(ContextCompat.getColor(ctx, R.color.grey_dark_color))
                audience_type.setTextColor(ContextCompat.getColor(ctx, R.color.grey_dark_color))
            }
            else{
                llBackground_color.setBackgroundColor(ContextCompat.getColor(ctx, R.color.statusHeigh))
                tv_title.setTextColor(ContextCompat.getColor(ctx, R.color.white))
                tv_description.setTextColor(ContextCompat.getColor(ctx, R.color.white))
                createdAt.setTextColor(ContextCompat.getColor(ctx, R.color.white))
                audience_type.setTextColor(ContextCompat.getColor(ctx, R.color.white))
            }

            when {
                notification_list.audience_type == "1" -> audience_type.text = ctx.resources.getString(R.string.program)+ ": "+notification_list.program
                notification_list.audience_type == "2" -> audience_type.text = ctx.resources.getString(R.string.classes)+ ": "+notification_list.classes
                else -> audience_type.text = ctx.resources.getString(R.string.all_school)
            }

            row_click.setTag(R.string.send_notification_data, notification_list)
            row_click.setOnClickListener(ctx)

        }
    }
}