package com.teacherapp.adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.prolificinteractive.materialcalendarview.*
import com.teacherapp.R
import com.teacherapp.activity.EditTaskManagementActivity
import com.teacherapp.activity.FilterListWithDate
import com.teacherapp.activity.HomeActivity
import com.teacherapp.fragment.TaskListFragment
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.pojo.TaskListDataPojo
import com.teacherapp.util.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashSet


class TaskAdapter(context: FragmentActivity, var task_list: ArrayList<TaskListDataPojo>, val ctxFragment: TaskListFragment, val ctx: HomeActivity, val userData: LoginPojo) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), OnDateSelectedListener {
    lateinit var holder: RecyclerView.ViewHolder

    private lateinit var datesLow: HashSet<CalendarDay>
    private lateinit var datesMedium: HashSet<CalendarDay>
    private lateinit var datesHigh: HashSet<CalendarDay>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        return when (viewType) {
            TYPE_ITEM -> {
                //Inflating recycle view item layout
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.task_adapter_row, parent, false)
                ItemViewHolder(itemView)
            }
            TYPE_HEADER -> {
                //Inflating header view
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.header_layout, parent, false)
                HeaderViewHolder(itemView)
            }
            else -> null
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        val position = holder.adapterPosition
        if (holder is HeaderViewHolder) {
            this.holder = holder
            datesLow = HashSet()
            datesMedium = HashSet()
            datesHigh = HashSet()
            datesLow.clear()
            datesMedium.clear()
            datesHigh.clear()
            for (i in 0 until task_list.size) {
                val date = task_list[i].start_date.split("-")
                val day = date[0].toInt()
                val month = date[1].toInt() - 1
                val year = date[2].toInt()
                val statusComplete = task_list[i].complete_status.toInt()
                val dateToHighLight = CalendarDay.from(year, month, day)
                when {
                    statusComplete <= 30 -> datesLow.add(dateToHighLight)
                    statusComplete in 31..60 -> datesMedium.add(dateToHighLight)
                    statusComplete > 60 -> datesHigh.add(dateToHighLight)
                }
            }
            holder.calendarView.setOnDateChangedListener(this)
            holder.calendarView.addDecorators(CircleDecoratorLow(ctx, datesLow), CircleDecoratorMedium(ctx, datesMedium), CircleDecoratorHigh(ctx, datesHigh))
        } else if (holder is ItemViewHolder) {
            holder.tv_taskName.text = task_list[position - 1].title

            holder.tv_date.text = ctx.resources.getString(R.string.from) + ": " + ctx.getChangedDate(task_list[position - 1].start_date) + " " + ctx.resources.getString(R.string.to) + ": " + ctx.getChangedDate(task_list[position - 1].end_date)

            // holder.tv_date.text = showDateFormat(task_list.get(position-1).start_date)
            holder.tv_description.text = task_list[position - 1].description
            val value = task_list[position - 1].complete_status + "%"
            val sd = task_list[position - 1].complete_status.toInt()
            if (task_list[position - 1].programs == null || task_list[position - 1].programs == "") {
                holder.detailLayout.visibility = View.GONE
                holder.namePartition.visibility = View.GONE
                holder.edit_img.visibility = View.GONE
            } else {
                holder.tv_program.text = task_list[position - 1].programs
                holder.tv_batch.text = task_list[position - 1].batch_name
                holder.tv_grade.text = task_list[position - 1].classes
                holder.studentName.text = task_list[position - 1].name
            }
            //do nothing
            when {
                sd <= 30 -> {
                    holder.tv_progress.text = value
                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list)
                }
                sd in 31..60 -> {
                    holder.tv_progress.text = value
                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_medium)
                }
                sd > 60 -> {
                    holder.tv_progress.text = value
                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_heigh)
                }
            }
            var count = 0
            val status = arrayOf("0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%")
            // Create an ArrayAdapter using a simple spinner layout and languages array
//            val aa = ArrayAdapter(ctx, android.R.layout.simple_spinner_item, plants)
            val aa = ArrayAdapter(ctx, R.layout.simple_spinner_item, status)
            // Set layout to use when the list of choices appear
//            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Set Adapter to Spinner
            holder.progress_spinner.adapter = aa

            val spinnerPosition = aa.getPosition(value)
            holder.progress_spinner.setSelection(spinnerPosition)
            holder.progress_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, positions: Int, id: Long) {
                    if (count != 0) {
                        val tasks = parent.getItemAtPosition(positions).toString()
                        if (tasks != "select") {
                            val progress = tasks.split("%")[0].toInt()
                            when {
                                progress <= 30 -> {
                                    holder.tv_progress.text = tasks
                                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list)
                                }
                                progress in 31..60 -> {
                                    holder.tv_progress.text = tasks
                                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_medium)
                                }
                                progress > 60 -> {
                                    holder.tv_progress.text = tasks
                                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_heigh)
                                }
                            }

                            updateTask(task_list[position - 1].id, task_list[position - 1].student_id, task_list[position - 1].title, task_list[position - 1].start_date, task_list[position - 1].end_date, task_list[position - 1].assign_email, task_list[position - 1].assign_name, task_list[position - 1].description, task_list[position - 1].reminder_time, task_list[position - 1].school_id, progress.toString(), task_list[position - 1].program_id, task_list[position - 1].batch_id, task_list[position - 1].classes_id)
                            task_list[position - 1].complete_status = progress.toString()
                            notifyDataSetChanged()
                        }
                    } else {
                        count++
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    //do nothing
                }
            }
            holder.edit_img.setOnClickListener {
                ctx.startActivity(Intent(ctx, EditTaskManagementActivity::class.java).putExtra("value", task_list[position - 1]))
            }

            holder.itemView.setOnClickListener {
                if (task_list[position - 1].description != "")
                    displayCommonDialog(task_list[position - 1].description, task_list[position - 1].title)
            }
        }
    }

    private fun displayCommonDialog(msg: String, title: String) {
        val btn_yes_exit_LL: LinearLayout
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar)
        dialog.setContentView(R.layout.custom_dialog_withheader)

        val msg_textView = dialog.findViewById(R.id.text_exit) as TextView
        val header = dialog.findViewById(R.id.text_header) as TextView

        header.visibility = View.VISIBLE
        header.text = title
        msg_textView.text = msg
        btn_yes_exit_LL = dialog.findViewById(R.id.btn_yes_exit_LL) as LinearLayout

        btn_yes_exit_LL.setOnClickListener { dialog.dismiss() }

        if (!ctx.isFinishing)
            dialog.show()
    }

    override fun onDateSelected(widget: MaterialCalendarView, date: CalendarDay, selected: Boolean) {
        val df = SimpleDateFormat(ctx.getString(R.string.dateformat), Locale.US)
        val mon = df.format(date.date)
        ctx.startActivity(Intent(ctx, FilterListWithDate::class.java).putExtra("datess", mon))
    }

    private fun updateTask(taskId: String, student_id: String, task_name: String, starting_date: String, due_date: String, assign_email: String, assign_name: String, desc: String, reminder_time: String, school_id: String, complete_status: String, programId: String, batchId: String, classId: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = TeacherDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.updateTask(taskId, ctx.getFromPrefs(TeacherConstant.ID).toString(),
                    student_id, task_name, starting_date, due_date, assign_email,
                    assign_name, desc, reminder_time, school_id, complete_status, programId, batchId, classId,
                    ctx.getTeacherName())
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            ctx.displayToast(ctx.resources.getString(R.string.no_internet_connection))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            TYPE_HEADER
        } else {
            TYPE_ITEM
        }
    }

    override fun getItemCount(): Int {
        return task_list.size + 1
    }

    private inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var calendarView: MaterialCalendarView = view.findViewById(R.id.calendar_view)
    }

    private inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tv_taskName: TextView = itemView.findViewById(R.id.txt_task_name)
        internal var tv_date: TextView = itemView.findViewById(R.id.txt_date)
        internal var tv_description: TextView = itemView.findViewById(R.id.txt_descriptions)
        internal var tv_progress: TextView = itemView.findViewById(R.id.txt_progress)
        internal var progress_spinner: Spinner = itemView.findViewById(R.id.spinner)
        internal var edit_img: ImageView = itemView.findViewById(R.id.img_edit)
        val tv_program: TextView = itemView.findViewById(R.id.tv_program)
        val tv_batch: TextView = itemView.findViewById(R.id.tv_batch)
        val tv_grade: TextView = itemView.findViewById(R.id.tv_grade)
        val studentName: TextView = itemView.findViewById(R.id.studentName)
        val detailLayout: LinearLayout = itemView.findViewById(R.id.detailLayout)
        val namePartition: View = itemView.findViewById(R.id.namePartition)
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1

        private val colors = intArrayOf(
                R.drawable.circle,
                R.drawable.circle_task_low,
                R.drawable.circle_medium)
    }

    inner class CircleDecoratorLow(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[1])
        private var datesLow: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesLow.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))
            view.setSelectionDrawable(drawable)
        }
    }

    inner class CircleDecoratorMedium(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[2])
        private var datesMedium: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesMedium.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))

            view.setSelectionDrawable(drawable)
        }
    }

    inner class CircleDecoratorHigh(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {

        private val drawable: Drawable = ContextCompat.getDrawable(context, colors[0])
        private var datesHigh: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesHigh.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.WHITE))

            view.setSelectionDrawable(drawable)
        }
    }
}