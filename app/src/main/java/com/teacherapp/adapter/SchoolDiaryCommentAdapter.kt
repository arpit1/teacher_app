package com.teacherapp.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.SchoolDiaryDetailActivity
import com.teacherapp.pojo.EventCommentDataPojo
import com.teacherapp.util.TeacherConstant
import de.hdodenhof.circleimageview.CircleImageView

class SchoolDiaryCommentAdapter(val context: Activity, val feedbackCommentList: ArrayList<EventCommentDataPojo>) : RecyclerView.Adapter<SchoolDiaryCommentAdapter.ViewHolder>() {

    var ctx: SchoolDiaryDetailActivity = context as SchoolDiaryDetailActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolDiaryCommentAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_commentfeedback_parent, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: SchoolDiaryCommentAdapter.ViewHolder, position: Int) {
        holder.bindItems(ctx, feedbackCommentList[position], feedbackCommentList.size, position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return feedbackCommentList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(ctx: SchoolDiaryDetailActivity, feedBackCommentList: EventCommentDataPojo,
                      sizeList: Int, position: Int) {

            val tvTime: TextView = itemView.findViewById(R.id.tvTime)
            val userName: TextView = itemView.findViewById(R.id.userName)
            val tvMessage: TextView = itemView.findViewById(R.id.tvMessage)
            val userImage: CircleImageView = itemView.findViewById(R.id.userImage)
            val ll_parent_commnet: LinearLayout = itemView.findViewById(R.id.ll_parent_commnet)
            val ll_admin_comment: LinearLayout = itemView.findViewById(R.id.ll_admin_comment)

            if (feedBackCommentList.type == "1") {
                ll_parent_commnet.visibility = View.VISIBLE
                ll_admin_comment.visibility = View.GONE
                if (feedBackCommentList.created_at != "") {
                    val new_date = feedBackCommentList.created_at.replace(".000Z", "").split("T")
                    tvTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
                }

                tvMessage.text = feedBackCommentList.comment
                if (feedBackCommentList.profile_pic != null && feedBackCommentList.profile_pic != "") {
                    ctx.setProfileImageInLayout(ctx, TeacherConstant.STUDENT_PROFILE_BASE_URL + feedBackCommentList.profile_pic, userImage)
                } else {
                    userImage.setImageResource(R.mipmap.usericon)
                }
                userName.text = feedBackCommentList.father_name

            }

            ll_parent_commnet.setOnClickListener(ctx)
            ll_parent_commnet.setTag(R.string.send_comment_list, feedBackCommentList)
        }
    }
}