package com.teacherapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.teacherapp.R
import com.teacherapp.activity.EventGalleryActivity
import com.teacherapp.pojo.ImagePojo
import com.teacherapp.util.TeacherConstant

/**
 * Created by hitesh.mathur on 12/8/2017.
 */
class VideoAdapter(ctxext: EventGalleryActivity, activityPostList: ArrayList<ImagePojo>) : BaseAdapter() {

    var ctx : EventGalleryActivity = ctxext
    var activiy_list = activityPostList
    private lateinit var user_image : ImageView
    private lateinit var playvideoicon : ImageView

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.activity_grid_row, parent, false)
        user_image = v.findViewById(R.id.img_profile)
        playvideoicon = v.findViewById(R.id.playvideoicon)
        playvideoicon.visibility = View.VISIBLE
        Picasso.with(ctx).load(TeacherConstant.THUMB_IMAGE_URl +""+activiy_list[position].thumbnail).placeholder(R.mipmap.user_icon_default).error(R.mipmap.ic_launcher).into(user_image)

        return v
    }

    override fun getItem(position: Int): Any
    {
        return 0
    }

    override fun getItemId(position: Int): Long
    {
        return 0
    }

    override fun getCount(): Int
    {
        return activiy_list.size
    }
}