package com.teacherapp.adapter

import android.app.Dialog
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.teacherapp.R
import com.teacherapp.activity.EditTaskManagementActivity
import com.teacherapp.activity.FilterListWithDate
import com.teacherapp.pojo.LoginPojo
import com.teacherapp.pojo.TaskListDataPojo
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.TeacherConstant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilterListWithDateAdapter(val ctx: FilterListWithDate, var taskList: ArrayList<TaskListDataPojo>, val userData: LoginPojo) : RecyclerView.Adapter<FilterListWithDateAdapter.ViewHolder>() {

    override
    fun getItemCount(): Int {
        return taskList.size
    }

    override
    fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): FilterListWithDateAdapter.ViewHolder {
        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.task_adapter_row, parent, false)
        return ViewHolder(v)
    }

    override
    fun onBindViewHolder(holder: FilterListWithDateAdapter.ViewHolder?, position: Int) {
        val task_list: TaskListDataPojo = taskList[position]
        holder!!.tv_taskName.text = task_list.title
        holder.tv_date.text = task_list.start_date
        holder.tv_description.text = task_list.description

        val value = task_list.complete_status + "%"
        val sd = task_list.complete_status.toInt()
        if (task_list.programs == null || task_list.programs == "") {
            holder.detailLayout.visibility = View.GONE
            holder.namePartition.visibility = View.GONE
            holder.img_edit.visibility = View.GONE
        } else {
            holder.tv_program.text = task_list.programs
            holder.tv_batch.text = task_list.batch_name
            holder.tv_grade.text = task_list.classes
            holder.studentName.text = task_list.name
        }
        //do nothing
        if (sd.toString() != "select") {
            when {
                sd <= 30 -> {
                    holder.tv_progress.text = value
                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list)
                }
                sd in 31..60 -> {
                    holder.tv_progress.text = value
                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_medium)
                }
                sd > 60 -> {
                    holder.tv_progress.text = value
                    holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_heigh)
                }
            }
        }
        val status = arrayOf("0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%")
        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(ctx, R.layout.simple_spinner_item, status)
        var count = 0
        // Set Adapter to Spinner
        holder.progress_spinner.adapter = aa

        val spinnerPosition = aa.getPosition(value)
        holder.progress_spinner.setSelection(spinnerPosition)

        holder.progress_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, positiond: Int, id: Long) {
                if (count != 0) {
                    val tasks = parent.getItemAtPosition(positiond).toString()
                    if (tasks != "select") {
                        val progress = tasks.split("%")[0].toInt()
                        when {
                            progress <= 30 -> {
                                holder.tv_progress.text = tasks
                                holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list)
                            }
                            progress in 31..60 -> {
                                holder.tv_progress.text = tasks
                                holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_medium)
                            }
                            progress > 60 -> {
                                holder.tv_progress.text = tasks
                                holder.tv_progress.setBackgroundResource(R.drawable.gradient_task_list_heigh)
                            }
                        }
                        updateTask(task_list.id, task_list.student_id, task_list.title, task_list.start_date, task_list.end_date, task_list.assign_email, task_list.assign_name, task_list.description, task_list.reminder_time, task_list.school_id, progress.toString(), task_list.program_id, task_list.batch_id, task_list.classes_id)
                        task_list.complete_status = progress.toString()
                    }
                } else {
                    count++
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                //do nothing
            }
        }
        holder.img_edit.setOnClickListener {
            ctx.startActivity(Intent(ctx, EditTaskManagementActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("value", task_list))
        }

        holder.itemView.setOnClickListener {
            if (task_list.description != "")
                displayCommonDialog(task_list.description, task_list.title)
        }
    }

    private fun displayCommonDialog(msg: String, title: String) {
        val btn_yes_exit_LL: LinearLayout
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar)
        dialog.setContentView(R.layout.custom_dialog_withheader)

        val msg_textView = dialog.findViewById(R.id.text_exit) as TextView
        val header = dialog.findViewById(R.id.text_header) as TextView

        header.visibility = View.VISIBLE
        header.text = title
        msg_textView.text = msg
        btn_yes_exit_LL = dialog.findViewById(R.id.btn_yes_exit_LL) as LinearLayout

//        val dialog_header_cross = dialog.findViewById(R.id.cross_dialog) as ImageView
//        dialog_header_cross.setOnClickListener { dialog.dismiss() }

        btn_yes_exit_LL.setOnClickListener { dialog.dismiss() }

        if (!ctx.isFinishing)
            dialog.show()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tv_taskName: TextView = itemView.findViewById(R.id.txt_task_name)
        val tv_date: TextView = itemView.findViewById(R.id.txt_date)
        val tv_description: TextView = itemView.findViewById(R.id.txt_descriptions)
        val tv_progress: TextView = itemView.findViewById(R.id.txt_progress)
        val img_edit: ImageView = itemView.findViewById(R.id.img_edit)
        val progress_spinner: Spinner = itemView.findViewById(R.id.spinner)
        val tv_program: TextView = itemView.findViewById(R.id.tv_program)
        val tv_batch: TextView = itemView.findViewById(R.id.tv_batch)
        val tv_grade: TextView = itemView.findViewById(R.id.tv_grade)
        val studentName: TextView = itemView.findViewById(R.id.studentName)
        val detailLayout: LinearLayout = itemView.findViewById(R.id.detailLayout)
        val namePartition: View = itemView.findViewById(R.id.namePartition)
    }

    fun updateTask(task_id: String, student_id: String, task_name: String, starting_date: String, due_date: String, assign_email: String, assign_name: String, desc: String, reminder_time: String, school_id: String, complete_status: String, programId: String, batchId: String, classId: String) {
        val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
        val call: Call<LoginPojo> = apiService.updateTask(task_id, ctx.getFromPrefs(TeacherConstant.ID).toString(),
                student_id, task_name, starting_date, due_date, assign_email,
                assign_name, desc, reminder_time, school_id, complete_status, programId, batchId, classId,
                ctx.getTeacherName())
        call.enqueue(object : Callback<LoginPojo> {
            override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                if (response != null) {
                }
            }

            override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                println(t.toString())
            }
        })
    }
}