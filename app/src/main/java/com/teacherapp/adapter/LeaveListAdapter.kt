package com.teacherapp.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.TeacherShowLeaveListActivity
import com.teacherapp.pojo.LeaveManagementDataPojo
import com.teacherapp.util.TeacherConstant
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 12/4/2017.
 */
class LeaveListAdapter(val context: Activity, val leave_list: ArrayList<LeaveManagementDataPojo>, _qualification: String) : RecyclerView.Adapter<LeaveListAdapter.ViewHolder>() {

    var ctx: TeacherShowLeaveListActivity = context as TeacherShowLeaveListActivity
    val qualification = _qualification

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaveListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_leave_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: LeaveListAdapter.ViewHolder, position: Int) {
        holder.bindItems(leave_list[position], ctx, qualification)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return leave_list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(leave_list: LeaveManagementDataPojo, ctx: TeacherShowLeaveListActivity, qualification: String) {
            val iv_profile_pic: CircleImageView = itemView.findViewById(R.id.iv_profile_pic)
            val tv_description: TextView = itemView.findViewById(R.id.tv_description)
            val tv_qualification: TextView = itemView.findViewById(R.id.tv_qualification)
            val apply_date_leave: TextView = itemView.findViewById(R.id.apply_date_leave)
            val tv_from_date: TextView = itemView.findViewById(R.id.tv_from_date)
            val tv_to_date: TextView = itemView.findViewById(R.id.tv_to_date)
            val view: View = itemView.findViewById(R.id.view)

            if (leave_list.profile_pic != null && leave_list.profile_pic != "") {
                ctx.setProfileImageInLayout(ctx, TeacherConstant.IMAGE_BASE_URL + leave_list.profile_pic, iv_profile_pic)
            } else {
                iv_profile_pic.setImageResource(R.mipmap.user_icon_default)
            }
            tv_description.text = leave_list.reason

            val create_date = leave_list.created_at.split("T").toMutableList()
            val created_time = create_date[1].split(".").toMutableList()
            apply_date_leave.text = ctx.changeDateTimeFormat(create_date[0] + " " + created_time[0])

            val from_date = leave_list.from_date.split("T").toMutableList()
            tv_from_date.text = ctx.resources.getString(R.string.leave_from)+" " + ctx.changeDateFormat(from_date[0])

            val to_date = leave_list.to_date.split("T").toMutableList()
            tv_to_date.text = ctx.resources.getString(R.string.leave_to)+" " + ctx.changeDateFormat(to_date[0])
            if (leave_list.qualification != "") {
                tv_qualification.text = leave_list.qualification
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }
    }
}