package com.teacherapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.BaseActivity
import com.teacherapp.activity.HomeActivity
import com.teacherapp.pojo.EventPojoItem
import com.teacherapp.util.TeacherConstant

/**
 * Created by hitesh.mathur on 12/8/2017.
 */
class EventAdapter(ctxext: HomeActivity, activityPostList: ArrayList<EventPojoItem>) : BaseAdapter() {

    private var ctx: HomeActivity = ctxext
    private var activity_list: MutableList<EventPojoItem> = activityPostList

    private lateinit var user_image: ImageView
    private lateinit var tv_name: TextView
    private lateinit var tv_date: TextView
    private lateinit var tv_des: TextView
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v: View? = null
        if (v == null)
            v = LayoutInflater.from(parent!!.context).inflate(R.layout.event_grid_row, parent, false)

        user_image = v!!.findViewById(R.id.img_profile)
        tv_name = v.findViewById(R.id.tv_name)
        tv_date = v.findViewById(R.id.tv_date)
        tv_des = v.findViewById(R.id.tv_des)

        tv_name.text = activity_list[position].event_name
        tv_date.text = ctx.getChangedDate(activity_list[position].dates)
        tv_des.text = activity_list[position].category

        if ((activity_list[position].upload_type == "Images" || activity_list[position].upload_type == "Image") && activity_list[position].file.size > 0) {
            (ctx as BaseActivity).setHomePostImageInLayout(ctx, TeacherConstant.IMAGE_URl + "" + activity_list[position].file[0].file, user_image)
        } else if (activity_list[position].upload_type == "Video" && activity_list[position].file.size > 0) {
            (ctx as BaseActivity).setHomePostImageInLayout(ctx, TeacherConstant.THUMB_IMAGE_URl + "" + activity_list[position].file[0].thumbnail, user_image)
        }
        return v
    }

    override fun getItem(position: Int): Any {
        return 0
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return activity_list.size
    }
}