package com.teacherapp.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.teacherapp.R
import com.teacherapp.activity.TeacherTaskListActivity
import com.teacherapp.pojo.TaskListDataPojo

/**
 * Created by upasna.mishra on 4/13/2018.
 */
class TeacherTaskAdapter(val context: Activity, private val teacherList: ArrayList<TaskListDataPojo>) : RecyclerView.Adapter<TeacherTaskAdapter.ViewHolder>() {
    var ctx: TeacherTaskListActivity = context as TeacherTaskListActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeacherTaskAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.task_adapter_row, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: TeacherTaskAdapter.ViewHolder, position: Int) {
        holder.bindItems(teacherList[position], ctx, position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return teacherList.size
    }

    //the class is holding the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(taskList: TaskListDataPojo, ctx: TeacherTaskListActivity, position: Int) {
            val txt_task_name : TextView = itemView.findViewById(R.id.txt_task_name)
            val txt_progress : TextView = itemView.findViewById(R.id.txt_progress)
            val img_edit : ImageView = itemView.findViewById(R.id.img_edit)
            val spinner : Spinner = itemView.findViewById(R.id.spinner)
            val tv_program : TextView = itemView.findViewById(R.id.tv_program)
            val tv_batch : TextView = itemView.findViewById(R.id.tv_batch)
            val tv_grade : TextView = itemView.findViewById(R.id.tv_grade)
            val studentName : TextView = itemView.findViewById(R.id.studentName)
            val txt_date : TextView = itemView.findViewById(R.id.txt_date)
            val txt_descriptions : TextView = itemView.findViewById(R.id.txt_descriptions)
            val viewOne : View = itemView.findViewById(R.id.viewOne)
            val viewTwo : View = itemView.findViewById(R.id.viewTwo)
            tv_batch.visibility = View.GONE
            tv_grade.visibility = View.GONE
            img_edit.visibility = View.GONE
            viewOne.visibility = View.GONE
            viewTwo.visibility = View.GONE

            txt_task_name.text = taskList.title
            tv_program.text = taskList.qualification
            studentName.text = taskList.name
            txt_descriptions.text = taskList.description
            txt_date.text = ctx.resources.getString(R.string.from) + ": " +
                    ctx.getChangedDate(taskList.start_date) + " " +
                    ctx.resources.getString(R.string.to) + ": " + ctx.getChangedDate(taskList.end_date)
            val value = taskList.complete_status + "%"
            val sd = taskList.complete_status.toInt()

            when {
                sd <= 30 -> {
                    txt_progress.text = value
                    txt_progress.setBackgroundResource(R.drawable.gradient_task_list)
                }
                sd in 31..60 -> {
                    txt_progress.text = value
                    txt_progress.setBackgroundResource(R.drawable.gradient_task_list_medium)
                }
                sd > 60 -> {
                    txt_progress.text = value
                    txt_progress.setBackgroundResource(R.drawable.gradient_task_list_heigh)
                }
            }

            var count = 0
            val completeStatus = arrayOf("0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%")
            // Create an ArrayAdapter using a simple spinner layout and languages array
            val aa = ArrayAdapter(ctx, android.R.layout.simple_spinner_item, completeStatus)
            // Set layout to use when the list of choices appear
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Set Adapter to Spinner
           spinner.adapter = aa

            val spinnerPosition = aa.getPosition(value)
            spinner.setSelection(spinnerPosition)
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, positions: Int, id: Long) {
                    if (count != 0) {
                        val tasks = parent.getItemAtPosition(positions).toString()
                        if (tasks != "select") {
                            val progress = tasks.split("%")[0].toInt()
                            when {
                                progress <= 30 -> {
                                    txt_progress.text = tasks
                                    txt_progress.setBackgroundResource(R.drawable.gradient_task_list)
                                }
                                progress in 31..60 -> {
                                    txt_progress.text = tasks
                                    txt_progress.setBackgroundResource(R.drawable.gradient_task_list_medium)
                                }
                                progress > 60 -> {
                                    txt_progress.text = tasks
                                    txt_progress.setBackgroundResource(R.drawable.gradient_task_list_heigh)
                                }
                            }
                            taskList.complete_status = progress.toString()
                            ctx.updateTaskList(taskList.id, progress.toString(), position, taskList.type)
                           // notifyDataSetChanged()
                        }
                    } else {
                        count++
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    //do nothing
                }
            }
        }
    }
}
