package com.teacherapp.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.teacherapp.R
import com.teacherapp.activity.HomeActivity
import com.teacherapp.pojo.GetDiaryDataPojo
import com.teacherapp.util.TeacherConstant
import de.hdodenhof.circleimageview.CircleImageView


class DiaryListAdapter(val context: Activity, private val diaryList: ArrayList<GetDiaryDataPojo>) : RecyclerView.Adapter<DiaryListAdapter.ViewHolder>() {
    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiaryListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.custom_diary_data, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: DiaryListAdapter.ViewHolder, position: Int) {
        holder.bindItems(diaryList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return diaryList.size
    }

    //the class is holding the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(diary: GetDiaryDataPojo, ctx: HomeActivity) {
            val rl_diary: RelativeLayout = itemView.findViewById(R.id.rl_diary)
            val iv_user_img: CircleImageView = itemView.findViewById(R.id.iv_user_img)
            val tv_title: TextView = itemView.findViewById(R.id.tv_title)
            val tv_program: TextView = itemView.findViewById(R.id.tv_program)
            val tv_batch: TextView = itemView.findViewById(R.id.tv_batch)
            val tv_grade: TextView = itemView.findViewById(R.id.tv_grade)
            val tv_date: TextView = itemView.findViewById(R.id.tv_date)
            val tv_description: TextView = itemView.findViewById(R.id.tv_description)

            if (diary.profile_pic != null && diary.profile_pic != "") {
                ctx.setProfileImageInLayout(ctx, TeacherConstant.IMAGE_BASE_URL + diary.profile_pic, iv_user_img)
            } else {
                iv_user_img.setImageResource(R.mipmap.user_icon_default)
            }
            tv_title.text = diary.title
            tv_program.text = diary.programs
            tv_batch.text = diary.batch_name
            tv_grade.text = diary.class_name
            tv_description.text = diary.description

            tv_date.text = ctx.changeDateTimeFormatDiary(diary.dates)
            rl_diary.setTag(R.string.data, diary)
            rl_diary.setOnClickListener(ctx)
        }
    }
}
